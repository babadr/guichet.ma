<?php

return [
    /**
     * View titles & buttons
     */
    'tab_title' => 'SEO Meta',
    'seo_title' => 'Meta Titre',
    'seo_description' => 'Meta Description',
    'seo_alias' => 'URL Alias',
    'seo_keywords' => 'Meta Mots-Clés',
    'seo_robots' => 'Meta Robots',
    'cannot_save' => 'Impossible de sauvegarder les metas SEO, les erreurs sont affichées ci-dessous.'
];
