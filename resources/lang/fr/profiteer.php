<?php

return [
    'full_name' => 'Nom et Prénom',
    'is_minor' => 'Est-il mineur ?',
    'cin' => 'N° CIN',
    'created_at' => 'Inscrit le',
    'updated' => 'Bénéficiaire mis à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder ce bénéficiaire, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver ce bénéficiaire.',
    'edit' => 'Editer le bénéficiaire <small>:name</small>',
    'edit_details' => 'Détails bénéficiaire : #:id',
];