<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouveau producteur',
    'add' => 'Ajouter un producteur',
    'edit' => 'Editer le producteur <small>:name</small>',
    'list' => 'Liste des producteurs',
    'add_details' => 'Détails producteur',
    'edit_details' => 'Détails producteur : #:id',
    'manage_providers' => 'Gestion des producteurs',

    'active' => 'Activée ?',
    'title' => 'Producteur',
    'created_at' => 'Créée le',
    'description' => 'Déscrption',
    'phone' => 'Téléphone',
    'email' => 'Email',
    'secure_code' => 'Code de sécurité',
    'fax' => 'Fax',
    'logo' => 'Logo',
    'commission' => 'Commission Guichet',

    'new_created' => 'Nouveau producteur créé avec succès.',
    'updated' => 'Producteur mis à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder ce producteur, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver ce producteur.',
    'role_associated_users' => 'Impossible de supprimer ce producteur, une erreur s\'est produite.',
    'providers' => 'Producteurs'
];
