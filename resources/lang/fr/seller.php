<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouveau point de vente',
    'add' => 'Ajouter un point de vente',
    'edit' => 'Editer le point de vente <small>:name</small>',
    'list' => 'Liste des points de vente',
    'add_details' => 'Détails point de vente',
    'edit_details' => 'Détails point de vente : #:id',
    'manage_sellers' => 'Gestion des points de vente',

    'active' => 'Activée ?',
    'title' => 'Titre',
    'email' => 'Email',
    'created_at' => 'Créée le',
    'provider_id' => 'Producteur',

    'new_created' => 'Nouveau point de vente créé avec succès.',
    'updated' => 'Point de vente mis à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder ce point de vente, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver ce point de vente.',
    'role_associated_users' => 'Impossible de supprimer ce point de vente, une erreur s\'est produite.',
];
