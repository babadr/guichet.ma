<?php

return [
    'waiting' => 'En attente de paiement',
    'accepted' => 'Paiement accépté',
    'validated' => 'Validée',
    'add' => 'Ajouter une commande',
    'canceled' => 'Annulée',
    'captured' => 'Paiement reçu',
    'expired' => 'Expirée',
    'refused' => 'Refusée',
    'reserved' => 'Reservée',
    'defrauded' => 'Fraudée',
    'reference' => 'Référence',

    'user' => 'Client - Nom complet',
    'email' => 'Client - email',
    'total_paid' => 'Montant payé',
    'status' => 'Etat',
    'created_at' => 'Date d\'achat',
    'manage_orders' => 'Gestion des commandes',
    'payment_method' => 'Mode de paiement',
    'list' => 'Liste des commandes',
    'show' => 'Détails de la commande #:reference',

    'new' => 'Nouvelle commande Offline',
    'edit' => 'Editer la commande <small>:name</small>',
    'list' => 'Liste des commandes',
    'add_details' => 'Détails commande',
    'edit_details' => 'Détails commande : #:id',
    'manage_deals' => 'Gestion des commandes',
    'validator' => 'Validation des tickets',
    'offered_by' => 'Invitation offerte par'
];
