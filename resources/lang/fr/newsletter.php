<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvelle inscription',
    'add' => 'Ajouter une inscription',
    'edit' => 'Editer l\'inscription <small>:name</small>',
    'list' => 'Liste des newsletters',
    'add_details' => 'Détails inscription',
    'edit_details' => 'Détails inscription : #:id',
    'newsletters' => 'Newsletters',

    'last_name' => 'Nom',
    'full_name' => 'Nom & Prénom',
    'first_name' => 'Prénom',
    'email' => 'E-mail',
    'created_at' => 'Inscrit le',
    'manage_newsletters' => 'Gestion des newsletters',
    'birth_date' => 'Date de naissance',
    'source' => 'Source',
    'phone' => 'Téléphone',
    'civility' => 'Civilité',
    'export' => 'Exporter',

    'new_created' => 'Nouvelle inscription créée avec succès.',
    'updated' => 'Inscription mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette inscription, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette inscription.',
    'error_subscription' => 'Il y a eu un problème lors de l’inscription : Vous êtes déjà inscris.',
    'success_subscription' => 'Merci de votre inscription.',
];
