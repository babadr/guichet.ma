<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvel événement',
    'add' => 'Ajouter un événement',
    'edit' => 'Editer l\'événement <small>:name</small>',
    'list' => 'Liste des événements',
    'add_details' => 'Détails événement',
    'edit_details' => 'Détails événement : #:id',
    'manage_deals' => 'Gestion des événements',

    'title' => 'Titre',
    'created_at' => 'Créé le',
    'active' => 'Activée ?',
    'category_id' => 'Catégorie',
    'provider_id' => 'Producteur',
    'quantity' => 'Quantité disponible',
    'bought' => 'Quantité vendue de départ',
    'start_at' => 'Date début de publication',
    'end_at' => 'Date fin de publication',
    'short_description' => 'Description courte',
    'description' => 'Description',
    'address' => 'Adresse',
    'position' => 'Position sur la carte',
    'media' => 'Images',
    'is_cover_media' => 'Images de couverture',
    'is_miniature_media' => 'Miniature',
    'city_id' => 'Ville',
    'type' => 'Type',
    'opening' => 'Heure d\'ouverture des portes',
    'start_time' => 'Heure du spectacle',
    'show_on_slider' => 'Afficher sur le slider',
    'buy_now' => 'J\'achète',
    'provider_info' => 'Infos Vendeur',
    'quantity_wanted' => 'Quantité',
    'custom_field' => 'Valabilité de l\'offre',
    'custom_ticket' => 'Personnaliser le ticket',
    'seating_plan_id' => 'Plan de salle',
    'reserved_seats' => 'Places réservées',
    'barcode_type' => 'Type de code',
    'barcode_prefix' => 'Préfixe code à barre',

    'types' => [
        \App\Enums\Deal\Type::DEAL => "Deal",
        \App\Enums\Deal\Type::TICKET => "Ticket",
    ],

    'new_created' => 'Nouvel événement créé avec succès.',
    'updated' => 'Événement mis à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cet événement, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cet événement.',
    'role_associated_users' => 'Impossible de supprimer cet événement, une erreur s\'est produite.',
    'deals' => 'Evénements',
    'current_events' => 'Evénements en cours',
    'sellers' => 'Points de vente'
];
