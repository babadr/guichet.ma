<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvel article',
    'add' => 'Ajouter un article',
    'edit' => 'Editer l\'article <small>:name</small>',
    'list' => 'Liste des articles',
    'add_details' => 'Détails article',
    'edit_details' => 'Détails article : #:id',

    'title' => 'Titre',
    'content' => 'Contenu',
    'created_at' => 'Créé le',
    'category_id' => 'Catégorie',
    'manage_posts' => 'Gestion des articles',
    'active' => 'Activé ?',
    'info' => 'Informations',
    'image' => 'Image',
    'news' => 'Nos actualités',
    'next_post' => 'Article suivant',
    'previous_post' => 'Article précédent',

    'new_created' => 'Nouvel article créé avec succès.',
    'updated' => 'Article mis à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cet article, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cet article.',
    'posts' => 'Articles',
];
