<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvelle question',
    'add' => 'Ajouter une question',
    'edit' => 'Editer la question <small>:name</small>',
    'list' => 'Liste questions',
    'add_details' => 'Détails question',
    'edit_details' => 'Détails question : #:id',
    'manage_faqs' => 'Foire aux questions',

    'title' => 'Titre',
    'content' => 'Contenu',
    'created_at' => 'Créée le',
    'active' => 'Activée ?',
    'info' => 'Informations',
    'faqs' => 'Aide / Foire aux questions',

    'new_created' => 'Nouvelle question créée avec succès.',
    'updated' => 'Question mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette question, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette question.',
    'role_associated_users' => 'Impossible de supprimer cette question, une erreur s\'est produite.',
];
