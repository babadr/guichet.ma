<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvelle catégorie',
    'add' => 'Ajouter une catégorie',
    'edit' => 'Editer la catégorie <small>:name</small>',
    'list' => 'Liste des catégories',
    'add_details' => 'Détails catégorie',
    'edit_details' => 'Détails catégorie : #:id',

    'name' => 'Nom de la catégorie',
    'created_at' => 'Créée le',
    'manage_categories' => 'Gestion des catégories',
    'active' => 'Activée ?',
    'info' => 'Informations',

    'new_created' => 'Nouvelle catégorie créée avec succès.',
    'updated' => 'Catégorie mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette catégorie, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette catégorie.',
    'category_posts' => 'Catégories articles',
    'cannot_delete_category' => 'Impossible de supprimer cette catégorie, elle est attachée à un ou plusieurs articles !!',
];
