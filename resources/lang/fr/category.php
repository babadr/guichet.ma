<?php
use App\Enums\Category\Color;
return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvelle catégorie',
    'add' => 'Ajouter une catégorie',
    'edit' => 'Editer la catégorie <small>:name</small>',
    'list' => 'Liste des catégories',
    'add_details' => 'Détails catégorie',
    'edit_details' => 'Détails catégorie : #:id',

    'title' => 'Catégorie',
    'created_at' => 'Créée le',
    'manage_categories' => 'Gestion des catégories',
    'active' => 'Activée ?',
    'parent_id' => 'Catégorie parent',
    'show_on_menu' => 'Afficher sur le menu',
    'show_on_home' => 'Afficher sur la page d\'accueil',
    'color' => 'Couleur',
    'order' => 'Ordre',
    'icon' => 'Icone',

    'colors' => [
        Color::BROWN  => "Marron",
        Color::BLACK  => "Noir",
        Color::BLUE   => "Bleu",
        Color::BROWN  => "Marron",
        Color::GREEN  => "Vert",
        Color::RED    => "Rouge",
        Color::YELLOW => "Jaune",
        Color::GRAY   => "Gris",
        Color::ORANGE => "Orange",
        Color::PINK   => "Rose",
        Color::PURPLE => "Violet",
    ],

    'new_created' => 'Nouvelle catégorie créée avec succès.',
    'updated' => 'Catégorie mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette catégorie, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette catégorie.',
    'role_associated_users' => 'Impossible de supprimer cette catégorie, une erreur s\'est produite.',
    'categories' => 'Catégories'
];
