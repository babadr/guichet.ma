<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvelle page',
    'add' => 'Ajouter une page',
    'edit' => 'Editer la page <small>:name</small>',
    'list' => 'Liste des pages',
    'add_details' => 'Détails page',
    'edit_details' => 'Détails page : #:id',

    'title' => 'Titre',
    'content' => 'Contenu',
    'created_at' => 'Créée le',
    'manage_pages' => 'Gestion des pages',
    'active' => 'Activée ?',
    'info' => 'Informations',

    'new_created' => 'Nouvelle page créée avec succès.',
    'updated' => 'Page mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette page, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette page.',
    'role_associated_users' => 'Impossible de supprimer cette page, une erreur s\'est produite.',
    'pages' => 'Pages statiques'
];
