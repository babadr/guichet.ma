<?php

return [
    'cart' => 'Panier',
    'checkout' => 'Commander',
    'continue_shopping' => 'Continuer mes achats',
    'checkout' => 'Passer ma commande',
    'product' => 'Produit',
    'price' => 'Prix',
    'quantity' => 'Quantité',
    'total' => 'Total',
    'sub_total' => 'Sous-total',
    'shipping' => 'Livraison',
    'global_amount' => 'Montant global',
    'success_add' => 'Evénement ajouté avec succès à votre panier',
    'continue_shopping' => 'Continuer mes achats',
    'proceed_checkout' => 'Finaliser ma commande',
];
