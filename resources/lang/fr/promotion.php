<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvelle promotion',
    'add' => 'Ajouter une promotion',
    'edit' => 'Editer le promotion <small>:name</small>',
    'list' => 'Liste des promotions',
    'add_details' => 'Détails promotion',
    'edit_details' => 'Détails promotion : #:id',

    'name' => 'Titre',
    'created_at' => 'Créée le',
    'manage_promotions' => 'Gestion des promotions',
    'active' => 'Activée ?',

    'type' => 'Type',
    'expires_on' => 'Date d\'expiration',
    'typeable_type' => 'Appliquer la réduction au',
    'percentage' => 'Pourcentage (%)',
    'amount' => 'Montant (DH)',
    'category' => 'Catégorie',
    'customer' => 'Client',
    'event' => 'Evénement',
    'start_at' => 'Date de bébut de validité',
    'end_at' => 'Date de fin de validité',
    'apply_discount' => 'Appliquer une réduction',
    'code' => 'Code de réduction',

    'new_created' => 'Nouvelle promotion créée avec succès.',
    'updated' => 'Promotion mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette promotion, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette promotion.',
    'role_associated_users' => 'Impossible de supprimer cette promotion, une erreur s\'est produite.',
    'promotions' => 'Promotions'
];
