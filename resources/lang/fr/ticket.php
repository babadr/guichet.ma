<?php

return [
    'reference' => 'Référence',
    'user' => 'Client - Nom complet',
    'email' => 'Client - email',
    'total_paid' => 'Montant payé',
    'status' => 'Etat',
    'seat' => 'N° Place',
    'created_at' => 'Date d\'achat',
    'manage_tickets' => 'Gestion des tickets <small>:name</small>',
    'list' => 'Liste des tickets',
    'offer' => 'Offre achetée',
    'number' => 'N° Ticket',
    'product_price' => 'Prix d\'achat',
    'full_name' => 'Client - Nom complet',
    'back_to_list' => 'Retour au tableau de board'
];
