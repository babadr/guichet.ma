<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvelle offre',
    'add' => 'Ajouter une offre',
    'edit' => 'Editer l\'offre <small>:name</small>',
    'list' => 'Liste des offres',
    'add_details' => 'Détails offre',
    'edit_details' => 'Détails offre : #:id',
    'manage_offers' => 'Gestion des offres',
    'offers' => 'Offres',

    'title' => 'Titre de l\'offre',
    'created_at' => 'Créée le',
    'active' => 'Activée ?',
    'deal_id' => 'Deal',
    'description' => 'Déscrption',
    'price' => 'Prix',
    'old_price' => 'Ancien prix',
    'reservation_price' => 'Prix de réservation',
    'quantity' => 'Quantité disponible',
    'default' => 'Offre par défaut',
    'quantity_max' => 'Quantité max autorisée',

    'new_created' => 'Nouvelle offre créée avec succès.',
    'updated' => 'Offre mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette offre, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette offre.',
    'role_associated_users' => 'Impossible de supprimer cette offre, une erreur s\'est produite.',
    'order' => 'Commandes'
];
