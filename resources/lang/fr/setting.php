<?php

return [
    /**
     * View titles & buttons
     */
    'settings' => 'Paramètres',
    'app_settings' => 'Paramètres de l\'application',
    'updated' => 'Mise à jour effectuée avec succès.',
    'seo' => 'Paramètres SEO',
    'site_info' => 'Information site',
    'site_contact' => 'Information contact',

    /* settings */
    'textarea_placeholder' => 'Vore texte ici',
    'default_placeholder' => 'Saisir ...',
    'settings' => 'Modification des paramètres du site',

    /* Info site */
    'logo_site' => 'Logos du site',
    'app_parameters' => 'Information générale',
    'site_name' => 'Nom du site',
    'logo_home' => 'Logo Menu ',
    'logo' => 'Logo Site',
    'logo_footer' => 'Logo Footer',

    /* mails */

    'emails' => 'Emails du site',
    'to_email' => 'Email de réception',
    'form_email' => 'Email SPORTHOME',
    'form_email_contact' => 'Email formulaire de contact',

    /* Code google */

    'google_code_site' => 'Codes Google',
    'google_analytics_code' => 'Code Google Analytics',
    'google_verification_code' => 'Code Google Verification',
    'google_map_api_key' => 'Clé Api google map',

    /* Adress */

    'adress_site' => 'Coordonnées du site',
    'address_1' => 'Adresse 1',
    'address_2' => 'Adresse 2',
    'zip_code' => 'Code postal',
    'city' => 'Ville',
    'country' => 'Pays',
    'phone' => 'Numéro de téléphone',
    'email' => 'Adresse e-mail',
    'site_description' => 'Description',
    'site_slogan' => 'Slogan',
    'location' => 'Géolocalisation',
    'help_location' => 'Ex : latitude,longitude',

    /* networking */
    'networking' => 'Social network',
    'facebook_url' => 'Lien Facebook',
    'twitter_url' => 'Lien Twitter',
    'linkedin_url' => 'Lien Linkedin',
    'google_url' => 'Lien Google +',
    'pinterest_url' => 'Lien Pinterest',
    'youtube_url' => 'Lien Youtube',
    'instagram_url' => 'Lien Instagram',

    /* meta_parameters */
    'meta_parameters' => 'Meta Tags page d\'accueil',
    'meta_title' => 'Titre',
    'meta_description' => 'Description',
    'meta_keywords' => 'Mots-Clés',
    'meta_robots' => 'Robots',
    'help_meta_home' => 'Les variables autorisées : {site_name}',

    /* payment_parameters */
    'payment_fpay' => 'FPAY API',
    'payment_merchant_id' => 'ID du marchand',
    'payment_url' => 'URL FPAY',
    'payment_key_hmac' => 'Clef HMAC',
    'help_payment_merchant_id' => 'Identifiant du compte Fpay du marchand',
    'help_payment_url' => 'URL de la plateforme FPAY',
    'help_payment_key_hmac' => 'Clé secrète du marchand',
    'facebook_pixel_code' => 'Facebook Pixel'
];
