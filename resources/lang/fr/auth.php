<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ces informations d\'identification ne correspondent à aucun utilisateur.',
    'throttle' => 'Trop de tentatives de connexion. essayez à nouveau dans :seconds secondes.',
    'logout' => 'Déconnexion',
    'my_profile' => 'Mon profil',
    'admin_panel_login' => 'Admin Panel - Authentification',
    'admin_panel_forget_password' => 'Admin Panel - Mot de passe oublié ?',
    'admin_panel_reset_password' => 'Admin Panel - Réinitialiser le mot de passe',
    'remember_me' => 'Se souvenir de moi',
    'forgot_password' => 'Mot de passe oublié ?',
    'email' => 'Adresse e-mail',
    'password' => 'Mot de passe',
    'confirm_password' => 'Confirmation du mot de passe',
    'welcome' => 'Bienvenue',
    'please_login' => 'Veuillez vous connecter',
    'login' => 'Connexion',
    'submit' => 'Envoyer',
    'back' => 'Retour',
    'reset_password_msg' => 'Saisissez votre adresse e-mail pour que nous puissions vous envoyer un lien permettant de réinitialiser votre mot de passe.',
    'reset_password' => 'Réinitialiser le mot de passe',
    'reset_password_title' => 'Veuillez renseigner les informations ci-dessous :',
    'reset' => 'Réinitialiser',
    'confirmation' => 'Nous avons envoyé un code d\'activation. Veuillez vérifier votre e-mail.',
];
