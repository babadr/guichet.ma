<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvlle scène',
    'add' => 'Ajouter une scène',
    'edit' => 'Editer la scène <small>:name</small>',
    'list' => 'Liste des scènes',
    'add_details' => 'Détails scène',
    'edit_details' => 'Détails scène : #:id',
    'manage_scenes' => 'Gestion des scènes',

    'name' => 'Nom',
    'code' => 'Code',
    'export_code' => 'Code de l\'export',
    'quantity' => 'Quantité codes flottants',

    'new_created' => 'Nouvelle scène créée avec succès.',
    'updated' => 'Scène mise à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder cette scène, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette scène.',
];
