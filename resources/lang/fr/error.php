<?php
return [
    'Error'          => 'Erreur',
    'meanwhile_desc' => '<a href="/:url">Revenir à la page d\'accueil</a>.',
    '404_title'      => 'Page introuvable.',
    '404_desc'       => 'Nous ne pouvons pas trouver la page que vous recherchez.',
    '500_title'      => 'Une erreur est survenue.',
    '500_desc'       => 'Nous avons été prévenu de cette erreur, nous nous excusons pour la gêne occasionnée.',
    '403_title'      => 'Accès non autorisé.',
    '403_desc'       => 'Vous n\'avez pas les autorisations pour accèder à cette page.',
    '520_title'      => 'Oups! Erreur serveur',
    '520_desc'       => 'Le serveur Web a rencontré une condition inattendue qui l\'empêche de remplir la requête pour accéder à l\'URL demandée.',
];
