<?php
return array(
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    "accepted"         => "Le champ :attribute doit être accepté.",
    "active_url"       => "Le champ :attribute n'est pas une URL valide.",
    "after"            => "Le champ :attribute doit être une date postérieure au :date.",
    "like_or_after"    => "Le champ :attribute doit être une date postérieure ou égale au :date.",
    "alpha"            => "Le champ :attribute doit seulement contenir des lettres.",
    "alpha_dash"       => "Le champ :attribute doit seulement contenir des lettres, des chiffres et des tirets.",
    "alpha_num"        => "Le champ :attribute doit seulement contenir des chiffres et des lettres.",
    "before"           => "Le champ :attribute doit être une date antérieure au :date.",
    "before_or_equal"  => "Le champ :attribute doit être une heure antérieure à :date.",
    "after_or_equal"   => "Le champ :attribute doit être une heure postérieure à :date.",
    "between"          => array(
        "numeric" => "La valeur de :attribute doit être comprise entre :min et :max.",
        "file"    => "Le fichier :attribute doit avoir une taille entre :min et :max kilobytes.",
        "string"  => "Le texte :attribute doit avoir entre :min et :max caractères.",
    ),
    "confirmed"        => "Les champs :attribute et :other doivent être identiques.",
    "date"             => "Le champ :attribute n'est pas une date valide.",
    "date_format"      => "Le champ :attribute ne correspond pas au format :format.",
    "different"        => "Les champs :attribute et :other doivent être différents.",
    "digits"           => "Le champ :attribute doit avoir :digits chiffres.",
    "digits_between"   => "Le champ :attribute doit avoir entre :min and :max chiffres.",
    "email"            => "Le format du champ :attribute est invalide.",
    "exists"           => "Le champ :attribute sélectionné est invalide.",
    "image"            => "Le champ :attribute doit être une image.",
    "in"               => "Le champ :attribute est invalide.",
    "integer"          => "Le champ :attribute doit être un entier.",
    "ip"               => "Le champ :attribute doit être une adresse IP valide.",
    "max"              => array(
        "numeric" => "La valeur de :attribute ne peut être supérieure à :max.",
        "file"    => "Le fichier :attribute ne peut être plus gros que :max kilobytes.",
        "string"  => "Ce champ ne peut contenir plus de :max caractères.",
    ),
    "mimes"            => "Le champ :attribute doit être un fichier de type : :values.",
    "min"              => array(
        "numeric" => "La valeur de :attribute doit être supérieure à :min.",
        "file"    => "Le fichier :attribute doit être plus que gros que :min kilobytes.",
        "string"  => "Le texte :attribute doit contenir au moins :min caractères.",
    ),
    "not_in"           => "Le champ :attribute sélectionné n'est pas valide.",
    "numeric"          => "Le champ :attribute doit contenir un nombre.",
    "regex"            => "Le format du champ :attribute est invalide.",
    "required"         => "Le champ :attribute est obligatoire.",
    "required_if"      => "Le champ :attribute est obligatoire quand la valeur de :other est :value.",
    "required_with"    => "Le champ :attribute est obligatoire quand :values est présent.",
    "required_without" => "Le champ :attribute est obligatoire quand :values n'est pas présent.",
    "same"             => "Les champs :attribute et :other doivent être identiques.",
    "size"             => array(
        "numeric" => "La taille de la valeur de :attribute doit être :size.",
        "file"    => "La taille du fichier de :attribute doit être de :size kilobytes.",
        "string"  => "Le texte de :attribute doit contenir :size caractères.",
    ),
    "unique"           => "La valeur du champ :attribute est déjà utilisée.",
    "url"              => "Le format de l'URL de :attribute n'est pas valide.",
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom' => array(),
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => array(),


    /**
     * Validation of app's field in javascript
     */
    'required_field_msg' => 'Le champ ":field" est obligatoire.',
    /**
     * Validation of app's field in javascript
     */
    'this_field_is_required_msg' => 'Ce champ est obligatoire.',

    'FormatColorMsg' => 'Le format de cette couleur n\'est pas correct (#XXXXXX ou #XXX).',
    'FormatWebSiteMsg' => 'Doit-être une url correcte (protocol://domaine.ext).',
    'format_email_msg' => 'Doit-être un e-mail correct.',
    'FormatAlphaNumericMsg' => 'Ne doit contenir que des chiffres ou des lettres.',

    'password_confirmation_message' => 'Les deux mots de passe doivent être identiques.',

    'start_date_before_end_date' => 'La date de début doit être antérieur à la date de fin.',
    'end_date_after_start_date' => 'La date de fin doit être postérieur à la date de début.',
    'RequiredLogoMsg' => 'Le Logo est obligatoire',
    'file_extension_error' => 'Extensions autorisées: :exts',
    'file_maxsize_exceeded' => 'Le fichier ne doit pas dépasser :max',
    'file_empty' => 'Le fichier sélectionné est vide !',
    'StartDateBeforeEndDate' => 'La date de début doit être antérieur à la date de fin.',
    'EndDateAfterStartDate' => 'La date de fin doit être postérieur à la date de début.',
    'g_recaptcha_failed' => 'Vous devez confirmer que vous n\'êtes pas un robot.',
    'form_errors' => 'Vous avez des erreurs. Veuillez vérifier ci-dessous.',
    'form_success' => 'Votre abonnement à bien été créé !',
    'iban_validation' => 'Le code IBAN dois contenir 27 caractères',
    'bic_code_validation' => 'Le code BIC doit contenir 11 caractères',
    'establishment_code_validation' => 'Le code établissement doit contenir 5 chiffres',
    'wicket_code_validation' => 'Le code guichet doit contenir 5 chiffres',
    'account_number_validation' => 'Le numéro de compte doit contenir 11 caractères',
    'rib_key_validation' => 'La clé RIB doit contenir 2 chiffres',
    'boolean' => 'Le champ ":attribute" doit être vrai ou faux.',
);
