<?php

return [
    'full_name' => 'Nom et Prénom',
    'email' => 'E-mail',
    'phone' => 'Téléphone',
    'address' => 'Adresse',
    'city' => 'Ville',
    'zip_code' => 'Code postal',
    'avatar' => 'Photo',
    'cin' => 'N° CIN',
    'birth_date' => 'Date de naissance',
    'offer_name' => 'Carte',
    'created_at' => 'Inscrit le',
    'manage_recipients' => 'Gestion des abonnés RAJA',
    'list' => 'Liste des abonnés',
    'export' => 'Exporter les abonnés'
];
