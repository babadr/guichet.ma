<?php

return [
    'megarama_casa_s8' => 'Megarama - Casablanca (salle 8)',
    'megarama_fes' => 'Megarama - Fès',
    'theatre_med_5_oujda' => 'Théatre Mohamed V - Oujda',
    'theatre_med_5_rabat' => 'Théatre Mohamed V - Rabat',
    'prestigia_marrakech' => 'Prestigia - Marrakech',
    'zenith_rabat_souad_massi' => 'Zenith - Rabat (Souad Massi)',
    'rialto_casa_souad_massi' => 'Rialto - Casablanca (Souad Massi)',
    'theatre_med_5_balcon_rabat' => 'Théatre Mohamed V - Rabat (Balcon)',
    'megarama_casa_s8_2' => 'Megarama - Casablanca (salle 8) - BIS',
    'megarama_casa_s8_3' => 'Megarama - Casablanca (salle 8) - New',
    'theatre_med_5_vip_rabat' => 'Théatre Mohamed V - Rabat (VIP)',
	'le_studio_arts_vivants' => 'Le Studio Arts Vivants',
	'le_studio_arts_vivants_plan2' => 'Le Studio Arts Vivants plan 2',
	'megarama_fes_hanane' => 'Megarama - Fès - HANANE FADILY',
	'theatre_med_5_oujda_plan2' => 'Théatre Mohamed V - Oujda PLAN02',
    'plan_marrakech' => 'Plan Marrakech',
    'plan_tanger' => 'Plan Tanger',
];
