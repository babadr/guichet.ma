<?php

return [
    /**
     * View titles & buttons
     */
    'view' => 'Détails de la demande : <small>:name</small>',
    'list' => 'Liste des demandes',
    'add_details' => 'Détails de la demande',
    'edit_details' => 'Détails de la demande : #:id',

    'subject' => 'Sujet',
    'message' => 'Message',
    'contact_us' => 'Contactez-nous',
    'created_at' => 'Envoyée le',
    'manage_contacts' => 'Gestion des demandes de contact',
    'contact_info' => 'Contact Infos',
    'contact_form' => 'Formulaire de contact',
    'our_location' => 'Notre emplacement',
    'subject_email' => 'Demande de contact',
    'your_subject'  => 'Sujet de votre demande',
    'contacts' => 'Contacts',
    /**
     * Fields
     */
    'last_name' => 'Nom',
    'full_name' => 'Nom & Prénom',
    'first_name' => 'Prénom',
    'email' => 'E-mail',
    'from' => 'Nom et prénom',
    'phone' => 'Téléphone',
    'received_on' => 'Reçu le',
    'your_last_name' => 'Votre nom',
    'your_first_name' => 'Votre prénom',
    'your_email' => 'Votre adresse e-mail',
    'your_phone' => 'Votre téléphone',
    'your_message' => 'Votre message',
    'send_message' => 'Envoyer le message',
    'read' => 'Lu ?',
    /**
     * Messages
     */
    'contact_success' => 'Votre message a bien été transmis à votre conseiller. Nous revenons vers vous dans les plus brefs délais.',
    'contact_failed' => 'Une erreur s\est produite lors de de l\'envoi de votre mail. Veuillez réessayer à nouveau.',
    'cannot_save' => 'Impossible d\'envoyer votre demande, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver cette demande.',
    'front_title' => 'Vos données seront en sécurité !',
    'front_message' => 'N\'hésitez pas à nous contacter en envoyant un email via le formulaire ci-dessous ou par téléphone sur le :phone. Nos conseillers sont à l\'écoute !'
];
