<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouvel utilisateur',
    'add' => 'Ajouter un utilisateur',
    'edit' => 'Editer l\'utilisateur <small>:name</small>',
    'list' => 'Liste des utilisateurs',
    'add_details' => 'Détails utilisateur',
    'edit_details' => 'Détails utilisateur : #:id',

    'login' => 'Connexion',
    'sign_up' => 'Inscription',
    'member_since' => 'Membre depuis le :date',
    'password' => 'Mot de passe',
    'password_confirmation' => 'Confirmation de mot de passe',
    'avatar' => 'Photo',
    'last_name' => 'Nom',
    'first_name' => 'Prénom',
    'provider_id' => 'Producteur',
    'email' => 'E-mail',
    'your_email' => 'Votre adresse e-mail',
    'status' => 'Statut',
    'phone' => 'Téléphone',
    'address' => 'Adresse',
    'zip_code' => 'Code postal',
    'city' => 'Ville',
    'country' => 'Pays',
    'role_id' => 'Rôle',
    'profil' => 'Profil',
    'since_at' => 'Membre depuis',
    'active' => 'Activé ?',
    'ManageUsers' => 'Gestion des utilisateurs',
    'UsersList' => 'Liste des utilisateurs',
    'AddUser' => 'Ajouter un utilisateur',
    'remember_me' => 'Se souvenir de moi',
    'forget_your_password' => 'Mot de passe oublié ?',
    'registration' => 'Nouveau compte ?',
    'reset_password' => 'Réinitialiser votre mot de passe',
    'send_reset_link' => 'Envoyer le lien de réinitialisation',
    'my_personal_info' => 'Mes informations personnelles',
    'export' => 'Exporter les clients',

    'new_created' => 'Nouvel utilisateur créé avec succès.',
    'updated' => 'Porfil mis à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder ce profil, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver ce profil.',
    'account_settings' => 'Paramètres du compte',
    'Personal_Info' => 'Informations personnelles',
    'Change_Password' => 'Changer le mot de passe',
    'Change_Avatar' => 'Changer d\'avatar',
    'cannot_delete_user' => 'Impossible de supprimer cet utilisateur.',
    'registration_message' => 'Si vous avez des difficultés ou des questions pour vous inscrire, consultez la FAQ',
    'forget_password_message' => 'Saisissez votre adresse e-mail pour que nous puissions vous envoyer un lien permettant de réinitialiser votre mot de passe.',
    'users' => 'Utilisateurs'
];
