<?php

return [
    'auto_message' => 'Ceci est un message automatique, veuillez ne pas y répondre directement.',
    'view_message' => 'Si ce mail ne s\'affiche pas correctement, <a href=":link" style="color: #888888; text-decoration: underline;"><span style="color: #888888; text-decoration: underline;">cliquez ici</span></a>.',
    'reset' => ':sitename - Réinitialisation de votre mot de passe',
    'welcome' => ':sitename vous souhaite la bienvenue',
    'confirm' => 'Veuillez confirmer votre e-mail',
    'new_message' => ':sitename - Vous avez reçu un message',
    'delivery_canceled' => 'Livraison annulée'
];
