<?php

return [
    'read' => 'Afficher',
    'update' => 'Modifier',
    'delete' => 'Supprimer',
    'create' => 'Ajouter',
    'export' => 'Exporter',
    'matrix_permissions' =>' Matrice des permissions',
    'global_permissions' => 'Permissions global',
    'matrix' => 'Matrice des permissions',
    'administration_part' => 'PARTIE ADMINISTRATION',
    'site_part' => 'PARTIE SITE',
    'updated' => 'Modifications effectuées avec succès.',
    'permissions' => 'Permissions'
];