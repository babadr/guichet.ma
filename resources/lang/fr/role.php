<?php

return [
    /**
     * View titles & buttons
     */
    'new' => 'Nouveau rôle',
    'add' => 'Ajouter un rôle',
    'edit' => 'Editer le rôle <small>:name</small>',
    'list' => 'Liste des rôles',
    'add_details' => 'Détails rôle',
    'edit_details' => 'Détails rôle : #:id',

    'name' => 'Titre',
    'display_name' => 'Nom affiché',
    'description' => 'description',
    'manage_roles' => 'Gestion des rôles',

    'new_created' => 'Nouveau rôle créé avec succès.',
    'updated' => 'Rôle mis à jour avec succès.',
    'cannot_save' => 'Impossible de sauvegarder ce rôle, les erreurs sont affichées ci-dessous.',
    'not_found' => 'Impossible de retrouver ce rôle.',
    'role_associated_users' => 'Impossible de supprimer ce rôle, il est associé à un ou plusieurs utilisateurs.',
    'roles' => 'Rôles'
];
