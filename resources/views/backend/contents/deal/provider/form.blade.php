@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-globe-alt font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('provider.edit_details', ['id' => $model->id]) : trans('provider.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.provider.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div>
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <h3 class="page-header">Informations producteur</h3>
                            <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="active">
                                    @Lang('provider.active') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control make-switch" name="active"
                                           id="active" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ empty($model->id) || $model->active ? 'checked' : '' }} />
                                    @if ($errors->has('active'))
                                        <small id="active-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('active') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="title">
                                    @Lang('provider.title') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="text" name="title" id="title"
                                           value="{{ old('title') ? old('title') : ($model->title ? $model->title : '') }}"
                                           required class="form-control" />
                                    @if ($errors->has('title'))
                                        <small id="title-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('title') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="description">@Lang('deal.description')</label>
                                <div class="col-md-7 error-container">
                                    <textarea name="description" id="description"
                                              class="form-control summernote">{{ old('description') ? old('description') : ($model->description ? $model->description : '') }}</textarea>
                                    @if ($errors->has('description'))
                                        <small id="description-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('description') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('commission') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="commission">
                                    @Lang('provider.commission')
                                </label>
                                <div class="col-md-2 error-container">
                                    <div class="input-group">
                                        <input type="number" name="commission" id="commission" class="form-control"
                                               value="{{ old('commission') ? old('commission') : ($model->commission ? $model->commission : '') }}" />
                                        <span class="input-group-addon"> % </span>
                                    </div>
                                    @if ($errors->has('commission'))
                                        <small id="commission-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('commission') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('secure_code') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="secure_code">
                                    @Lang('provider.secure_code')  <span class="required"> * </span>
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="secure_code" id="secure_code" class="form-control" required
                                           value="{{ old('secure_code') ? old('secure_code') : ($model->secure_code ? $model->secure_code : '') }}" />
                                    @if ($errors->has('secure_code'))
                                        <small id="secure_code-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('secure_code') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="phone">
                                    @Lang('provider.phone')
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="phone" id="phone" class="form-control"
                                           value="{{ old('phone') ? old('phone') : ($model->phone ? $model->phone : '') }}" />
                                    @if ($errors->has('phone'))
                                        <small id="phone-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('phone') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('fax') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="fax">
                                    @Lang('provider.fax')
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="fax" id="fax" class="form-control"
                                           value="{{ old('fax') ? old('fax') : ($model->fax ? $model->fax : '') }}" />
                                    @if ($errors->has('fax'))
                                        <small id="fax-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('fax') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="city">
                                    @Lang('user.city')
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="city" id="city" class="form-control"
                                           value="{{ old('city') ? old('city') : ($model->user ? $model->user->city : '') }}" />
                                    @if ($errors->has('city'))
                                        <small id="city-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('city') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="address">
                                    @Lang('user.address')
                                </label>
                                <div class="col-md-5 error-container">
                                    <input type="text" name="address" id="address" class="form-control"
                                           value="{{ old('address') ? old('address') : ($model->user ? $model->user->address : '') }}" />
                                    @if ($errors->has('address'))
                                        <small id="address-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('address') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group {{ $errors->has('logo') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">
                                    @Lang('provider.logo')
                                </label>
                                <div class="col-md-7 error-container">
                                    <div class="fileinput {{ $model->logo ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail"
                                             style="width: 200px; height: 150px;">
                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&text=aucune+image" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px;"><img src="{{ $model->id ? asset_cdn($model->logo) . '?w=200&h=150&fit=clip&auto=format,compress&q=80' : '' }}" alt=""/></div>
                                        <div>
                                        <span class="btn blue btn-file">
                                            <span class="fileinput-new"> @Lang('app.select_image') </span>
                                            <span class="fileinput-exists"> @Lang('app.change') </span>
                                            <input type="file" name="logo"  data-rule-accept="image/*" data-msg-accept="Les types de fichiers autorisés sont jpg, jpeg, png et gif" accept=".jpeg,.png,.jpg,.gif">
                                        </span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                               data-dismiss="fileinput"> @Lang('app.remove') </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3 class="page-header">Informations compte</h3>
                            <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="last_name">
                                    @Lang('user.last_name')  <span class="required"> * </span>
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="last_name" id="last_name" class="form-control" required
                                           value="{{ old('last_name') ? old('last_name') : ($model->user ? $model->user->last_name : '') }}" />
                                    @if ($errors->has('last_name'))
                                        <small id="last_name-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('last_name') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="first_name">
                                    @Lang('user.first_name')  <span class="required"> * </span>
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="first_name" id="first_name" class="form-control" required
                                           value="{{ old('first_name') ? old('first_name') : ($model->user ? $model->user->first_name : '') }}" />
                                    @if ($errors->has('first_name'))
                                        <small id="first_name-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('first_name') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="email">
                                    @Lang('provider.email')  <span class="required"> * </span>
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="email" id="email" class="form-control" required
                                           value="{{ old('email') ? old('email') : ($model->email ? $model->email : '') }}" />
                                    @if ($errors->has('email'))
                                        <small id="email-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('email') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label"
                                       for="password">@Lang('user.password') @if (!$model->user) <span
                                            class="required"> * </span>@endif</label>
                                <div class="col-md-3 error-container">
                                    <input class="form-control" type="password" name="password"
                                           id="password" {!! $model->user ? : 'required' !!}>
                                    @if ($errors->has('password'))
                                        <small id="password-error" class="has-error help-block help-block-error">{{ $errors->first('password') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label"
                                       for="password-confirm">@Lang('user.password_confirmation') @if (!$model->user)
                                        <span class="required"> * </span>@endif</label>
                                <div class="col-md-3 error-container">
                                    <input class="form-control" type="password" name="password_confirmation"
                                           id="password-confirm" data-rule-equalto="#password"
                                           data-msg-equalto="Le mot de passe est différent. Merci de saisir un mot de passe identique." {!! $model->user ? : 'required' !!}>
                                    @if ($errors->has('password_confirmation'))
                                        <small id="password-confirmation-error" class="has-error help-block help-block-error">{{ $errors->first('password_confirmation') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/summernote.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/lang/summernote-fr-FR.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection