@if (Entrust::can(['backend.current_event.read']))
    {{--<a href="{{ route('backend.provider.statistics', ['partner_id' => $model->id]) }}" alt="Rapports & Statistiques"--}}
       {{--title="Rapports & Statistiques" class="btn btn-outline blue btn-xs">--}}
        {{--<i class="fa fa-bar-chart"></i>--}}
    {{--</a>--}}
    <a href="{{ route('backend.dashboard', ['id' => $model->id]) }}" alt="Rapports & Statistiques"
       title="Rapports & Statistiques" class="btn btn-outline blue btn-xs">
        <i class="fa fa-bar-chart"></i>
    </a>
@endif
@if (Entrust::can(['backend.' . $modelName . '.update']))
    <a href="{{ route('backend.' . $modelName . '.edit', ['id' => $model->id]) }}" alt="@Lang('app.Edit')"
       title="@Lang('app.Edit')" class="btn btn-outline yellow btn-xs">
        <i class="fa fa-edit"></i>
    </a>
@endif
@if (Entrust::can(['backend.' . $modelName . '.delete']))
    <a href="{{ route('backend.' . $modelName . '.delete', ['id' => $model->id]) }}" alt="@Lang('app.Delete')"
       title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-element">
        <i class="fa fa-trash"></i>
    </a>
@endif
@if (Entrust::can(['backend.' . $modelName . '.delete']) && $model->id === 143)
    <a href="{{ route('backend.provider.export_subscriptions', ['id' => $model->id]) }}"
       title="Exporter les achats sans abonnement" class="btn btn-outline green btn-xs">
        <i class="fa fa-file-excel-o"></i>
    </a>
@endif