@extends('layouts.backend')

@section('title')
    @Lang('provider.manage_providers')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-globe-alt font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('provider.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.provider.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.provider.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('provider.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
