@extends('layouts.backend')

@section('title')
    Bloquer des places pour l'évenement "{{ $event->title }}"
@endsection

@section('content')
    <form action="{{ route('backend.deal.post_reserve_seats', $event->id) }}" method="POST" id="app-form"
          class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-ticket font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Bloquer des places</span>
                </div>
                <div class="actions">
                    @if (Entrust::hasRole(['partner']))
                        <a href="{{ route('backend.index') }}" class="btn default">
                            <i class="fa fa-reply"></i>
                            <span class="hidden-480">Retour au tableau de bord</span>
                        </a>
                    @else
                        <a href="{{ route('backend.deal.index') }}" class="btn default">
                            <i class="fa fa-reply"></i>
                            <span class="hidden-480">@Lang('app.back_to_list')</span>
                        </a>
                    @endif

                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet grey-cascade box {{ str_replace('seat.', '', config('enums.seating_plans')[$event->seating_plan_id]) }}">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-map-signs"></i>Plan de salle
                            - {{ trans(config('enums.seating_plans')[$event->seating_plan_id]) }}
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="seat-container">
                                    <div id="seat-map">
                                        <div id="legend"></div>
                                        <div class="front-indicator">Scène</div>
                                        @if($event->seating_plan_id == 1 || $event->seating_plan_id == 4 || $event->seating_plan_id == 5 || $event->seating_plan_id == 8)
                                            <div class="governance">Régie</div>
                                        @endif
                                        @if($event->seating_plan_id == 2)
                                            <div class="porte_1">Porte 1 Orchestre</div>
                                            <div class="porte_2">Porte 2 Orchestre</div>
                                        @endif
                                        @if($event->seating_plan_id == 6 || $event->seating_plan_id == 7)
                                            <div class="porte_1">Orchestre</div>
                                            <div class="porte_2">Balcon</div>
                                        @endif
                                    </div>
                                    <div class="booking-details">
                                        @if($event->seating_plan_id == 8 || $event->seating_plan_id == 10)
                                            <div style="color: red; margin: 10px 0; font-weight: bold; font-size: 12px">
                                                * Placements libres en Balcon
                                            </div>
                                        @endif
                                        <h4> Places bloquées (<span id="counter">0</span>) :</h4>
                                        <ul id="selected-seats"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset('/frontend/js/jquery.seat-charts.js') }}"></script>
    <script src="{{ asset('/backend/js/seat.js') }}" type="text/javascript"></script>
    <script>
        var onSubmitForm = function () {
            $('#app-form').submit(function () {
                var seats = [];
                $.each(window.selectedSeats, function (index, item) {
                    seats.push(item.seat);
                });
                $('<input />').attr('type', 'hidden')
                    .attr('name', "seats")
                    .attr('value', seats.join(","))
                    .appendTo('#app-form');

                return true;
            });
        };
        jQuery(document).ready(function () {
            window.reservedSeats = {!! json_encode($reservedSeats) !!};
            window.rows = {!! json_encode($rows) !!};
            window.mapCharts = {!! json_encode($mapCharts) !!};
            window.seats = {!! json_encode($seat_offers) !!};
            window.items = {!! json_encode($items) !!};
            window.selectedSeats = [];
            window.blockedSeats = {!! json_encode($bloqued) !!};
            SeatingPlan.initSeatCharts();
            SeatingPlan.initReservedSeats();
            SeatingPlan.initBloquedSeats();
            onSubmitForm();
        });
    </script>
@endsection
