@extends('layouts.backend')

@section('title')
    @Lang('deal.manage_deals')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-tag font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('deal.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.deal.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.deal.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('deal.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
