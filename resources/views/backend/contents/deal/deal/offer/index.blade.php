<div class="pull-right">
    <a class="btn blue btn-outline btn-circle" data-toggle="modal" data-target="#modal_offer">
        <i class="fa fa-plus"></i>
        <span class="hidden-xs"> @lang('offer.add') </span>
    </a>
</div>

<div class="m-section">
    <div class="m-section__content">
        <table class="table table-striped m-table">
            <thead>
            <tr>
                <th>#</th>
                <th>@lang('offer.title')</th>
                <th>@lang('offer.price')</th>
                <th>@lang('offer.old_price')</th>
                <th>@lang('offer.reservation_price')</th>
                <th>@lang('offer.quantity')</th>
                <th>@lang('offer.quantity_max')</th>
                <th>@lang('offer.default')</th>
                <th>@lang('offer.active')</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
                @foreach($model->offers as $offer)
                    <tr>
                        <th scope="row">{{ $offer->id }}</th>
                        <td>{{ $offer->title }}</td>
                        <td>{{ $offer->price }}</td>
                        <td>{{ $offer->old_price }}</td>
                        <td>{{ $offer->reservation_price }}</td>
                        <td>{{ $offer->quantity }}</td>
                        <td>{{ $offer->quantity_max }}</td>
                        <td>
                            <input type="checkbox" class="offer-switch"
                                   {!! $offer->default ? 'checked' : '' !!}
                                   data-on-color="success"
                                   data-off-color="warning"
                                   data-size="mini"
                                   data-on-text="@Lang('app.yes')"
                                   data-off-text="@Lang('app.no')"
                                   data-switch-field="default"
                                   data-switch-url="{{ route('backend.offer.switchDefault', ['id' => $offer->id]) }}">
                        </td>
                        <td>
                            <input type="checkbox" class="make-switch"
                                   {!! $offer->active ? 'checked' : '' !!}
                                   data-on-color="success"
                                   data-off-color="warning"
                                   data-size="mini"
                                   data-on-text="@Lang('app.yes')"
                                   data-off-text="@Lang('app.no')"
                                   data-switch-field="active"
                                   data-switch-url="{{ route('backend.offer.switchActive', ['id' => $offer->id]) }}">
                        </td>
                        <td>
                            @if (Entrust::can(['backend.offer.delete']))
                                <a href="{{ route('backend.offer.delete', ['id' => $offer->id]) }}"
                                   title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-element">
                                    <i class="fa fa-trash"></i>
                                </a>
                            @endif
                            @if (Entrust::can(['backend.offer.update']))
                                <a class="btn blue btn-outline btn-xs edit-offer" data-url="{{ route( 'backend.offer.find', ['id' => $offer->id]) }}" title="@Lang('app.Edit')">
                                    <i class="fa fa-edit"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
