<form action="{{ route('backend.offer.store') }}" method="post" id="app-form-offer"
      class="form-horizontal form-validate" accept-charset="UTF-8" novalidate="novalidate"
      enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="modal fade" id="modal_offer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">@lang('offer.add_details')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="active">
                            @Lang('offer.active') <span class="required"> * </span>
                        </label>
                        <div class="col-md-7 error-container">
                            <input type="checkbox" class="form-control switch-status" name="active"
                                   id="active" data-on-text="Oui" data-off-text="Non"
                                   data-on-color="success" data-off-color="warning"
                                   data-size="normal" checked/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="default">
                            @Lang('offer.default') <span class="required"> * </span>
                        </label>
                        <div class="col-md-7 error-container">
                            <input type="checkbox" class="form-control switch-status" name="default"
                                   id="default" data-on-text="Oui" data-off-text="Non" data-size="normal"
                                   data-on-color="success" data-off-color="warning"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="title">
                            @Lang('offer.title') <span class="required"> * </span>
                        </label>
                        <div class="col-md-7 error-container">
                            <input type="text" name="title" id="title" maxlength="70" required class="form-control"
                                   value=""/>
                            <div class="help-block"> 70 caractères maximum</div>
                        </div>
                    </div>

                    <div class="form-group hidden">
                        <label class="col-md-3 control-label" for="description">@Lang('offer.description')</label>
                        <div class="col-md-7 error-container">
                            <textarea name="description" id="description" class="form-control summernote"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="price">
                            @Lang('offer.price') <span class="required"> * </span>
                        </label>
                        <div class="col-md-2 error-container">
                            <input type="text" name="price" id="price" class="form-control" required value=""/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="old_price">
                            @Lang('offer.old_price')
                        </label>
                        <div class="col-md-2 error-container">
                            <input type="text" name="old_price" id="old_price" class="form-control"
                                   value=""/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="reservation_price">
                            @Lang('offer.reservation_price')
                        </label>
                        <div class="col-md-2 error-container">
                            <input type="text" name="reservation_price" id="reservation_price" class="form-control"
                                   value=""/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="quantity">
                            @Lang('offer.quantity') <span class="required"> * </span>
                        </label>
                        <div class="col-md-2 error-container">
                            <input type="number" name="quantity" id="quantity" class="form-control" required value=""/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="quantity_max">
                            @Lang('offer.quantity_max')
                        </label>
                        <div class="col-md-2 error-container">
                            <input type="number" name="quantity_max" id="quantity_max" class="form-control" value="0"/>
                        </div>
                    </div>

                    @if($model && $model->is_football)
                        <div class="form-group">
                            <label class="col-md-3 control-label">
                                Canevas des billets
                            </label>
                            <div class="col-md-5 error-container">
                                <div class="fileinput fileinput-new"
                                     data-provides="fileinput">
                                    <div class="input-group input-large">
                                        <div class="form-control uneditable-input input-fixed input-medium"
                                             data-trigger="fileinput">
                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                            <span class="fileinput-filename"> </span>
                                        </div>
                                        <span class="input-group-addon btn default btn-file">
                                                                <span class="fileinput-new"> @Lang('app.select_file') </span>
                                                                <span class="fileinput-exists"> @Lang('app.change') </span>
                                                                <input type="file" name="caneva" id="caneva"
                                                                       data-rule-extension="xls|xlsx"
                                                                       data-msg-extension="Seuls les fichiers EXCEL sont autorisés"> </span>
                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists"
                                           data-dismiss="fileinput"> @Lang('app.remove') </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('app.cancel')</button>
                    <button type="submit" class="btn btn-primary">@lang('app.Submit')</button>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="deal_id" value="{{ $model->id ? $model->id : '' }}"/>
    <input type="hidden" name="id" id="id" value="0"/>

</form>
