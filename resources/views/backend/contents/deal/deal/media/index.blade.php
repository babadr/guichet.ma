<div class="row">
    <div class="col-md-12">
        <div id="drag-and-drop-zone" class="dm-uploader dropzone-file-area">
            <h3 class="mb-5 mt-5 text-muted">Faites glisser et déposer des fichiers ici </h3>

            <div class="btn btn-block mb-5">
                <span>Chercher les fichiers sur votre ordinateur</span>
                <input type="file" title='Click to add Files' />
            </div>
        </div>
    </div>
</div>

<div class="m-section">
    <div class="m-section__content">
        <table class="table table-striped m-table">
            <thead>
            <tr>
                <th>@lang('deal.media')</th>
                <th>@lang('deal.is_cover_media')</th>
                <th>@lang('deal.is_miniature_media')</th>
                <th>@lang('permission.delete')</th>
            </tr>
            </thead>
            <tbody id="files">
            @foreach($model->medias as $media)
                <tr>
                    <th scope="row">
                        <img src="{{ asset_cdn($media->filename) . '?w=50&h=50&fit=crop&auto=format,compress&q=80' }}"
                             class="img-responsive" alt="">
                    </th>
                    <td>
                        <input type="checkbox" class="media-switch"
                               {!! $media->is_cover ? 'checked' : '' !!}
                               data-on-color="success"
                               data-off-color="warning"
                               data-size="mini"
                               data-on-text="@Lang('app.yes')"
                               data-off-text="@Lang('app.no')"
                               data-switch-field="is_cover"
                               data-switch-url="{{ route('backend.deal.switchCover', ['id' => $media->id, 'deal_id' => $model->id]) }}">
                    </td>
                    <td>
                        <input type="checkbox" class="media-switch"
                               {!! $media->is_miniature ? 'checked' : '' !!}
                               data-on-color="success"
                               data-off-color="warning"
                               data-size="mini"
                               data-on-text="@Lang('app.yes')"
                               data-off-text="@Lang('app.no')"
                               data-switch-field="is_miniature"
                               data-switch-url="{{ route('backend.deal.switchMiniature', ['id' => $media->id, 'deal_id' => $model->id]) }}">
                    </td>
                    <td>
                        <a href="{{ route('backend.deal.delete.media', ['id' => $media->id]) }}"
                           title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-media">
                            <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
