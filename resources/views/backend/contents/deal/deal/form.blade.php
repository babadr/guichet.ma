@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="type" id="type" value="{{ old('type') ? old('type') : ($model->type ? $model->type : 'ticket') }}">
        <input type="hidden" name="bought" id="bought" value="{{ old('bought') ? old('bought') : ($model->bought ? $model->bought : '0') }}">
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-tag font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('deal.edit_details', ['id' => $model->id]) : trans('deal.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.deal.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line settings">
                    <ul class="nav nav-tabs ">
                        <li role="presentation" class="active">
                            <a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">@Lang('app.info')</a>
                        </li>
                        @if($model->id)
                            <li role="presentation">
                                <a href="#tab_3" aria-controls="tab_3" role="tab" data-toggle="tab">@Lang('offer.offers')</a>
                            </li>
                            <li role="presentation">
                                <a href="#tab_4" aria-controls="tab_4" role="tab" data-toggle="tab">@Lang('deal.media')</a>
                            </li>
                        @endif
                        <li role="presentation">
                            <a href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">@Lang('seo.tab_title')</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="active">
                                    @Lang('deal.active') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control switch-status" name="active"
                                           id="active" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ $model->active ? 'checked' : '' }} />
                                    @if ($errors->has('active'))
                                        <small id="active-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('active') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('show_on_slider') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="show_on_slider">
                                    @Lang('deal.show_on_slider') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control switch-status" name="show_on_slider"
                                           id="show_on_slider" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ $model->show_on_slider ? 'checked' : '' }} />
                                    @if ($errors->has('show_on_slider'))
                                        <small id="show_on_slider-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('show_on_slider') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('show_on_slider') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="is_football">
                                   Ticket Football ?<span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control switch-status" name="is_football"
                                           id="is_football" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ $model->is_football ? 'checked' : '' }} />
                                    @if ($errors->has('is_football'))
                                        <small id="is_football-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('is_football') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('is_private') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="is_private">
                                    Evénement privé ?<span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control switch-status" name="is_private"
                                           id="is_private" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ $model->is_private ? 'checked' : '' }} />
                                    @if ($errors->has('is_private'))
                                        <small id="is_private-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('is_private') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('barcode_type') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="barcode_type">
                                    @Lang('deal.barcode_type') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control switch-status" name="barcode_type"
                                           id="barcode_type" data-on-text="Barcode" data-off-text="QRCode"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ $model->barcode_type ? 'checked' : '' }} />
                                    @if ($errors->has('barcode_type'))
                                        <small id="barcode_type-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('barcode_type') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('barcode_prefix') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="barcode_prefix">
                                    @Lang('deal.barcode_prefix')
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="barcode_prefix" id="barcode_prefix"
                                           value="{{ old('barcode_prefix') ? old('barcode_prefix') : ($model->barcode_prefix ? $model->barcode_prefix : '') }}"
                                           class="form-control"/>
                                    @if ($errors->has('barcode_prefix'))
                                        <small id="barcode_prefix-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('barcode_prefix') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('order') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="order">
                                    @Lang('category.order') <span class="required"> * </span>
                                </label>
                                <div class="col-md-1 error-container">
                                    <input type="number" name="order" id="order"
                                           value="{{ old('order') ? old('order') : ($model->order ? $model->order : '') }}"
                                           required class="form-control"/>
                                    @if ($errors->has('order'))
                                        <small id="order-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('order') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="title">
                                    @Lang('deal.title') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="text" name="title" id="title" required class="form-control"
                                           value="{{ old('title') ? old('title') : ($model->title ? $model->title : '') }}" />
                                    @if ($errors->has('title'))
                                        <small id="title-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('title') }}</small>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="type">--}}
                                    {{--@Lang('deal.type') <span class="required"> * </span>--}}
                                {{--</label>--}}
                                {{--<div class="col-md-2 error-container">--}}
                                    {{--<select name="type" id="type" required class="form-control select2">--}}
                                        {{--<option value="">@Lang('app.select')</option>--}}
                                        {{--@foreach ($types as $key => $type)--}}
                                            {{--<option {{ (old('type') && old('type') == $key) ? 'selected' : (($model->type && $model->type == $key) ? 'selected' : '') }}--}}
                                                    {{--value="{{ $key }}">--}}
                                                {{--{{ $type }}--}}
                                            {{--</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                    {{--@if ($errors->has('type'))--}}
                                        {{--<small id="type-error" class="has-error help-block help-block-error">--}}
                                            {{--{{ $errors->first('type') }}--}}
                                        {{--</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="category_id">
                                    @Lang('deal.category_id') <span class="required"> * </span>
                                </label>
                                <div class="col-md-3 error-container">
                                    <select name="category_id" id="category_id" required class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($categories as $category)
                                            <option {{ (old('category_id') && old('category_id') == $category->id) ? 'selected' : (($model->category_id && $model->category_id == $category->id) ? 'selected' : '') }}
                                                    value="{{ $category->id }}">
                                                {{ $category->title }}
                                            </option>
                                            @foreach ($category->children as $child)
                                                <option {{ (old('category_id') && old('category_id') == $child->id) ? 'selected' : (($model->category_id && $model->category_id == $child->id) ? 'selected' : '') }}
                                                        value="{{ $child->id }}">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $child->title }}
                                                </option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <small id="category_id-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('category_id') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('provider_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="provider_id">
                                    @Lang('deal.provider_id') <span class="required"> * </span>
                                </label>
                                <div class="col-md-3 error-container">
                                    <select name="provider_id" id="provider_id" required class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($providers as $key => $provider)
                                            <option {{ (old('provider_id') && old('provider_id') == $key) ? 'selected' : (($model->provider_id && $model->provider_id == $key) ? 'selected' : '') }}
                                                    value="{{ $key }}">
                                                {{ $provider }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('provider_id'))
                                        <small id="provider_id-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('provider_id') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('city_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="city_id">
                                    @Lang('deal.city_id') <span class="required"> * </span>
                                </label>
                                <div class="col-md-3 error-container">
                                    <select name="city_id" id="city_id" required class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($cities as $key => $city)
                                            <option {{ (old('city_id') && old('city_id') == $key) ? 'selected' : (($model->city_id && $model->city_id == $key) ? 'selected' : '') }}
                                                    value="{{ $key }}">
                                                {{ $city }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('city_id'))
                                        <small id="city_id-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('city_id') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('seating_plan_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="seating_plan_id">
                                    @Lang('deal.seating_plan_id')
                                </label>
                                <div class="col-md-3 error-container">
                                    <select name="seating_plan_id" id="seating_plan_id" class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($seats as $key => $seat)
                                            <option {{ (old('seating_plan_id') && old('seating_plan_id') == $key) ? 'selected' : (($model->seating_plan_id && $model->seating_plan_id == $key) ? 'selected' : '') }}
                                                    value="{{ $key }}">
                                                {{ trans($seat) }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('seating_plan_id'))
                                        <small id="seating_plan_id-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('seating_plan_id') }}</small>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group {{ $errors->has('reserved_seats') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="reserved_seats">--}}
                                    {{--@Lang('deal.reserved_seats')--}}
                                {{--</label>--}}
                                {{--<div class="col-md-7 error-container">--}}
                                    {{--<input type="text" name="reserved_seats" id="reserved_seats" class="form-control"--}}
                                           {{--value="{{ old('reserved_seats') ? old('reserved_seats') : ($model->reserved_seats ? $model->reserved_seats : '') }}" />--}}
                                    {{--<div class="help-block">Veuillez séparer les places par "," </div>--}}
                                    {{--@if ($errors->has('reserved_seats'))--}}
                                        {{--<small id="reserved_seats-error"--}}
                                               {{--class="has-error help-block help-block-error">{{ $errors->first('reserved_seats') }}</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group {{ $errors->has('start_at') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="start_at">
                                    @Lang('deal.start_at') <span class="required"> * </span>
                                </label>
                                <div class="col-md-2 error-container">
                                    <div class="input-group  margin-bottom-5" >
                                        <input type="text" name="start_at" id="start_at" autocomplete="off" required class="form-control datetimepicker" data-date-format="dd/mm/yyyy hh:ii"
                                               value="{{ old('start_at') ? Carbon\Carbon::parse(old('start_at'))->format('d/m/Y H:i') : ($model->start_at ? $model->start_at->format('d/m/Y H:i') : '') }}" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    @if ($errors->has('start_at'))
                                        <small id="start_at-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('start_at') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('end_at') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="end_at">
                                    @Lang('deal.end_at') <span class="required"> * </span>
                                </label>
                                <div class="col-md-2 error-container">
                                    <div class="input-group margin-bottom-5">
                                        <input type="text" name="end_at" id="end_at" autocomplete="off" required class="form-control datetimepicker" data-date-format="dd/mm/yyyy hh:ii"
                                               value="{{ old('end_at') ? Carbon\Carbon::parse(old('end_at'))->format('d/m/Y H:i') : ($model->end_at ? $model->end_at->format('d/m/Y H:i') : '') }}" />
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    @if ($errors->has('end_at'))
                                        <small id="end_at-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('end_at') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('opening') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="opening">
                                    @Lang('deal.opening')
                                </label>
                                <div class="col-md-2 error-container">
                                    <input type="text" name="opening" id="opening" class="form-control"
                                           value="{{ old('opening') ? old('opening') : ($model->opening ? $model->opening : '') }}" />
                                    <div class="help-block">sous la forme : hh:mm </div>
                                    @if ($errors->has('opening'))
                                        <small id="opening-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('opening') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('start_time') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="start_time">
                                    @Lang('deal.start_time')
                                </label>
                                <div class="col-md-2 error-container">
                                    <input type="text" name="start_time" id="start_time" class="form-control"
                                           value="{{ old('start_time') ? old('start_time') : ($model->start_time ? $model->start_time : '') }}" />
                                    <div class="help-block">sous la forme : hh:mm </div>
                                    @if ($errors->has('start_time'))
                                        <small id="start_time-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('start_time') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="address">
                                    @Lang('deal.address')
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="text" name="address" id="address" required class="form-control"
                                           value="{{ old('address') ? old('address') : ($model->address ? $model->address : '') }}" />
                                    @if ($errors->has('address'))
                                        <small id="address-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('address') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="hidden form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="quantity">
                                    @Lang('deal.quantity') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="number" name="quantity" id="quantity" value="200"
                                           {{--value="{{ old('quantity') ? old('quantity') : ($model->quantity ? $model->quantity : '') }}"--}}
                                           required class="form-control"/>
                                    @if ($errors->has('quantity'))
                                        <small id="quantity-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('quantity') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('commission') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="commission">
                                    @Lang('provider.commission')  <span class="required"> * </span>
                                </label>
                                <div class="col-md-2 error-container">
                                    <div class="input-group">
                                        <input type="number" name="commission" id="commission" class="form-control" required
                                               value="{{ old('commission') ? old('commission') : ($model->commission ? $model->commission : '') }}" />
                                        <span class="input-group-addon"> % </span>
                                    </div>
                                    @if ($errors->has('commission'))
                                        <small id="commission-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('commission') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('offline_commission') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="offline_commission">
                                    Commission Guichet Offline  <span class="required"> * </span>
                                </label>
                                <div class="col-md-2 error-container">
                                    <div class="input-group">
                                        <input type="number" name="offline_commission" id="offline_commission" class="form-control" required
                                               value="{{ old('offline_commission') ? old('offline_commission') : ($model->offline_commission ? $model->offline_commission : 4) }}" />
                                        <span class="input-group-addon"> % </span>
                                    </div>
                                    @if ($errors->has('offline_commission'))
                                        <small id="offline_commission-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('offline_commission') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('offered_price') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="offered_price">
                                    Commission Ticket Offert
                                </label>
                                <div class="col-md-2 error-container">
                                    <div class="input-group">
                                        <input type="number" name="offered_price" id="offered_price" class="form-control"
                                               value="{{ old('offered_price') ? old('offered_price') : ($model->offered_price ? $model->offered_price : 0.00) }}" />
                                        <span class="input-group-addon"> DH </span>
                                    </div>
                                    @if ($errors->has('offered_price'))
                                        <small id="offered_price-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('offered_price') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            {{--<div class="form-group {{ $errors->has('bought') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="bought">--}}
                                    {{--@Lang('deal.bought')--}}
                                {{--</label>--}}
                                {{--<div class="col-md-2 error-container">--}}
                                    {{--<input type="number" name="bought" id="bought" class="form-control"--}}
                                           {{--value="{{ old('bought') ? old('bought') : ($model->bought ? $model->bought : '') }}" />--}}
                                    {{--@if ($errors->has('bought'))--}}
                                        {{--<small id="bought-error" class="has-error help-block help-block-error">--}}
                                            {{--{{ $errors->first('bought') }}--}}
                                        {{--</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="form-group {{ $errors->has('custom_field') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="custom_field">
                                    @Lang('deal.custom_field')
                                </label>
                                <div class="col-md-7 error-container">
                                    <input name="custom_field" id="custom_field"
                                           value="{{ old('custom_field') ? old('custom_field') : ($model->custom_field ? $model->custom_field : '') }}"
                                           class="form-control"/>
                                    @if ($errors->has('custom_field'))
                                        <small id="custom_field-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('custom_field') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('short_description') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="short_description">@Lang('deal.short_description')</label>
                                <div class="col-md-7 error-container">
                                    <textarea name="short_description" id="short_description"
                                              class="form-control">{{ old('short_description') ? old('short_description') : ($model->short_description ? $model->short_description : '') }}</textarea>
                                    @if ($errors->has('short_description'))
                                        <small id="short_description-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('short_description') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="description">@Lang('deal.description')</label>
                                <div class="col-md-7 error-container">
                                    <textarea name="description" id="description"
                                              class="form-control summernote">{{ old('description') ? old('description') : ($model->description ? $model->description : '') }}</textarea>
                                @if ($errors->has('description'))
                                        <small id="description-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('description') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="row form-group ">
                                <label class="control-label col-md-3">
                                    @Lang('deal.position')
                                </label>
                                <div class="col-md-7 errors-container">
                                    <div id="floating-panel">
                                        <input id="map_address" type="text" value="22, Bd Yacoub AlMansour, Casablanca">
                                        <input id="map_search" type="button" value="Chercher">
                                    </div>
                                    <div id="map" style="height: 350px" class="col-md-12"></div>
                                    <input type="hidden" name="latitude" id="address"
                                           value="{{ old('latitude') ? old('latitude') : ($model->latitude ? $model->latitude : '') }}" />
                                    <input type="hidden" name="longitude" id="longitude"
                                           value="{{ old('longitude') ? old('longitude') : ($model->longitude ? $model->longitude : '') }}" />
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('custom_ticket') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">
                                    @Lang('deal.custom_ticket')
                                </label>
                                <div class="col-md-7 error-container">
                                    <div class="fileinput {{ $model->custom_ticket ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail"
                                             style="width: 200px; height: 150px;">
                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&text=aucune+image" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px;"><img src="{{ $model->custom_ticket ? asset_cdn($model->custom_ticket) . '?w=200&h=150&fit=clip&auto=format,compress&q=80' : '' }}" alt=""/></div>
                                        <div>
                                        <span class="btn blue btn-file">
                                            <span class="fileinput-new"> @Lang('app.select_image') </span>
                                            <span class="fileinput-exists"> @Lang('app.change') </span>
                                            <input type="file" name="custom_ticket"  data-rule-accept="image/*" data-msg-accept="Les types de fichiers autorisés sont jpg, jpeg, png et gif" accept=".jpeg,.png,.jpg,.gif">
                                        </span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                               data-dismiss="fileinput"> @Lang('app.remove') </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            @include('backend.elements.seo')
                        </div>
                        @if($model->id)
                            <div class="tab-pane" id="tab_3">
                                @include('backend.contents.deal.deal.offer.index')
                            </div>
                            <div class="tab-pane" id="tab_4">
                                @include('backend.contents.deal.deal.media.index')
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>

    @include('backend.contents.deal.deal.offer.form')
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/uploader-widget/css/jquery.dm-uploader.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/css/map-picker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/summernote.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/lang/summernote-fr-FR.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/backend/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js') }}" type="text/javascript"></script>

    <script src="{{ asset('/backend/js/map-picker.js') }}" type="text/javascript"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ Setting::get('google_map_api_key') }}&callback=initMap"></script>
    <script src="{{ asset('/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/offer.js?v1.0') }}" type="text/javascript"></script>

    <script src="{{ asset('/backend/plugins/uploader-widget/js/jquery.dm-uploader.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/media-ui.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/media-config.js') }}" type="text/javascript"></script>

    <script>
        window.uploadUrl = '{{ route('backend.deal.upload-media', $model->id) }}';
    </script>
@endsection
