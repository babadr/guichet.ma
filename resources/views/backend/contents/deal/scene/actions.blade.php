@if (Entrust::hasRole(['superadmin', 'admin']))
    <a href="{{ route('backend.' . $modelName . '.edit', ['id' => $model->id]) }}"
       title="@Lang('app.Edit')" class="btn btn-outline yellow btn-xs">
        <i class="fa fa-edit"></i>
    </a>
    <a href="{{ route('backend.scene.export.all', ['id' => $model->id]) }}" alt="Exporter tous les tickets"
       title="Exporter tous les tickets" class="btn btn-outline blue btn-xs">
        <i class="fa fa-download"></i>
    </a>
    <a href="{{ route('backend.scene.export', ['id' => $model->id]) }}" alt="Exporter les tickets du jour"
       title="Exporter les tickets du jour" class="btn btn-outline green btn-xs">
        <i class="fa fa-file-excel-o"></i>
    </a>
@endif