@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-download font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('scene.edit_details', ['id' => $model->id]) : trans('scene.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.scene.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="name">
                        @Lang('scene.name') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <input type="text" name="name" id="name"
                               value="{{ old('name') ? old('name') : ($model->name ? $model->name : '') }}"
                               required class="form-control" />
                        @if ($errors->has('name'))
                            <small id="name-error" class="has-error help-block help-block-error">
                                {{ $errors->first('name') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="code">
                        @Lang('scene.code') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <input type="text" name="code" id="code"
                               value="{{ old('code') ? old('code') : ($model->code ? $model->code : '') }}"
                               required class="form-control" />
                        @if ($errors->has('code'))
                            <small id="code-error" class="has-error help-block help-block-error">
                                {{ $errors->first('code') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('export_code') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="export_code">
                        @Lang('scene.export_code') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <input type="text" name="export_code" id="export_code"
                               value="{{ old('export_code') ? old('export_code') : ($model->export_code ? $model->export_code : '') }}"
                               required class="form-control" />
                        @if ($errors->has('export_code'))
                            <small id="export_code-error" class="has-error help-block help-block-error">
                                {{ $errors->first('export_code') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="quantity">
                        @Lang('scene.quantity') <span class="required"> * </span>
                    </label>
                    <div class="col-md-2 error-container">
                        <input type="number" name="quantity" id="quantity"
                               value="{{ old('quantity') ? old('quantity') : ($model->quantity ? $model->quantity : '') }}"
                               required class="form-control" />
                        @if ($errors->has('quantity'))
                            <small id="quantity-error" class="has-error help-block help-block-error">
                                {{ $errors->first('quantity') }}
                            </small>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
