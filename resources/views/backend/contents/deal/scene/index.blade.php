@extends('layouts.backend')

@section('title')
    @Lang('scene.manage_scenes')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-map-marker font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('scene.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::hasRole(['superadmin', 'admin']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.scene.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('scene.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
