@extends('layouts.backend')

@section('title')
    Événements en cours
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-tag font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">Événements en cours</span>
            </div>
        </div>
        <div class="portlet-body">
            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead class="flip-content">
                <tr>
                    <th width="30px"> #</th>
                    <th> Titre</th>
                    <th class="numeric"> Nombre d'acheteurs</th>
                    <th class="numeric"> Quantité vendue</th>
                    <th> Quantité vendue / Catégorie</th>
                    <th class="numeric"> Total des ventes</th>
                </tr>
                </thead>
                <tbody>
                @foreach($currentDeals as $key => $deal)
                    <tr>
                        <td class="text-center"> {{ $key + 1 }}</td>
                        <td> {{ $deal->title }}</td>
                        <td class="numeric text-center"> {{ $deal->getRealBought() }}</td>
                        <td class="numeric text-center"> {{ $deal->getOrderedQuantity(false) }}</td>
                        <td>
                            <ul>
                                @foreach($deal->offers()->orderBy('price', 'DESC')->get() as $offer)
                                    <li>{{ $offer->title }}
                                        => {{ $offer->getOrderedQuantity(false) . ' / ' . $offer->quantity  }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td class="numeric text-center"> {{ Tools::displayPrice($deal->getTotalOrderedAmount(false) + $deal->getTotalOrderedAmountOffline(false), 2) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
