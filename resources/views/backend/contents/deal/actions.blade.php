<a href="{{ $model->is_private ? route('frontend.event.private', $model->private_token) : route('frontend.deal.preview', $model->id) }}" target="_blank"
   title="@Lang('app.View')" class="btn btn-outline blue btn-xs">
    <i class="fa fa-eye"></i>
</a>
@if (Entrust::can(['backend.' . $modelName . '.update']))
    <a href="{{ route('backend.' . $modelName . '.edit', ['id' => $model->id]) }}"
       title="@Lang('app.Edit')" class="btn btn-outline yellow btn-xs">
        <i class="fa fa-edit"></i>
    </a>
@endif
@if (Entrust::can(['backend.' . $modelName . '.delete']))
    <a href="{{ route('backend.' . $modelName . '.delete', ['id' => $model->id]) }}"
       title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-element">
        <i class="fa fa-trash"></i>
    </a>
@endif
@if (Entrust::can(['backend.' . $modelName . '.delete']))
    <a href="{{ route('backend.deal.export_tickets', ['id' => $model->id]) }}"
       title="Exporter les ventes" class="btn btn-outline green btn-xs">
        <i class="fa fa-file-excel-o"></i>
    </a>
@endif
@if (Entrust::hasRole(['partner','superadmin','admin']) && !empty($model->seating_plan_id))
    <a href="{{ route('backend.deal.reserve_seats', ['id' => $model->id]) }}"
       title="Bloquer des places" class="btn btn-outline blue-dark btn-xs">
        <i class="fa fa-ticket"></i>
    </a>
@endif