@extends('layouts.backend')

@section('title')
    @Lang('category.manage_categories')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-grid font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('category.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.category.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.category.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('category.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
