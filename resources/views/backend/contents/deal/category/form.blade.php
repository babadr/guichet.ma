@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-grid font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('category.edit_details', ['id' => $model->id]) : trans('category.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.category.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line settings">
                    <ul class="nav nav-tabs ">
                        <li role="presentation" class="active">
                            <a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">
                                @Lang('app.info')
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">
                                @Lang('seo.tab_title')
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="active">
                                    @Lang('category.active') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control make-switch" name="active"
                                           id="active" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ empty($model->id) || $model->active ? 'checked' : '' }} />
                                    @if ($errors->has('active'))
                                        <small id="active-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('active') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="title">
                                    @Lang('category.title') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="text" name="title" id="title"
                                           value="{{ old('title') ? old('title') : ($model->title ? $model->title : '') }}"
                                           required class="form-control"/>
                                    @if ($errors->has('title'))
                                        <small id="title-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('title') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('parent_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="parent_id">
                                    @Lang('category.parent_id')
                                </label>
                                <div class="col-md-7 error-container">
                                    <select name="parent_id" id="parent_id" class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($categories as $key => $category)
                                            <option {{ (old('parent_id') && old('parent_id') == $key) ? 'selected' : (($model->parent_id && $model->parent_id == $key) ? 'selected' : '') }}
                                                    value="{{ $key }}">
                                                {{ $category }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('parent_id'))
                                        <small id="parent_id-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('parent_id') }}</small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('order') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="order">
                                    @Lang('category.order') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="number" name="order" id="order"
                                           value="{{ old('order') ? old('order') : ($model->order ? $model->order : '') }}"
                                           required class="form-control"/>
                                    @if ($errors->has('order'))
                                        <small id="order-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('order') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('show_on_menu') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="show_on_menu">
                                    @Lang('category.show_on_menu') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control make-switch" name="show_on_menu"
                                           id="show_on_menu" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ empty($model->id) || $model->show_on_menu ? 'checked' : '' }} />
                                    @if ($errors->has('show_on_menu'))
                                        <small id="show_on_menu-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('show_on_menu') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('show_on_home') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="show_on_home">
                                    @Lang('category.show_on_home') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control make-switch" name="show_on_home"
                                           id="show_on_home" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ empty($model->id) || $model->show_on_home ? 'checked' : '' }} />
                                    @if ($errors->has('show_on_home'))
                                        <small id="show_on_home-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('show_on_home') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('color') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="color">
                                    @Lang('category.color') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <select name="color" id="color" required class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($colors as $key => $color)
                                            <option {{ (old('color') && old('color') == $key) ? 'selected' : (($model->color && $model->color == $key) ? 'selected' : '') }}
                                                    value="{{ $key }}">
                                                {{ $color }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('color'))
                                        <small id="color-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('color') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('icon') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="icon">
                                    @Lang('category.icon') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <div class="input-group iconpicker-container">
                                        <input type="text" name="icon" id="icon" required data-placement="bottomRight"
                                               value="{{ old('icon') ? old('icon') : ($model->icon ? $model->icon : '') }}"
                                               class="form-control icp icp-auto iconpicker-element iconpicker-input"/>
                                        <span class="input-group-addon"><i class="fa fa-archive"></i></span>
                                    </div>
                                    @if ($errors->has('icon'))
                                        <small id="icon-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('icon') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tab_2">
                            @include('backend.elements.seo')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-iconpicker/css/fontawesome-iconpicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-iconpicker/js/fontawesome-iconpicker.min.js') }}" type="text/javascript"></script>
    <script>
        $('#icon').iconpicker();
    </script>
@endsection