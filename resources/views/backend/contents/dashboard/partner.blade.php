@extends('layouts.backend')

@section('title')
    {{ $provider->title }}
@endsection

@section('smallTitle')
    événements et rapports
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 text-right">
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <form id="form-select-event" class="form-horizontal form-validate"
                          action="{{ auth()->user()->hasRole('partner') ? route('backend.index.event', ['partner_id' => $provider->id]) : route('backend.provider.statistics', ['partner_id' => $provider->id]) }}"
                          method="GET">
                        <div class="form-group">
                            <label class="col-md-8 control-label">
                                Listes des événements
                            </label>
                            <div class="col-md-4 error-container text-left">
                                <select id="event-list" class="form-control select2">
                                    <option value="" {{ $event_id ? '' : 'selected' }}>Tous les événements</option>
                                    @foreach($listDeals as $key => $event)
                                        <option value="{{ $key }}" {{ $event_id == $key ? 'selected' : '' }}>{{ $event }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 blue">
                <div class="visual">
                    <i class="fa fa-tags"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $deals->count() }}">0</span>
                    </div>
                    <div class="desc"> Événements en ligne</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 red">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span>{{ Tools::displayPrice($totalAmount - $totalCommission) }}</span>
                    </div>
                    <div class="desc"> Total des ventes</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 yellow">
                <div class="visual">
                    <i class="fa fa-dollar"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span>{{ Tools::displayPrice($totalCommission) }}</span>
                    </div>
                    <div class="desc"> Commission Guichet</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 green">
                <div class="visual">
                    <i class="fa fa-shopping-cart"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $totalQuantity }}">0</span>
                    </div>
                    <div class="desc"> Quantités commandées</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 yellow-lemon">
                <div class="visual">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $totalOfferedTickets }}"></span>
                    </div>
                    <div class="desc"> Total des tickets offerts</div>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
            <div class="dashboard-stat dashboard-stat-v2 purple">
                <div class="visual">
                    <i class="fa fa-clock-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <span data-counter="counterup" data-value="{{ $tickets }}"></span>
                    </div>
                    <div class="desc"> Tickets en attente de validation</div>
                </div>
            </div>
        </div>
    </div>
    @if($provider->sellers->count())
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">Détails des ventes</span>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat dashboard-stat-v2 blue">
                            <div class="visual">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{ Tools::displayPrice($totalAmountOnLine - $totalCommissionOnLine) }}</span>
                                </div>
                                <div class="desc"> Total des ventes en ligne</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat dashboard-stat-v2 red">
                            <div class="visual">
                                <i class="fa fa-bar-chart-o"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{ Tools::displayPrice($totalAmountOffLine - $totalCommissionOffLine) }}</span>
                                </div>
                                <div class="desc"> Total des ventes hors ligne</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat dashboard-stat-v2 yellow">
                            <div class="visual">
                                <i class="fa fa-dollar"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{ Tools::displayPrice($totalCommissionOnLine) }}</span>
                                </div>
                                <div class="desc"> Commission Guichet en ligne</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="dashboard-stat dashboard-stat-v2 green">
                            <div class="visual">
                                <i class="fa fa-dollar"></i>
                            </div>
                            <div class="details">
                                <div class="number">
                                    <span>{{ Tools::displayPrice($totalCommissionOffLine) }}</span>
                                </div>
                                <div class="desc"> Commission Guichet hors ligne</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-tag font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">Liste des événements</span>
            </div>
            <div class="actions">
                @if (Entrust::hasRole(['superadmin', 'admin', 'partner']))
                    <a class="btn blue btn-outline btn-circle"
                       href="{{ route('backend.index.export_tickets', ['provider_id' => $partner_id]) }}">
                        <i class="fa fa-file-excel-o"></i>
                        <span class="hidden-xs"> Exporter les tickets </span>
                    </a>
                @endif
                @if (Entrust::hasRole(['superadmin', 'admin', 'partner']))
                    <a class="btn green btn-outline btn-circle"
                       href="{{ route('backend.index.export', ['provider_id' => $partner_id]) }}">
                        <i class="fa fa-file-excel-o"></i>
                        <span class="hidden-xs"> Exporter les ventes </span>
                    </a>
                @endif
                @if (Entrust::hasRole(['superadmin', 'admin']) && $partner_id == 123)
                    <a class="btn yellow btn-outline btn-circle"
                       href="{{ route('backend.provider.export.oasis', ['provider_id' => $partner_id]) }}">
                        <i class="fa fa-file-excel-o"></i>
                        <span class="hidden-xs"> Export Oasis Festival </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body flip-scroll">
            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead class="flip-content">
                <tr>
                    <th width="30px"> #</th>
                    <th> Titre</th>
                    {{--<th> Publié le</th>--}}
                    <th class="numeric"> Nombre d'acheteurs</th>
                    <th class="numeric"> Quantité vendue</th>
                    <th> Quantité vendue / Catégorie</th>
                    <th class="numeric"> Total des ventes</th>
                    <th class="numeric"> Total des ventes en ligne</th>
                    <th class="numeric"> Total des ventes hors ligne</th>
                    <th> Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($deals as $key => $deal)
                    <tr>
                        <td class="text-center"> {{ $key + 1 }}</td>
                        <td> {{ $deal->title }}</td>
                        {{--<td> {{ date('d/m/Y', strtotime($deal->start_at)) }}</td>--}}
                        <td class="numeric text-center"> {{ $deal->getRealBought() }}</td>
                        <td class="numeric text-center"> {{ $deal->getOrderedQuantity(false) }}</td>
                        <td>
                            <ul>
                                @foreach($deal->offers()->orderBy('price', 'DESC')->get() as $offer)
                                    <li>{{ $offer->title }}
                                        => {{ $offer->getOrderedQuantity(false) . ' / ' . $offer->quantity  }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td class="numeric text-center"> {{ Tools::displayPrice($deal->getTotalOrderedAmount(false) + $deal->getTotalOrderedAmountOffline(false), 2) }}</td>
                        <td class="numeric text-center"> {{ Tools::displayPrice($deal->getTotalOrderedAmount(false), 2) }}</td>
                        <td class="numeric text-center"> {{ Tools::displayPrice($deal->getTotalOrderedAmountOffline(false), 2) }}</td>
                        <td class="numeric text-center">
                            <a href="{{ route('backend.ticket.index', ['deal_id' => $deal->id, 'partner_id' => $partner_id]) }}"
                               alt="@Lang('app.View')"
                               title="@Lang('app.View')" class="btn btn-outline blue btn-xs">
                                <i class="fa fa-eye"></i>
                            </a>
                            @if (Entrust::hasRole(['partner','superadmin','admin']) && !empty($deal->seating_plan_id))
                                <a href="{{ route('backend.deal.reserve_seats', ['id' => $deal->id]) }}"
                                   title="Bloquer des places" class="btn btn-outline green btn-xs">
                                    <i class="fa fa-ticket"></i>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @if($provider->sellers->count())
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-map-marker font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Liste des points de vente</span>
                </div>
            </div>
            <div class="portlet-body flip-scroll">
                @foreach($provider->sellers as $seller)
                    <div class="portlet grey-cascade box">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-map-marker"></i>{{ $seller->title }}
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-condensed flip-content">
                                    <thead class="flip-content">
                                    <tr>
                                        <th width="30px"> #</th>
                                        <th> Titre</th>
                                        {{--<th> Catégorie</th>--}}
                                        {{--<th> Publié le</th>--}}
                                        <th class="numeric"> Nombre d'acheteurs</th>
                                        <th class="numeric"> Quantité vendue</th>
                                        <th> Quantité vendue / Catégorie</th>
                                        <th class="numeric"> Total des ventes</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($deals as $key => $deal)
                                        <tr>
                                            <td class="text-center"> {{ $key + 1 }}</td>
                                            <td> {{ $deal->title }}</td>
                                            {{--<td> {{ $deal->category->title }}</td>--}}
                                            {{--<td> {{ date('d/m/Y', strtotime($deal->start_at)) }}</td>--}}
                                            <td class="numeric text-center"> {{ $deal->getRealBoughtOffline($seller->id) }}</td>
                                            <td class="numeric text-center"> {{ $deal->getOrderedQuantityOffline(false, $seller->id) }}</td>
                                            <td>
                                                <ul>
                                                    @foreach($deal->offers()->orderBy('price', 'DESC')->get() as $offer)
                                                        <li>{{ $offer->title }}
                                                            => {{ $offer->getOrderedQuantityOffline(false, $seller->id) . ' / ' . $offer->quantity  }}</li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td class="numeric text-center"> {{ Tools::displayPrice($deal->getTotalOrderedAmountOffline(false, $seller->id), 2) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/provider.js') }}" type="text/javascript"></script>
@endsection
