@extends('layouts.backend')

@section('title')
    {{ $provider->title }}
@endsection

@section('smallTitle')
    événements et rapports
@endsection

@section('content')
    <div class="tabbable-custom nav-justified">
        <ul class="nav nav-tabs nav-justified">
            <li {{ $history ? '' : 'class=active' }}>
                <a href="{{ route('backend.dashboard', ['id' => $provider->id]) }}"> Evénements en cours (Statistiques
                    et rapports) </a>
            </li>
            <li {{ $history ? 'class=active' : '' }}>
                <a href="{{ route('backend.dashboard', ['id' => $provider->id, 'history' => 'history']) }}"> Historiques
                    des événements
                    (Statistiques et rapports) </a>
            </li>
        </ul>
        <div class="tab-content" style="padding-top: 40px">
            <div class="tab-pane active">
                <input type="hidden" name="start_at" id="start_at" value="{{ date('d/m/Y', strtotime($start_at)) }}">
                <input type="hidden" name="end_at" id="end_at" value="{{ date('d/m/Y', strtotime($end_at)) }}">
                <div class="row">
                    <div class="col-lg-12 col-xs-12 col-sm-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-database font-green-haze"></i>
                                    <span class="caption-subject bold uppercase">Aperçu en chiffre</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3>
                                                        <span>{{ number_format($totalAmount, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                    </h3>
                                                    <small>TOTAL DE CHIFFRES D'AFFAIRES</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-dollar font-blue-ebonyclay"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3>
                                                        <span>{{ number_format($totalAmountOnLine - ($totalCommissionOnLine + $totalCommissionOffLine), '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                    </h3>
                                                    <small>NET A PAYER</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-check-square-o font-yellow-gold"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3>
                                                        <span>{{ number_format($totalAmountOnLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                    </h3>
                                                    <small>TOTAL DE VENTES EN LIGNE</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-bar-chart font-red"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3>
                                                        <span>{{ number_format($totalAmountOffLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                    </h3>
                                                    <small>TOTAL DE VENTES OFFLINE</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-bar-chart font-red"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3>
                                                        <span>{{ number_format($totalCommissionOnLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                    </h3>
                                                    <small>COMMISSION GUICHET EN LIGNE</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-money font-red"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3>
                                                        <span>{{ number_format($totalCommissionOffLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                    </h3>
                                                    <small>COMMISSION DE VENTES OFFLINE</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-money font-red"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3 class="font-yellow-haze">
                                                        <span>{{ $currentDeals->count() }}
                                                            <small>en ligne</small></span>
                                                    </h3>
                                                    <small>Nombre d'évènements</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-tag font-yellow-haze"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3 class="font-purple">
                                                        <span>{{ $totalOrders }}
                                                            <small>validées</small></span>
                                                    </h3>
                                                    <small>Total des commandes</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-shopping-cart font-purple"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3 class="font-green">
                                                        <span>{{ $totalQuantity }}
                                                            <small>tickets</small></span>
                                                    </h3>
                                                    <small>NOMBRE DE TICKETS VENDUS</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-ticket font-green"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                                        <div class="dashboard-stat2 bordered">
                                            <div class="display">
                                                <div class="number">
                                                    <h3 class="font-blue">
                                                        <span>{{ $totalOfferedTickets }}
                                                            <small>tickets</small></span>
                                                    </h3>
                                                    <small>NOMBRE DE TICKETS OFFERTS</small>
                                                </div>
                                                <div class="icon">
                                                    <i class="fa fa-ticket font-blue"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="fa fa-line-chart font-green-haze"></i>
                                    <span class="caption-subject bold uppercase">Evolution du chiffre d'affaires</span>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bordered">
                                        <div id="highchart-sales" style="height:400px;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($provider->sellers->count())
                        <div class="col-md-6">
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-green-haze">
                                        <i class="icon-bar-chart font-green-haze"></i>
                                        <span class="caption-subject bold uppercase">Chiffre d'affaires Offline par point de vente</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bordered">
                                            <div id="dashboard_amount_amchart" class="CSSAnimationChart"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="col-lg-12 col-xs-12 col-sm-12">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-tag font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase">Liste des événements</span>
                                </div>
                                <div class="actions">
                                    @if (Entrust::hasRole(['superadmin', 'admin', 'partner']))
                                        <a class="btn blue btn-outline btn-circle"
                                           href="{{ route('backend.index.export_tickets', ['provider_id' => $provider->id]) }}">
                                            <i class="fa fa-file-excel-o"></i>
                                            <span class="hidden-xs"> Exporter les tickets </span>
                                        </a>
                                    @endif
                                    @if (Entrust::hasRole(['superadmin', 'admin', 'partner']))
                                        <a class="btn green btn-outline btn-circle"
                                           href="{{ route('backend.index.export', ['provider_id' => $provider->id]) }}">
                                            <i class="fa fa-file-excel-o"></i>
                                            <span class="hidden-xs"> Exporter les ventes </span>
                                        </a>
                                    @endif
                                    @if (Entrust::hasRole(['superadmin', 'admin']) && $provider->id == 123)
                                        <a class="btn yellow btn-outline btn-circle"
                                           href="{{ route('backend.provider.export.oasis', ['provider_id' => $provider->id]) }}">
                                            <i class="fa fa-file-excel-o"></i>
                                            <span class="hidden-xs"> Export Oasis Festival </span>
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="portlet-body flip-scroll">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <ul class="nav nav-tabs tabs-left">
                                            @foreach($currentDeals as $key => $deal)
                                                <li class="{{ $key == 0 ? 'active' : '' }}">
                                                    <a href="#tab_event_{{ $key }}" data-toggle="tab">
                                                        {{ $deal->title }} <br>
                                                        <small>Du {{ date('d/m/Y', strtotime($deal->start_at)) }}
                                                            au {{ date('d/m/Y', strtotime($deal->end_at)) }}</small>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <div class="tab-content">
                                            @foreach($currentDeals as $key => $deal)
                                                @php
                                                    $dealAmountOnLine = $deal->getTotalOrderedAmount(false);
                                                    $dealAmountOffLine = $deal->getTotalOrderedAmountOffline(false);
                                                    $dealCommissionOnLine = ($deal->getTotalOrderedAmount(false) * $deal->commission) / 100;
                                                    $dealCommissionOffLine = (($deal->getTotalOrderedAmountOffline(false) * $deal->offline_commission) / 100) + $deal->getTotalOfferedCommission(false);
                                                    $dealCommission = $dealCommissionOnLine + $dealCommissionOffLine;
                                                @endphp
                                                <div class="tab-pane {{ $key == 0 ? 'active' : '' }}"
                                                     id="tab_event_{{ $key }}">
                                                    <div class="row">
                                                        <div class="col-md-6" style="margin-bottom: 20px">
                                                            <div class="btn-group btn-group btn-group-justified">
                                                                <a href="{{ route('backend.ticket.index', ['deal_id' => $deal->id, 'partner_id' => $provider->id]) }}"
                                                                   class="btn blue">
                                                                    <i class="fa fa-eye"></i> Lister les tickets
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" style="margin-bottom: 20px">
                                                            <div class="btn-group btn-group btn-group-justified">
                                                                @if (Entrust::hasRole(['partner','superadmin','admin']) && !empty($deal->seating_plan_id))
                                                                    <a href="{{ route('backend.deal.reserve_seats', ['id' => $deal->id]) }}"
                                                                       class="btn green">
                                                                        <i class="fa fa-ticket"></i> Bloquer des places
                                                                    </a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3>
                                                        <span>{{ number_format($dealAmountOnLine + $dealAmountOffLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                                        </h3>
                                                                        <small>TOTAL DE CHIFFRES D'AFFAIRES</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-dollar font-blue-ebonyclay"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3>
                                                        <span>{{ number_format($dealAmountOnLine - $dealCommission, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                                        </h3>
                                                                        <small>NET A PAYER</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-check-square-o font-yellow-gold"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3>
                                                        <span>{{ number_format($dealAmountOnLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                                        </h3>
                                                                        <small>TOTAL DE VENTES EN LIGNE</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-bar-chart font-red"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3>
                                                        <span>{{ number_format($dealAmountOffLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                                        </h3>
                                                                        <small>TOTAL DE VENTES OFFLINE</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-bar-chart font-red"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3>
                                                        <span>{{ number_format($dealCommissionOnLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                                        </h3>
                                                                        <small>COMMISSION GUICHET EN LIGNE</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-money font-red"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3>
                                                        <span>{{ number_format($dealCommissionOffLine, '2', ',', ' ') }}
                                                            <small>DHS</small></span>
                                                                        </h3>
                                                                        <small>COMMISSION DE VENTES OFFLINE</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-money font-red"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3 class="font-green">
                                                        <span>{{ $deal->getOrderedQuantity(false) }}
                                                            <small>tickets</small></span>
                                                                        </h3>
                                                                        <small>NOMBRE DE TICKETS VENDUS</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-ticket font-green"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                            <div class="dashboard-stat2 bordered">
                                                                <div class="display">
                                                                    <div class="number">
                                                                        <h3 class="font-blue">
                                                        <span>{{ $deal->getOfferedTickets() }}
                                                            <small>tickets</small></span>
                                                                        </h3>
                                                                        <small>NOMBRE DE TICKETS OFFERTS</small>
                                                                    </div>
                                                                    <div class="icon">
                                                                        <i class="fa fa-ticket font-blue"></i>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="portlet light bordered">
                                                                <div class="portlet-title">
                                                                    <div class="caption font-red-sunglo">
                                                                        <i class="icon-grid font-red-sunglo"></i>
                                                                        <span class="caption-subject bold uppercase">Quantité vendue / Catégorie</span>
                                                                    </div>
                                                                </div>
                                                                <div class="portlet-body flip-scroll">
                                                                    <div class="row">
                                                                        @foreach($deal->offers()->orderBy('price', 'DESC')->get() as $offer)
                                                                            <div class="col-md-2">
                                                                                <div class="uppercase profile-stat-text"> {{ $offer->title }} </div>
                                                                                <div class="uppercase profile-stat-title">{{ $offer->getOrderedQuantity(false) . ' / ' . $offer->quantity  }}</div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            @if($provider->sellers->count())
                                                                <div class="portlet light bordered">
                                                                    <div class="portlet-title">
                                                                        <div class="caption font-red-sunglo">
                                                                            <i class="fa fa-map-marker font-red-sunglo"></i>
                                                                            <span class="caption-subject bold uppercase">Liste des points de vente</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="portlet-body flip-scroll">
                                                                        @foreach($provider->sellers as $seller)
                                                                            <div class="portlet grey-cascade box">
                                                                                <div class="portlet-title">
                                                                                    <div class="caption">
                                                                                        <i class="fa fa-map-marker"></i>{{ $seller->title }}
                                                                                    </div>
                                                                                </div>
                                                                                <div class="portlet-body">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                                                            <div class="dashboard-stat2 bordered">
                                                                                                <div class="display">
                                                                                                    <div class="number">
                                                                                                        <h3 class="font-green">
                                                        <span>{{ $deal->getOrderedQuantityOffline(false, $seller->id) }}
                                                            <small>tickets</small></span>
                                                                                                        </h3>
                                                                                                        <small>NOMBRE DE TICKETS VENDUS</small>
                                                                                                    </div>
                                                                                                    <div class="icon">
                                                                                                        <i class="fa fa-ticket font-green"></i>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bordered">
                                                                                            <div class="dashboard-stat2 bordered">
                                                                                                <div class="display">
                                                                                                    <div class="number">
                                                                                                        <h3 class="font-blue">
                                                        <span>{{ Tools::displayPrice($deal->getTotalOrderedAmountOffline(false, $seller->id), 2) }}
                                                            <small>DHS</small></span>
                                                                                                        </h3>
                                                                                                        <small>TOTAL DE CHIFFRES D'AFFAIRES</small>
                                                                                                    </div>
                                                                                                    <div class="icon">
                                                                                                        <i class="fa fa-money font-blue"></i>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12">
                                                                                            <div class="portlet light bordered">
                                                                                                <div class="portlet-title">
                                                                                                    <div class="caption font-red-sunglo">
                                                                                                        <i class="icon-grid font-red-sunglo"></i>
                                                                                                        <span class="caption-subject bold uppercase">Quantité vendue / Catégorie</span>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="portlet-body flip-scroll">
                                                                                                    <div class="row">
                                                                                                        @foreach($deal->offers()->orderBy('price', 'DESC')->get() as $offer)
                                                                                                            <div class="col-md-2">
                                                                                                                <div class="uppercase profile-stat-text"> {{ $offer->title }} </div>
                                                                                                                <div class="uppercase profile-stat-title">{{ $offer->getOrderedQuantityOffline(false, $seller->id) . ' / ' . $offer->quantity  }}</div>
                                                                                                            </div>
                                                                                                        @endforeach
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/highcharts/js/highcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/highcharts/js/highcharts-3d.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/highcharts/js/highcharts-more.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            window.results = {!! json_encode($results) !!};
            window.sellers = {!! json_encode($sellers) !!};
        });
    </script>
    <script src="{{ asset('/backend/js/partner.js?v=1.2.0') }}" type="text/javascript"></script>
@endsection


