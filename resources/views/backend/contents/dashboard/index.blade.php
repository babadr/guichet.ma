@extends('layouts.backend')

@section('title')
    Admin Dashboard
@endsection

@section('smallTitle')
    Statistiques, commandes, événements et rapports
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12 col-xs-12 col-sm-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-bar-chart font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Statistiques et rapports</span>
                    </div>
                    <div class="actions">
                        <form id="form-filter" class="form-horizontal form-validate" action="" method="GET">
                            <input type="hidden" name="start_at" id="start_at" value="{{ $start_at }}">
                            <input type="hidden" name="end_at" id="end_at" value="{{ $end_at }}">
                        </form>
                        <a href="javascript:;" id="reload-stats"
                           class="btn btn-sm btn-circle red easy-pie-chart-reload">
                            <i class="fa fa-repeat"></i> Recharger </a>
                        <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body"
                             data-placement="bottom" data-original-title="Modifier la période">
                            <i class="icon-calendar"></i>&nbsp;
                            <span class="thin uppercase hidden-xs"></span>&nbsp;
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    {{--<div class="col-md-6">--}}
                    {{--<div id="dashboard_amount_amchart" class="CSSAnimationChart"></div>--}}
                    {{--</div>--}}
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-yellow-haze">
                                            <span>{{ $activeDeals }} <small>en ligne</small></span>
                                        </h3>
                                        <small>Nombre d'évènements</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-tag font-yellow-haze"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-purple">
                                            <span>{{ $totalOrder }} <small>validées</small></span>
                                        </h3>
                                        <small>Total des commandes</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-shopping-cart font-purple"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-blue">
                                            <span>{{ $totalUsers }} <small>inscriptions</small></span>
                                        </h3>
                                        <small>Total des utilisateurs</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-users font-blue"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span>{{ number_format($totalAmount, '2', ',', ' ') }} <small>DHS</small></span>
                                        </h3>
                                        <small>Total des ventes</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-bar-chart font-green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span>{{ number_format($totalAmountOnLine, '2', ',', ' ') }} <small>DHS</small></span>
                                        </h3>
                                        <small>Total des ventes en ligne</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-bar-chart font-green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span>{{ number_format($totalAmountOffLine, '2', ',', ' ') }} <small>DHS</small></span>
                                        </h3>
                                        <small>Total des ventes offline</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-bar-chart font-green"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span>{{ number_format($totalCommission, '2', ',', ' ') }} <small>DHS</small></span>
                                        </h3>
                                        <small>Total commission Guichet</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-money font-red"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span>{{ number_format($totalCommissionOffline, '2', ',', ' ') }} <small>DHS</small></span>
                                        </h3>
                                        <small>Total commission Guichet Offline</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-money font-red"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span>{{ number_format($totalCommissionOnline, '2', ',', ' ') }} <small>DHS</small></span>
                                        </h3>
                                        <small>Total commission Guichet en ligne</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-money font-red"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 bordered">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3>
                                            <span>{{ number_format($totalCommissionNET, '2', ',', ' ') }} <small>DHS</small></span>
                                        </h3>
                                        <small>Total commission Guichet NET</small>
                                    </div>
                                    <div class="icon">
                                        <i class="fa fa-money font-red"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-clock font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Aperçu de l'activité</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row" style="padding: 0 15px">
                        <div class="col-md-12">
                            <div class="mt-element-list">
                                <div class="mt-list-container list-default no-padding no-border">
                                    <ul>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <i class="icon-globe"></i>
                                            </div>
                                            <div class="list-datetime">{{-- Tracker::sessions(30)->count() --}}</div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase bold">
                                                    Visiteurs en ligne
                                                </h3>
                                                <p>Dans les 30 dernières minutes</p>
                                            </div>
                                        </li>
                                        <li class="mt-list-item">
                                            <div class="list-icon-container">
                                                <i class="fa fa-shopping-cart"></i>
                                            </div>
                                            <div class="list-datetime">{{ $currentOrders }}</div>
                                            <div class="list-item-content">
                                                <h3 class="uppercase bold">
                                                    Paniers actifs
                                                </h3>
                                                <p>Dans les 30 dernières minutes</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding: 0 15px; margin-top: 25px">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-clock"></i>
                                        <span class="caption-subject">Actuellement en attente</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="mt-element-list">
                                        <div class="mt-list-container list-simple no-padding no-border">
                                            <ul>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $pendingOrders }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Commandes en attente paiement
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $abandonedOrders }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Commandes abandonnées
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $pendingTickets }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Tickets en attente de validation
                                                        </h3>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding: 0 15px;">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-info"></i>
                                        <span class="caption-subject">Notifications</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="mt-element-list">
                                        <div class="mt-list-container list-simple no-padding no-border">
                                            <ul>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $totalMessages }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Nouveaux messages
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $validatedTickets }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Tickets validés
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $activeDeals }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Événements en ligne
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $disabledDeals }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Événements hors ligne
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $expiredDeals }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Événements expirés
                                                        </h3>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="padding: 0 15px;">
                        <div class="col-md-12">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-user"></i>
                                        <span class="caption-subject">Clients & Newsletters</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="mt-element-list">
                                        <div class="mt-list-container list-simple no-padding no-border">
                                            <ul>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $newUsers }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Nouveaux clients
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $newSubscriptions }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Nouveaux abonnements
                                                        </h3>
                                                    </div>
                                                </li>
                                                <li class="mt-list-item">
                                                    <div class="list-datetime"> {{ $totalSubscriptions }} </div>
                                                    <div class="list-item-content no-padding">
                                                        <h3>
                                                            Total des abonnés
                                                        </h3>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-line-chart font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Aperçu en chiffre</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable tabbable-tabdrop">
                        <ul id="tab-charts" class="nav nav-pills">
                            <li class="active">
                                <a href="#tab-chart-sales" data-toggle="tab">
                                    <i class="fa fa-money"></i> Ventes
                                </a>
                            </li>
                            <li>
                                <a href="#tab-chart-commission" data-toggle="tab">
                                    <i class="fa fa-database"></i> Commission Guichet
                                </a>
                            </li>
                            <li>
                                <a href="#tab-chart-orders" data-toggle="tab">
                                    <i class="fa fa-shopping-cart"></i> Commandes
                                </a>
                            </li>
                            <li>
                                <a href="#tab-chart-visitors" data-toggle="tab">
                                    <i class="fa fa-users"></i> Inscriptions
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-chart-sales">
                                <div id="highchart-sales" style="height:500px;"></div>
                            </div>
                            <div class="tab-pane" id="tab-chart-commission">
                                <div id="highchart-commission" style="height:500px;"></div>
                            </div>
                            <div class="tab-pane" id="tab-chart-orders">
                                <div id="highchart-orders" style="height:500px;"></div>
                            </div>
                            <div class="tab-pane" id="tab-chart-visitors">
                                <div id="highchart-visitors" style="height:500px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-bar-chart font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Événements et Ventes</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="tabbable tabbable-tabdrop">
                        <ul class="nav nav-pills">
                            <li class="active">
                                <a href="#tab-orders" data-toggle="tab">
                                    <i class="icon-fire"></i> Commandes récentes
                                </a>
                            </li>
                            <li>
                                <a href="#tab-deals" data-toggle="tab">
                                    <i class="icon-trophy"></i> Meilleures Ventes
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-orders">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nom du client</th>
                                            <th class="text-center">Événements</th>
                                            <th class="text-center">Total payé</th>
                                            <th class="text-center">Date</th>
                                            <th class="text-center">État</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($lastOrders as $deal)
                                            <tr>
                                                <td>
                                                    @if($deal->user)
                                                        @if (Entrust::can(['backend.user.update']))
                                                            <a href="{{ route('backend.user.edit', $deal->user_id) }}">{{ $deal->user->full_name }}</a>
                                                        @else
                                                            {{ $deal->user->full_name }}
                                                        @endif
                                                    @else
                                                        ---
                                                    @endif
                                                </td>
                                                <td class="text-center"> {{ $deal->lines->sum('quantity') }}</td>
                                                <td class="text-center">
                                                    @if($deal->offered)
                                                        Commande offerte
                                                    @else
                                                        {{ Tools::displayPrice($deal->total_paid) }}
                                                    @endif
                                                </td>
                                                <td class="text-center"> {{ date('d/m/Y H:i', strtotime($deal->created_at)) }}</td>
                                                <td class="text-center">
                                                    @if($deal->status == \App\Models\Store\Order::STATUS_WAITING)
                                                        <label class="label label-info label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @elseif($deal->status == \App\Models\Store\Order::STATUS_ACCEPTED)
                                                        <label class="label label-success label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @elseif($deal->status == \App\Models\Store\Order::STATUS_VALIDATED)
                                                        <label class="label label-primary label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @elseif($deal->status == \App\Models\Store\Order::STATUS_CANCELED)
                                                        <label class="label label-warning label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @elseif($deal->status == \App\Models\Store\Order::STATUS_CAPTURED)
                                                        <label class="label label-success label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @elseif($deal->status == \App\Models\Store\Order::STATUS_EXPIRED)
                                                        <label class="label label-warning label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @elseif($deal->status == \App\Models\Store\Order::STATUS_REFUSED)
                                                        <label class="label label-danger label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @elseif($deal->status == \App\Models\Store\Order::STATUS_RESERVED)
                                                        <label class="label label-default label-sm">
                                                            {{ trans(config('enums.order_status')[$deal->status]) }}
                                                        </label>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (Entrust::can(['backend.order.read']))
                                                        <a href="{{ route('backend.order.show', $deal->id) }}"
                                                           class="btn btn-outline blue btn-xs"><i
                                                                    class="fa fa-search"></i> </a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab-deals">
                                <div class="table-scrollable">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Image</th>
                                            <th class="text-center">Événement</th>
                                            <th class="text-center">Catégorie</th>
                                            <th class="text-center">Total vendu</th>
                                            <th class="text-center">Ventes</th>
                                            <th class="text-center">Comission</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($bestSellers as $order)
                                            @foreach($deals as $deal)
                                                @if ($deal->id == $order->deal_id)
                                                    @php $dealAmount = $deal->getTotalOrderedAmount(false) @endphp
                                                    @php $dealCommission = ($dealAmount * $deal->commission) / 100 @endphp
                                                    @php $dealAmountOffline = $deal->getTotalOrderedAmountOffline(false) @endphp
                                                    @php $dealCommissionOffline = (($dealAmountOffline * $deal->offline_commission) / 100) + $deal->getTotalOfferedCommission(false) @endphp
                                                    <tr>
                                                        <td class="text-center"><img
                                                                    src="{{ asset_cdn($deal->miniature) }}?w=200&h=150&fit=clip&auto=format,compress&q=80"
                                                                    alt=""
                                                                    height="40px"></td>
                                                        <td style="vertical-align: middle">{{ str_limit($deal->title, 35) }}</td>
                                                        <td class="text-center"
                                                            style="vertical-align: middle"> {{ $deal->category->title }}</td>
                                                        <td class="text-center"
                                                            style="vertical-align: middle">{{ $order->total }}</td>
                                                        <td class="text-center"
                                                            style="vertical-align: middle">{{ Tools::displayPrice(($dealAmount + $dealAmountOffline) - ($dealCommission + $dealCommissionOffline)) }}</td>
                                                        <td class="text-center"
                                                            style="vertical-align: middle">{{ Tools::displayPrice($dealCommission + $dealCommissionOffline) }}</td>
                                                    </tr>
                                                    @break
                                                @endif
                                            @endforeach
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/highcharts/js/highcharts.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/highcharts/js/highcharts-3d.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/highcharts/js/highcharts-more.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/dashboard.js?v=1.2.0') }}" type="text/javascript"></script>
@endsection