@if($parameter == 'title')
    <h3 class="page-header">@Lang('setting.' . $parameterKey)</h3>
@else
    @php $field_key = is_array($parameter) ? $parameterKey : $parameter @endphp
    @php $columns = isset($parameter['columns']) ? $parameter['columns'] : 12 @endphp
    @php $label_columns = isset($parameter['label_columns']) ? $parameter['label_columns'] : 4 @endphp
    @php $form_columns = isset($parameter['form_columns']) ? $parameter['form_columns'] : 6 @endphp
    <div class="form-group {{ $errors->has($parameterKey) ? ' has-error' : '' }}">
        <label class="control-label col-md-{{$label_columns}}"
               for="{{$field_key}}">@Lang('setting.'. $field_key)</label>
        <div class="col-md-{{$form_columns}} errors-container">
            @if(isset($parameter['type']))
                @if($parameter['type'] == 'textarea')
                    <textarea class="form-control"
                              placeholder="{{isset($parameter['placeholder']) ? $parameter['placeholder'] : trans('setting.default_placeholder')}}"
                              name="{{$field_key}}" id="{{$field_key}}">{{isset($settingsValues[$field_key]) ? $settingsValues[$field_key] : ''}}</textarea>
                @elseif($parameter['type'] == 'select')
                    <select name="{{$field_key}}" id="{{$field_key}}"
                            class="form-control select2">
                        @foreach($parameter['list'] as $lKey => $lVal)
                            <option value="{{$lKey}}"
                                    @if(isset($settingsValues[$field_key]) && $settingsValues[$field_key] == $lKey) selected @endif
                            >{{$lVal}}</option>
                        @endforeach
                    </select>
                @elseif($parameter['type'] == 'editor')
                    <textarea class="form-control summernote"
                              placeholder="{{isset($parameter['placeholder']) ? $parameter['placeholder'] : trans('setting.default_placeholder')}}"
                              name="{{$field_key}}" id="{{$field_key}}">{{isset($settingsValues[$field_key]) ? $settingsValues[$field_key] : ''}}
                    </textarea>
                @elseif($parameter['type'] == 'boolean')
                    <div>
                        <input type="checkbox" class="form-control make-switch" name="{{$field_key}}"
                               id="{{$field_key}}" data-on-text="Oui" data-off-text="Non"
                               data-on-color="success" data-off-color="warning"
                               data-size="normal" @if(empty($settingsValues[$field_key])) checked @endif />
                    </div>
                @elseif($parameter['type'] == 'image')
                    <div class="fileinput {{ !empty($settingsValues[$field_key]) ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&text=aucune+image" alt=""/>
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                            <img src="{{ !empty($settingsValues[$field_key] ) ? asset_cdn($settingsValues[$field_key]) . '?w=200&h=150&fit=clip&auto=format,compress&q=80' : '' }}" alt=""/>
                        </div>
                        <div>
                                                  <span class="btn blue btn-file">
                                                      <span class="fileinput-new"> @Lang('app.select_image') </span>
                                                      <span class="fileinput-exists"> @Lang('app.change') </span>
                                                      <input type="file" name="{{$field_key}}" data-rule-accept="image/*"
                                                             data-msg-accept="Les types de fichiers autorisés sont jpg, jpeg, png et gif" accept=".jpeg,.png,.jpg,.gif">
                                                  </span>
                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> @Lang('app.remove') </a>
                        </div>
                    </div>
                @elseif($parameter['type'] == 'file')
                    <div class="fileinput {{ !empty($settingsValues[$field_key]) ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                        <div class="input-group input-large">
                            <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                <span class="fileinput-filename"> </span>
                            </div>
                            <span class="input-group-addon btn default btn-file">
                                                                    <span class="fileinput-new"> Select file </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="{{$field_key}}"> </span>
                            <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                    </div>
                @endif
            @else
                <input type="text" name="{{$field_key}}" id="{{$field_key}}"
                       class="form-control"
                       value="{{ isset($settingsValues[$field_key]) ?  $settingsValues[$field_key] : '' }}"
                       placeholder="{{isset($parameter['placeholder']) ? $parameter['placeholder'] : trans('setting.default_placeholder')}}"/>

            @endif
            @if(!empty($parameter['suffix']))
                <div class="input-group-addon">{!! $parameter['suffix'] !!}</div>
            @endif
        </div>
        @if ($errors->has($parameterKey))
            <small id="{{$parameterKey}}-error"
                   class="has-error help-block help-block-error">{{ $errors->first($parameterKey) }}</small>
        @endif
        @if(!empty($parameter['help']))
            <div class="help-block col-md-offset-4 col-md-6"> @Lang('setting.'. $parameter['help']) </div>
        @endif
    </div>
@endif