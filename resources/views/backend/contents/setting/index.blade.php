@extends('layouts.backend')

@section('title')
    @Lang('setting.app_settings')
@endsection

@section('content')
    <form action="{{ route('backend.setting.update') }}" method="POST" id="app-form"
          class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-settings font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> @Lang('setting.settings')</span>
                </div>
                <div class="actions">
                    @if (Entrust::can('backend.setting.update'))
                        <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save_changes')">
                            <i class="fa fa-check"></i>
                            <span class="hidden-480">@Lang('app.save_changes')</span>
                        </button>
                    @endif
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line settings">
                    <ul class="nav nav-tabs ">
                        @php $i=0 @endphp
                        @foreach($parameters as $key => $value)
                            <li role="presentation" @if($i++ == 0)class="active"@endif>
                                <a href="#tab_{{$key}}" aria-controls="tab_{{$key}}" role="tab" data-toggle="tab">@Lang('setting.' . $key)</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @php $i=0 @endphp
                        @foreach($parameters as $key => $value)
                            <div class="tab-pane @if($i++ == 0) active @endif" id="tab_{{$key}}">
                                @foreach($value as $parameterKey => $parameter)
                                    @include('backend.contents.setting.field')
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection