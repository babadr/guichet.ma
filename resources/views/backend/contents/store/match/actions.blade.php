@if (Entrust::hasRole(['superadmin','admin']))
    <a href="{{ route('backend.match.profiteers.index', ['event_id' => $model->id]) }}"
       title="Voir les bénéficiaires" class="btn btn-outline blue btn-xs">
        <i class="fa fa-eye"></i>
    </a>
@endif