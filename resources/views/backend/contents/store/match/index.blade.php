@extends('layouts.backend')

@section('title')
    Gestion des Matchs
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-soccer-ball-o font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">Liste des matchs</span>
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
