@if($status == \App\Models\Store\Order::STATUS_ACCEPTED || $status == \App\Models\Store\Order::STATUS_CAPTURED)
    <a title="Télécharger les tickets" href="{{ route('backend.ticket.download', ['id' => $order_id, 'ticket_id' => $ticket_id]) }}"
       class="btn btn-outline green btn-xs">
        <i class="fa fa-download"></i>
    </a>
@endif
{{--@if (Entrust::can(['backend.order.update']))--}}
    {{--<a href="{{ route('backend.order.profiteers.edit', ['id' => $order_id]) }}"--}}
       {{--title="Modifier le bénéficiaire" class="btn btn-outline yellow btn-xs">--}}
        {{--<i class="fa fa-user"></i>--}}
    {{--</a>--}}
{{--@endif--}}

