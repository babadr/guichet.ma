@if( $model->{$field} )
    <span class="label label-sm label-success"> Validé </span>
@else
    <span class="label label-sm label-warning"> En attente de validation </span>
@endif
