@extends('layouts.backend')

@section('title')
    @Lang('ticket.manage_tickets', ['name' => $deal->title])
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-tags font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('ticket.list')</span>
            </div>
            <div class="actions">
                @if(auth()->user()->hasRole('validator'))
                    <a href="{{ route('backend.validator.index') }}"
                       class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('ticket.back_to_list')</span>
                    </a>
                @else
                    <a href="{{ empty($partner_id) ? route('backend.index') : route('backend.provider.statistics', ['partner_id' => $partner_id]) }}"
                       class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('ticket.back_to_list')</span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @if(auth()->user()->hasRole('validator'))
                <div class="row" style="border-bottom: 1px solid #c2cad8; margin-bottom: 25px">
                    <form id="form-validate-ticket" class="form-horizontal form-validate" accept-charset="UTF-8"
                          novalidate="novalidate" enctype="multipart/form-data">
                        <div class="form-group">
                            <label style="text-align: left; margin-left: 15px" class="col-md-1 control-label" for="ticket-token">N° de ticket</label>
                            <div class="col-md-2 error-container">
                                <input id="ticket-token" name="ticket-token" value="" required autofocus="autofocus"
                                       class="form-control">
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-check-circle"></i>
                                    <span class="hidden-480">Valider</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            @endif
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
