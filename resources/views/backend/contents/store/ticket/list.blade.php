@extends('layouts.backend')

@section('title')
    Gestion des tickets
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-ticket font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('ticket.list')</span>
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
