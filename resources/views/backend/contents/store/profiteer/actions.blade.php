@if (Entrust::hasRole(['superadmin','admin']))
    <a href="{{ route('backend.match.profiteers.edit', ['event_id' => $model->line->offer->deal_id, 'id' => $model->id]) }}"
       title="Modifier" class="btn btn-outline yellow btn-xs">
        <i class="fa fa-edit"></i>
    </a>
    @if($model->line->order->status == \App\Models\Store\Order::STATUS_ACCEPTED || $model->line->order->status == \App\Models\Store\Order::STATUS_CAPTURED)
        <a title="Télécharger les tickets" href="{{ route('backend.ticket.download', ['id' => $model->line->order->id]) }}"
           class="btn btn-outline green btn-xs">
            <i class="fa fa-download"></i>
        </a>
    @endif
@endif