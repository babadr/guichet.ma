@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-soccer-ball-o font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('profiteer.edit_details', ['id' => $model->id]) : trans('profiteer.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.match.profiteers.index', ['event_id' => $event_id]) }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-group {{ $errors->has('is_minor') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="is_minor">
                        @Lang('profiteer.is_minor')
                    </label>
                    <div class="col-md-7 error-container">
                        <input type="checkbox" class="form-control make-switch" name="is_minor"
                               id="is_minor" data-on-text="Oui" data-off-text="Non"
                               data-on-color="success" data-off-color="warning"
                               data-size="normal" {{ empty($model->id) || $model->is_minor ? 'checked' : '' }} />
                        @if ($errors->has('is_minor'))
                            <small id="is_minor-error" class="has-error help-block help-block-error">
                                {{ $errors->first('is_minor') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('full_name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="full_name">
                        @Lang('profiteer.full_name') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <input type="text" name="full_name" id="full_name"
                               value="{{ old('full_name') ? old('full_name') : ($model->full_name ? $model->full_name : '') }}"
                               required class="form-control"/>
                        @if ($errors->has('full_name'))
                            <small id="full_name-error" class="has-error help-block help-block-error">
                                {{ $errors->first('full_name') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('cin') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="cin">
                        @Lang('profiteer.cin') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <input type="text" name="cin" id="cin"
                               value="{{ old('cin') ? old('cin') : ($model->cin ? $model->cin : '') }}"
                               required class="form-control"/>
                        @if ($errors->has('cin'))
                            <small id="cin-error" class="has-error help-block help-block-error">
                                {{ $errors->first('cin') }}
                            </small>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
@endsection