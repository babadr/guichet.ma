@extends('layouts.backend')

@section('title')
    @Lang('recipient.manage_recipients')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-soccer-ball-o font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('recipient.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::hasRole(['superadmin', 'admin']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.recipient.export') }}">
                        <i class="fa fa-file-excel-o"></i>
                        <span class="hidden-xs"> @Lang('recipient.export') </span>
                    </a>
                    <a href="{{ route('backend.provider.export_subscriptions', ['id' => 143]) }}"
                       class="btn btn-outline red btn-circle">
                        <i class="fa fa-file-excel-o"></i> Exporter les achats sans abonnement
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
