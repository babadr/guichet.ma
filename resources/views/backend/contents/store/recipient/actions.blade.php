@if (!$status)
    <a href="{{ route('backend.ticket.update', ['token' => $token]) }}" alt="Valider"
       title="Valider" class="btn btn-outline btn-success btn-xs validate-ticket">
        <i class="fa fa-check"></i> Valider
    </a>
@endif

