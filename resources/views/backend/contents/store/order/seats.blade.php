<div class="portlet grey-cascade box {{ str_replace('seat.', '', config('enums.seating_plans')[$deal->seating_plan_id]) }}">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-map-signs"></i>Plan de salle - {{ trans(config('enums.seating_plans')[$deal->seating_plan_id]) }}
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <div class="seat-container">
                    <div id="seat-map">
                        <div id="legend"></div>
                        <div class="front-indicator">Scène</div>
                        @if($deal->seating_plan_id == 1 || $deal->seating_plan_id == 4 || $deal->seating_plan_id == 5 || $deal->seating_plan_id == 8)
                            <div class="governance">Régie</div>
                        @endif
                        @if($deal->seating_plan_id == 2)
                            <div class="porte_1">Porte 1 Orchestre</div>
                            <div class="porte_2">Porte 2 Orchestre</div>
                        @endif
                        @if($deal->seating_plan_id == 6 || $deal->seating_plan_id == 7)
                            <div class="porte_1">Orchestre</div>
                            <div class="porte_2">Balcon</div>
                        @endif
                    </div>
                    <div class="booking-details">
                        @if($deal->seating_plan_id == 8 || $deal->seating_plan_id == 10)
                            <div style="color: red; margin: 10px 0; font-weight: bold; font-size: 12px">* Placements libres en Balcon</div>
                        @endif
                        <h4> Places sélectionnées (<span id="counter">0</span>) :</h4>
                        <ul id="selected-seats"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
