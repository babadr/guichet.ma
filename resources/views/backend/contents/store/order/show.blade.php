@extends('layouts.backend')

@section('title')
    @Lang('order.manage_orders')
@endsection

@section('content')
    <form action="{{ route('backend.order.update', $order->id) }}" method="post" id="app-form" class="form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-shopping-cart font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ trans('order.show', ['reference' => $order->reference]) }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.order.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    @if (Entrust::can(['backend.order.update']) && !in_array($order->status, [2, 3, 5, 9]))
                        <button type="submit" name="saveandcontinue" class="btn btn-warning"
                                title="@Lang('app.save_continue')">
                            <i class="fa fa-check-circle"></i>
                            <span class="hidden-480">@Lang('app.save_continue')</span>
                        </button>
                    @endif
                    @if($order->status == \App\Models\Store\Order::STATUS_ACCEPTED || $order->status == \App\Models\Store\Order::STATUS_CAPTURED)
                        <a title="Télécharger les tickets"
                           href="{{ route('backend.ticket.download', ['id' => $order->id]) }}"
                           class="btn btn-danger btn-sm ripple-effect">
                            <i class="fa fa-download"></i> Télécharger les tickets
                        </a>
                        @if (Entrust::hasRole(['superadmin', 'admin']))
                            <a title="Télécharger les tickets A4"
                               href="{{ route('backend.ticket.download.a4', ['id' => $order->id]) }}"
                               class="btn btn-success btn-sm ripple-effect">
                                <i class="fa fa-download"></i> Télécharger les tickets A4
                            </a>
                        @endif
                    @endif
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet green box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Détails de la commande
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row static-info">
                                    <div class="col-md-5 name"> Commande #Réf :</div>
                                    <div class="col-md-7 value"> {{ $order->reference }}  </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> Date de la commande :</div>
                                    <div class="col-md-7 value"> {{ $order->created_at->format('d-m-Y H:i') }} </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> Etat de la commande :</div>
                                    <div class="col-md-5 value">
                                        <select name="status" id="status" required class="form-control select2">
                                            @foreach ($status as $key => $value)
                                                <option {{ $order->status == $key ? 'selected' : '' }} value="{{ $key }}">{{ trans($value) }}</option>
                                            @endforeach
                                        </select>
                                        {{--@if($order->status == \App\Models\Store\Order::STATUS_WAITING)--}}
                                        {{--<label class="label label-info label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@elseif($order->status == \App\Models\Store\Order::STATUS_ACCEPTED)--}}
                                        {{--<label class="label label-success label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@elseif($order->status == \App\Models\Store\Order::STATUS_VALIDATED)--}}
                                        {{--<label class="label label-primary label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@elseif($order->status == \App\Models\Store\Order::STATUS_CANCELED)--}}
                                        {{--<label class="label label-warning label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@elseif($order->status == \App\Models\Store\Order::STATUS_CAPTURED)--}}
                                        {{--<label class="label label-success label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@elseif($order->status == \App\Models\Store\Order::STATUS_EXPIRED)--}}
                                        {{--<label class="label label-danger label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@elseif($order->status == \App\Models\Store\Order::STATUS_REFUSED)--}}
                                        {{--<label class="label label-danger label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@elseif($order->status == \App\Models\Store\Order::STATUS_RESERVED)--}}
                                        {{--<label class="label label-default label-sm">--}}
                                        {{--{{ trans(config('enums.order_status')[$order->status]) }}--}}
                                        {{--</label>--}}
                                        {{--@endif--}}
                                    </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> Total :</div>
                                    <div class="col-md-7 value">  {{ $order->offered ? 'Commande offerte' : Tools::displayPrice($order->total_paid, 2) }} </div>
                                </div>
                                @if($order->offered && $order->offered_by)
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Invitation offerte par :</div>
                                        <div class="col-md-7 value"> {{ $order->offered_by }} </div>
                                    </div>
                                @endif
                                <div class="row static-info">
                                    <div class="col-md-5 name"> Commentaire client :</div>
                                    <div class="col-md-7 value"> {!! $order->comment ? $order->comment : 'Pas de message client' !!} </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> Mode de paiement :</div>
                                    <div class="col-md-5 value">
                                        <select name="payment_method" id="payment_method" required
                                                class="form-control select2">
                                            @foreach ($payments as $key => $payment)
                                                <option {{ $order->payment_method == $key ? 'selected' : '' }} value="{{ $key }}">{{ trans($payment) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if (in_array($order->status, [2, 3, 5]) && $order->payment_method == 1 && !empty($order->payment_transaction))
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Transaction ID :</div>
                                        <div class="col-md-7 value"> {{ $order->payment_transaction }} </div>
                                    </div>

                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Transaction Date :</div>
                                        <div class="col-md-7 value"> {{ $order->payment_transaction_date->format('d-m-Y H:i') }} </div>
                                    </div>
                                @endif
                                @if (in_array($order->status, [2, 3, 5]) && in_array($order->payment_method, [2, 3, 4]) && !empty($order->payment_transaction_date))
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Date règlement :</div>
                                        <div class="col-md-7 value"> {{ $order->payment_transaction_date->format('d-m-Y H:i') }} </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        @if ($order->user)
                            <div class="portlet blue-hoki box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i> Informations client
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Nom du client :</div>
                                        <div class="col-md-7 value"> {{ $order->full_name }} </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Email :</div>
                                        <div class="col-md-7 value"> {{ $order->user->email }} </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Adresse :</div>
                                        <div class="col-md-7 value"> {{ $order->user->address }} {{ $order->user->city }} </div>
                                    </div>
                                    <div class="row static-info">
                                        <div class="col-md-5 name"> Numéro de téléphone :</div>
                                        <div class="col-md-7 value"> {{ $order->user->phone }} </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet grey-cascade box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-cogs"></i>Détails
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th> @lang('cart.product') </th>
                                            <th> Prix unitaire</th>
                                            <th> @lang('cart.quantity') </th>
                                            <th> @lang('cart.total') </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($order->lines as $line)
                                            <tr>
                                                <td class="details" style="width: 50%">
                                                    <div class="clearfix">
                                                        <span class="title"> {{ $line->product_name }} </span> <br/>
                                                        <small>{{ $line->offer_name }}</small>
                                                        @if($line->seats)
                                                            <br><span>Places : {{ $line->seats }}</span>
                                                        @endif
                                                    </div>
                                                </td>
                                                <td class="unit-price">
                                                    {{ Tools::displayPrice($line->product_price, 2) }}
                                                </td>
                                                <td class="qty text-center">
                                                    <strong>x{{ $line->quantity }}</strong>
                                                </td>
                                                <td class="total-price">
                                                    {{ Tools::displayPrice($line->total_price, 2) }}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <div class="well">
                            <div class="row static-info align-reverse">
                                <div class="col-md-8 name"> @lang('cart.sub_total'):</div>
                                <div class="col-md-3 value"> {{ $order->offered ? 'Commande offerte' : Tools::displayPrice($order->total_paid, 2)}} </div>
                            </div>
                            <div class="row static-info align-reverse">
                                <div class="col-md-8 name"> @lang('cart.global_amount'):</div>
                                <div class="col-md-3 value"> {{ $order->offered ? 'Commande offerte' : Tools::displayPrice($order->total_paid, 2) }} </div>
                            </div>
                        </div>
                    </div>
                </div>

                @if (in_array($order->status, [2, 3, 5]))
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="portlet yellow box">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-cogs"></i> Tickets
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th> Numéro</th>
                                                <th> @lang('cart.product') </th>
                                                <th> Prix</th>
                                                <th> Statut</th>
                                                <th> Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($order->lines as $line)
                                                @foreach($line->tickets as $ticket)
                                                    <tr>
                                                        <td> {{ $ticket->number }} </td>
                                                        <td style="width: 50%">
                                                            {{ $line->product_name }} <br/>
                                                            <small>{{ $line->offer_name }}</small>
                                                        </td>
                                                        <td> {{ Tools::displayPrice($line->product_price, 2) }} </td>
                                                        <td>
                                                            @if( $ticket->status )
                                                                <span class="label label-sm label-success"> Validé </span>
                                                            @else
                                                                <span class="label label-sm label-warning"> En attente de validation </span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{--@if( auth()->user()->hasRole(['superadmin', 'admin']) && ($order->seller_id || empty($order->user_id)) && !$ticket->status && $line->quantity > 1)--}}
                                                            @if( auth()->user()->hasRole(['superadmin', 'admin']) && !$ticket->status && $line->quantity > 1)
                                                                <a href="{{ route('backend.ticket.delete', ['id' => $ticket->id]) }}"
                                                                   alt="@Lang('app.Delete')"
                                                                   title="@Lang('app.Delete')"
                                                                   class="btn btn-outline red btn-xs delete-ticket">
                                                                    <i class="fa fa-trash"></i> Supprimer
                                                                </a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/order.detail.js') }}" type="text/javascript"></script>
@endsection
