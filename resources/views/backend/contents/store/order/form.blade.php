@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-shopping-cart font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('order.edit_details', ['id' => $model->id]) : trans('order.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.order.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div>
                    <div class="tab-content">
                        <div class="tab-pane active">
                            {{--<h3 class="page-header">Informations client</h3>--}}
                            {{--<input type="hidden" name="password" id="password" value="PASS2018">--}}
                            {{--<input type="hidden" name="password_confirmation" id="password-confirm" value="PASS2018">--}}
                            {{--<div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="last_name">--}}
                                    {{--@Lang('user.last_name')--}}
                                {{--</label>--}}
                                {{--<div class="col-md-3 error-container">--}}
                                    {{--<input type="text" name="last_name" id="last_name" class="form-control"--}}
                                           {{--value="{{ old('last_name') ? old('last_name') : ($model->user ? $model->user->last_name : '') }}" />--}}
                                    {{--@if ($errors->has('last_name'))--}}
                                        {{--<small id="last_name-error" class="has-error help-block help-block-error">--}}
                                            {{--{{ $errors->first('last_name') }}--}}
                                        {{--</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="first_name">--}}
                                    {{--@Lang('user.first_name')--}}
                                {{--</label>--}}
                                {{--<div class="col-md-3 error-container">--}}
                                    {{--<input type="text" name="first_name" id="first_name" class="form-control"--}}
                                           {{--value="{{ old('first_name') ? old('first_name') : ($model->user ? $model->user->first_name : '') }}" />--}}
                                    {{--@if ($errors->has('first_name'))--}}
                                        {{--<small id="first_name-error" class="has-error help-block help-block-error">--}}
                                            {{--{{ $errors->first('first_name') }}--}}
                                        {{--</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="email">--}}
                                    {{--@Lang('seller.email')  <span class="required"> * </span>--}}
                                {{--</label>--}}
                                {{--<div class="col-md-3 error-container">--}}
                                    {{--<input type="text" name="email" id="email" class="form-control" required data-rule-email="true" data-msg-email="Veuillez saisir une adresse email valide."--}}
                                           {{--value="{{ old('email') ? old('email') : ($model->user ? $model->user->email : '') }}" />--}}
                                    {{--@if ($errors->has('email'))--}}
                                        {{--<small id="email-error" class="has-error help-block help-block-error">--}}
                                            {{--{{ $errors->first('email') }}--}}
                                        {{--</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="phone">--}}
                                    {{--@Lang('provider.phone')--}}
                                {{--</label>--}}
                                {{--<div class="col-md-3 error-container">--}}
                                    {{--<input type="text" name="phone" data-rule-phone="true" data-msg-phone="Veuillez saisir un numéro de téléphone valide." id="phone" class="form-control"--}}
                                           {{--value="{{ old('phone') ? old('phone') : ($model->user ? $model->user->phone : '') }}" />--}}
                                    {{--@if ($errors->has('phone'))--}}
                                        {{--<small id="phone-error" class="has-error help-block help-block-error">--}}
                                            {{--{{ $errors->first('phone') }}--}}
                                        {{--</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">--}}
                                {{--<label class="col-md-3 control-label" for="city">--}}
                                    {{--@Lang('user.city')--}}
                                {{--</label>--}}
                                {{--<div class="col-md-3 error-container">--}}
                                    {{--<input type="text" name="city" id="city" class="form-control"--}}
                                           {{--value="{{ old('city') ? old('city') : ($model->user ? $model->user->city : '') }}" />--}}
                                    {{--@if ($errors->has('city'))--}}
                                        {{--<small id="city-error" class="has-error help-block help-block-error">--}}
                                            {{--{{ $errors->first('city') }}--}}
                                        {{--</small>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <h3 class="page-header">Informations commande</h3>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="active">
                                    Commande offerte ?
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control make-switch" name="offered"
                                           id="offered" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" />
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('offered_by') ? ' has-error' : '' }}" id="offered-by">
                                <label class="col-md-3 control-label" for="offered_by">
                                    @Lang('order.offered_by')
                                </label>
                                <div class="col-md-3 error-container">
                                    <input type="text" name="offered_by" id="offered_by" maxlength="30"
                                           value="{{ old('offered_by') ? old('offered_by') : ($model->offered_by ? $model->offered_by : '') }}"
                                           class="form-control"/>
                                    @if ($errors->has('offered_by'))
                                        <small id="offered_by-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('offered_by') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="city">
                                    Veuillez séléctionner un produit et cliquer sur ajouter
                                </label>
                                <div class="col-md-5 error-container">
                                    <select name="deal_id" id="deal_id" class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($deals as $deal)
                                            <option {{ (old('deal_id') && old('deal_id') == $deal->id) ? 'selected' : '' }}
                                                    value="{{ $deal->id }}">
                                                {{ $deal->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-2 error-container">
                                    <button type="button" class="btn btn-warning" id="add-deal">
                                        <i class="fa fa-plus"></i>
                                        <span class="hidden-480">Ajouter</span>
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="cart-container" style="margin-top: 20px">

                                </div>
                            </div>
                            <div class="row" id="total-order" style="display: none">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <div class="well">
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name"> Sous-total:</div>
                                            <div class="col-md-3 value sub-total"> 0,00 DH </div>
                                        </div>
                                        <div class="row static-info align-reverse">
                                            <div class="col-md-8 name"> Montant global:</div>
                                            <div class="col-md-3 value sub-total"> 0,00 DH </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/frontend/js/jquery.seat-charts.js') }}"></script>
    <script src="{{ asset('/backend/js/seat.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/order.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            window.reservedSeats = [];
            window.rows = [];
            window.mapCharts = [];
            window.seats = [];
            window.items = [];
            window.selectedSeats = [];
        });
    </script>
@endsection