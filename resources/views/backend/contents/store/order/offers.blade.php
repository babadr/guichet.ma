<div class="portlet grey-cascade box">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-tag"></i>Détails de l'événement - {{ $deal->title }}
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-hover table-bordered table-striped">
                <thead>
                <tr>
                    <th> </th>
                    <th class="text-center"> Offre </th>
                    <th class="text-center"> Prix unitaire</th>
                    <th class="text-center"> Quantité </th>
                    <th class="text-center"> Total </th>
                </tr>
                </thead>
                <tbody>
                @foreach($offers as $offer)
                    <tr>
                        <td class="image text-center" width="12%">
                            <img src="{{ $deal->miniature }}" height="80px" alt="">
                        </td>
                        <td class="details" style="width: 50%">
                            <div class="clearfix">
                                <span class="title"> {{ $offer->title }} </span>
                                @if ($offer->reservation_price)
                                    <br><small>Prix : {{ number_format($offer->price, 0, ',', ' ') }} DH
                                        (Paiement de {{ number_format($offer->reservation_price, 0, ',', ' ') }} DH sur le site)</small>
                                @endif
                            </div>
                        </td>
                        <td class="unit-price text-center">
                            @if ($offer->reservation_price)
                                {{ number_format($offer->reservation_price, 0, ',', ' ') }} DH
                            @else
                                {{ number_format($offer->price, 0, ',', ' ') }} DH
                            @endif
                        </td>
                        <td class="qty text-center" style="width: 7%">
                            <input type="text" readonly value="0" data-price="{{ $offer->reservation_price ? $offer->reservation_price : $offer->price }}" name="quantity[{{ $offer->id  }}]" id="quantity-wanted-{{ $offer->id  }}"
                                   class="quantity-wanted">
                            <div class="cart-quantity-button clearfix">
                                <a rel="nofollow" data-price="{{ $offer->reservation_price ? $offer->reservation_price : $offer->price }}" data-id-offer="{{ $offer->id }}"
                                   class="cart-quantity-down btn btn-default button-minus"
                                   href="#"
                                   title="Soustraire"> <span><i class="fa fa-minus"></i></span>
                                </a>
                                <a rel="nofollow" data-price="{{ $offer->reservation_price ? $offer->reservation_price : $offer->price }}" data-id-offer="{{ $offer->id  }}"
                                   class="cart-quantity-up btn btn-default button-plus"
                                   href="#"
                                   title="Ajouter"><span><i class="fa fa-plus"></i></span>
                                </a>
                            </div>
                        </td>
                        <td class="total-price text-center" width="15%" id="total-price-{{ $offer->id  }}">
                            0 DH
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
