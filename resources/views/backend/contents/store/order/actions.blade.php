@if (Entrust::can(['backend.order.read']))
    <a href="{{ route('backend.order.show', ['id' => $order->id]) }}" alt="@Lang('order.detail')"
       title="@Lang('order.detail')" class="btn btn-outline blue btn-xs">
        <i class="fa fa-eye"></i>
    </a>
    @if($order->status == \App\Models\Store\Order::STATUS_ACCEPTED || $order->status == \App\Models\Store\Order::STATUS_CAPTURED)
        <a title="Télécharger les tickets" href="{{ route('backend.ticket.download', ['id' => $order->id]) }}"
           class="btn btn-outline green btn-xs">
            <i class="fa fa-download"></i>
        </a>
    @endif
@endif
{{--@if (Entrust::can(['backend.order.delete']) && !empty($order->seller_id))--}}
@if (Entrust::can(['backend.order.delete']))
    <a href="{{ route('backend.order.delete', ['id' => $order->id]) }}"
       title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-element">
        <i class="fa fa-trash"></i>
    </a>
@endif
@if (Entrust::can(['backend.order.update']))
    <a href="{{ route('backend.order.profiteers.edit', ['id' => $order->id]) }}"
       title="Modifier les bénéficiaires" class="btn btn-outline yellow btn-xs">
        <i class="fa fa-users"></i>
    </a>
@endif
