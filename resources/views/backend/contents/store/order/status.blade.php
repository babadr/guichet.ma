@if($model->{$field} == \App\Models\Store\Order::STATUS_WAITING)
    <label class="label label-info label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_ACCEPTED)
    <label class="label label-success label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_VALIDATED)
    <label class="label label-primary label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_CANCELED)
    <label class="label label-warning label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_CAPTURED)
    <label class="label label-success label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_EXPIRED)
    <label class="label label-warning label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_REFUSED)
    <label class="label label-danger label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_RESERVED)
    <label class="label label-default label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@elseif($model->{$field} == \App\Models\Store\Order::STATUS_DEFRAUDED)
    <label class="label label-danger label-sm">
        {{ trans(config('enums.order_status')[$model->{$field}]) }}
    </label>
@endif
