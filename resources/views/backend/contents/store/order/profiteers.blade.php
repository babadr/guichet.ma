@extends('layouts.backend')

@section('title')
    Bénéficiaires de la commande :  #Ref. {{ $order->reference }}
@endsection

@section('content')
    <form action="{{ route('backend.order.profiteers.edit', ['id' => $order->id]) }}" method="POST" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-shopping-cart font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase">Bénéficiaires de la commande :  #Ref. {{ $order->reference }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.order.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div>
                    <div class="tab-content">
                        <div class="tab-pane active">
                            @php $j = 1 @endphp
                            @foreach($lines as $id => $line)
                                @php $isFootball = Tools::checkFootball($line['offer_id']) @endphp
                                <h3 class="page-header">Bénéficiaires offre - {{ $line['offer'] }}</h3>
                                @for ($i = 0; $i < $line['total']; $i++)
                                    <h4>Informations Bénéficiaire - {{ $j }}</h4>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="title">
                                            Nom & Prénom <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4 error-container">
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][full_name]"
                                                   id="rows_{{ $id }}_{{ $i }}_full_name" required class="form-control"
                                                   value="{{ isset($line['profiteers'][$i]['full_name']) ? $line['profiteers'][$i]['full_name'] : $order->user->full_name }}" />
                                        </div>
                                    </div>
                                    @if ($isFootball)
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="is_minor">
                                                @Lang('profiteer.is_minor')
                                            </label>
                                            <div class="col-md-7 error-container">
                                                <input type="checkbox" class="form-control make-switch" name="rows[{{ $id }}][{{ $i }}][is_minor]"
                                                       id="rows_{{ $id }}_{{ $i }}_is_minor" data-on-text="Oui" data-off-text="Non"
                                                       data-on-color="success" data-off-color="warning"
                                                       data-size="normal" {{ (isset($line['profiteers'][$i]['is_minor']) && $line['profiteers'][$i]['is_minor'] == 1) ? 'checked' : '' }} />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="title">
                                                {{ (isset($line['profiteers'][$i]['is_minor']) && $line['profiteers'][$i]['is_minor'] == 1) ? 'N° CIN Tuteur' : 'N° CIN' }} <span class="required"> * </span>
                                            </label>
                                            <div class="col-md-4 error-container">
                                                <input type="text" name="rows[{{ $id }}][{{ $i }}][cin]"
                                                       id="rows_{{ $id }}_{{ $i }}_cin" required class="form-control"
                                                       value="{{ isset($line['profiteers'][$i]['cin']) ? $line['profiteers'][$i]['cin'] : '' }}" />
                                            </div>
                                        </div>
                                    @endif
                                    @php $j++ @endphp
                                @endfor
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
@endsection