@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-tags font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('promotion.edit_details', ['id' => $model->id]) : trans('promotion.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.promotion.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="active">
                        @Lang('promotion.active')
                    </label>
                    <div class="col-md-7 error-container">
                        <input type="checkbox" class="form-control make-switch" name="active"
                               id="active" data-on-text="Oui" data-off-text="Non"
                               data-on-color="success" data-off-color="warning"
                               data-size="normal" {{ empty($model->id) || $model->active ? 'checked' : '' }} />
                        @if ($errors->has('active'))
                            <small id="active-error" class="has-error help-block help-block-error">
                                {{ $errors->first('active') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="name">
                        @Lang('promotion.name') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <input type="text" name="name" id="name"
                               value="{{ old('name') ? old('name') : ($model->name ? $model->name : '') }}"
                               required class="form-control"/>
                        @if ($errors->has('name'))
                            <small id="name-error" class="has-error help-block help-block-error">
                                {{ $errors->first('name') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="code">
                        @Lang('promotion.code') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <input type="text" name="code" id="code"
                               value="{{ old('code') ? old('code') : ($model->code ? $model->code : '') }}"
                               required class="form-control"/>
                        @if ($errors->has('code'))
                            <small id="code-error" class="has-error help-block help-block-error">
                                {{ $errors->first('code') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('start_at') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="start_at">
                        @Lang('promotion.start_at') <span class="required"> * </span>
                    </label>
                    <div class="col-md-2 error-container">
                        <div class="input-group  margin-bottom-5" >
                            <input type="text" name="start_at" id="start_at" autocomplete="off" required class="form-control datepicker" data-rule-date="true" data-date-format="dd/mm/yyyy"
                                   value="{{ old('start_at') ? Carbon\Carbon::parse(old('start_at'))->format('d/m/Y') : ($model->start_at ? Carbon\Carbon::parse($model->start_at)->format('d/m/Y') : '') }}" />
                            <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                        </div>
                        @if ($errors->has('start_at'))
                            <small id="start_at-error" class="has-error help-block help-block-error">
                                {{ $errors->first('start_at') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('end_at') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="end_at">
                        @Lang('promotion.end_at') <span class="required"> * </span>
                    </label>
                    <div class="col-md-2 error-container">
                        <div class="input-group margin-bottom-5">
                            <input type="text" name="end_at" id="end_at" autocomplete="off" required class="form-control datepicker" data-rule-date="true" data-date-format="dd/mm/yyyy"
                                   value="{{ old('end_at') ? Carbon\Carbon::parse(old('end_at'))->format('d/m/Y') : ($model->end_at ? Carbon\Carbon::parse($model->end_at)->format('d/m/Y') : '') }}" />
                            <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                        </div>
                        @if ($errors->has('end_at'))
                            <small id="end_at-error" class="has-error help-block help-block-error">
                                {{ $errors->first('end_at') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label">@Lang('promotion.apply_discount') <span
                                class="required"> * </span></label>
                    <div class="col-md-3 error-container">
                        <div class="mt-radio-inline">
                            @foreach($types as $key => $type)
                                <label class="mt-radio" style="margin-bottom: 0px">
                                    <input type="radio" class="radio-type" required name="type" id="type-{{ $key }}" value="{{ $key }}"
                                           @if(old('type') == $key || $model->type == $key) checked @endif> {{ $type }}
                                    <span></span>
                                </label>
                            @endforeach
                        </div>
                        @if ($errors->has('type'))
                            <small id="type-error" class="has-error help-block help-block-error">
                                {{ $errors->first('type') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('discount') ? ' has-error' : '' }}" id="percentage-type">
                    <label class="col-md-3 control-label" for="percentage">
                        @Lang('promotion.percentage') <span
                                class="required"> * </span></label>
                    </label>
                    <div class="col-md-3 error-container">
                        <div class="input-group">
                            <input type="number" name="percentage_discount" required id="percentage"
                                   value="{{ old('discount') ? old('discount') : ($model->discount ? $model->discount : '') }}"
                                   class="form-control touchspin"/>
                            <span class="input-group-addon">%</span>
                        </div>
                        @if ($errors->has('discount'))
                            <small id="percentage-error" class="has-error help-block help-block-error">
                                {{ $errors->first('discount') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('discount') ? ' has-error' : '' }}"  id="amount-type">
                    <label class="col-md-3 control-label" for="discount">
                        @Lang('promotion.amount') <span
                                class="required"> * </span></label>
                    </label>
                    <div class="col-md-3 error-container">
                        <div class="input-group">
                            <input type="number" name="amount_discount" required id="amount"
                                   value="{{ old('discount') ? old('discount') : ($model->discount ? $model->discount : '') }}"
                                   class="form-control"/>
                            <span class="input-group-addon">DH</span>
                        </div>

                        @if ($errors->has('discount'))
                            <small id="amount-error" class="has-error help-block help-block-error">
                                {{ $errors->first('discount') }}
                            </small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('typeable_type') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="typeable_type">
                        @Lang('promotion.typeable_type') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <select name="typeable_type" id="typeable_type" required class="form-control select2">
                            <option value="">@Lang('app.select')</option>
                            @foreach ($typeable_types as $key => $typeable)
                                <option {{ (old('typeable_type') && old('typeable_type') == $key) ? 'selected' : (($model->typeable_type && $model->typeable_type == $key) ? 'selected' : '') }}
                                        value="{{ $key }}">
                                    {{ $typeable }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('typeable_type'))
                            <small id="typeable_type-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('typeable_type') }}</small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('typeable_id') ? ' has-error' : '' }}" id="typeable-category">
                    <label class="col-md-3 control-label" for="category_id">
                        @Lang('promotion.category') <span class="required"> * </span>
                    </label>
                    <div class="col-md-3 error-container">
                        <select name="category_id" id="category_id" required class="form-control select2">
                            <option value="">@Lang('app.select')</option>
                            @foreach ($categories as $key => $category)
                                <option {{ (old('category_id') && old('category_id') == $key) ? 'selected' : (($model->typeable_id && $model->typeable_id == $key) ? 'selected' : '') }}
                                        value="{{ $key }}">
                                    {{ $category }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('typeable_id'))
                            <small id="typeable_id-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('typeable_id') }}</small>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('typeable_id') ? ' has-error' : '' }}" id="typeable-event">
                    <label class="col-md-3 control-label" for="event_id">
                        @Lang('promotion.event') <span class="required"> * </span>
                    </label>
                    <div class="col-md-5 error-container">
                        <select name="event_id" id="event_id" required class="form-control select2">
                            <option value="">@Lang('app.select')</option>
                            @foreach ($events as $key => $event)
                                <option {{ (old('typeable_id') && old('typeable_id') == $key) ? 'selected' : (($model->typeable_id && $model->typeable_id == $key) ? 'selected' : '') }}
                                        value="{{ $key }}">
                                    {{ $event }}
                                </option>
                            @endforeach
                        </select>
                        @if ($errors->has('typeable_id'))
                            <small id="typeable_id-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('typeable_id') }}</small>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-touchspin/bootstrap.touchspin.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-touchspin/bootstrap.touchspin.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/promotion.js') }}" type="text/javascript"></script>
@endsection