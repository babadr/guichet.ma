@extends('layouts.backend')

@section('title')
    @Lang('promotion.manage_promotions')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-tags font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('promotion.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::hasRole(['superadmin', 'admin']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.promotion.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('promotion.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
