@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-doc font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('page.edit_details', ['id' => $model->id]) : trans('page.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.page.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line settings">
                    <ul class="nav nav-tabs ">
                        <li role="presentation" class="active">
                            <a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">@Lang('page.info')</a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">@Lang('seo.tab_title')</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="title">@Lang('post.title') <span
                                            class="required"> * </span></label>
                                <div class="col-md-7 error-container">
                                    <input type="text" name="title" id="title"
                                           value="{{ $model->title }}" required class="form-control"/>
                                    @if ($errors->has('title'))
                                        <small id="title-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('title') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="content">@Lang('post.content')</label>
                                <div class="col-md-7 error-container">
                                    <textarea type="text" name="content" id="content"
                                               class="form-control summernote">{{ $model->content }}</textarea>
                                    @if ($errors->has('content'))
                                        <small id="content-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('content') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            @include('backend.elements.seo')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/summernote.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/lang/summernote-fr-FR.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
@endsection