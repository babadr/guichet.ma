@extends('layouts.backend')

@section('title')
    @Lang('page.manage_pages')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-doc font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('page.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.page.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.page.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('page.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
