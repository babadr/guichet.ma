@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-docs font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('post.edit_details', ['id' => $model->id]) : trans('post.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.post.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line settings">
                    <ul class="nav nav-tabs ">
                        <li role="presentation" class="active">
                            <a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">@Lang('page.info')</a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">@Lang('seo.tab_title')</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="category_id">
                                    @Lang('post.category_id')
                                </label>
                                <div class="col-md-3 error-container">
                                    <select name="category_id" id="category_id" class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach ($categories as $key => $category)
                                            <option {{ (old('category_id') && old('category_id') == $key) ? 'selected' : (($model->category_id && $model->category_id == $key) ? 'selected' : '') }}
                                                    value="{{ $key }}">
                                                {{ $category }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('category_id'))
                                        <small id="category_id-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('category_id') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="title">@Lang('post.title') <span
                                            class="required"> * </span></label>
                                <div class="col-md-7 error-container">
                                    <input type="text" name="title" id="title"
                                           value="{{ $model->title }}" required class="form-control"/>
                                    @if ($errors->has('title'))
                                        <small id="title-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('title') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group {{ $errors->has('content') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="content">@Lang('post.content') <span
                                            class="required"> * </span></label>
                                <div class="col-md-7 error-container">
                                    <textarea type="text" name="content" id="content" required data-rule-summernote="true"
                                           class="form-control summernote">{{ $model->content }}</textarea>
                                    @if ($errors->has('content'))
                                        <small id="content-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('content') }}</small>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label">
                                    @Lang('post.image')
                                </label>
                                <div class="col-md-7 error-container">
                                    <div class="fileinput {{ $model->image ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail"
                                             style="width: 200px; height: 150px;">
                                            <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&text=aucune+image" alt=""/>
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 200px; max-height: 150px;"><img src="{{ $model->id ? asset_cdn($model->image) . '?w=200&h=150&fit=clip&auto=format,compress&q=80' : '' }}" alt=""/></div>
                                        <div>
                                        <span class="btn blue btn-file">
                                            <span class="fileinput-new"> @Lang('app.select_image') </span>
                                            <span class="fileinput-exists"> @Lang('app.change') </span>
                                            <input type="file" name="image"  data-rule-accept="image/*" data-msg-accept="Les types de fichiers autorisés sont jpg, jpeg, png et gif" accept=".jpeg,.png,.jpg,.gif">
                                        </span>
                                            <a href="javascript:;" class="btn red fileinput-exists"
                                               data-dismiss="fileinput"> @Lang('app.remove') </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            @include('backend.elements.seo')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/summernote.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-summernote/lang/summernote-fr-FR.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection
