@extends('layouts.backend')

@section('title')
    @Lang('post.manage_posts')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-docs font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('post.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.post.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.post.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('post.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
