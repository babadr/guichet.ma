@extends('layouts.backend')

@section('title')
    @Lang('faq.manage_faqs')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-doc font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('faq.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.faq.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.faq.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('faq.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
