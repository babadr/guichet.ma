<a href="/{{ $model->seo ? $model->seo->seo_alias : '#' }}" target="_blank" alt="@Lang('app.View')"
   title="@Lang('app.View')" class="btn btn-outline blue btn-xs">
    <i class="fa fa-eye"></i>
</a>
@if (Entrust::can(['backend.' . $modelName . '.update']))
    <a href="{{ route('backend.' . $modelName . '.edit', ['id' => $model->id]) }}" alt="@Lang('app.Edit')"
       title="@Lang('app.Edit')" class="btn btn-outline yellow btn-xs">
        <i class="fa fa-edit"></i>
    </a>
@endif
@if (Entrust::can(['backend.' . $modelName . '.delete']))
    <a href="{{ route('backend.' . $modelName . '.delete', ['id' => $model->id]) }}" alt="@Lang('app.Delete')"
       title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-element">
        <i class="fa fa-trash"></i>
    </a>
@endif