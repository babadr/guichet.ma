@extends('layouts.backend')

@section('title')
    @Lang('category_post.manage_categories')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-tag font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('category_post.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.post.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.category_post.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('category_post.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
