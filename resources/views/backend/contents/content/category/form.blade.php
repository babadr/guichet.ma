@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-tag font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('category_post.edit_details', ['id' => $model->id]) : trans('category_post.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.category_post.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line settings">
                    <ul class="nav nav-tabs ">
                        <li role="presentation" class="active">
                            <a href="#tab_1" aria-controls="tab_1" role="tab" data-toggle="tab">
                                @Lang('app.info')
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="#tab_2" aria-controls="tab_2" role="tab" data-toggle="tab">
                                @Lang('seo.tab_title')
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="active">
                                    @Lang('category_post.active') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="checkbox" class="form-control make-switch" name="active"
                                           id="active" data-on-text="Oui" data-off-text="Non"
                                           data-on-color="success" data-off-color="warning"
                                           data-size="normal" {{ empty($model->id) || $model->active ? 'checked' : '' }} />
                                    @if ($errors->has('active'))
                                        <small id="active-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('active') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="col-md-3 control-label" for="name">
                                    @Lang('category_post.name') <span class="required"> * </span>
                                </label>
                                <div class="col-md-7 error-container">
                                    <input type="text" name="name" id="name"
                                           value="{{ old('name') ? old('name') : ($model->name ? $model->name : '') }}"
                                           required class="form-control"/>
                                    @if ($errors->has('name'))
                                        <small id="name-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('name') }}
                                        </small>
                                    @endif
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="tab_2">
                            @include('backend.elements.seo')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-iconpicker/css/fontawesome-iconpicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-iconpicker/js/fontawesome-iconpicker.min.js') }}" type="text/javascript"></script>
    <script>
        $('#icon').iconpicker();
    </script>
@endsection