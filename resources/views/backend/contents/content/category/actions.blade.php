@if (Entrust::can(['backend.post.update']))
    <a href="{{ route('backend.category_post.edit', ['id' => $model->id]) }}" alt="@Lang('app.Edit')"
       title="@Lang('app.Edit')" class="btn btn-outline yellow btn-xs">
        <i class="fa fa-edit"></i>
    </a>
@endif
@if (Entrust::can(['backend.post.delete']))
    <a href="{{ route('backend.category_post.delete', ['id' => $model->id]) }}" alt="@Lang('app.Delete')"
       title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-element">
        <i class="fa fa-trash"></i>
    </a>
@endif