@extends('layouts.backend')

@section('title')
    {{ $provider->title }}
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="fa fa-tags font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">Liste des événements</span>
            </div>
        </div>
        <div class="portlet-body flip-scroll">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content">
                    <tr>
                        <th width="30px"> #</th>
                        <th> Titre</th>
                        <th> Catégorie</th>
                        <th> Publié le</th>
                        <th class="numeric"> Nombre d'acheteurs</th>
                        <th class="numeric"> Quantité vendue</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($deals as $key => $deal)
                        <tr>
                            <td class="text-center"> {{ $key + 1 }}</td>
                            <td> {{ $deal->title }}</td>
                            <td> {{ $deal->category->title }}</td>
                            <td> {{ date('d/m/Y', strtotime($deal->start_at)) }}</td>
                            <td class="numeric text-center"> {{ $deal->getRealBought() }}</td>
                            <td class="numeric text-center"> {{ $deal->getOrderedQuantity(false) }}</td>
                            <td class="numeric text-center">
                                <a href="{{ route('backend.ticket.tickets', ['deal_id' => $deal->id, 'partner_id' => $provider->id]) }}"
                                   alt="@Lang('app.View')"
                                   title="@Lang('app.View')" class="btn btn-outline blue btn-xs">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
@endsection
