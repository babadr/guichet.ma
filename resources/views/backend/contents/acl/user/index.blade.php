@extends('layouts.backend')

@section('title')
    @Lang('user.ManageUsers')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-user font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('user.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.user.read']))
                    <a class="btn red btn-outline btn-circle" id="export-data" href="{{ route('backend.user.export') }}">
                        <i class="fa fa-file-excel-o"></i>
                        <span class="hidden-xs"> @Lang('user.export') </span>
                    </a>
                @endif
                @if (Entrust::can(['backend.user.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.user.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('user.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
