@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <div class="profile-sidebar">
        <div class="portlet light profile-sidebar-portlet ">
            <div class="profile-userpic">
                <img src="{{ $model->id ? asset_cdn($model->avatar) . '?w=150&h=150&fit=crop&auto=format,compress&q=80' : 'https://www.placehold.it/150x150/EFEFEF/AAAAAA&text=aucune+image' }}" class="img-responsive" alt=""></div>
            <div class="profile-usertitle">
                <div class="profile-usertitle-name"> {{ $model->id ? $model->full_name : '' }} </div>
                <div class="profile-usertitle-job"> {{ $model->id ? $model->roles->first()->display_name : '' }} </div>
            </div>
            <div class="profile-usermenu">
                <ul class="nav">
                    <li class="active">
                        <a href="javascript:;">
                            <i class="icon-settings"></i> @Lang('user.account_settings') </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="profile-content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-validate"
                      accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md font-red-sunglo">
                                <i class="icon-user font-red-sunglo"></i>
                                <span class="caption-subject bold uppercase">{{ $model->id ? trans('user.edit_details', ['id' => $model->id]) : trans('user.add_details') }}</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">@Lang('user.Personal_Info')</a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab">@Lang('user.Change_Password')</a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">@Lang('user.Change_Avatar')</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    @if ($model->id != auth()->user()->id)
                                        <div class="form-group {{ $errors->has('active') ? ' has-error' : '' }}">
                                            <label class="control-label" for="active">@Lang('user.status')</label>
                                            <div>
                                                <input type="checkbox" class="form-control make-switch" name="active"
                                                       id="active" data-on-text="Oui" data-off-text="Non"
                                                       data-on-color="success" data-off-color="warning"
                                                       data-size="normal" {{ empty($model->id) || $model->active ? 'checked' : '' }} />
                                            </div>
                                            @if ($errors->has('active'))
                                                <small id="active-error"
                                                       class="has-error help-block help-block-error">{{ $errors->first('active') }}</small>
                                            @endif
                                        </div>
                                        <div class="form-group {{ $errors->has('role_id') ? ' has-error' : '' }}">
                                            <label class="control-label" for="role-id">
                                                @Lang('user.role_id')
                                                <span class="required"> * </span>
                                            </label>
                                            <select name="role_id" id="role-id" required class="form-control select2">
                                                <option value="">@Lang('app.select')</option>
                                                @foreach ($roles as $key => $role)
                                                    <option {{ ($model->id && $model->roles->first()->id == $key) ? 'selected' : '' }} value="{{ $key }}">{{ $role }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('role_id'))
                                                <small id="role-id-error"
                                                       class="has-error help-block help-block-error">{{ $errors->first('role_id') }}</small>
                                            @endif
                                        </div>
                                    @endif
                                        <div class="form-group {{ $errors->has('provider_id') ? ' has-error' : '' }}">
                                            <label class="control-label" for="provider-id">
                                                @Lang('user.provider_id')
                                            </label>
                                            <select name="provider_id" id="provider-id" class="form-control select2">
                                                <option value="">@Lang('app.select')</option>
                                                @foreach ($providers as $key => $provider)
                                                    <option {{ ($model->provider_id == $key) ? 'selected' : '' }} value="{{ $key }}">{{ $provider }}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('provider_id'))
                                                <small id="provider-id-error"
                                                       class="has-error help-block help-block-error">{{ $errors->first('role_id') }}</small>
                                            @endif
                                        </div>
                                    <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                        <label class="control-label" for="last-name">
                                            @Lang('user.last_name')
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" name="last_name" id="last-name"
                                               value="{{ $model->last_name }}" required class="form-control"/>
                                        @if ($errors->has('last_name'))
                                            <small id="first-name-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('last_name') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                        <label class="control-label" for="first-name">
                                            @Lang('user.first_name')
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" name="first_name" id="first-name" class="form-control"
                                               value="{{ $model->first_name }}" required/>
                                        @if ($errors->has('first_name'))
                                            <small id="last-name-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('first_name') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label" for="email">
                                            @Lang('user.email')
                                            <span class="required"> * </span>
                                        </label>
                                        <input type="text" name="email" id="email" class="form-control"
                                               value="{{ $model->email }}" required data-rule-email="true" data-msg-email="Format email invalid" />
                                        @if ($errors->has('email'))
                                            <small id="email-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('email') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label class="control-label" for="phone">
                                            @Lang('user.phone') <span class="required"> * </span>
                                        </label>
                                        <input type="text" name="phone" id="phone" required class="form-control"
                                               value="{{ $model->phone }}" data-rule-phone="true" data-msg-phone="Format phone invalid" />
                                        @if ($errors->has('phone'))
                                            <small id="phone-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('phone') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                        <label class="control-label" for="country">
                                            @Lang('user.country')
                                        </label>
                                        <select name="country" id="country" class="form-control select2">
                                            <option value="">@Lang('app.select')</option>
                                            @foreach (config("countries") as $country)
                                                <option {{ (old('country') && old('country') == $country[1]) ? 'selected' : (($model->country && $model->country == $country[1]) ? 'selected' : '') }}
                                                        value="{{ $country[1] }}">
                                                    {{ $country[3] }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                            <small id="country-error" class="has-error help-block help-block-error">
                                                {{ $errors->first('country') }}
                                            </small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                        <label class="control-label" for="city">
                                            @Lang('user.city')
                                        </label>
                                        <input type="text" name="city" id="city" class="form-control"
                                               value="{{ $model->city }}" data-rule-city="true" data-msg-city="Format city invalid" />
                                        @if ($errors->has('city'))
                                            <small id="city-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('city') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label class="control-label" for="address">
                                            @Lang('user.address')
                                        </label>
                                        <input type="text" name="address" id="address" class="form-control"
                                               value="{{ $model->address }}" data-rule-address="true" data-msg-address="Format address invalid" />
                                        @if ($errors->has('address'))
                                            <small id="address-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('address') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_1_2">
                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="control-label"
                                               for="password">@Lang('user.password') @if (!$model->id) <span
                                                    class="required"> * </span>@endif</label>
                                        <input class="form-control" type="password" name="password"
                                               id="password" {!! $model->id ? : 'required' !!}>
                                        @if ($errors->has('password'))
                                            <small id="password-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('password') }}</small>
                                        @endif
                                    </div>
                                    <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label class="control-label"
                                               for="password-confirm">@Lang('user.password_confirmation') @if (!$model->id)
                                                <span class="required"> * </span>@endif</label>
                                        <input class="form-control" type="password" name="password_confirmation"
                                               id="password-confirm" data-rule-equalto="#password"
                                               data-msg-equalto="Le mot de passe est différent. Merci de saisir un mot de passe identique." {!! $model->id ? : 'required' !!}>
                                        @if ($errors->has('password_confirmation'))
                                            <small id="password-confirmation-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('password_confirmation') }}</small>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_1_3">
                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                        brunch. Food truck quinoa nesciunt laborum
                                        eiusmod. </p>
                                    <div class="form-group {{ $errors->has('avatar') ? ' has-error' : '' }}">
                                        <div class="fileinput {{ $model->avatar ? 'fileinput-exists' : 'fileinput-new' }}" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail"
                                                 style="width: 200px; height: 150px;">
                                                <img src="https://www.placehold.it/200x150/EFEFEF/AAAAAA&text=aucune+image" alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                 style="max-width: 200px; max-height: 150px;"><img src="{{ $model->id ? asset_cdn($model->avatar) . '?w=190&h=190&fit=clip&auto=format,compress&q=80' : '' }}" alt=""/></div>
                                            <div>
                                                                            <span class="btn blue btn-file">
                                                                                <span class="fileinput-new"> @Lang('app.select_image') </span>
                                                                                <span class="fileinput-exists"> @Lang('app.change') </span>
                                                                                <input type="file" name="avatar"  data-rule-accept="image/*" data-msg-accept="Les types de fichiers autorisés sont jpg, jpeg, png et gif" accept=".jpeg,.png,.jpg,.gif"> </span>
                                                <a href="javascript:;" class="btn red fileinput-exists"
                                                   data-dismiss="fileinput"> @Lang('app.remove') </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9 errors-container col-lg-offset-3">
                                        @if ($errors->has('avatar'))
                                            <small id="avatar-error"
                                                   class="has-error help-block help-block-error">{{ $errors->first('avatar') }}</small>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="margiv-top-10">
                                <a href="{{ route('backend.user.index') }}" class="btn default">
                                    <i class="fa fa-reply"></i>
                                    <span class="hidden-480">@Lang('app.back_to_list')</span>
                                </a>
                                <button type="submit" name="save" class="btn btn-info"
                                        title="@Lang('app.save')">
                                    <i class="fa fa-check"></i>
                                    <span class="hidden-480">@Lang('app.save')</span>
                                </button>
                                <button type="submit" name="saveandcontinue" class="btn btn-success"
                                        title="@Lang('app.save_continue')">
                                    <i class="fa fa-check-circle"></i>
                                    <span class="hidden-480">@Lang('app.save_continue')</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/backend/css/profile.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}"
            type="text/javascript"></script>
@endsection