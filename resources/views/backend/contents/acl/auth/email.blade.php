@extends('layouts.auth')

@section('title')
    @Lang('auth.admin_panel_forget_password')
@endsection

@section('content')
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form form-validate" method="POST" action="{{ route('backend.auth.password.forgot') }}" no>
        {{ csrf_field() }}
        <h2 class="text-center font-grey">@Lang('auth.forgot_password')</h2>
        <p class="font-grey">@Lang('auth.reset_password_msg')</p>
        @include('backend.includes.flash')
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label visible-ie8 visible-ie9">@Lang('auth.email')</label>
            <input class="form-control placeholder-no-fix" type="text" id="email" autocomplete="off" autofocus placeholder="@Lang('auth.email')" required
                   data-rule-email="true"
                   name="email" value="{{ old('email') }}"/>
            @if ($errors->has('email'))
                <small id="email-error" class="has-error help-block help-block-error">{{ $errors->first('email') }}</small>
            @endif
        </div>
        <div class="form-actions">
            <a href="{{ route('backend.auth.showLogin') }}" id="back-btn" class="btn btn-default uppercase">
                <span>@Lang('auth.back')</span>
            </a>
            <button type="submit" class="btn red uppercase uppercase pull-right">@Lang('auth.submit')</button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
@endsection
