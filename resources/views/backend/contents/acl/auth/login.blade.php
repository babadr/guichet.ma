@extends('layouts.auth')

@section('title')
    @Lang('auth.admin_panel_login')
@endsection

@section('content')
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form form-validate" action="{{ route('backend.auth.login') }}" method="POST" novalidate>
        {{ csrf_field() }}
        <div class="form-title">
            <span class="form-title">@Lang('auth.welcome').</span>
            <span class="form-subtitle">@Lang('auth.please_login').</span>
        </div>
        @include('backend.includes.flash')
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9" for="email">@Lang('auth.email')</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="emain" type="text" required
                   data-rule-email="true" placeholder="@Lang('auth.email')" name="email" value="{{ old('email') }}" autofocus/>
            @if ($errors->has('email'))
                <small id="email-error"
                       class="has-error help-block help-block-error">{{ $errors->first('email') }}</small>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label visible-ie8 visible-ie9">@Lang('auth.password')</label>
            <input class="form-control form-control-solid placeholder-no-fix" id="password" type="password"
                   autocomplete="off" required
                   placeholder="@Lang('auth.password')" name="password"/>
            @if ($errors->has('password'))
                <small id="password-error"
                       class="has-error help-block help-block-error">{{ $errors->first('password') }}</small>
            @endif
        </div>
        <div class="form-actions">
            <button type="submit" class="btn red btn-block uppercase">@Lang('auth.login')</button>
        </div>
        <div class="form-actions">
            <div class="pull-left">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" value="1"
                           name="remember" {{ old('remember') ? 'checked' : '' }}/> @Lang('auth.remember_me')
                    <span></span>
                </label>
            </div>
            <div class="pull-right forget-password-block">
                <a href="{{ route('backend.auth.password.showForgot') }}" id="forget-password"
                   class="forget-password">@Lang('auth.forgot_password')</a>
            </div>
        </div>
    </form>
    <!-- END LOGIN FORM -->
@endsection
