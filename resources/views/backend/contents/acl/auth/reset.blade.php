@extends('layouts.auth')

@section('title')
    @Lang('auth.admin_panel_reset_password')
@endsection

@section('content')
    <form class="reset-form form-validate" method="POST" action="{{ route('backend.auth.password.reset') }}" novalidate="novalidate">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
        <h2 class="text-center font-grey">@Lang('auth.reset_password')</h2>
        <p class="font-grey">@Lang('auth.reset_password_title')</p>
        @include('backend.includes.flash')
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="control-label visible-ie8 visible-ie9">@Lang('auth.email')</label>
            <input id="email" type="email" class="form-control placeholder-no-fix" name="email" placeholder="@Lang('auth.email')"
                   value="{{ $email or old('email') }}" required data-rule-email="true">
            @if ($errors->has('email'))
                <small id="email-error" class="has-error help-block help-block-error">{{ $errors->first('email') }}</small>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="control-label visible-ie8 visible-ie9">@Lang('auth.password')</label>
            <input id="password" type="password" class="form-control placeholder-no-fix" placeholder="@Lang('auth.password')" name="password" required>
            @if ($errors->has('password'))
                <small id="password-error" class="has-error help-block help-block-error">{{ $errors->first('password') }}</small>
            @endif
        </div>

        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            <label for="password-confirm" class="control-label visible-ie8 visible-ie9">@lang('auth.confirm_password')</label>
            <input id="password-confirm" type="password" placeholder="@lang('auth.confirm_password')" class="form-control placeholder-no-fix"
                   name="password_confirmation" required>
            @if ($errors->has('password_confirmation'))
                <small id="password-confirm-error" class="has-error help-block help-block-error">{{ $errors->first('password_confirmation') }}</small>
            @endif
        </div>
        <div class="form-actions">
            <a href="{{ route('backend.auth.showLogin') }}" id="register-back-btn" class="btn btn-default uppercase">
                <span>@Lang('auth.back')</span>
            </a>
            <button type="submit" id="reset-submit-btn" class="btn red uppercase pull-right">@Lang('auth.reset')</button>
        </div>
    </form>
@endsection
