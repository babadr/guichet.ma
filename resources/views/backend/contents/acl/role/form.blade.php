@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-users font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('role.edit_details', ['id' => $model->id]) : trans('role.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.role.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="name">@Lang('role.name') <span
                                class="required"> * </span></label>
                    <div class="col-md-4 error-container">
                        <input type="text" name="name" id="name"
                               value="{{ $model->name }}" required class="form-control"/>
                        @if ($errors->has('name'))
                            <small id="name-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('display_name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="display-name">@Lang('role.display_name') <span
                                class="required"> * </span></label>
                    <div class="col-md-4 error-container">
                        <input type="text" name="display_name" id="display-name"
                               value="{{ $model->display_name }}" required class="form-control"/>
                        @if ($errors->has('display_name'))
                            <small id="display-name-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('display_name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="description">@Lang('role.description')</label>
                    <div class="col-md-6 error-container">
                        <input type="text" name="description" id="description"
                               value="{{ $model->description }}" class="form-control"/>
                        @if ($errors->has('description'))
                            <small id="description-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('description') }}</small>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
