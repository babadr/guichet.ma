@extends('layouts.backend')

@section('title')
    @Lang('role.manage_roles')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-users font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('role.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.role.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.role.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('role.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
