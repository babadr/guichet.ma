@extends('layouts.backend')

@section('title')
    @Lang('permission.matrix')
@endsection

@section('content')
    <form action="{{ route('backend.permission.update') }}" method="POST" id="app-form"
          class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-lock font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> @Lang('permission.global_permissions')</span>
                </div>
                <div class="actions">
                    @if (Entrust::can('backend.permission.update'))
                        <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save_changes')">
                            <i class="fa fa-check"></i>
                            <span class="hidden-480">@Lang('app.save_changes')</span>
                        </button>
                    @endif
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable" id="category">
                    <table class="table table-striped table-bordered table-hover accordion">
                        <thead>
                        <tr>
                            <th id="category"></th>
                            @foreach($roles as $role)
                                <th class="text-center"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    data-original-title="{!! $role->description !!}" id="category">
                                    {!! $role->display_name !!}
                                </th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr class="category">
                                <td class="color-view bg-grey bg-font-grey bold uppercase"
                                    colspan="{{ count($roles) + 1 }}">{!! $category->display_name !!}
                                </td>
                            </tr>
                            @foreach($category->permissions as $permission)
                                <tr>
                                    @php $names = explode(' / ', $permission->display_name) @endphp
                                    <td style="padding-left: 50px;">{!! trans($names[0]) . ' / ' . trans($names[1]) !!}</td>
                                    @foreach($roles as $role)
                                        <td class="text-center">
                                            <input type="checkbox"
                                                   name="roles[{!!$role->id!!}][permissions][{!!$permission->id!!}]"
                                                    {!! $role->hasPermission($permission->name) ? "checked" : "" !!}>
                                        </td>
                                    @endforeach
                                </tr>
                            @endforeach
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </form>
@endsection