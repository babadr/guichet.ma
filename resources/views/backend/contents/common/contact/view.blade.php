@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="#" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8">
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-envelope font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('contact.edit_details', ['id' => $model->id]) : trans('page.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.contact.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="tabbable-line settings">
                    <div class="form-group">
                        <label class="col-md-3 control-label">@Lang('contact.received_on')</label>
                        <div class="col-md-3">
                            <input class="form-control" readonly="" value="{{ date('d/m/Y - H:i', strtotime($model->created_at)) }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">@Lang('contact.phone')</label>
                        <div class="col-md-3">
                            <input class="form-control" readonly="" value="{{ $model->phone }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">@Lang('contact.email')</label>
                        <div class="col-md-3">
                            <input class="form-control" readonly="" value="{{ $model->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">@Lang('contact.from')</label>
                        <div class="col-md-3">
                            <input class="form-control" readonly="" value="{{ $model->last_name . ' ' . $model->first_name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">@Lang('contact.subject')</label>
                        <div class="col-md-8">
                            <input class="form-control" readonly="" value="{{ $model->subject }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">@Lang('contact.message')</label>
                        <div class="col-md-8">
                            <textarea class="form-control" rows="10" readonly="">{{ $model->message }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection