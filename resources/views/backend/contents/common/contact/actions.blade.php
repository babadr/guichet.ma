@if (Entrust::can(['backend.' . $modelName . '.read']))
    <a href="{{ route('backend.' . $modelName . '.view', ['id' => $model->id]) }}" alt="@Lang('app.View')"
       title="@Lang('app.View')" class="btn btn-outline blue btn-xs">
        <i class="fa fa-eye"></i>
    </a>
@endif
@if (Entrust::can(['backend.' . $modelName . '.delete']))
    <a href="{{ route('backend.' . $modelName . '.delete', ['id' => $model->id]) }}" alt="@Lang('app.Delete')"
       title="@Lang('app.Delete')" class="btn btn-outline red btn-xs delete-element">
        <i class="fa fa-trash"></i>
    </a>
@endif