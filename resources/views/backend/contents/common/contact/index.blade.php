@extends('layouts.backend')

@section('title')
    @Lang('contact.manage_contacts')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-envelope font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('contact.list')</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
