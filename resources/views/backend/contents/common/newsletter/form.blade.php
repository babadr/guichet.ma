@extends('layouts.backend')

@section('title')
    {!! $title !!}
@endsection

@section('content')
    <form action="{{ $route }}" method="{{ $method }}" id="app-form" class="form-horizontal form-validate"
          accept-charset="UTF-8" novalidate="novalidate" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="portlet light portlet-form bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="icon-paper-plane font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> {{ $model->id ? trans('newsletter.edit_details', ['id' => $model->id]) : trans('newsletter.add_details') }}</span>
                </div>
                <div class="actions">
                    <a href="{{ route('backend.newsletter.index') }}" class="btn default">
                        <i class="fa fa-reply"></i>
                        <span class="hidden-480">@Lang('app.back_to_list')</span>
                    </a>
                    <button type="submit" name="save" class="btn btn-info" title="@Lang('app.save')">
                        <i class="fa fa-check"></i>
                        <span class="hidden-480">@Lang('app.save')</span>
                    </button>
                    <button type="submit" name="saveandcontinue" class="btn btn-success"
                            title="@Lang('app.save_continue')">
                        <i class="fa fa-check-circle"></i>
                        <span class="hidden-480">@Lang('app.save_continue')</span>
                    </button>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="last_name">@Lang('newsletter.last_name')</label>
                    <div class="col-md-4 error-container">
                        <input type="text" name="last_name" id="last_name"
                               value="{{ $model->last_name }}" class="form-control"/>
                        @if ($errors->has('last_name'))
                            <small id="last_name-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('last_name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="first_name">@Lang('newsletter.first_name')</label>
                    <div class="col-md-4 error-container">
                        <input type="text" name="first_name" id="first_name"
                               value="{{ $model->first_name }}" class="form-control"/>
                        @if ($errors->has('first_name'))
                            <small id="first_name-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('first_name') }}</small>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-md-3 control-label" for="email">@Lang('newsletter.email') <span
                                class="required"> * </span></label>
                    <div class="col-md-4 error-container">
                        <input type="text" name="email" id="email"
                               value="{{ $model->email }}" required data-rule-email="true" class="form-control"/>
                        @if ($errors->has('email'))
                            <small id="email-error"
                                   class="has-error help-block help-block-error">{{ $errors->first('email') }}</small>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
