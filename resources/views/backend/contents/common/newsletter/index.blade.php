@extends('layouts.backend')

@section('title')
    @Lang('newsletter.manage_newsletters')
@endsection

@section('content')
    <div class="portlet light bordered">
        <div class="portlet-title">
            <div class="caption font-red-sunglo">
                <i class="icon-paper-plane font-red-sunglo"></i>
                <span class="caption-subject bold uppercase">@Lang('newsletter.list')</span>
            </div>
            <div class="actions">
                @if (Entrust::can(['backend.newsletter.read']))
                    <a class="btn red btn-outline btn-circle" id="export-data" href="{{ route('backend.newsletter.export') }}">
                        <i class="fa fa-file-excel-o"></i>
                        <span class="hidden-xs"> @Lang('newsletter.export') </span>
                    </a>
                @endif
                @if (Entrust::can(['backend.newsletter.create']))
                    <a class="btn blue btn-outline btn-circle" href="{{ route('backend.newsletter.create') }}">
                        <i class="fa fa-plus"></i>
                        <span class="hidden-xs"> @Lang('newsletter.add') </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="portlet-body">
            @include('backend.elements.datatable')
        </div>
    </div>
@endsection
