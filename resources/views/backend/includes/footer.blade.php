<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> {!! date('Y') !!} &copy; {{ Setting::get('site_name') }}. Admin Panel</div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->