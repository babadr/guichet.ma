<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-light" data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200" style="padding-top: 40px">
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item start {{ Tools::activeMenu(['admin', '*/tickets', 'admin/dashboard/*']) }}">
                <a href="{{ route('backend.index') }}" class="nav-link {{ Tools::activeMenu(['admin']) }}">
                    <i class="icon-home"></i>
                    <span class="title">@Lang('menu.dashboard')</span>
                </a>
            </li>
            @if (Entrust::hasRole(['superadmin', 'admin']))
                <li class="nav-item">
                    <a href="{{ route('backend.export.all') }}" class="nav-link">
                        <i class="fa fa-download"></i>
                        <span class="title">Générer le rapport</span>
                    </a>
                </li>
            @endif
            {{--@if (Entrust::can('backend.current_event.read'))--}}
                {{--<li class="nav-item {{ Tools::activeMenu(['admin/current-events']) }}">--}}
                    {{--<a href="{{ route('backend.event.current') }}" class="nav-link">--}}
                        {{--<i class="fa fa-clock-o"></i>--}}
                        {{--<span class="title">Événements en cours</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--@endif--}}
            {{--@if (Entrust::hasRole(['superadmin', 'admin']))--}}
                {{--<li class="nav-item {{ Tools::activeMenu(['admin/scene', 'admin/scene/*']) }}">--}}
                    {{--<a href="{{ route('backend.scene.index') }}" class="nav-link">--}}
                        {{--<i class="fa fa-download"></i>--}}
                        {{--<span class="title">Les accès Mawazine</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            {{--@endif--}}
            @if (Entrust::can(['backend.order.*']))
                <li class="heading">
                    <h3 class="uppercase">@Lang('menu.store')</h3>
                </li>
                @if (Entrust::can('backend.order.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/order', 'admin/order/*']) }}">
                        <a href="{{ route('backend.order.index') }}" class="nav-link">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="title">@Lang('menu.orders')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::hasRole(['superadmin', 'admin']))
                    <li class="nav-item {{ Tools::activeMenu(['admin/recipient', 'admin/recipient/*']) }}">
                        <a href="{{ route('backend.recipient.index') }}" class="nav-link">
                            <i class="fa fa-credit-card"></i>
                            <span class="title">Liste des abonnés RAJA</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Tools::activeMenu(['admin/match', 'admin/match/*']) }}">
                        <a href="{{ route('backend.match.index') }}" class="nav-link">
                            <i class="fa fa-soccer-ball-o"></i>
                            <span class="title">Gestion des Matchs</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Tools::activeMenu(['admin/tickets', 'admin/tickets/*']) }}">
                        <a href="{{ route('backend.tickets.index') }}" class="nav-link">
                            <i class="fa fa-ticket"></i>
                            <span class="title">Gestion des tickets</span>
                        </a>
                    </li>
                @endif
            @endif
            @if (Entrust::can(['backend.provider.*', 'backend.category.*', 'backend.deal.*', 'backend.seller.*']))
                <li class="heading">
                    <h3 class="uppercase">@Lang('menu.deals')</h3>
                </li>
                @if (Entrust::can('backend.provider.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/provider', 'admin/provider/*']) }}">
                        <a href="{{ route('backend.provider.index') }}" class="nav-link">
                            <i class="icon-globe-alt"></i>
                            <span class="title">@Lang('menu.providers')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.seller.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/seller', 'admin/seller/*']) }}">
                        <a href="{{ route('backend.seller.index') }}" class="nav-link">
                            <i class="fa fa-map-marker"></i>
                            <span class="title">@Lang('menu.sellers')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.category.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/category', 'admin/category/*']) }}">
                        <a href="{{ route('backend.category.index') }}" class="nav-link">
                            <i class="icon-grid"></i>
                            <span class="title">@Lang('menu.categories')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.deal.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/deal', 'admin/deal/*']) }}">
                        <a href="{{ route('backend.deal.index') }}" class="nav-link">
                            <i class="icon-tag"></i>
                            <span class="title">@Lang('menu.deals')</span>
                        </a>
                    </li>
                @endif
            @endif
            @if (Entrust::can(['backend.contact.read', 'backend.newsletter.read']))
                <li class="heading">
                    <h3 class="uppercase">@Lang('menu.common')</h3>
                </li>
                @if (Entrust::can('backend.contact.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/contact', 'admin/contact/*']) }}">
                        <a href="{{ route('backend.contact.index') }}" class="nav-link">
                            <i class="icon-envelope"></i>
                            <span class="title">@Lang('menu.contacts')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.newsletter.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/newsletter', 'admin/newsletter/*']) }}">
                        <a href="{{ route('backend.newsletter.index') }}" class="nav-link">
                            <i class="icon-paper-plane"></i>
                            <span class="title">@Lang('menu.newsletters')</span>
                        </a>
                    </li>
                @endif
            @endif
            @if (Entrust::can(['backend.page.*', 'backend.post.*']))
                <li class="heading">
                    <h3 class="uppercase">@Lang('menu.content_management')</h3>
                </li>
                @if (Entrust::can('backend.page.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/page', 'admin/page/*']) }}">
                        <a href="{{ route('backend.page.index') }}" class="nav-link">
                            <i class="icon-doc"></i>
                            <span class="title">@Lang('menu.pages')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.post.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/category-post', 'admin/category-post/*']) }}">
                        <a href="{{ route('backend.category_post.index') }}" class="nav-link">
                            <i class="icon-tag"></i>
                            <span class="title">Magazine catégories</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.post.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/post', 'admin/post/*']) }}">
                        <a href="{{ route('backend.post.index') }}" class="nav-link">
                            <i class="icon-docs"></i>
                            <span class="title">Guichet Magazine</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.faq.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/faq', 'admin/faq/*']) }}">
                        <a href="{{ route('backend.faq.index') }}" class="nav-link">
                            <i class="icon-question"></i>
                            <span class="title">@Lang('menu.faq')</span>
                        </a>
                    </li>
                @endif
            @endif
            @if (Entrust::can(['backend.user.read', 'backend.role.*', 'backend.permission.*', 'backend.setting.*']))
                <li class="heading">
                    <h3 class="uppercase">@Lang('menu.administration')</h3>
                </li>
                @if (Entrust::can('backend.user.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/user', 'admin/user/*']) }}">
                        <a href="{{ route('backend.user.index') }}" class="nav-link">
                            <i class="icon-user"></i>
                            <span class="title">@Lang('menu.users')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.role.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/role', 'admin/role/*']) }}">
                        <a href="{{ route('backend.role.index') }}" class="nav-link">
                            <i class="icon-users"></i>
                            <span class="title">@Lang('menu.roles')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.permission.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/permission', 'admin/permission/*']) }}">
                        <a href="{{ route('backend.permission.index') }}" class="nav-link">
                            <i class="icon-lock"></i>
                            <span class="title">@Lang('menu.permissions')</span>
                        </a>
                    </li>
                @endif
                @if (Entrust::can('backend.setting.read'))
                    <li class="nav-item {{ Tools::activeMenu(['admin/setting', 'admin/setting/*']) }}">
                        <a href="{{ route('backend.setting.index') }}" class="nav-link">
                            <i class="icon-settings"></i>
                            <span class="title">@Lang('menu.settings')</span>
                        </a>
                    </li>
                @endif
            @endif
        </ul>
    </div>
</div>