@if (session()->has('flash_notification'))
    @foreach (session('flash_notification', collect())->toArray() as $message)
        <div class="note note-{{ $message['level'] }} alert">
            <button type="button" class="close" data-dismiss="alert"></button>
            <p>{!! $message['message'] !!}</p>
        </div>
    @endforeach
    {{ session()->forget('flash_notification') }}
@endif