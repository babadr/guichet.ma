<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="{{ route('backend.index') }}">
                <img src="/backend/img/logo-white-2.png" alt="{{ Setting::get('site_name') }}" class="logo-default"/> </a>
            <div class="menu-toggler sidebar-toggler">
                <span></span>
            </div>
        </div>
        <div class="page-actions">
            <div class="btn-group">
                <button type="button" class="btn btn-circle btn-outline white dropdown-toggle" data-toggle="dropdown">
                    <span>Actions rapides&nbsp;</span>&nbsp;<i class="fa fa-angle-down"></i>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="{{ route('frontend.index') }}" target="_blank">
                            <i class="icon-globe"></i> Aller sur le site
                        </a>
                    </li>
                    @if (Entrust::can('backend.user.create'))
                        <li>
                            <a href="{{ route('backend.user.create') }}">
                                <i class="icon-user"></i> Ajouter un utilisateur
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <img alt="" class="img-circle" src="{{ auth()->user()->avatar ? asset_cdn(auth()->user()->avatar) . '?w=30&h=30&fit=crop&auto=format,compress&q=80' : 'https://www.placehold.it/30x30/EFEFEF/AAAAAA&text=aucune+image' }}"/>
                        <span class="username username-hide-on-mobile"> {{ auth()->user()->full_name }} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{ route('backend.user.edit', ['id' => auth()->user()->id]) }}">
                                <i class="icon-user"></i> @Lang('app.my_profil') </a>
                        </li>
                        <li>
                            <a href="{{ route('backend.auth.logout') }}">
                                <i class="icon-logout"></i> @Lang('auth.logout') </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->