<input type="hidden" id="seo_id" name="seo_id" value="{{ $model->seo ? $model->seo->id : '' }}">
<div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
    <label class="col-md-3 control-label" for="seo_title">@Lang('seo.seo_title')</label>
    <div class="col-md-7 error-container">
        <input type="text" name="seo_title" id="seo_title"
               value="{{ old('seo_title') ? old('seo_title') : ($model->seo ? $model->seo->seo_title : '') }}"
               class="form-control"/>
        @if ($errors->has('seo_title'))
            <small id="seo-title-error"
                   class="has-error help-block help-block-error">{{ $errors->first('seo_title') }}</small>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('seo_alias') ? ' has-error' : '' }}">
    <label class="col-md-3 control-label" for="seo_alias">@Lang('seo.seo_alias')</label>
    <div class="col-md-7 error-container">
        <input type="text" name="seo_alias" id="seo_alias"
               value="{{ old('seo_alias') ? old('seo_alias') : ($model->seo ? $model->seo->seo_alias : '') }}"
               class="form-control"/>
        @if ($errors->has('seo_alias'))
            <small id="seo-alias-error"
                   class="has-error help-block help-block-error">{{ $errors->first('seo_alias') }}</small>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('seo_description') ? ' has-error' : '' }}">
    <label class="col-md-3 control-label" for="seo_description">@Lang('seo.seo_description')</label>
    <div class="col-md-7 error-container">
        <textarea type="text" name="seo_description" id="seo_description"
                  class="form-control">{{ old('seo_description') ? old('seo_description') : ($model->seo ? $model->seo->seo_description : '') }}</textarea>
        @if ($errors->has('seo_description'))
            <small id="seo-description-error"
                   class="has-error help-block help-block-error">{{ $errors->first('seo_description') }}</small>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('seo_keywords') ? ' has-error' : '' }}">
    <label class="col-md-3 control-label" for="seo_keywords">@Lang('seo.seo_keywords')</label>
    <div class="col-md-7 error-container">
        <textarea type="text" name="seo_keywords" id="seo_keywords"
                  class="form-control">{{ old('seo_description') ? old('seo_description') : ($model->seo ? $model->seo->seo_keywords : '') }}</textarea>
        @if ($errors->has('seo_description'))
            <small id="seo-keywords-error"
                   class="has-error help-block help-block-error">{{ $errors->first('seo_keywords') }}</small>
        @endif
    </div>
</div>
<div class="form-group {{ $errors->has('seo_robots') ? ' has-error' : '' }}">
    <label class="col-md-3 control-label" for="seo_robots">@Lang('seo.seo_robots')</label>
    <div class="col-md-3 error-container">
        <select name="seo_robots" id="seo_robots" class="form-control select2">
            <option {{ $model->seo && $model->seo->seo_robots == 'index, follow' ? 'selected' : '' }} value="index, follow">
                index, follow
            </option>
            <option {{ $model->seo && $model->seo->seo_robots == 'noindex, follow' ? 'selected' : '' }} value="noindex, follow">
                noindex, follow
            </option>
            <option {{ $model->seo && $model->seo->seo_robots == 'index, nofollow' ? 'selected' : '' }} value="index, nofollow">
                index, nofollow
            </option>
            <option {{ $model->seo && $model->seo->seo_robots == 'noindex, nofollow' ? 'selected' : '' }} value="noindex, nofollow">
                noindex, nofollow
            </option>
        </select>
        @if ($errors->has('seo_robots'))
            <small id="seo-robos-error"
                   class="has-error help-block help-block-error">{{ $errors->first('seo_robots') }}</small>
        @endif
    </div>
</div>