<input type="checkbox" class="make-switch" {!! $checked ? 'checked' : '' !!} data-on-color="success" data-off-color="warning" data-size="mini"
       data-on-text="@Lang('app.yes')" data-off-text="@Lang('app.no')" data-switch-field="{{ $field }}" data-switch-url="{{ route('backend.' . $modelName . '.switchActive', ['id' => $model->id]) }}">
