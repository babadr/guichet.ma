@if ($model->{$field})
    <label class="label label-success label-sm">@Lang('app.yes')</label>
@else
    <label class="label label-danger label-sm">@Lang('app.no')</label>
@endif