<table class="table table-striped table-bordered table-hover table-checkable dt-responsive" width="100%" id="app-datatable">
    <thead>
    <tr role="row" class="heading">
        @foreach ($columns as $key => $field)
            @if ($key == 'id')
                <th width="5%">{!! trans($field['title']) !!}</th>
            @else
                <th class="{!! isset($field['class']) ? $field['class'] : '' !!}">{!! trans($field['title']) !!}</th>
            @endif
        @endforeach
        <th width="5%">Actions</th>
    </tr>
    <tr role="row" class="filter">
        @foreach ($columns as $key => $field)
            <td width="{{ isset($field['width']) ? $field['width'] : '' }}">
                @if (!$field['filterKey'])
                    ---
                @endif
                @if (($field['type'] == 'int' || $field['type'] == 'text') && $field['filterKey'])
                    <input type="text" class="form-control form-filter input-sm" name="{!! $key !!}">
                @endif
                @if ($field['type'] == 'money' && $field['filterKey'])
                    <div class="margin-bottom-5">
                        <input type="text" class="form-control form-filter input-sm" name="{!! $key !!}_from" placeholder="Entre">
                    </div>
                    <div>
                        <input type="text" class="form-control form-filter input-sm" name="{!! $key !!}_to" placeholder="Et">
                    </div>
                @endif
                @if ($field['type'] == 'bool' && $field['filterKey'])
                    <select name="{!! $key !!}" class="form-control form-filter input-sm select2">
                        <option value="">---</option>
                        <option value="1">@Lang('app.yes')</option>
                        <option value="0">@Lang('app.no')</option>
                    </select>
                @endif
                @if ($field['type'] == 'select' && $field['filterKey'])
                    <select name="{!! $key !!}" class="form-control form-filter input-sm select2">
                        <option value="">---</option>
                        @foreach (${$field['list']} as $index => $value)
                            <option value="{{ $index }}">{{ $value }}</option>
                        @endforeach
                    </select>
                @endif
                @if ($field['type'] == 'date' && $field['filterKey'])
                    <div class="input-group date datepicker margin-bottom-5" data-date-format="dd/mm/yyyy">
                        <input type="text" class="form-control form-filter input-sm" readonly name="{!! $key !!}_from" placeholder="@Lang('app.From')">
                        <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                    </div>
                    <div class="input-group date datepicker" data-date-format="dd/mm/yyyy">
                        <input type="text" class="form-control form-filter input-sm" readonly name="{!! $key !!}_to" placeholder="@Lang('app.To')">
                        <span class="input-group-btn">
                                        <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                    </div>
                @endif
            </td>
        @endforeach
        <td>
            <div class="margin-bottom-5">
                <button class="btn btn-sm green btn-outline filter-submit margin-bottom"><i class="fa fa-search"></i> @Lang('app.Search')</button>
            </div>
            <button class="btn btn-sm red btn-outline filter-cancel"><i class="fa fa-times"></i> @Lang('app.Reset')</button>
        </td>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>

@section('stylesheets')
    <link href="{{ asset('/backend/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('scripts')
    <script src="{{ asset('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('/backend/js/datatable.ajax.js') }}" type="text/javascript"></script>
@endsection