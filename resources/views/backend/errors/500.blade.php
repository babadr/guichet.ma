@extends('layouts.backend')

@section('title')
    @Lang('app.error_500')
@endsection

@section('pageTitle')
    @Lang('app.error_500')
@endsection

@section('content')
    <div class="col-md-12 page-404">
        <div class="number font-red-sunglo"> 500</div>
        <div class="details">
            <h3>Oops! You're lost.</h3>
            <p>
                We can not find the page you're looking for.
            </p>
            <p>
                <a href="{{ route('backend.index') }}" class="btn red-sunglo btn-outline"> Return home </a>
            </p>
        </div>
    </div>
@endsection

@section('stylesheets')
    <link href="{{ asset('/backend/css/error.min.css') }}" rel="stylesheet" type="text/css"/>
@endsection