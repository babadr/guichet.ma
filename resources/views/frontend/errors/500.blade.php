@extends('layouts.frontend')

@section('content')
    <div class="content-area">
        <div id="content" class="site-content" role="main">
            <div class="hr-link orange">
                <hr class="mBtm-50 mTop-30">
                <h1 class="list-title">Not Found</h1>
            </div>
            <div class="page-wrapper">
                <div class="page-content">
                    <h2>This is somewhat embarrassing, isn’t it?</h2>
                    <p>It looks like nothing was found at this location.</p>
                </div><!-- .page-content -->
            </div><!-- .page-wrapper -->
        </div><!-- #content -->
    </div>
@endsection