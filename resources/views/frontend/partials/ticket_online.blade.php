<html>
<head>
    <style>
        @page {
            size: auto;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
        }

        .page {
            background-image: url('/frontend/images/ticket-online.jpg');
            background-size: 100%;
            background-repeat: no-repeat;
            background-position: left bottom;
            height: 100%;
            width: 100%;
            padding: 0;
            color: rgb(34, 34, 34);
            font-family: monospace;
            font-weight: 900;
        }

        .container {
            padding: 42px 57px;
        }

        .header {
            width: 100%;
            text-align: right;
            font-family: 'moskmedium';
            font-weight: 800;
            font-size: 22px;
        }

        .footer {
            width: 100%;
            margin-top: 20px;
        }

        .header .provider {
            margin-top: 15px;
            font-size: 30px;
        }

        .ticket-content {
            width: 65%;
            float: left;
            padding-top: 35px;
        }

        .ticket-code {
            width: 32%;
            float: right;
            text-align: center;
            padding-top: 35px;
        }

        .title {
            font-family: 'moskextrabold';
            text-transform: uppercase;
            font-size: 26px
        }

        .custom-date {
            font-family: 'moskmedium';
            font-size: 28px;
            text-transform: capitalize;
            margin-top: 5px;
        }

        .user {
            font-family: 'mosklight';
            font-size: 38px;
            margin-top: 20px;
            font-weight: 300;
        }

        .price {
            font-family: 'moskmedium';
            font-size: 22px;
        }

        .place {
            font-family: 'moskextrabold';
            font-size: 30px;
            width: 40%;
            float: left;
        }

        .ticket {
            width: 30%;
            float: left;
            font-size: 12px;
            margin-top: 5px;
        }

        .bought {
            font-family: 'moskmedium';
            width: 60%;
            float: left;
            font-size: 12px;
        }

        .sep {
            width: 100%;
            margin-top: -30px;
            font-size: 11px;
        }

        .help {
            width: 60%;
            float: left;
        }

        .map {
            width: 40%;
            float: left;
        }

        .help-title {
            font-family: 'moskmedium';
            font-size: 22px;
            margin-bottom: 10px;
        }

        .help ol li {
            padding: 0;
            margin: 0;
            line-height: 35px;
            font-size: 14px;
        }

        .logo-partner {
            max-width: 150px;
            max-height: 90px;
        }

        ul {
            padding: 0;
            margin: 0;
        }

        ul li {
            padding: 0;
            margin: 0;
            list-style-type: none;
        }

        .football-content-left {
            float: left;
            margin-top: 185px;
            margin-left: 10px;
            width: 50%;
        }

        .football-content-right {
            float: right;
            margin-right: 15px;
            margin-top: 105px;
            width: 40%;
            text-align: right;
        }

        .football-ticket-number {
            text-align: left;
            float: left;
            color: #fff;
            font-family: 'moskextrabold';
            font-size: 12px;
            margin-top: 60px;
        }

        .football-user {
            float: left;
            color: #fff;
            font-family: 'moskmedium';
            font-size: 24px;
            width: 100%;
            font-weight: bold;
            margin-top: 5px;
        }

        .football-title {
            float: left;
            color: #fff;
            font-family: 'moskmedium';
            font-size: 16px;
            width: 100%;
            margin-top: 10px;
        }

        .football-price {
            float: left;
            color: #fff;
            font-family: 'moskextrabold';
            font-size: 22px;
            width: 100%;
            margin-top: 38px;
        }

        .football-price div {
            float: left;
            width: 125px;
            text-align: center;
        }

        .football-seat {
            float: left;
            color: #fff;
            font-family: 'moskextrabold';
            font-size: 22px;
            width: 100%;
            margin-top: 32px;
        }

        .football-seat div {
            float: left;
            width: 82px;
            text-align: center;
        }

        .football-footer-left {
            float: left;
            margin-top: 460px;
            margin-left: 30px;
            width: 40%;
            font-family: 'moskextrabold';
            font-size: 12px;
        }

        .football-footer-right {
            float: right;
            width: 40%;
            text-align: right;
            margin-right: 40px;
        }

        .football-ticket-number-footer {
            margin-top: 40px;
            margin-left: 10px;
        }
    </style>
</head>

<body>

@foreach($order->lines as $line)
    @php
        $key = 0;
        $profiteers = $line->profiteers->toArray();
        $tickets = $line->tickets;
        if (isset($ticket_id) && $ticket_id) {
            foreach($tickets as $index => $ticket) {
                if ($ticket->id == $ticket_id) {
                    $key = $index;
                }
            }
            $tickets = $line->tickets()->where('id', $ticket_id)->get();
        }
    @endphp
    @foreach($tickets as $ticket)
        @if ($line->offer->deal->is_football)
            @php $footTicket = Tools::getTicket($line->offer_id, $ticket->hash) @endphp
            <style>
                .page-{{ $ticket->id }}  {
                    background-image: url({{ asset_cdn($line->offer->deal->custom_ticket) . '?w=1240&h=1754&fit=clip&auto=format,compress&q=80' }});
                }
            </style>
            <div class="page page-{{ $ticket->id }}">
                <div class="container">
                    <div class="football-content-left">
                        <div class="football-user">
                            @if(isset($profiteers[$key]))
                                {{ isset($profiteers[$key]['full_name']) ? $profiteers[$key]['full_name'] : $order->user->full_name }}<br>
                                {{ (isset($profiteers[$key]['is_minor']) && $profiteers[$key]['is_minor'] == 1) ? 'N° CIN Tuteur' : 'N° CIN' }} {{ isset($profiteers[$key]['cin']) ? $profiteers[$key]['cin'] : '' }}
                            @else
                                {{ $order->user->full_name }}<br>
                            @endif
                        </div>
                        <div class="football-title">
                            {{ $line->product_name }}<br>
                            {{ $line->offer->deal->address }}<br>
                            {{ $line->offer->deal->custom_field }}
                        </div>
                        <div class="football-price">
                            <div>
                                {{ Tools::displayPrice($line->product_price, 0) }}
                            </div>
                            <div>
                                {{ isset($footTicket['zone']) ? $footTicket['zone'] : '' }}
                            </div>
                        </div>
                        <div class="football-seat">
                            <div>
                                {{ isset($footTicket['access']) ? $footTicket['access'] : '' }}
                            </div>
                            <div>
                                {{ isset($footTicket['sector']) ? $footTicket['sector'] : '' }}
                            </div>
                            <div>
                                {{ isset($footTicket['seat']) ? $footTicket['seat'] : '' }}
                            </div>
                        </div>
                    </div>
                    <div class="football-content-right">
                        <div style="margin-top: 12px; margin-right: 55px">
                            <img src="{{ url('/tickets/barcode/'. $ticket->id . '/80/1.png') }}">
                        </div>
                        <div class="football-ticket-number">
                            TICKET {{ $ticket->number }} <br>
                            Achat {{ $order->created_at }}
                        </div>
                    </div>
                </div>
                <div class="football-footer-left">
                    <div class="football-ticket-number-footer">Achat {{ $order->created_at }}</div>
                    <div style="margin-left: 10px; font-size: 14px">TICKET {{ $ticket->number }}</div>
                </div>
                <div class="football-footer-right">
                    <img src="{{ url('/tickets/barcode/'. $ticket->id . '/80/1.png') }}">
                </div>
            </div>
        @else
            @if ($line->offer->deal->category_id == 15)
                <style>
                    .page-{{ $ticket->id }}  {
                        background-image: url('/frontend/images/ticket-online-mawazine.jpg');
                    }
                </style>
            @endif
            <div class="page page-{{ $ticket->id }}">
                <div class="container">
                    <div class="header">
                        <div>TICKET {{ $ticket->number }}</div>
                        <div class="provider">{{ $line->offer->deal->provider->title }}</div>
                    </div>
                    <div class="ticket-content">
                        <div class="title">{{ $line->product_name }}</div>
                        <div class="custom-date">
                            {{ $line->offer->deal->custom_field }}
                        </div>
                        <div class="user">
                            @if($order->user && !$order->offered)
                                @if(isset($profiteers[$key]))
                                    {{ isset($profiteers[$key]['full_name']) ? $profiteers[$key]['full_name'] : $order->user->full_name }}<br>
                                @else
                                    {{ $order->user->full_name }}
                                @endif
                            @endif
                            @if($order->offered && $order->offered_by)
                                Invitation offerte par {{ $order->offered_by }}
                            @endif
                            {{--{{ $order->user->full_name }}--}}
                        </div>
                        <div class="price">
                            @if($order->offered)
                                Ticket Offert
                            @else
                                {{ Tools::displayPrice($line->product_price, 0) }}
                            @endif
                            - <span>{{ $line->offer_name }}</span>
                        </div>
                    </div>
                    <div class="ticket-code">
                        @if ($line->offer->deal->barcode_type)
                            <div style="margin-bottom: 10px;font-family: 'mosklight';">
                                <img class="logo-partner"
                                     src="/imagecache/partner/{{ $line->offer->deal->provider->logo }}">
                                @if ($line->offer->deal->category_id == 15)
                                    <br>Du 21 au 29 juin 2019
                                @endif
                            </div>
                            <img src="{{ url('/tickets/barcode/'. $ticket->id . '/100.png') }}"><br>
                            <span>{{ strtoupper($ticket->hash) }}</span>
                        @else
                            @if ($line->offer->deal->category_id == 17)
                                <div style="margin-bottom: 10px;font-family: 'mosklight';">
                                    <img class="logo-partner"
                                         src="/imagecache/partner/{{ $line->offer->deal->provider->logo }}">
                                </div>
                                <img src="{{ url('/tickets/qrcode/'. $ticket->id . '/100.png') }}"><br>
                            @else
                                <img src="{{ url('/tickets/qrcode/'. $ticket->id . '/200.png') }}">
                            @endif
                        @endif
                    </div>
                    <div class="footer">
                        @if($ticket->seat)
                            <div class="place">
                                @if(($line->offer->deal->seating_plan_id == 8 || $line->offer->deal->seating_plan_id == 10 || $line->offer->deal->seating_plan_id == 13) && strtolower($line->offer_name) == 'balcon')
                                    <span style="font-size: 24px">Placement libre</span>
                                @else
                                    PLACE <span>{{ $ticket->seat }}</span>
                                @endif
                            </div>
                        @else
                            &nbsp;&nbsp;
                        @endif
                        {{--<div class="ticket">--}}
                        {{--TICKET {{ $ticket->number }} <br>--}}
                        {{--{{ $line->offer->deal->address }}--}}
                        {{--</div>--}}
                        <div class="bought">
                            Achat {{ $order->created_at }} <br>
                            <span style="font-size: 16px; font-family: 'moskextrabold';">{{ $line->offer->deal->address }}</span>
                        </div>
                        @if ($line->offer->deal->category_id == 17)
                            <div style="text-align: center; width: 100%; margin-top: 15px">
                                <span style="font-size: 16px; font-family: 'moskextrabold';color:green">Votre carte sera disponible au Raja Store Oasis Casablanca</span>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="sep">
                    <img src="/frontend/images/separator.png">
                </div>
                <div class="container" style="padding: 0 57px">
                    <div class="help">
                        <div class="help-title">Comment utiliser ce bon d'achat</div>
                        <ol>
                            <li>Imprimer ce ticket</li>
                            <li>Vérifier les conditions de l’offre</li>
                            <li>Présentez vous avec ce ticket</li>
                            <li>Amusez vous bien !</li>
                        </ol>
                    </div>
                    <div class="map">
                        <div class="help-title">Maps</div>
                        <div>
                            <img src="https://maps.googleapis.com/maps/api/staticmap?center={{ $line->offer->deal->latitude }},{{ $line->offer->deal->longitude }}&zoom=16&size=300x200&markers=color:red%7C{{ $line->offer->deal->latitude }},{{ $line->offer->deal->longitude }}&maptype=roadmap&key=AIzaSyDxgwZozKuD0KhVn7lF3pWMGeh0vHF3A74">
                        </div>
                    </div>
                </div>
                @if ($line->offer->deal->category_id == 15)
                    <div style="padding: 0 57px; font-size: 9px; text-align: justify; font-family: Calibri">
                        <div class="help-title">INFORMATIONS PRATIQUES :</div>
                        <ul>
                            <li>
                                Le public est informé que pendant les manifestations, il est susceptible d’être
                                photographié
                                et filmé (notamment en raison des retransmissions télévisées). Ces images pouvant par la
                                suite être utilisées par Maroc Cultures ainsi que par différents médias.
                            </li>
                            <li>
                                Il est strictement interdit d’introduire dans l’espace réservé aux détenteurs de
                                supports
                                d’accès tout objet présentant un danger pour autrui ou pour soi­même. L’introduction
                                d’animaux, de bouteilles, de boîtes métalliques, d’objets tranchants et/ou contondants,
                                de
                                chaises, de canettes et d’objets dangereux est strictement interdite.
                            </li>
                            <li>
                                Il est également interdit d’introduire des signes et banderoles de toute taille de
                                nature
                                politique, religieuse, idéologique ou publicitaire.
                            </li>
                            <li>
                                Les appareils photographiques, les caméras, les appareils enregistreurs ou autres sont
                                interdits. Toute photo est interdite, avec ou sans flash. Les spectateurs munis de ces
                                outils se verront confisquer leurs appareils à l’entrée ou à l’intérieur de l’enceinte
                                du
                                site.
                            </li>
                            <li>
                                Pour des raisons de sécurité, d’urgence ou pour assurer le bon fonctionnement du
                                concert,
                                les spectateurs doivent se conformer strictement aux instructions du personnel de
                                sécurité
                                ainsi qu’à toutes les mesures de contrôle et de vérification.
                            </li>
                            <li>
                                Le spectateur qui ne se conforme pas aux règles précitées pourra se voir refuser
                                l’entrée au
                                site ou se voir expulser.
                            </li>
                            <li>
                                L’acquisition de ce billet auprès d’un tiers autre que le réseau de l’association Maroc
                                Cultures, expose son détenteur à des risques de falsification et/ou de fraude empêchant
                                son
                                accès aux sites.
                            </li>
                            <li>
                                Présentation obligatoire de la pièce d'identité ayant servie à l'achat. ­ Seule la
                                première
                                copie du voucher est acceptée. ­ Nous vous conseillons de vous présenter au moins une
                                heure
                                avant sur le lieu du spectacle. ­ Les places que vous achetez sont debout sauf au
                                théâtre
                                Mohamed V. ­ Tous les spectateurs (enfants compris) doivent obligatoirement être munis
                                d'un
                                support d'accès. ­ Les enfants de moins de 15 ans munis d'un support doivent
                                impérativement
                                être accompagnés par un adulte. ­ Les spectateurs ayant pénétré dans l'enceinte du site
                                et
                                dont le billet a été contrôlé à l'entrée ne peuvent sortir que de manière définitive. ­
                                Le
                                billet ne peut être scanné qu'une seule fois. ­ La confidentialité et la protection des
                                données du billet relèvent strictement de l'acheteur. ­ En cas de tentative de fraude,
                                l'interessé sera automatiquement présenté aux services de police antifraude présents sur
                                place.
                            </li>
                        </ul>
                    </div>
                @endif
            </div>
        @endif
        @php
            $key++;
        @endphp
    @endforeach
@endforeach

</body>
</html>