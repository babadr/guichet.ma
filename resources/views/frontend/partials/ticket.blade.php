<html>
<head>
    <style>
        @page {
            size: auto;
            margin-top: 0;
            margin-right: 0;
            margin-bottom: 0;
            margin-left: 0;
        }

        .page {
            background-image: url('/frontend/images/ticket-enligne-guichet.png');
            background-size: 100%;
            background-repeat: no-repeat;
            height: 484px;
            width: 933px;
            padding: 0;
            color: rgb(34, 34, 34);
            font-family: monospace;
            font-weight: 900;
        }

        .ticket-left {
            float: left;
            width: 335px;
            padding-left: 35px;
            padding-top: 90px;
        }

        .ticket-center {
            float: left;
            width: 105px;
            padding-top: 10px;
        }

        .ticket-right {
            float: right;
            width: 120px;
            text-align: center;
            padding-top: 50px;
        }

        .container {
            height: 110px;
        }

        .provider {
            font-family: 'moskmedium';
            font-size: 11pt;
        }

        .title {
            font-family: 'moskextrabold';
            text-transform: uppercase;
        }

        .custom-date {
            font-family: 'moskmedium';
            font-size: 14pt;
            text-transform: capitalize;
        }

        .user {
            font-family: 'mosklight';
            font-size: 14pt;
            height: 25px;
        }

        .price {
            font-family: 'moskmedium';
            font-size: 12pt;
        }

        .place {
            font-family: 'moskextrabold';
            font-size: 20px;
            margin-left: 200px;
            height: 50px;
            margin-top: -25px;
            text-align: right;
        }

        .footer {
            font-family: 'moskmedium';
            font-size: 9pt;
            float: left;
            width: 72%;
            padding-left: 35px;
        }

        .footer div {
            width: 50%;
            float: left;
        }

        .ticket-right .address {
            font-family: 'moskmedium';
            font-size: 8pt;
            width: 90px;
            margin: 0 auto;
            height: 40px;
        }

        .ticket-right .date {
            font-family: 'moskextrabold';
            font-size: 8pt;
            width: 100px;
            margin: 0px auto;
            height: 50px;
        }

        .ticket-right .price {
            font-family: 'moskextrabold';
            font-size: 10pt !important;
        }

        .ticket-right .place {
            font-family: 'moskextrabold';
            font-size: 10.3pt;
            margin-top: 20px;
            text-align: center !important;
        }


    </style>
</head>

<body>

@foreach($order->lines as $line)
    @foreach($line->tickets as $ticket)
        {{--@if (!empty($line->offer->deal->custom_ticket))--}}
        {{--<style>--}}
        {{--.page {--}}
        {{--background-image: url({{ '/imagecache/original/' . $line->offer->deal->custom_ticket }});--}}
        {{--}--}}
        {{--</style>--}}
        {{--@endif--}}
        @if ($isOffile)
            <style>
                .page {
                    background-image: none;
                }
            </style>
        @endif
        <div class="page">
            <div class="ticket-left">
                <div class="container">
                    <div class="provider">
                        {{ $line->offer->deal->provider->title }}
                    </div>
                    @if (strlen($line->product_name) > 180)
                        <div class="title" style="font-size: 14px">
                            {{ $line->product_name }}
                        </div>
                    @elseif (strlen($line->product_name) > 160)
                        <div class="title" style="font-size: 16px">
                            {{ $line->product_name }}
                        </div>
                    @elseif (strlen($line->product_name) > 110)
                        <div class="title" style="font-size: 18px">
                            {{ $line->product_name }}
                        </div>
                    @elseif (strlen($line->product_name) > 80)
                        <div class="title" style="font-size: 18px">
                            {{ $line->product_name }}
                        </div>
                    @elseif (strlen($line->product_name) > 40)
                        <div class="title" style="font-size: 23px">
                            {{ $line->product_name }}
                        </div>
                    @else
                        <div class="title" style="font-size: 24px">
                            {{ $line->product_name }}
                        </div>
                    @endif
                    <div class="custom-date">
                        {{ str_limit($line->offer->deal->custom_field, 50, '...') }}
                    </div>
                </div>
                <div class="user">
                    @if($order->user && !$order->offered)
                        {{ $order->user->full_name }}
                    @endif
                    @if($order->offered && $order->offered_by)
                        Invitation offerte par {{ $order->offered_by }}
                    @endif
                </div>
                <div class="price">
                    @if($order->offered)
                        Ticket Offert
                    @else
                        {{ number_format($line->product_price, 0) }} Dhs
                    @endif
                    - <span>{{ $line->offer_name }}</span>
                </div>
                <div class="place">
                    @if($ticket->seat)
                        @if(($line->offer->deal->seating_plan_id == 8 || $line->offer->deal->seating_plan_id == 10 || $line->offer->deal->seating_plan_id == 13) && strtolower($line->offer_name) == 'balcon')
                            <span style="font-size: 16px">Placement libre</span>
                        @else
                            PLACE <span>{{ $ticket->seat }}</span>
                        @endif
                    @endif
                </div>
            </div>
            <div class="ticket-center">
                @if ($line->offer->deal->barcode_type)
                    <img src="{{ url('/tickets/barcode/'. $ticket->id . '/80.png') }}">
                @else
                    <img src="{{ url('/tickets/qrcode/'. $ticket->id . '/80.png') }}">
                @endif
            </div>
            <div class="ticket-right">
                @if ($line->offer->deal->barcode_type)
                    <img src="{{ url('/tickets/barcode/'. $ticket->id . '/50.png') }}">
                @else
                    <img src="{{ url('/tickets/qrcode/'. $ticket->id . '/50.png') }}">
                @endif
                <div class="address">
                    {{ str_limit($line->offer->deal->address, 65, '...') }}
                </div>
                <div class="date">
                    {{ str_limit($line->offer->deal->custom_field, 90, '...') }}
                </div>
                <div class="price">
                    @if($order->offered)
                        Ticket Offert
                    @else
                        {{ number_format($line->product_price, 0) }} Dhs
                    @endif
                    <br/>
                    <span>{{ $line->offer_name }}</span>
                </div>
                <div class="place">
                    @if($ticket->seat)
                        @if(($line->offer->deal->seating_plan_id == 8 || $line->offer->deal->seating_plan_id == 10 || $line->offer->deal->seating_plan_id == 13) && strtolower($line->offer_name) == 'balcon')
                            <span style="font-size: 9pt">Placement libre</span>
                        @else
                            PLACE <span>{{ $ticket->seat }}</span>
                        @endif
                    @endif
                </div>
            </div>
            <div class="footer">
                <div class="ticket">
                    TICKET : {{ $ticket->number }}
                </div>
                <div class="bought">
                    Achat {{ $order->created_at }}
                </div>
                <div class="address">
                    {{ str_limit($line->offer->deal->address, 100, '...') }}
                </div>
                <div class="address">
                    {{ str_limit($line->offer->deal->address, 100, '...') }}
                </div>
            </div>
        </div>
    @endforeach
@endforeach

</body>
</html>