<div class="inner-side shadow mTop-0">
    <div class="widget">
        <div class="hr-link orange">
            <hr class="mBtm-50 mTop-30">
            <h3 class="list-title">@lang('contact.contact_info')</h3>
        </div>
        <address>
            <p>
                <i class="fa fa-map-marker"></i> {{ Setting::get('address_1') }}
                <br>
                @if (Setting::has('address_2'))
                    {{ Setting::get('address_2') }}
                    <br>
                @endif
                {{ Setting::get('zip_code') . ' ' . Setting::get('city') . ' - ' . Setting::get('country') }}
            </p>
            <p><i class="fa fa-phone"></i> {{ Setting::get('phone') }}</p>
            <p><i class="fa fa-envelope"></i> {{ Setting::get('email') }}</p>
        </address>
    </div>
</div>