<div class="entry-share mTop-10">
    <div class="helper"> @lang('front.share_or_save_this_post') </div>
    <ul class="social social-share-btn">
        <li>
            <a class="social-btn social-facebook" target="_blank"
               href="{{ Share::load($share_url, $share_title)->facebook() }}">
                <i class="ti-facebook"></i>
            </a>
        </li>
        <li>
            <a class="social-btn social-twitter" target="_blank"
               href="{{ Share::load($share_url, $share_title)->twitter() }}">
                <i class="ti-twitter"></i>
            </a>
        </li>
        <li>
            <a class="social-btn social-pinterest" target="_blank"
               href="{{ Share::load($share_url, $share_title)->pinterest() }}">
                <i class="ti-pinterest"></i>
            </a>
        </li>
        <li>
            <a class="social-btn social-linkedin" target="_blank"
               href="{{ Share::load($share_url, $share_title)->linkedin() }}">
                <i class="ti-linkedin"></i>
            </a>
        </li>
        <li>
            <a class="social-btn social-google" target="_blank"
               href="{{ Share::load($share_url, $share_title)->gplus() }}">
                <i class="ti-google"></i>
            </a>
        </li>
    </ul>
    {{--<div class="like-btn">--}}
        {{--<iframe src="https://www.facebook.com/plugins/like.php?href={{ $share_url }}&width=85&layout=button_count&action=like&size=small&show_faces=false&share=false&height=21&appId"--}}
                {{--width="85" height="21" style="border:none;overflow:hidden" scrolling="no" frameborder="0"--}}
                {{--allowTransparency="true" allow="encrypted-media"></iframe>--}}
    {{--</div>--}}
</div>