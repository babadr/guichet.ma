<div class="widget widget-menu">
    <div class="hr-link orange">
        <hr class="mBtm-50 mTop-30">
        <h3 class="list-title">Deals Catégories</h3>
    </div>
    <ul class="nav sidebar-nav">
        @foreach($listCategories as $cat)
            @php $count = $cat->children()->count() @endphp
            @if(!$count)
                <li>
                    <a href="/{{ $cat->seo->seo_alias }}">
                        <i class="{{ $cat->icon }}"></i> {{ $cat->title }}
                    </a>
                </li>
            @else
                <li class="dropdown">
                    <a class="ripple-effect dropdown-toggle" href="#" data-toggle="dropdown">
                        <i class="{{ $cat->icon }}"></i> {{ $cat->title }}
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach($cat->children as $child)
                            <li>
                                <a href="/{{ $child->seo->seo_alias }}">
                                    <i></i> {{ $child->title }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endif
        @endforeach
    </ul>
</div>