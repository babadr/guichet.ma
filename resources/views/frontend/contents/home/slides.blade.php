<div class="slider">
    <div class="container">
        <div class="row">
            <div id="grid-slider" class="flexslider">
                <ul class="slides" id="front-deals-slider">
                    @foreach($slides as $deal)
                        @if(!$deal->end_at->isPast())
                            <li>
                                <div class="col-sm-7 col-lg-8 omega">
                                    <a href="/{{ $deal->seo->seo_alias }}">
                                        <article class="bg-image entry-lg" data-image-size="cover"
                                                 data-image-src="{{ asset_cdn($deal->cover) }}?w=900&h=580&fit=clip&auto=format,compress&q=80"></article>
                                    </a>
                                </div>
                                <div class="col-sm-5 col-lg-4 alpha entry-lg {{ $deal->category->color }}">
                                    <div class="buyPanel animated fadeInLeft bg-white shadow Aligner">
                                        <div class="content">
                                            <div class="deal-content">
                                                <a href="/{{ $deal->category->seo->seo_alias }}">
                                                    {{ $deal->category->title }}
                                                </a>
                                                <h5><i class="fa fa-map-marker"></i> {{ $deal->city->city }}</h5>
                                                <a href="/{{ $deal->seo->seo_alias }}">
                                                    <h3 class="mTop-0"> {{ $deal->title }} </h3>
                                                </a>
                                                @if(strlen($deal->title) < 150)
                                                    <p> {{ str_limit($deal->short_description, 190 - strlen($deal->title)) }} </p>
                                                @endif
                                            </div>
                                            <ul class="deal-price list-unstyled list-inline">
                                                <li class="price">
                                                    <h3> {{ $deal->price }} DH </h3>
                                                </li>
                                                <li class="buy-now">
                                                    @if($deal->end_at->isPast() || !$deal->checkAvailability())
                                                        <a href="/{{ $deal->seo->seo_alias }}"
                                                           class="btn btn-default btn-raised btn-sm disabled">
                                                            @lang('front.sold_out')
                                                        </a>
                                                    @else
                                                        <a href="/{{ $deal->seo->seo_alias }}"
                                                           class="btn btn-primary btn-raised ripple-effect">
                                                            @lang('front.buy_now')
                                                        </a>
                                                    @endif
                                                </li>
                                            </ul>
                                            <div class="dealAttributes">
                                                @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                                    <div class="valueInfo bg-light shadow">
                                                        <div class="value">
                                                            <p class="value"> {{ $deal->old_price }} DH </p>
                                                            <p class="text"> @lang('front.value') </p>
                                                        </div>
                                                        <div class="discount">
                                                            <p class="value"> {{ $deal->discount }}% </p>
                                                            <p class="text"> @lang('front.discount') </p>
                                                        </div>
                                                        <div class="save">
                                                            <p class="value"> {{ $deal->saving }} DH </p>
                                                            <p class="text"> @lang('front.saving') </p>
                                                        </div>
                                                    </div>
                                                @endif
                                                <div class="timeLeft text-center">
                                                    <p class="hidden"> @lang('front.hurry_up_only_few_deals_left') </p>
                                                    <span class="time clock-countdown"
                                                          data-date="{{ $deal->end_at->format('m/d/Y H:i:s') }}">
                                                        @if($deal->end_at->isFuture())
                                                            <i class="ti-timer color-green"> </i>
                                                            <b class="days"></b> j
                                                            <b class="hours"></b> h
                                                            <b class="minutes"></b> m
                                                            <b class="seconds"></b> s
                                                        @endif
                                                    </span>
                                                </div>
                                                <ul class="statistic list-unstyled list-inline">
                                                    @if(!$deal->category->isTicketing())
                                                        <li>
                                                            <i class="ti-tag"></i>
                                                            <b>{{ $deal->getOrderedQuantity() }}</b>
                                                            {{ trans_choice('front.bought', $deal->getOrderedQuantity()) }}
                                                        </li>
                                                    @endif
                                                    <li>
                                                        <i class="ti-face-smile"></i>
                                                        @lang('front.offer_success')
                                                    </li>
                                                </ul>
                                                @include('frontend.partials.share', ['share_url' => config('app.url') . $deal->seo->seo_alias, 'share_title' =>  $deal->title ])
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /#buypanel -->
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>