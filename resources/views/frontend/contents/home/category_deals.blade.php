@if (!Agent::isMobile())
    <div class="row">
        @endif
        @php $index = 1 @endphp
        @foreach($deals as $key => $deal)
            @if(!$deal->end_at->isPast())

                <div class="col-sm-3">

                    <div class="deal-entry {{ $color }}">
                        @if(!empty($deal->discount))
                            <div class="offer-discount">
                                -{{ $deal->discount }}%
                            </div>
                        @endif
                        <div class="image">
                            <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                @if(Agent::isMobile())
                                    <img src="{{ asset_cdn($deal->miniature) }}?w=340&h=200&fit=clip&auto=format,compress&q=80"
                                         alt="{{ $deal->title }}" class="img-responsive">
                                @else
                                    <img src="{{ asset_cdn($deal->miniature) }}?w=305&h=176&fit=clip&auto=format,compress&q=80"
                                         alt="{{ $deal->title }}" class="img-responsive">
                                @endif
                            </a>
                            @if(!$deal->category->isTicketing())
                                <span class="bought">
                            <i class="ti-tag"></i>
                                    {{ $deal->getOrderedQuantity() }}
                        </span>
                            @endif
                        </div>
                        <div class="title">
                            <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                {{ $deal->title }}
                            </a>
                            <h5 class="event-city"><i class="fa fa-map-marker"></i>{{ $deal->city->city }}</h5>
                        </div>
                    {{--<div class="entry-content">--}}
                    {{--<div class="prices clearfix">--}}
                    {{--@if(!$deal->category->isTicketing() && !empty($deal->discount))--}}
                    {{--<div class="procent">--}}
                    {{---{{ $deal->discount }}%--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    {{--<div class="price">--}}
                    {{--<b> {{ $deal->price }} DH </b>--}}
                    {{--</div>--}}
                    {{--@if(!$deal->category->isTicketing() && !empty($deal->discount))--}}
                    {{--<div class="old-price">--}}
                    {{--<span> {{ $deal->old_price }} DH </span>--}}
                    {{--</div>--}}
                    {{--@endif--}}
                    {{--</div>--}}
                    {{--<p> {{ str_limit(strip_tags($deal->short_description), 100) }} </p>--}}
                    {{--</div>--}}
                    <!--/.entry content -->
                        <footer class="info_bar clearfix">
                            <ul class="unstyled list-inline row">
                                <li class="time col-sm-7 col-xs-6 col-lg-8 clock-countdown"
                                    data-date="{{ $deal->end_at->format('m/d/Y H:i:s') }}">
                                    @if($deal->end_at->isFuture())
                                        <i class="ti-timer"> </i>
                                        <b class="days"></b> j
                                        <b class="hours"></b> h
                                        <b class="minutes"></b> m
                                        <b class="seconds"></b> s
                                    @endif
                                </li>
                                <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                                    <div class="prices">
                                        {{--@if(!empty($deal->discount))--}}
                                        {{--<div class="procent">--}}
                                        {{---{{ $deal->discount }}%--}}
                                        {{--</div>--}}
                                        {{--@endif--}}
                                        <div class="price">
                                            <b> {{ $deal->price }} DH </b>
                                        </div>
                                        {{--@if(!empty($deal->discount))--}}
                                        {{--<div class="old-price">--}}
                                        {{--<span> {{ $deal->old_price }} DH </span>--}}
                                        {{--</div>--}}
                                        {{--@endif--}}
                                    </div>
                                    {{--@if($deal->end_at->isPast() || !$deal->checkAvailability())--}}
                                    {{--<a href="/{{ $deal->seo->seo_alias }}"--}}
                                    {{--class="btn btn-default btn-raised btn-sm disabled">--}}
                                    {{--@lang('front.sold_out')--}}
                                    {{--</a>--}}
                                    {{--@else--}}
                                    {{--<a href="/{{ $deal->seo->seo_alias }}"--}}
                                    {{--class="btn btn-block btn-primary btn-raised btn-sm">--}}
                                    {{--@lang('deal.buy_now')--}}
                                    {{--</a>--}}
                                    {{--@endif--}}
                                </li>
                            </ul>
                        </footer>
                        <div class="btn-buy-event">
                            @if($deal->end_at->isPast() || !$deal->checkAvailability())
                                <a href="/{{ $deal->seo->seo_alias }}"
                                   class="btn btn-default btn-raised btn-sm disabled">
                                    @lang('front.sold_out')
                                </a>
                            @else
                                <a href="/{{ $deal->seo->seo_alias }}"
                                   class="btn btn-block btn-primary btn-raised btn-sm">
                                    @lang('deal.buy_now')
                                </a>
                            @endif
                        </div>
                    </div>
                </div>

                @if($index == 4 && !Agent::isMobile())
    </div>

    <div class="row">
        @php $index = 0 @endphp
        @endif
        @php $index++ @endphp
        @endif
        @endforeach

        @if (!Agent::isMobile())
    </div>
@endif

