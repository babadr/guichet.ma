@extends('layouts.frontend')

@section('content')
    <div class="row category-feed">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-12 clearfix">
                    <div class="hr-link orange">
                        <hr class="mBtm-50 mTop-30">
                        <h2 class="list-title">Les événements en cours</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @if(!$events->count())
                    <div class="col-sm-12">
                        Guichet.ma vous proposera des événements très prochainement pour cette catégorie.
                    </div>
                @endif
                @foreach($events as $key => $deal)
                    <div class="col-sm-4">
                        <div class="deal-entry orange">
                            @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                <div class="offer-discount">
                                    -{{ $deal->discount }}%
                                </div>
                            @endif
                            <div class="image">
                                <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                    <img src="{{ $deal->miniature }}" alt="{{ $deal->title }}" class="img-responsive">
                                </a>
                                @if(!$deal->category->isTicketing())
                                    <span class="bought">
                                            <i class="ti-tag"></i>
                                        {{ $deal->getOrderedQuantity() }}
                                        </span>
                                @endif
                            </div>
                            <!-- /.image -->
                            <div class="title">
                                <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                    {{ $deal->title }}
                                </a>
                                <h5 class="event-city"><i class="fa fa-map-marker"></i> {{ $deal->city->city }}</h5>
                            </div>
                            <div class="entry-content">
                                <div class="prices clearfix">
                                    @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                        <div class="procent">
                                            -{{ $deal->discount }}%
                                        </div>
                                    @endif
                                    <div class="price">
                                        <b> {{ $deal->price }} DH </b>
                                    </div>
                                    @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                        <div class="old-price">
                                            <span> {{ $deal->old_price }} DH </span>
                                        </div>
                                    @endif
                                </div>
                                <p> {{ str_limit(strip_tags($deal->short_description), 90) }} </p>
                            </div>
                            <!--/.entry content -->
                            <footer class="info_bar clearfix">
                                <ul class="unstyled list-inline row">
                                    <li class="time col-sm-7 col-xs-6 col-lg-8 clock-countdown"
                                        data-date="{{ $deal->end_at->format('m/d/Y H:i:s') }}">
                                        @if($deal->end_at->isFuture())
                                            <i class="ti-timer"> </i>
                                            <b class="days"></b> j
                                            <b class="hours"></b> h
                                            <b class="minutes"></b> m
                                            <b class="seconds"></b> s
                                        @endif
                                    </li>
                                    <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                                        @if($deal->end_at->isPast() || !$deal->checkAvailability())
                                            <a href="/{{ $deal->seo->seo_alias }}"
                                               class="btn btn-default btn-raised btn-sm disabled">
                                                @lang('front.sold_out')
                                            </a>
                                        @else
                                            <a href="/{{ $deal->seo->seo_alias }}"
                                               class="btn btn-block btn-primary btn-raised btn-sm">
                                                @lang('deal.buy_now')
                                            </a>
                                        @endif
                                    </li>
                                </ul>
                            </footer>
                        </div>
                    </div>
                    @if(($key + 1) % 3 == 0)
            </div>
            <div class="row">
                @endif
                @endforeach
            </div>
            <div class="col-sm-12 clearfix paginate">
                {{ $events->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
@endsection
