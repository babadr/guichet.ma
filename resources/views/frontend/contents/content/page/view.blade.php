@extends('layouts.frontend')


@section('content')
    <div class="row">
        <div class="col-sm-8 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">{{ $page->title }}</h1>
                </div>
                <div class="site-content">
                    {!! $page->content !!}
                </div>
            </div>
        </div>
        <div class="col-sm-4 sidebar">

        </div>
    </div>
@endsection

