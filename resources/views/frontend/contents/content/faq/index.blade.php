@extends('layouts.frontend')


@section('content')
    <div class="row">
        <div class="col-sm-8 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">@lang('faq.faqs')</h1>
                </div>
                <div class="site-content">
                    <p>
                        Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Nulla vitae elit libero, a
                        pharetra augue. Maecenas faucibus mollis interdum. Donec id elit non mi porta gravida at eget metus.
                    </p>
                    <div class="divide30"></div>
                    <div class="panel-group" id="faq-accordion">
                        @foreach($faqs as $index => $faq)
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" class="panel-toggle {{ $index == 0 ? '' : 'collapsed' }}" data-parent="#faq-accordion"
                                           href="#collapse-{{ $index + 1 }}">
                                            {{ $index + 1 }}. {{ $faq->title }}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse-{{ $index + 1 }}" class="panel-collapse {{ $index == 0 ? 'in' : 'collapse' }}">
                                    <div class="panel-body">
                                        {!! $faq->content !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4 sidebar">

        </div>
    </div>
@endsection

