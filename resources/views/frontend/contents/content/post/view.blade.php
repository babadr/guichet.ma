@extends('layouts.frontend')


@section('content')
    <div class="row">
        <div class="col-md-12 mTop-30">
            <div class="hr-link orange">
                <hr class="mBtm-50 mTop-30">
                <h1 class="list-title">Guichet Magazine</h1>
            </div>
            <a href="/magazine"
               class="btn btn-raised btn-default {{Tools::activeMenu(['magazine'], [' btn-warning'])}}">
                Tous
            </a>
            @foreach($categories as $category)
                <a href="/{{ $category->seo->seo_alias }}"
                   class="btn btn-raised btn-default {{Tools::activeMenu([$category->seo->seo_alias, $category->seo->seo_alias . '/*'], [' btn-warning'])}}">
                    {{ $category->name }}
                </a>
            @endforeach
        </div>
        <div class="col-sm-12 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">{{ $post->title }}</h1>
                </div>
                <div class="site-content">
                    <div class="entry post">
                        @if ($post->image)
                            <div class="entry-thumbnail">
                                <a href="/{{ $post->seo->seo_alias }}">
                                    {{--<div class="image "--}}
                                         {{--style="background-image: url('{{ asset_cdn($post->image) }}?w=900&h=600&fit=clip&auto=format,compress&q=80'); height: 475px"--}}
                                         {{--data-width="845" data-height="475">--}}
                                    {{--</div>--}}
                                    <img src="{{ asset_cdn($post->image) }}?w=900&h=600&fit=clip&auto=format,compress&q=80" width="100%">
                                </a>
                            </div>
                        @endif
                        <div class="entry-data">
                            <div class="entry-header">
                                <div class="entry-meta">
                                    <span class="date">
                                        <a href="/{{ $post->seo->seo_alias }}"
                                           title="{{ $post->title }}"
                                           rel="bookmark">
                                            <time class="entry-date"
                                                  datetime="{{ $post->created_at->format('Y-m-d - H:i:s') }}">{{ $post->created_at->format('d/m/Y - H:i') }}</time>
                                        </a>
                                    </span>
                                </div>
                            </div>
                            <div class="entry-content">
                                {!! $post->content !!}
                            </div>
                        </div>
                    </div>
                    <nav class="navigation post-navigation clearfix">
                        <div class="nav-links">
                            @if ($previous)
                                <a href="/{{ $previous->seo->seo_alias }}"
                                   rel="prev"><i class="prev"></i>
                                    <div class="prev-post">
                                        <span class="helper">@lang('post.previous_post')</span>
                                        <span class="title">{{ $previous->title }}</span>
                                    </div>
                                </a>
                            @endif
                            @if ($next)
                                <a href="/{{ $next->seo->seo_alias }}"
                                   rel="next"><i class="next"></i>
                                    <div class="next-post">
                                        <span class="helper">@lang('post.next_post')</span>
                                        <span class="title">{{ $next->title }}</span>
                                    </div>
                                </a>
                            @endif
                        </div><!-- .nav-links -->
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

