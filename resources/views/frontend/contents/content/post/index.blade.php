@extends('layouts.frontend')


@section('content')
    <div class="row">
        <div class="col-sm-12 mTop-30">
            <div class="clearfix">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">Guichet Magazine</h1>
                </div>
                <div class="site-content archive">
                    <div class="col-sm-12 mBtm-20">
                        <a href="/magazine"
                           class="btn btn-raised btn-default {{Tools::activeMenu(['magazine'], [' btn-warning'])}}">
                            Tous
                        </a>
                        @foreach($categories as $category)
                            <a href="/{{ $category->seo->seo_alias }}"
                               class="btn btn-raised btn-default {{Tools::activeMenu([$category->seo->seo_alias, $category->seo->seo_alias . '/*'], [' btn-warning'])}}">
                                {{ $category->name }}
                            </a>
                        @endforeach
                    </div>
                    @foreach($posts as $index => $post)
                        <div class="col-sm-4">
                            <div class="deal-entry entry post">
                                <div class="image">
                                    <a href="/{{ $post->seo->seo_alias }}" title="{{ $post->title }}">
                                        <img src="{{ asset_cdn($post->image) }}?w=480&h=240&fit=clip&auto=format,compress&q=80"
                                             alt="{{ $post->title }}" class="img-responsive">
                                    </a>
                                    @if($post->category)
                                        <span class="bought">
                                        <i class="ti-tag">
                                        </i>
                                            {{ $post->category->name }}
                                    </span>
                                    @endif
                                </div>
                                <div class="title">
                                    <h2 class="entry-title">
                                        <a href="/{{ $post->seo->seo_alias }}" title="{{ $post->title }}">
                                            {{ $post->title }}
                                        </a>
                                    </h2>
                                </div>
                                <div class="entry-content">
                                    <div class="prices clearfix">
                                        <div class="post-date">
                                            Publié le : {{ $post->created_at->format('d/m/Y - H:i') }}
                                        </div>
                                    </div>
                                    <p>
                                        {{ str_limit(strip_tags($post->content), 130) }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-sm-4 entry post shadow">--}}
                        {{--@if ($post->image)--}}
                        {{--<div class="entry-thumbnail">--}}
                        {{--<a href="/{{ $post->seo->seo_alias }}">--}}
                        {{--<div class="image "--}}
                        {{--style="background-image: url('{{ asset_cdn($post->image) }}?w=900&h=600&fit=clip&auto=format,compress&q=80'); height: 475px"--}}
                        {{--data-width="845" data-height="475">--}}
                        {{--</div>--}}
                        {{--</a>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--<div class="entry-data">--}}
                        {{--<div class="entry-header">--}}
                        {{--<h2 class="entry-title">--}}
                        {{--<a href="/{{ $post->seo->seo_alias }}" rel="bookmark">{{ $post->title }}</a>--}}
                        {{--</h2>--}}
                        {{--<div class="entry-meta">--}}
                        {{--<span class="date">--}}
                        {{--<a href="/{{ $post->seo->seo_alias }}"--}}
                        {{--title="{{ $post->title }}"--}}
                        {{--rel="bookmark">--}}
                        {{--<time class="entry-date"--}}
                        {{--datetime="{{ $post->created_at->format('Y-m-d - H:i:s') }}">{{ $post->created_at->format('d/m/Y - H:i') }}</time>--}}
                        {{--</a>--}}
                        {{--</span>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="entry-content">--}}
                        {{--<p>{{ str_limit(strip_tags($post->content), 300) }}</p>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                    @endforeach
                </div>
                <div class="col-sm-12 clearfix paginate">
                    {{ $posts->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

