@extends('layouts.frontend')

@section('content')
    <section id="inner-page" class="container">
        <div class="row">
            <div class="col-lg-8 col-sm-7">
                <div class="post-media">
                    <div id="slider" class="flexslider">
                        <ul class="slides">
                            @foreach($medias as $media)
                                <li>
                                    <img class="img-responsive" alt="{{ $deal->title }}"
                                         src="{{ asset_cdn($media->filename) }}?w=900&h=600&fit=clip&auto=format,compress&q=80">
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @if (count($medias) > 1)
                        <div id="carousel" class="flexslider">
                            <ul class="slides slide-thumbs">
                                @foreach($medias as $media)
                                    <li>
                                        <img class="img-responsive" alt="{{ $deal->title }}"
                                             src="{{ asset_cdn($media->filename) }}?w=150&h=90&fit=clip&auto=format,compress&q=80">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                @if (!empty($deal->custom_field))
                    <div class="smallFrame shadow bg-white">
                        <i class="ti-calendar"></i>
                        <div class="content">
                            {!! $deal->custom_field !!}
                        </div>
                    </div>
                @endif
                <div class="row mTop-20">
                    <div class="col-sm-12 col-lg-12">
                        <div class="widget-inner bg-white shadow mBtm-20">
                            <div role="tabpanel" id="tabs" class="tabbable responsive">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#tab-description" aria-controls="home" role="tab" data-toggle="tab">
                                            @lang('front.description')
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tab-map" aria-controls="map" role="tab" data-toggle="tab">
                                            @lang('front.map_and_direction')
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tab-reviews" aria-controls="reviews" role="tab" data-toggle="tab">
                                            @lang('front.reviews')
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="tab-content">
                                            <div class="tab-pane fade active in" id="tab-description">
                                                {!! preg_replace('/(<[^>]+) style=".*?"/i', '$1', $deal->description) !!}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="tab-map">
                                                <div class="google-maps" id="map_canvas"></div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="tab-reviews">
                                                {{--<div id="fb-root"></div>--}}
                                                {{--<script>(function (d, s, id) {--}}
                                                        {{--var js, fjs = d.getElementsByTagName(s)[0];--}}
                                                        {{--if (d.getElementById(id)) return;--}}
                                                        {{--js = d.createElement(s);--}}
                                                        {{--js.id = id;--}}
                                                        {{--js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.12';--}}
                                                        {{--fjs.parentNode.insertBefore(js, fjs);--}}
                                                    {{--}(document, 'script', 'facebook-jssdk'));</script>--}}
                                                {{--<div class="fb-comments" data-href="{{ Request::url() }}"--}}
                                                     {{--data-mobile="true" data-width="100%" data-numposts="5"></div>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="widget-inner bg-white shadow">
                    <div class="buyPanel animated fadeInLeft bg-white Aligner text-center">
                        <div class="content">
                            <div class="deal-content">
                                <h1> {{ $deal->title }} </h1>
                                <p> {{ strip_tags($deal->short_description) }} </p>
                            </div>
                            @if ($offers->count() > 1)
                                <div class="icheck-list col-xs-12">
                                    @foreach($offers as $offer)
                                        <label>
                                            <input type="radio" name="offer_id"
                                                   {{ ($deal->end_at->isPast() || !$offer->checkAvailability(1)) ? 'disabled' : '' }} value="{{ $offer->id }}"
                                                   class="icheck offer-icheck"
                                                   {{ $offer->default ? 'checked' : '' }}
                                                   data-radio="iradio_line-orange"
                                                   data-label="{{ $offer->title . '<span class="new-price">' . $offer->price . ' DH</span>'}} {{ $offer->old_price ? '<span class="old-price">' . $offer->old_price . ' DH</span>' : '' }}">
                                        </label>
                                    @endforeach
                                </div>
                            @else
                                <div class="price">
                                    <h2> {!! $deal->old_price ? '<span class="old-price-event">' . $deal->old_price . ' DH</span>' : '' !!} {{ $deal->price }} DH </h2>
                                </div>
                            @endif
                            <div class="buy-now mBtm-10 col-xs-12">
                                @if($deal->end_at->isPast() || !$deal->checkAvailability())
                                    <a class="btn btn-default btn-lg btn-raised ripple-effect btn-block disabled">
                                        @lang('front.sold_out')
                                    </a>
                                @else
                                    @if (empty($deal->seating_plan_id))
                                        <div class="quantity-wanted">
                                            <label for="quantity-wanted"
                                                   class="visible-xs visible-sm">@lang('deal.quantity_wanted')</label>
                                            <span class="qty-ii">
                                            <a href="#" data-field-qty="qty" class="button-minus product_quantity_down"> <span><i
                                                            class="fa fa-minus-square-o"></i></span> </a>
                                            <input min="1" name="qty" id="quantity-wanted" class="text" value="1">
                                            <a href="#" data-field-qty="qty"
                                               class="button-plus product_quantity_up"> <span><i
                                                            class="fa fa-plus-square-o"></i></span> </a>
                                        </span>
                                        </div>
                                        <div class="lab-cart">
                                            <a href="{{ route('frontend.cart.add') }}" id="ajax-add-to-cart"
                                               data-id-offer="{{ $deal->defaultOffer()->id }}"
                                               class="btn btn-success btn-lg btn-raised ripple-effect btn-block add-to-cart">
                                                @lang('front.buy_now') <span class="link animate"></span>
                                            </a>
                                        </div>
                                    @else
                                        <a href="{{ route('frontend.deal.seat', ['id' => $deal->id]) }}"
                                           class="btn btn-success btn-lg btn-raised ripple-effect btn-block view-seating-plan">
                                            Achetez via le plan <span class="link animate"></span>
                                        </a>
                                    @endif
                                @endif
                            </div>
                            <div class="deal-attributes col-xs-12">
                                @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                    <div class="valueInfo bg-light shadow">
                                        <div class="value">
                                            <p class="value"> {{ $deal->old_price }} DH </p>
                                            <p class="text"> @lang('front.value') </p>
                                        </div>
                                        <div class="discount">
                                            <p class="value"> {{ $deal->discount }}% </p>
                                            <p class="text"> @lang('front.discount') </p>
                                        </div>
                                        <div class="save">
                                            <p class="value"> {{ $deal->saving }} DH </p>
                                            <p class="text"> @lang('front.saving') </p>
                                        </div>
                                    </div>
                                @endif
                                <div class="timeLeft text-center">
                                    @if(!$deal->end_at->isPast())
                                        <p> @lang('front.hurry_up_only_few_deals_left') </p>
                                        <span class="time clock-countdown"
                                              data-date="{{ $deal->end_at->format('m/d/Y H:i:s') }}">
                                          <i class="ti-timer color-green"> </i>
                                          <b class="days"></b> j
                                          <b class="hours"></b> h
                                          <b class="minutes"></b> m
                                          <b class="seconds"></b> s
                                    </span>
                                    @endif
                                </div>
                                @include('frontend.partials.share', ['share_url' => config('app.url') . $deal->seo->seo_alias, 'share_title' =>  $deal->title ])
                            </div>
                        </div>
                    </div>
                </div>
                @if(!$deal->category->isTicketing())
                    <div class="smallFrame shadow bg-white">
                        <i class="ti-shopping-cart"></i>
                        <div class="content">
                            <strong>
                                {{ $deal->getOrderedQuantity() }} Achats
                            </strong>
                        </div>
                    </div>
                @endif
                <div class="terms-and-conditions bg-white shadow mTop-20">
                    <div class="widget-inner">
                        <div class="hr-link">
                            <hr class="mBtm-50 mTop-30">
                            <h2 class="list-title">@lang('deal.provider_info')</h2>
                        </div>
                        <div class="content">
                            <img class="img-responsive" alt="{{ $deal->provider->title }}"
                                 src="{{ asset_cdn($deal->provider->logo) }}?w=200&h=150&fit=clip&auto=format,compress&q=80">
                            <br/>
                            <strong>
                                {{ $deal->provider->title }}
                            </strong>
                            <br/><br/>
                            @if ($deal->provider->user)
                                @if (!empty($deal->provider->user->address))
                                    <strong>Adresse : </strong>{{ $deal->provider->user->address }}<br/>
                                @endif
                                @if (!empty($deal->provider->user->city))
                                    <strong>Ville : </strong>{{ $deal->provider->user->city }}<br/>
                                @endif
                            @endif
                            <strong>Tél. : </strong>{{ $deal->provider->phone }}
                        </div>
                    </div>
                </div>
                {{--@if ($deals->count())--}}
                    {{--<div class="inner-side shadow suggestion">--}}
                        {{--<div class="widget-featured">--}}
                            {{--<div class="hr-link">--}}
                                {{--<hr class="mBtm-50 mTop-30">--}}
                                {{--<h2 class="list-title">Autres événements</h2>--}}
                            {{--</div>--}}
                            {{--@foreach($deals as $event)--}}
                                {{--<div class="media media-sm entry-rating">--}}
                                    {{--<div class="media-left">--}}
                                        {{--<a href="/{{ $event->seo->seo_alias }}" title="{{ $event->title }}">--}}
                                            {{--<img src="{{ asset_cdn($event->miniature) }}?w=120&h=80&fit=clip&auto=format,compress&q=80" alt="{{ $event->title }}"--}}
                                                 {{--class="media-object">--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    {{--<div class="media-body">--}}
                                        {{--<h5 class="media-heading">--}}
                                            {{--<a href="/{{ $event->seo->seo_alias }}">--}}
                                                {{--{{ $event->title }}--}}
                                            {{--</a>--}}
                                        {{--</h5>--}}
                                        {{--<div class="timeLeft text-center">--}}
                                            {{--<span class="time clock-countdown"--}}
                                                  {{--data-date="{{ $event->end_at->format('m/d/Y H:i:s') }}">--}}
                                                {{--<i class="ti-timer color-green"> </i>--}}
                                                {{--<b class="days"></b> j--}}
                                                {{--<b class="hours"></b> h--}}
                                                {{--<b class="minutes"></b> m--}}
                                                {{--<b class="seconds"></b> s--}}
                                            {{--</span>--}}
                                        {{--</div>--}}
                                        {{--<ul class="list-inline">--}}
                                            {{--<li>--}}
                                                {{--<p class="price">--}}
                                                    {{--<a href="/{{ $event->seo->seo_alias }}">--}}
                                                        {{--@lang('deal.buy_now')--}}
                                                    {{--</a>--}}
                                                {{--</p>--}}
                                            {{--</li>--}}
                                        {{--</ul>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endif--}}

            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        window.lat = '{{ $deal->latitude }}';
        window.lng = '{{ $deal->longitude }}';
    </script>
    <script src="{{ asset_cdn('/frontend/js/map-picker.js') }}" type="text/javascript"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ Setting::get('google_map_api_key') }}&callback=initMap"></script>
    @if (!empty($deal->seating_plan_id))
        <script src="{{ asset_cdn('/frontend/js/seat.js') }}" type="text/javascript"></script>
        <script>
            jQuery(document).ready(function () {
                window.selectedCartSeats = "{{ route('frontend.cart.selectedSeats', ['id' => $deal->id]) }}";
                window.reservedSeats = "{{ route('frontend.deal.reservedSeats', ['id' => $deal->id]) }}";
                window.rows = {!! json_encode(config('enums.seating_rows')[$deal->seating_plan_id]) !!};
                window.mapCharts = {!! json_encode(config('enums.seating_map')[$deal->seating_plan_id]) !!};
                window.seats = {!! json_encode($seat_offers) !!};
                window.items = {!! json_encode($items) !!};
                SeatingPlan.init();
            });
        </script>
    @endif
@endsection