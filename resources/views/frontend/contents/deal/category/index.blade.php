@extends('layouts.frontend')

@section('slides')
    @include('frontend.contents.home.slides')
@endsection

@section('content')
    @if (in_array($category->id, [6, 7]))
        <div class="row mTop-30 clearfix">
            <div class="col-sm-12 text-center">
                <a href="https://bit.ly/2ZWSbQ3" target="_blank">
                    @if (Agent::isMobile())
                        <img src="{{ asset('frontend/images/hit_radio_320_100.gif') }}" alt="">
                    @else
                        <img src="{{ asset('frontend/images/hit_radio_970_90.gif') }}" alt="">
                    @endif
                </a>
            </div>
        </div>
    @endif
    <div class="row category-feed">
        <div class="col-sm-12">
            @if ($deals->count() > 0)
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <div class="hr-link {{ $category->color }}">
                            <hr class="mBtm-50 mTop-30">
                            <h1 class="list-title">{{ $category->title }}</h1>
                        </div>
                    </div>
                </div>
            @endif
            <div class="article-feed">
	
                <div class="row">
                    @foreach($deals as $key => $deal)
                        <div class="col-sm-4">
                            <div class="deal-entry {{ $category->color }}">
                                @if(!empty($deal->discount))
                                    <div class="offer-discount">
                                        -{{ $deal->discount }}%
                                    </div>
                                @endif
                                <div class="image">
                                    <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                        <img src="{{ asset_cdn($deal->miniature) }}?w=480&h=240&fit=clip&auto=format,compress&q=80" alt="{{ $deal->title }}" class="img-responsive">
                                    </a>
                                    @if(!$deal->category->isTicketing())
                                        <span class="bought">
                                            <i class="ti-tag"></i>
                                            {{ $deal->getOrderedQuantity() }}
                                        </span>
                                    @endif
                                </div>
                                <div class="title">
                                    <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                        {{ $deal->title }}
                                    </a>
                                    <h5 class="event-city"><i class="fa fa-map-marker"></i>{{ $deal->city->city }}</h5>
                                </div>
                                <div class="entry-content">
                                    <div class="prices clearfix">
                                        @if(!empty($deal->discount))
                                            <div class="procent">
                                                -{{ $deal->discount }}%
                                            </div>
                                        @endif
                                        <div class="price">
                                            <b> {{ $deal->price }} DH </b>
                                        </div>
                                        @if(!empty($deal->discount))
                                            <div class="old-price">
                                                <span> {{ $deal->old_price }} DH </span>
                                            </div>
                                        @endif
                                    </div>
                                    <p> {{ str_limit(strip_tags($deal->short_description), 90) }} </p>
                                </div>
                                <!--/.entry content -->
                                <footer class="info_bar clearfix">
                                    <ul class="unstyled list-inline row">
                                        <li class="time col-sm-7 col-xs-6 col-lg-8 clock-countdown"
                                            data-date="{{ $deal->end_at->format('m/d/Y H:i:s') }}">
                                            @if($deal->end_at->isFuture())
                                                <i class="ti-timer"> </i>
                                                <b class="days"></b> j
                                                <b class="hours"></b> h
                                                <b class="minutes"></b> m
                                                <b class="seconds"></b> s
                                            @endif
                                        </li>
                                        <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                                            @if($deal->end_at->isPast() || !$deal->checkAvailability())
                                                <a href="/{{ $deal->seo->seo_alias }}"
                                                   class="btn btn-default btn-raised btn-sm disabled">
                                                    @lang('front.sold_out')
                                                </a>
                                            @else
                                                <a href="/{{ $deal->seo->seo_alias }}"
                                                   class="btn btn-block btn-primary btn-raised btn-sm">
                                                    @lang('deal.buy_now')
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                        </div>
                        @if(($key + 1) % 3 == 0)
                            </div><div class="row">
                        @endif
                    @endforeach
                </div>
                <div class="col-sm-12 clearfix paginate">
                    {{--{{ $deals->links() }}--}}
                </div>
            </div>

            <div class="article-feed" style="float: left;">
                @if ($events->count() > 0)
                    <div class="row">
                        <div class="col-sm-12 clearfix">
                            <div class="hr-link {{ $category->color }}">
                                <hr class="mBtm-50 mTop-30">
                                <h2 class="list-title">Les événements passés</h2>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    @foreach($events as $key => $deal)
                        <div class="col-sm-4">
                            <div class="deal-entry {{ $category->color }}">
                                @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                    <div class="offer-discount">
                                        -{{ $deal->discount }}%
                                    </div>
                                @endif
                                <div class="image">
                                    <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                        <img src="{{ asset_cdn($deal->miniature) }}?w=480&h=240&fit=clip&auto=format,compress&q=80" alt="{{ $deal->title }}" class="img-responsive">
                                    </a>
                                    @if(!$deal->category->isTicketing())
                                        <span class="bought">
                                            <i class="ti-tag"></i>
                                            {{ $deal->getOrderedQuantity() }}
                                        </span>
                                    @endif
                                </div>
                                <!-- /.image -->
                                <div class="title">
                                    <a href="/{{ $deal->seo->seo_alias }}" title="{{ $deal->title }}">
                                        {{ $deal->title }}
                                    </a>
                                    <h5 class="event-city"><i class="fa fa-map-marker"></i> {{ $deal->city->city }}</h5>
                                </div>
                                <div class="entry-content">
                                    <div class="prices clearfix">
                                        @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                            <div class="procent">
                                                -{{ $deal->discount }}%
                                            </div>
                                        @endif
                                        <div class="price">
                                            <b> {{ $deal->price }} DH </b>
                                        </div>
                                        @if(!$deal->category->isTicketing() && !empty($deal->discount))
                                            <div class="old-price">
                                                <span> {{ $deal->old_price }} DH </span>
                                            </div>
                                        @endif
                                    </div>
                                    <p> {{ str_limit(strip_tags($deal->short_description), 90) }} </p>
                                </div>
                                <!--/.entry content -->
                                <footer class="info_bar clearfix">
                                    <ul class="unstyled list-inline row">
                                        <li class="time col-sm-7 col-xs-6 col-lg-8 clock-countdown"
                                            data-date="{{ $deal->end_at->format('m/d/Y H:i:s') }}">
                                            @if($deal->end_at->isFuture())
                                                <i class="ti-timer"> </i>
                                                <b class="days"></b> j
                                                <b class="hours"></b> h
                                                <b class="minutes"></b> m
                                                <b class="seconds"></b> s
                                            @endif
                                        </li>
                                        <li class="info_link col-sm-5 col-xs-6 col-lg-4">
                                            @if($deal->end_at->isPast() || !$deal->checkAvailability())
                                                <a href="/{{ $deal->seo->seo_alias }}"
                                                   class="btn btn-default btn-raised btn-sm disabled">
                                                    @lang('front.sold_out')
                                                </a>
                                            @else
                                                <a href="/{{ $deal->seo->seo_alias }}"
                                                   class="btn btn-block btn-primary btn-raised btn-sm">
                                                    @lang('deal.buy_now')
                                                </a>
                                            @endif
                                        </li>
                                    </ul>
                                </footer>
                            </div>
                        </div>
                        @if(($key + 1) % 3 == 0)
                            </div><div class="row">
                        @endif
                    @endforeach
                </div>
                <div class="col-sm-12 clearfix paginate">
                    {{ $events->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
