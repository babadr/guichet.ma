@extends('layouts.frontend')

@section('content')
    <div class="hr-link orange">
        <hr class="mBtm-50 mTop-30">
        <h1 class="list-title">détails de votre panier</h1>
    </div>
    <div>
        @if(Cart::count())
            <div id="cart-container">
                @include('frontend.contents.store.cart.items', ['order', $order])
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="inner-side shadow mTop-0">
                        <div class="hr-link orange">
                            <hr class="mBtm-50 mTop-30">
                            <h2 class="list-title">Votre compte</h2>
                        </div>
                        @if (auth()->check())
                            <form id="user-form" class="mBtm-20 form-validate"
                                  action="{{ route('frontend.order.updateUser') }}" method="post"
                                  novalidate="novalidate">
                            {{ csrf_field() }}
                            <!-- Row -->
                                <div class="row">
                                    <!-- Col -->
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                            <label for="last_name">Nom <span class="required">*</span></label>
                                            <input type="text" name="last_name" id="last_name" required
                                                   class="form-control" placeholder="Votre nom"
                                                   value="{{ $user->last_name or old('last_name') }}">
                                            @if ($errors->has('last_name'))
                                                <small id="last_name-error"
                                                       class="has-error help-block help-block-error">{{ $errors->first('last_name') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- /Col -->
                                    <!-- Col -->
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <label for="first_name">Prénom <span class="required">*</span></label>
                                            <input type="text" name="first_name" id="first_name" required
                                                   class="form-control" placeholder="Votre prénom"
                                                   value="{{ $user->first_name or old('first_name') }}">
                                            @if ($errors->has('first_name'))
                                                <small id="first_name-error"
                                                       class="has-error help-block help-block-error">{{ $errors->first('first_name') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- /Col -->
                                    <!-- Col -->
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <label for="phone">Téléphone <span class="required">*</span></label>
                                            <input type="text" name="phone" id="phone" required data-rule-phone="true"
                                                   class="form-control" placeholder="Votre téléphone"
                                                   value="{{ $user->phone or old('phone') }}">
                                            @if ($errors->has('phone'))
                                                <small id="phone-error"
                                                       class="has-error help-block help-block-error">{{ $errors->first('phone') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="phone">E-mail <span class="required">*</span></label>
                                            <input type="text" name="email" id="email" required data-rule-email="true"
                                                   class="form-control" placeholder="Votre e-mail"
                                                   value="{{ $user->email or old('email') }}">
                                            @if ($errors->has('email'))
                                                <small id="email-error"
                                                       class="has-error help-block help-block-error">{{ $errors->first('email') }}</small>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-success btn-raised ripple-effect">
                                            <i class="ti-check"></i>
                                            Mettre à jour
                                        </button>
                                    </div>
                                </div>
                                <!-- /Row -->
                            </form>
                        @else
                            <p>Veuillez vous authentifier ou créer un nouveau compte pour pouvoir passer votre commande.</p>
                            <div class="panel-group" id="account-accordion">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" aria-expanded="false"
                                               aria-controls="collapse-register" class="panel-toggle collapsed"
                                               data-parent="#account-accordion"
                                               href="#collapse-register">
                                                <span class="ti-plus"></span>
                                                Avez-vous déjà un compte sur guichet.ma ?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse-register" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <form id="login-form" class="login-form clearfix form-validate"
                                                  action="{{ route('frontend.auth.login') }}" method="post"
                                                  novalidate="novalidate">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                            <label for="auth-email">
                                                                @lang('contact.email')
                                                                <span class="required">*</span>
                                                            </label>
                                                            <input type="text" name="email" id="auth-email" required
                                                                   data-rule-email="true"
                                                                   class="form-control"
                                                                   placeholder="@lang('contact.your_email')"
                                                                   value="{{ old('email') }}">
                                                            @if ($errors->has('email'))
                                                                <small id="auth-email-error"
                                                                       class="has-error help-block help-block-error">
                                                                    {{ $errors->first('email') }}
                                                                </small>
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                            <label for="auth-password">
                                                                @lang('user.password')
                                                                <span class="required">*</span>
                                                            </label>
                                                            <input type="password" name="password" id="auth-password"
                                                                   required
                                                                   class="form-control"
                                                                   placeholder="@lang('user.password')"
                                                                   value="{{ old('password') }}">
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="login-button clearfix">
                                                    <input type="submit"
                                                           class="btn btn-raised ripple-effect btn-success"
                                                           id="submit-login" value="Se connecter">
                                                    <a class="btn btn-link"
                                                       href="{{ route('frontend.auth.password.showForgot') }}">
                                                        @lang('user.forget_your_password')
                                                    </a>
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" aria-expanded="true"
                                               aria-controls="collapse-account"
                                               class="panel-toggle collapsed"
                                               data-parent="#account-accordion"
                                               href="#collapse-account">
                                                <span class="ti-minus"></span>
                                                Vous n'êtes pas inscrit sur guichet.ma ?
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse-account" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <form id="registration-form"
                                                  class="registration-form clearfix form-validate"
                                                  action="{{ route('frontend.auth.showRegister') }}" method="post"
                                                  novalidate="novalidate">
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                            <label for="first_name">
                                                                @lang('user.first_name')
                                                                <span class="required">*</span>
                                                            </label>
                                                            <input type="text" name="first_name" id="first_name"
                                                                   required
                                                                   class="form-control"
                                                                   placeholder="@lang('user.first_name')"
                                                                   value="{{ old('first_name') }}">
                                                            @if ($errors->has('first_name'))
                                                                <small id="first_name-error"
                                                                       class="has-error help-block help-block-error">
                                                                    {{ $errors->first('first_name') }}
                                                                </small>
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                            <label for="last_name">
                                                                @lang('user.last_name')
                                                                <span class="required">*</span>
                                                            </label>
                                                            <input type="text" name="last_name" id="last_name" required
                                                                   class="form-control"
                                                                   placeholder="@lang('user.last_name')"
                                                                   value="{{ old('last_name') }}">
                                                            @if ($errors->has('last_name'))
                                                                <small id="last_name-error"
                                                                       class="has-error help-block help-block-error">
                                                                    {{ $errors->first('last_name') }}
                                                                </small>
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                                            <label for="city">
                                                                @lang('user.city')
                                                            </label>
                                                            <input type="text" name="city" id="city"
                                                                   class="form-control" placeholder="@lang('user.city')"
                                                                   value="{{ old('city') }}">
                                                            @if ($errors->has('city'))
                                                                <small id="city-error"
                                                                       class="has-error help-block help-block-error">
                                                                    {{ $errors->first('city') }}
                                                                </small>
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                            <label for="phone">
                                                                @lang('user.phone')
                                                                <span class="required">*</span>
                                                            </label>
                                                            <input type="text" name="phone" id="phone" required
                                                                   data-role-phone="true"
                                                                   class="form-control"
                                                                   placeholder="@lang('user.phone')"
                                                                   value="{{ old('phone') }}">
                                                            @if ($errors->has('phone'))
                                                                <small id="phone-error"
                                                                       class="has-error help-block help-block-error">
                                                                    {{ $errors->first('phone') }}
                                                                </small>
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                                            <label for="email">
                                                                @lang('user.email')
                                                                <span class="required">*</span>
                                                            </label>
                                                            <input type="text" name="email" id="email" required
                                                                   data-rule-email="true"
                                                                   class="form-control"
                                                                   placeholder="@lang('user.your_email')"
                                                                   value="{{ old('email') }}">
                                                            @if ($errors->has('email'))
                                                                <small id="email-error"
                                                                       class="has-error help-block help-block-error">
                                                                    {{ $errors->first('email') }}
                                                                </small>
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                        <p class="input-block form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                            <label for="password">
                                                                @lang('user.password')
                                                                <span class="required">*</span>
                                                            </label>
                                                            <input type="password" name="password" id="password"
                                                                   required
                                                                   class="form-control"
                                                                   placeholder="@lang('user.password')"
                                                                   value="{{ old('password') }}">
                                                            @if ($errors->has('password'))
                                                                <small id="password-error"
                                                                       class="has-error help-block help-block-error">
                                                                    {{ $errors->first('password') }}
                                                                </small>
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <p class="registration-button clearfix">
                                                    <button type="submit"
                                                            class="btn btn-raised ripple-effect btn-success"
                                                            id="submit-registration">
                                                        <i class="ti-check"></i>
                                                        S'inscrire
                                                    </button>
                                                </p>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="col-sm-6">
                    <form id="checkout-payment" action="{{ route('frontend.order.payment') }}" method="post"
                          class="mTop-10 form-validate">
                        {{ csrf_field() }}
                        <div class="inner-side shadow mTop-0">
                            <div class="hr-link orange">
                                <hr class="mBtm-40 mTop-30">
                                <h2 class="list-title">Laisser un message</h2>
                                <p>Si vous souhaitez ajouter un commentaire sur votre commande, veuillez l'écrire dans
                                    le
                                    champ ci-dessous.</p>
                                <p class="textarea-block form-group mBtm-20">
                                <textarea rows="5" cols="88" name="comment" id="comment"
                                          placeholder="@lang('contact.your_message')"
                                          class="form-control">{{ $order ? $order->comment : old('comment')  }}</textarea>
                                </p>
                                <div class="input-block form-group">
                                    <label class="mt-checkbox mt-checkbox-outline">
                                        <input type="checkbox" value="1" name="cgv" required data-msg-required="Veuillez accepter les conditions générales de vente"/>
                                        <span>J'ai lu <a href="/mentions-legales" target="_blank">les conditions générales de vente</a> et j'y adhère sans réserve.</span>
                                    </label>
                                </div>
                            </div>
                            <div class="hr-link orange">
                                <hr class="mBtm-40 mTop-30">
                                <h2 class="list-title">Mode de paiement</h2>
                                <p>Sélectionnez votre moyen de paiement :</p>
                                <div class="inputs-collapse">
                                    <div class="form-group radio-group">
                                        <div>
                                            <div>
                                                <input type="radio" {{ $default_payment == 1 ? 'checked' : '' }} name="payment_method" required data-msg-required="Le moyen de paiement est obligatoire. Merci de le renseigner." id="payment-method-1" value="1">
                                                <label for="payment-method-1">Paiement sécurisé en ligne par carte bancaire avec notre partenaire FPay  </label>
                                            </div>
                                            <div class="panel panel-box no-padding no-margin collapse {{ $default_payment == 1 ? 'in' : '' }}">
                                                <div class="pad-v-25 pad-h-25 mar-bottom-15">
                                                    <p class="no-margin">
                                                        Vous allez procéder à un paiement sécurisé d’un montant de <b><span class="shop-total-paid">{{ Cart::total() }} DH</span></b> à Guichet.ma <br>
                                                        Vous serez redirigé sur le site de notre partenaire FPay.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div>--}}
                                            {{--<div>--}}
                                                {{--<input type="radio" {{ $default_payment == 2 ? 'checked' : '' }} name="payment_method" required data-msg-required="Le moyen de paiement est obligatoire. Merci de le renseigner." id="payment-method-2" value="2">--}}
                                                {{--<label for="payment-method-2">Paiement sécurisé en ligne via PAYEXPRESS (Cash Plus)</label>--}}
                                            {{--</div>--}}
                                            {{--<div class="panel panel-box no-padding no-margin collapse {{ $default_payment == 2 ? 'in' : '' }}">--}}
                                                {{--<div class="pad-v-25 pad-h-25 mar-bottom-15">--}}
                                                    {{--<p class="no-margin">--}}
                                                        {{--Vous allez procéder à un paiement sécurisé d’un montant de <b><span class="shop-total-paid">{{ Cart::total() }} DH</span></b> à Guichet.ma <br>--}}
                                                        {{--Vous serez redirigé sur le site de notre partenaire Paypal.--}}
                                                    {{--</p>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        @if(!$hasSeat)
                                            <div>
                                                <div>
                                                    <input type="radio" {{ $default_payment == 3 ? 'checked' : '' }} name="payment_method" required data-msg-required="Le moyen de paiement est obligatoire. Merci de le renseigner." id="payment-method-3" value="3">
                                                    <label for="payment-method-3">Paiement par virement / versement bancaire </label>
                                                </div>
                                                <div class="panel panel-box no-padding no-margin collapse {{ $default_payment == 3 ? 'in' : '' }}">
                                                    <div class="pad-v-25 pad-h-25 mar-bottom-15">
                                                        <p class="mar-bottom-20">
                                                            Vous avez choisi de payer votre commande par virement / versement bancaire. <br>
                                                            Le montant s’élève à <b><span class="shop-total-paid">{{ Cart::total() }} DH</span></b>. <br>
                                                            <br />
                                                            Cliquez sur "Passer ma commande" pour valider votre commande et obtenir nos coordonnées bancaires pour effectuer votre virement / versement.<br />
                                                            <strong>Attention, votre commande ne sera validée que lorsque votre paiement sera effectif dans un délai de 24h.</strong>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Cart Buttons -->
                        <div class="cart-buttons clearfix">
                            <button type="submit"
                                    class="btn btn-raised btn-primary ripple-effect checkout" {{ auth()->check() ? '' : 'disabled' }}>
                                <i class="ti-shopping-cart"></i>
                                @lang('cart.checkout')
                            </button>
                            {{--<a class="btn btn-raised btn-warning ripple-effect checkout"--}}
                               {{--href="{{ route('frontend.index') }}">--}}
                                {{--<i class="ti-arrow-circle-left"></i>--}}
                                {{--@lang('cart.continue_shopping')--}}
                            {{--</a>--}}
                        </div>
                        <!-- /Cart Buttons -->
                    </form>
                </div>
            </div>
        @else
            <div class="panel-body frameLR bg-white shadow space-sm">Votre panier est vide. <a
                        href="{{ route('frontend.index') }}">@lang('cart.continue_shopping')</a></div>
        @endif
    </div>
@endsection

@section('plugins')
    <script src="{{ asset_cdn('/backend/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
@endsection

