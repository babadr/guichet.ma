<!-- Cart -->
<div class="cart shadow">
    <!-- Cart Contents -->
    <table class="cart-contents" id="cart-items">
        <thead>
        <tr>
            <th class="hidden-xs text-center">
                Image
            </th>
            <th>
                @lang('cart.product')
            </th>
            <th class="text-center">
                @lang('cart.quantity')
            </th>
            <th class="hidden-xs text-center">
                @lang('cart.price')
            </th>
            <th class="text-center">
                @lang('cart.total')
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach(Cart::content() as $key => $row)
            <tr>
                <td class="image hidden-xs">
                    <a href="{{ $row->options->url }}" class="title">
                        <img src="{{ asset_cdn($row->options->image) }}?w=120&h=80&fit=clip&auto=format,compress&q=80"
                             alt="">
                    </a>
                </td>
                <td class="details">
                    <div class="clearfix">
                        <div class="pull-left product-details">
                            <a href="{{ $row->options->url }}" class="title">
                                {{ $row->name }}
                            </a>
                            @if($row->options->has('offer'))
                                <span>{{ $row->options->offer }} @if ($row->options->reservation)
                                        : Prix {{ number_format($row->options->reservation, 0, ',', ' ') }} DH (Paiement
                                        de {{ number_format($row->price, 0, ',', ' ') }} DH sur le site)@endif</span>
                            @endif
                            @if($row->options->has('seats'))
                                <h6>Places sélectionnées : {{ $row->options->seats }}</h6>
                            @endif
                        </div>
                        <div class="action pull-left">
                            <div class="clearfix">
                                <button title="Supprimer"
                                        data-href="{{ route('frontend.cart.delete', ['id' => $key]) }}"
                                        class="btn-danger btn-raised ripple-effect remove-cart-item">
                                    <i class="ti-trash"></i>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mTop-20 mBtm-20">
                                @php $isFootball = Tools::checkFootball($row->id) @endphp
                                @for ($i = 1; $i <= $row->qty; $i++)
                                    <h6>Bénéficiaire - {{ $i }}</h6>
                                    <div class="row profiteer-group">
                                        @if ($isFootball)
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                        Est-il mineur ? &nbsp;&nbsp;&nbsp;
                                                    </label>
                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                        <input type="radio" class="check-profiteer"
                                                               id="items_{{ $row->id }}_{{ $i }}_is_minor_no"
                                                               name="items[{{ $row->id }}][{{ $i }}][is_minor]"
                                                               value="No"
                                                               checked> Non
                                                    </label>&nbsp;&nbsp;&nbsp;
                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                        <input type="radio" class="check-profiteer"
                                                               id="items_{{ $row->id }}_{{ $i }}_is_minor_yes"
                                                               name="items[{{ $row->id }}][{{ $i }}][is_minor]"
                                                               value="Yes"> Oui
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" name="items[{{ $row->id }}][{{ $i }}][full_name]"
                                                       id="items_{{ $row->id }}_{{ $i }}_full_name" required
                                                       class="form-control profiteer" maxlength="65"
                                                       placeholder="Nom & Prénom *" value="">
                                            </div>
                                        </div>
                                        @if ($isFootball)
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" name="items[{{ $row->id }}][{{ $i }}][cin]"
                                                           id="items_{{ $row->id }}_{{ $i }}_cin" required
                                                           class="form-control profiteer cin" maxlength="20"
                                                           placeholder="N° CIN *" value="">
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endfor
                                @if ($isFootball)
                                    <div class="row">
                                        <small class="col-md-12" style="color: red; font-weight: bold;">* Pour les
                                            mineurs, il est obligatoire de renseigner le N° CIN du tuteur
                                        </small>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </td>
                <td class="qty">
                    @if($row->options->has('seats'))
                        <input type="text" value="{{ $row->qty }}" disabled readonly name="quantity"
                               class="quantity-wanted">
                    @else
                        <input type="text" value="{{ $row->qty }}" name="quantity" id="quantity-wanted-{{ $key }}"
                               class="quantity-wanted">
                        <div class="cart-quantity-button clearfix">
                            <a rel="nofollow" data-id-cart="{{ $key }}"
                               class="cart-quantity-down btn btn-default button-minus"
                               href="{{ route('frontend.cart.update', ['id' => $key]) }}"
                               title="Soustraire"> <span><i class="fa fa-minus"></i></span>
                            </a>
                            <a rel="nofollow" data-id-cart="{{ $key }}"
                               class="cart-quantity-up btn btn-default button-plus"
                               href="{{ route('frontend.cart.update', ['id' => $key]) }}"
                               title="Ajouter"><span><i class="fa fa-plus"></i></span>
                            </a>
                        </div>
                    @endif
                </td>
                <td class="unit-price hidden-xs text-center">
                    {{ number_format($row->price, 0, ',', ' ') }} DH
                </td>
                <td class="total-price text-center">
                    {{ number_format($row->total, 0, ',', ' ') }} DH
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <!-- /Cart Contents -->
    <!-- Cart Summary -->
    <table class="cart-summary" width="100%">
        <tbody>
        <tr>
            <td class="terms">
                {{--<h5>--}}
                {{--<i class="ti-info-alt"></i>--}}
                {{--our return policy--}}
                {{--</h5>--}}
                {{--<p>--}}
                {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut--}}
                {{--labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco--}}
                {{--laboris nisi ut aliquip.--}}
                {{--</p>--}}
            </td>
            <td class="totals">
                <table class="cart-totals">
                    <tbody>
                    <tr>
                        <td>
                            @lang('cart.sub_total')
                        </td>
                        <td class="price text-center">
                            {{ Cart::subtotal() }} DH
                        </td>
                    </tr>
                    <tr>
                        <td class="cart-total">
                            @lang('cart.global_amount')
                        </td>
                        <td class="cart-total price text-center">
                            {{ Cart::total() }} DH
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <!-- /Cart Summary -->
</div>
<!-- /Cart -->

