<div class="modal fade" id="confirm-add-to-cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    @lang('cart.success_add')
                </h4>
            </div>
            <div class="modal-body">
                <div class="deal-entry green deal-entry-sm col-sm-6">
                    <div class="image ripple-effect">
                        <img src="{{ asset_cdn($item->options->image) }}?w=396&h=228&fit=clip&auto=format,compress&q=80" alt="" class="img-responsive">
                        <div class="caption">
                            <h5 class="media-heading">
                                {{ $item->name }} ({{ $item->options->offer }})
                            </h5>
                        </div>
                    </div>
                    <footer class="clearfix">
                        <div class="valueInfo">
                            <div class="value">
                                <p class="value">
                                    {{ number_format($item->price, 0, ',', ' ') }} DH
                                </p>
                                <p class="text">
                                    Prix
                                </p>
                            </div>
                            <div class="discount">
                                <p class="value">
                                    {{ $item->qty }}
                                </p>
                                <p class="text">
                                    Quantité
                                </p>
                            </div>
                            <div class="save">
                                <p class="value">
                                    {{ number_format($item->total, 0, ',', ' ') }} DH
                                </p>
                                <p class="text">
                                    Total
                                </p>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row ">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <a class="btn btn-danger btn-raised ripple-effect pull-left" data-dismiss="modal" aria-label="Close">
                            @lang('cart.continue_shopping')
                        </a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <a href="{{ route('frontend.cart.index') }}" class="btn btn-success btn-raised ripple-effect btn-block">
                            @lang('cart.proceed_checkout')
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>