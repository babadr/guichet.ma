@extends('layouts.frontend')

@section('content')
    <div class="hr-link orange">
        <hr class="mBtm-30 mTop-30">
        <h1 class="list-title">Formulaire d'abonnement Raja Club Athlétic</h1>
    </div>
    <div class="row">
        <div class="main-col col-md-12">
            <div class="panel-body frameLR bg-white shadow mTop-30 mBtm-50">
                <form id="form-subscription-raja"
                      action="{{ route('frontend.order.abonnemet.store', ['reference' => $order->reference]) }}"
                      method="post" novalidate="novalidate"
                      class="registration-form clearfix form-validate" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @php $j = 1 @endphp
                    @foreach($lines as $id => $line)
                        @for ($i = 1; $i <= $line['total']; $i++)
                            <h3>Informations abonné(e) - {{ $j }} - CARTE {{ $line['offer'] }}</h3>
                            <section>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_full_name">
                                                Nom et Prénom <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][full_name]"
                                                   id="rows_{{ $id }}_{{ $i }}_full_name" required
                                                   class="form-control"
                                                   value="{{ old('rows[' . $id . '][' . $i . '][full_name]', $j == 1 ? auth()->user()->full_name : '') }}">
                                        </p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_birth_date">
                                                Date de naissaice <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][birth_date]"
                                                   id="rows_{{ $id }}_{{ $i }}_birth_date" required
                                                   data-rule-date="true" placeholder="Format : DD/MM/YYYYY"
                                                   class="form-control datepicker" autocomplete="off"
                                                   value="{{ old('rows[' . $id . '][' . $i . '][birth_date]') ? Carbon\Carbon::parse(old('rows[' . $id . '][' . $i . '][birth_date]'))->format('d/m/Y') : '' }}">
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_address">
                                                Adresse <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][address]"
                                                   id="rows_{{ $id }}_{{ $i }}_address" required
                                                   class="form-control"
                                                   value="{{ old('rows[' . $id . '][' . $i . '][address]') }}">
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_city">
                                                Ville <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][city]"
                                                   id="rows_{{ $id }}_{{ $i }}_city" required
                                                   class="form-control"
                                                   value="{{ old('rows[' . $id . '][' . $i . '][city]') }}">
                                        </p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_zip_code">
                                                Code postal <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][zip_code]"
                                                   id="rows_{{ $id }}_{{ $i }}_zip_code" required
                                                   placeholder="Format : 00000"
                                                   class="form-control" data-rule-numeric="true"
                                                   value="{{ old('rows[' . $id . '][' . $i . '][zip_code]') }}">
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_email">
                                                E-mail <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][email]"
                                                   id="rows_{{ $id }}_{{ $i }}_email"
                                                   class="form-control" required data-rule-email="true"
                                                   value="{{ old('rows[' . $id . '][' . $i . '][email]', $j == 1 ? auth()->user()->email : '') }}">
                                        </p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_phone">
                                                Téléphone <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][phone]"
                                                   id="rows_{{ $id }}_{{ $i }}_phone"
                                                   class="form-control" required data-rule-phone="true"
                                                   value="{{ old('rows[' . $id . '][' . $i . '][phone]', $j == 1 ? auth()->user()->phone : '') }}">
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_avatar">
                                                Photo <span class="required">*</span>
                                            </label>
                                            <input type="file" class="form-control" data-rule-accept="image/*"
                                                   data-msg-accept="Les types de fichiers autorisés sont jpg, jpeg, png et gif"
                                                   accept=".jpeg,.png,.jpg,.gif"
                                                   name="rows[{{ $id }}][{{ $i }}][avatar]"
                                                   id="rows_{{ $id }}_{{ $i }}_avatar" required>
                                        </p>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <p class="input-block form-group">
                                            <label for="rows_{{ $id }}_{{ $i }}_cin">
                                                N° CIN <span class="required">*</span>
                                            </label>
                                            <input type="text" name="rows[{{ $id }}][{{ $i }}][cin]"
                                                   id="rows_{{ $id }}_{{ $i }}_cin"
                                                   class="form-control" required
                                                   value="{{ old('rows[' . $id . '][' . $i . '][cin]') }}">
                                        </p>
                                    </div>
                                </div>
                            </section>
                            @php $j++ @endphp
                        @endfor
                    @endforeach
                    <button type="submit" class="btn btn-success btn-raised ripple-effect btn-block" style="height: 50px;">
                        Enregistrer
                    </button>
                </form>
            </div>
        </div>

    </div>
@endsection
@section('stylesheets')
    <link href="{{ asset('/backend/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection
@section('plugins')
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('/backend/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js') }}"
            type="text/javascript"></script>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('.datepicker').each(function () {
            var $element = $(this);
            $element.datepicker({
                todayHighlight: true,
                dateFormat: 'dd/mm/yy',
                language: 'fr',
                keyboardNavigation: false,
                autoclose: true,
                forceParse: false,
                rtl: false,
                orientation: "bottom auto",
            });
        });
    </script>
@endsection

