@extends('layouts.frontend')

@section('content')
    <div class="row">
        <div class="col-sm-9 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30 bTop">
                    <h1 class="list-title">Mes commandes</h1>
                </div>
                @if ($orders->count() == 0)
                    <p>Aucune commande n'est disponible actuellement.</p>
                @else
                    <table class="cart-contents table-bordered">
                        <thead>
                        <tr>
                            <th>
                                Référence
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                état
                            </th>
                            <th>
                                Total
                            </th>
                            <th>

                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $key => $order)
                            <tr>
                                <td>
                                    {{ $order->reference }}
                                </td>
                                <td>
                                    {{ date('d/m/Y H:i', strtotime($order->created_at)) }}
                                </td>
                                <td>
                                    {{ trans(config('enums.order_status')[$order->status]) }}
                                </td>
                                <td>
                                    @if($order->offered)
                                        Commande offerte
                                    @else
                                        {{ number_format($order->total_paid, 0, ',', ' ') }} DH
                                    @endif
                                </td>
                                <td>
                                    <a title="Voir détails" href="{{ route('frontend.order.view', ['id' => $order->id]) }}"
                                       class="btn btn-success btn-sm ripple-effect mTop-10">
                                        <i class="fa fa-eye"></i> Voir détails
                                    </a><br>
                                    @if($order->status == \App\Models\Store\Order::STATUS_ACCEPTED || $order->status == \App\Models\Store\Order::STATUS_CAPTURED)
                                        <a title="Télécharger les tickets" href="{{ route('frontend.ticket.download', ['id' => $order->id]) }}"
                                           class="btn btn-warning btn-sm ripple-effect mTop-10">
                                            <i class="fa fa-download"></i> Télécharger les tickets
                                        </a><br>
                                    @endif
                                    @if(Tools::checkRecipient($order) && in_array($order->status, [\App\Models\Store\Order::STATUS_ACCEPTED, \App\Models\Store\Order::STATUS_VALIDATED, \App\Models\Store\Order::STATUS_CAPTURED, \App\Models\Store\Order::STATUS_RESERVED]))
                                        <a title="Télécharger les tickets" href="{{ route('frontend.order.abonnemet', ['reference' => $order->reference]) }}"
                                           class="btn btn-info btn-sm ripple-effect mTop-10">
                                            <i class="fa fa-user-circle"></i> Formulaire d'abonnement
                                        </a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif

                <p></p>
            </div>
        </div>
        <div class="col-sm-3 sidebar">

        </div>
    </div>
@endsection
