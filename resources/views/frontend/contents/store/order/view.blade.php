@extends('layouts.frontend')

@section('content')
    <div class="row">
        <div class="col-sm-9 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30 bTop">
                    <h1 class="list-title">Détails de la commande</h1>
                </div>
                <div>
                    <strong>Référence :</strong> {{ $order->reference }} passée
                    le {{ date('m/d/Y H:i', strtotime($order->created_at)) }}<br/><br/>
                    <strong>Etat de la commande :</strong> {{ trans(config('enums.order_status')[$order->status]) }}
                    <br/><br/>
                    <strong>Moyen de paiement :</strong>
                    @if ($order->payment_method == 1)
                        Carte bancaire
                    @elseif ($order->payment_method == 2)
                        PAYEXPRESS (Cash Plus)
                    @elseif ($order->payment_method == 3)
                        Virement / versement bancaire
                    @elseif ($order->payment_method == 4)
                        Espèce
                    @endif
                    <br/><br/>
                    @if (in_array($order->status, [2, 3, 5]) && $order->payment_method == 1 && !empty($order->payment_transaction))
                        <strong>Transaction ID :</strong> {{ $order->payment_transaction }}<br/><br/>
                        <strong>Transaction Date :</strong> {{ date('m/d/Y H:i', strtotime($order->payment_transaction_date)) }}<br/><br/>
                    @endif
                    @if (in_array($order->status, [2, 3, 5]) && in_array($order->payment_method, [2, 3, 4]) && !empty($order->payment_transaction_date))
                        <strong>Date règlement :</strong> {{ date('m/d/Y H:i', strtotime($order->payment_transaction_date)) }}<br/><br/>
                    @endif
                    <strong>Adresse :</strong> {{ $order->address }}
                </div>
                <div class="cart shadow">
                    <table class="cart-contents table-bordered">
                        <thead>
                        <tr>
                            <th>
                                @lang('cart.product')
                            </th>
                            <th>
                                Prix unitaire
                            </th>
                            <th>
                                @lang('cart.quantity')
                            </th>
                            <th>
                                @lang('cart.total')
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lines as $key => $line)
                            <tr>
                                <td class="details">
                                    <div class="clearfix">
                                        <div class="pull-left product-details">
                                            <span class="title">
                                                {{ $line->product_name }}
                                            </span>
                                            <span>{{ $line->offer_name }}</span>
                                            @if($line->seats)
                                                <span>Places : {{ $line->seats }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                                <td class="unit-price">
                                    {{ number_format($line->product_price, 0, ',', ' ') }} DH
                                </td>
                                <td class="qty text-center">
                                    <strong>x{{ $line->quantity }}</strong>
                                </td>
                                <td class="total-price">
                                    {{ number_format($line->total_price, 0, ',', ' ') }} DH
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="3">
                                @lang('cart.sub_total')
                            </td>
                            <td class="price">
                                @if($order->offered)
                                    Commande offerte
                                @else
                                    {{ number_format($order->total_paid, 0, ',', ' ')}} DH
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="cart-total" colspan="3">
                                @lang('cart.global_amount')
                            </td>
                            <td class="cart-total price">
                                @if($order->offered)
                                    Commande offerte
                                @else
                                    {{ number_format($order->total_paid, 0, ',', ' ')}} DH
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <p>

                </p>
            </div>
        </div>
        <div class="col-sm-3 sidebar">

        </div>
    </div>
@endsection
