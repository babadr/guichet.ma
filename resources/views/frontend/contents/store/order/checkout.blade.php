@extends('layouts.frontend')

@section('content')
    <div class="hr-link orange">
        <hr class="mBtm-30 mTop-30">
        <h1 class="list-title">Vérification de commande</h1>
    </div>
    <div class="row">
        <div class="main-col col-md-9">
            <div class="panel-body frameLR bg-white shadow mTop-30 mBtm-50">
                <!-- Checkout Accordion -->
                <div class="panel-group checkout" id="accordion">
                    <!-- Panel -->
                    <div class="panel panel-default">
                        <!-- Heading -->
                        <div class="panel-heading heading-iconed">
                            <h4 class="panel-title case-c">
                                <i class="icon-left">1. </i>
                                Détails de votre commande
                            </h4>
                        </div>
                        <!-- /Heading -->
                        <!-- Collapse -->
                        <div class="panel-collapse">
                            <!-- Panel Body -->
                            <div class="panel-body">
                                <!-- Row -->
                                <div class="row">
                                    <!-- Col -->
                                    <div class="col-md-12 mgb-30-xs">
                                        <!-- Cart Contents -->
                                        <table class="cart-contents table-bordered">
                                            <thead>
                                            <tr>
                                                <th class="hidden-xs">
                                                    Image
                                                </th>
                                                <th>
                                                    @lang('cart.product')
                                                </th>
                                                <th>
                                                    @lang('cart.quantity')
                                                </th>
                                                <th class="hidden-xs">
                                                    @lang('cart.price')
                                                </th>
                                                <th>
                                                    @lang('cart.total')
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach(Cart::content() as $key => $row)
                                                <tr>
                                                    <td class="image hidden-xs">
                                                        <a href="/{{ $row->options->url }}" class="title">
                                                            <img src="{{ $row->options->image }}" alt="">
                                                        </a>
                                                    </td>
                                                    <td class="details">
                                                        <div class="clearfix">
                                                            <div class="pull-left product-details">
                                                                <a href="/{{ $row->options->url }}" class="title">
                                                                    {{ $row->name }}
                                                                </a>
                                                                @if($row->options->has('offer'))
                                                                    <span>{{ $row->options->offer }} @if ($row->options->reservation)
                                                                            : Prix {{ number_format($row->options->reservation, 0, ',', ' ') }} DH (Paiement de {{ number_format($row->price, 0, ',', ' ') }} DH sur le site)@endif</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="qty">
                                                        {{ $row->qty }}
                                                    </td>
                                                    <td class="unit-price hidden-xs">
                                                        {{ number_format($row->price, 0, ',', ' ') }} DH
                                                    </td>
                                                    <td class="total-price">
                                                        {{ number_format($row->total, 0, ',', ' ') }} DH
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <!-- /Cart Contents -->
                                    </div>
                                    <!-- /Col -->
                                </div>
                                <!-- /Row -->
                            </div>
                            <!-- /Panel Body -->
                        </div>
                        <!-- /Collapse -->
                    </div>
                    <!-- /Panel -->
                    <!-- Panel -->
                    <div class="panel panel-default">
                        <!-- Heading -->
                        <div class="panel-heading heading-iconed">
                            <h4 class="panel-title">
                                <i class="icon-left">2. </i>
                                Vos informations personnelles
                            </h4>
                        </div>
                        <!-- /Heading -->
                        <!-- Collapse -->
                        <div class="panel-collapse">
                            <!-- Panel Body -->
                            <div class="panel-body">
                                <!-- Form -->
                                <form id="user-form" class="form-validate"
                                      action="{{ route('frontend.order.updateUser') }}" method="post"
                                      novalidate="novalidate">
                                    {{ csrf_field() }}
                                    <!-- Row -->
                                    <div class="row">
                                        <!-- Col -->
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                                <label for="last_name">Nom <span class="required">*</span></label>
                                                <input type="text" name="last_name" id="last_name" required class="form-control" placeholder="Votre nom" value="{{ $user->last_name or old('last_name') }}">
                                                @if ($errors->has('last_name'))
                                                    <small id="last_name-error"
                                                           class="has-error help-block help-block-error">{{ $errors->first('last_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- /Col -->
                                        <!-- Col -->
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                                <label for="first_name">Prénom <span class="required">*</span></label>
                                                <input type="text" name="first_name" id="first_name" required class="form-control" placeholder="Votre prénom" value="{{ $user->first_name or old('first_name') }}">
                                                @if ($errors->has('first_name'))
                                                    <small id="first_name-error"
                                                           class="has-error help-block help-block-error">{{ $errors->first('first_name') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- /Col -->
                                        <!-- Col -->
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                                <label for="country">Pays <span class="required">*</span></label>
                                                <select name="country" id="country" required class="form-control select2">
                                                    <option value="">@Lang('app.select')</option>
                                                    @foreach (config("countries") as $country)
                                                        <option {{ (old('country') && old('country') == $country[1]) ? 'selected' : (($user->country && $user->country == $country[1]) ? 'selected' : '') }}
                                                                value="{{ $country[1] }}">{{ $country[3] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('country'))
                                                    <small id="country-error"
                                                           class="has-error help-block help-block-error">{{ $errors->first('country') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- /Col -->
                                        <!-- Col -->
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                                <label for="city">Ville <span class="required">*</span></label>
                                                <input type="text" name="city" id="city" required class="form-control" placeholder="Votre ville" value="{{ $user->city or old('city') }}">
                                                @if ($errors->has('city'))
                                                    <small id="city-error"
                                                           class="has-error help-block help-block-error">{{ $errors->first('city') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- /Col -->
                                        <!-- Col -->
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                <label for="phone">Téléphone <span class="required">*</span></label>
                                                <input type="text" name="phone" id="phone" required data-rule-phone="true" class="form-control" placeholder="Votre téléphone" value="{{ $user->phone or old('phone') }}">
                                                @if ($errors->has('phone'))
                                                    <small id="phone-error"
                                                           class="has-error help-block help-block-error">{{ $errors->first('phone') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                                <label for="address">Adresse <span class="required">*</span></label>
                                                <input type="text" name="address" id="address" required class="form-control" placeholder="Votre adresse" value="{{ $user->address or old('address') }}">
                                                @if ($errors->has('address'))
                                                    <small id="address-error"
                                                           class="has-error help-block help-block-error">{{ $errors->first('address') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary btn-raised ripple-effect">
                                                Mettre à jour
                                            </button>
                                        </div>
                                        <!-- /Col -->
                                    </div>
                                    <!-- /Row -->
                                </form>
                                <!-- Form -->
                            </div>
                            <!-- /Panel Body -->
                        </div>
                        <!-- /Collapse -->
                    </div>
                    <!-- /Panel -->
                </div>
                <!-- /Accordion -->
            </div>
        </div>
        <!-- /Main Col -->
        <!-- Side Col -->
        <div class="side-col col-md-3">
            <div class="panel-body frameLR bg-white shadow mTop-30 mBtm-50">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-10">
                    <h2 class="list-title">Paiement</h2>
                </div>
                <!-- Side Widget -->
                <div class="order-summary">
                    <table id="cart-summary" class="std table">
                        <tbody>
                        <tr>
                            <td>
                                Sous-total
                            </td>
                            <td class="price">
                                {{ Cart::total() }} DH
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total à payer
                            </td>
                            <td class=" site-color" id="total-price">
                                {{ Cart::total() }} DH
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <a href="{{ route('frontend.cart.index') }}"
                       class="btn btn-danger btn-raised ripple-effect btn-block">
                        Modifier mon panier
                    </a>
                    <form id="checkout-payment" action="{{ route('frontend.order.payment', ['reference' => $reference]) }}" method="post" class="mTop-10">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-success btn-raised ripple-effect btn-block">
                            Régler mes achats
                        </button>
                    </form>
                </div>
                <!-- /Side Widget -->
            </div>
        </div>
        <!-- /Side Col -->
    </div>
@endsection

@section('stylesheets')
    <link href="{{ asset_cdn('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset_cdn('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('plugins')
    <script src="{{ asset_cdn('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/backend/plugins/select2/js/i18n/fr.js') }}" type="text/javascript"></script>
@endsection

