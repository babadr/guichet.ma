@extends('layouts.frontend')

@section('content')
    <div class="row text-center">

        <div class="col-sm-6 col-sm-offset-3">
            <h2 style="color:#0fad00">Ticket Confirmé</h2>
            <img src="/frontend/images/big-tick.png">
            <h3>Merci!</h3>
        </div>
    </div>
@endsection
