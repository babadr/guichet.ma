@extends('layouts.frontend')

@section('content')
    <div class="row text-center">
        <div class="col-sm-6 col-sm-offset-3">
            @if(is_object($ticket) && $ticket->hash == $token)
                <h2 style="color:#0fad00">Ticket accépté</h2>
                <h4> Ticket No: {{ $ticket->number }} </h4>
                <h4> Prix: {{ $ticket->line->product_price }} DH  </h4>
                <p>
                     Produit: {{ $ticket->line->product_name }} <br />
                     Offre: {{ $ticket->line->offer_name }}
                </p>
                <h3 style="color:#0fad00">Confirmer la validation</h3>
                <p style="color:#FF0000">
                    Cette action est irréversible! <br />
                    aprés la confirmation ce ticket ne sera plus valable
                </p>

                <form id="validate-form" class="login-form clearfix form-validate"
                      action="{{ route('frontend.ticket.validate') }}" method="post"
                      novalidate="novalidate">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}" />
                    <p class="clearfix">
                        <input type="submit" class="btn btn-raised ripple-effect btn-danger" value="Confirmer">
                        <a href="/" class="btn btn-success"> Annuler </a>
                    </p>
                </form>
            @elseif($ticket == 401)
                <h2 style="color:#FF0000">Ticket invalide</h2>
                <img src="/frontend/images/error.png"> <br /> <br />
                <p style="color:#FF0000">
                    Il semble que ce ticket a déjà été validé sur notre système <br />
                    Si vous l'avez activer par erreur, Merci de contacter le support technique sur contact@guichet.ma
                </p>
            @else
                <h2 style="color:#FF0000">Ticket non trouvé</h2>
                <img src="/frontend/images/error.png">
            @endif
        </div>
    </div>
@endsection
