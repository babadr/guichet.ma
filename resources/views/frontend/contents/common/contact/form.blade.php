@extends('layouts.frontend')


@section('content')
    <div class="row">
        <div class="col-sm-8 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">@lang('contact.contact_us')</h1>
                </div>
                <div class="google-maps" id="map_canvas"></div>
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h3 class="list-title">@lang('contact.contact_form')</h3>
                </div>
                <div class="widget contact-widget">
                    <h4 class="widget-title">@lang('contact.front_title')</h4>
                    <form id="contact-form" class="contact-form clearfix form-validate"
                          action="{{ route('frontend.contact.store') }}" method="post"
                          novalidate="novalidate">
                        {{ csrf_field() }}
                        <p>@lang('contact.front_message', ['phone' => Setting::get('phone')])</p>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name">@lang('contact.last_name') <span
                                                class="required">*</span></label>
                                    <input type="text" name="last_name" id="last_name" required class="form-control"
                                           placeholder="@lang('contact.your_last_name')" value="{{ old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                        <small id="last_name-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('last_name') }}</small>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name">@lang('contact.first_name') <span class="required">*</span></label>
                                    <input type="text" name="first_name" id="first_name" required class="form-control"
                                           placeholder="@lang('contact.your_first_name')"
                                           value="{{ old('last_name') }}">
                                    @if ($errors->has('first_name'))
                                        <small id="first_name-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('first_name') }}</small>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">@lang('contact.email') <span class="required">*</span></label>
                                    <input type="text" name="email" id="email" required data-rule-email="true"
                                           class="form-control" placeholder="@lang('contact.your_email')" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <small id="email-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('email') }}</small>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone">@lang('contact.phone')</label>
                                    <input type="text" name="phone" id="phone" data-rule-phone="true"
                                           class="form-control" placeholder="@lang('contact.your_phone')" value="{{ old('phone') }}">
                                    @if ($errors->has('email'))
                                        <small id="phone-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('phone') }}</small>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('subject') ? ' has-error' : '' }}">
                                    <label for="subject">@lang('contact.subject') <span class="required">*</span></label>
                                    <input type="text" name="subject" id="subject" required
                                           class="form-control" placeholder="@lang('contact.your_subject')" value="{{ old('subject') }}">
                                    @if ($errors->has('subject'))
                                        <small id="subject-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('subject') }}</small>
                                    @endif
                                </p>
                            </div>
                        </div>
                        <p class="textarea-block form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                            <label for="message">@lang('contact.message') <span class="required">*</span></label>
                            <textarea rows="6" cols="88" name="message" id="message" required
                                      placeholder="@lang('contact.your_message')"
                                      name="message" class="form-control">{{ old('message') }}</textarea>
                            @if ($errors->has('message'))
                                <small id="email-error"
                                       class="has-error help-block help-block-error">{{ $errors->first('message') }}</small>
                            @endif
                        </p>
                        <p class="contact-button clearfix">
                            <input type="submit" class="btn btn-raised ripple-effect btn-success" id="submit-contact"
                                   value="@lang('contact.send_message')">
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-4 sidebar">
            @include('frontend.partials.info')
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        window.lat = 33.578250;
        window.lng = -7.637680;
    </script>
    <script src="{{ asset_cdn('/frontend/js/map-picker.js') }}" type="text/javascript"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{ Setting::get('google_map_api_key') }}&callback=initMap"></script>
@endsection