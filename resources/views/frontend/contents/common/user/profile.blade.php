@extends('layouts.frontend')

@section('content')
    <div class="row">
        <div class="col-sm-8 col-md-offset-2 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">
                        @lang('user.my_personal_info')
                    </h1>
                </div>
                <div class="widget login-widget">
                    <form id="user-form" class="form-validate"
                          action="{{ route('frontend.profile.update') }}" method="post"
                          novalidate="novalidate">
                    {{ csrf_field() }}
                    <!-- Row -->
                        <div class="row">
                            <!-- Col -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                    <label for="last_name">Nom <span class="required">*</span></label>
                                    <input type="text" name="last_name" id="last_name" required class="form-control"
                                           placeholder="Votre nom" value="{{ $user->last_name or old('last_name') }}">
                                    @if ($errors->has('last_name'))
                                        <small id="last_name-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('last_name') }}</small>
                                    @endif
                                </div>
                            </div>
                            <!-- /Col -->
                            <!-- Col -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                    <label for="first_name">Prénom <span class="required">*</span></label>
                                    <input type="text" name="first_name" id="first_name" required class="form-control"
                                           placeholder="Votre prénom"
                                           value="{{ $user->first_name or old('first_name') }}">
                                    @if ($errors->has('first_name'))
                                        <small id="first_name-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('first_name') }}</small>
                                    @endif
                                </div>
                            </div>
                            <!-- /Col -->
                            <!-- Col -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                                    <label for="country">Pays <span class="required">*</span></label>
                                    <select name="country" id="country" required class="form-control select2">
                                        <option value="">@Lang('app.select')</option>
                                        @foreach (config("countries") as $country)
                                            <option {{ (old('country') && old('country') == $country[1]) ? 'selected' : (($user->country && $user->country == $country[1]) ? 'selected' : '') }}
                                                    value="{{ $country[1] }}">{{ $country[3] }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('country'))
                                        <small id="country-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('country') }}</small>
                                    @endif
                                </div>
                            </div>
                            <!-- /Col -->
                            <!-- Col -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city">Ville <span class="required">*</span></label>
                                    <input type="text" name="city" id="city" required class="form-control"
                                           placeholder="Votre ville" value="{{ $user->city or old('city') }}">
                                    @if ($errors->has('city'))
                                        <small id="city-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('city') }}</small>
                                    @endif
                                </div>
                            </div>
                            <!-- /Col -->
                            <!-- Col -->
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                                    <label for="phone">Téléphone <span class="required">*</span></label>
                                    <input type="text" name="phone" id="phone" required data-rule-phone="true"
                                           class="form-control" placeholder="Votre téléphone"
                                           value="{{ $user->phone or old('phone') }}">
                                    @if ($errors->has('phone'))
                                        <small id="phone-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('phone') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label for="address">Adresse <span class="required">*</span></label>
                                    <input type="text" name="address" id="address" required class="form-control"
                                           placeholder="Votre adresse" value="{{ $user->address or old('address') }}">
                                    @if ($errors->has('address'))
                                        <small id="address-error"
                                               class="has-error help-block help-block-error">{{ $errors->first('address') }}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-raised ripple-effect">
                                    Mettre à jour
                                </button>
                            </div>
                            <!-- /Col -->
                        </div>
                        <!-- /Row -->
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('stylesheets')
    <link href="{{ asset_cdn('/backend/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset_cdn('/backend/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('plugins')
    <script src="{{ asset_cdn('/backend/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/backend/plugins/select2/js/i18n/fr.js') }}" type="text/javascript"></script>
@endsection

