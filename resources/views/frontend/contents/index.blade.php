@extends('layouts.frontend')

@section('body-class') front-page @endsection

@section('slides')
    @include('frontend.contents.home.slides')
@endsection

@section('content')
    <div class="shadow bg-white mTop-20 frameLR banner">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="l-element">
                    <div class="box-icon">
                        <div class="icon-wrap">
                            <img src="{{ asset_cdn('/frontend/images/tickets.png') }}" height="80px" alt="Achetez des tickets">
                        </div>
                        <div class="text">
                            <h4>Achetez <span>des tickets</span></h4>
                            <p>Achetez des tickets de qualité pour les meilleurs événements en toute sécurité !</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="l-element">
                    <div class="box-icon">
                        <div class="icon-wrap">
                            <img src="{{ asset_cdn('/frontend/images/satisfaction.png') }}" alt="Notre garantie 100 %">
                        </div>
                        <div class="text">
                            <h4>Notre <span>garantie 100 %</span></h4>
                            <p>Éliminez les risques et assurez-vous une transaction sécuritaire et protégée en faisant
                                affaire avec guichet.ma</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="l-element">
                    <div class="box-icon">
                        <div class="icon-wrap">
                            <img src="{{ asset_cdn('/frontend/images/contact.png') }}" alt="Support 24/24H">
                        </div>
                        <div class="text">
                            <h4>Support <span>24/24H</span></h4>
                            <p>+212 6 45 765 765 / +212 6 62 08 48 40<br>
                                contact@guichet.ma</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row {{ Agent::isMobile() ? 'mTop-20' : 'mTop-50' }} clearfix">
        <div class="col-sm-12 text-center">
            <a href="https://bit.ly/2ZWSbQ3" target="_blank">
                @if (Agent::isMobile())
                    <img src="{{ asset('frontend/images/hit_radio_320_100.gif') }}" alt="">
                @else
                    <img src="{{ asset('frontend/images/hit_radio_970_90.gif') }}" alt="">
                @endif
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            {{--@foreach($categories as $category)--}}
            {{--@php $frontDeals = $category->getFrontDeals(9) @endphp--}}
            {{--@if($frontDeals->count() > 0)--}}
            {{--<div class="row">--}}
            {{--<div class="col-sm-12 clearfix">--}}
            {{--<div class="hr-link {{ $category->color }}">--}}
            {{--<hr class="mBtm-40 mTop-30">--}}
            {{--<h3 class="list-title">--}}
            {{--{{ $category->title }}--}}
            {{--</h3>--}}
            {{--<div class="view-all">--}}
            {{--<a href="/{{ $category->seo->seo_alias }}">--}}
            {{--<i class="fa fa-angle-double-right"></i>@lang('front.view_all')--}}
            {{--</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="{{ Agent::isMobile() ? 'category-slick' : 'category-home' }} {{ $category->color }}">--}}
            {{--@include('frontend.contents.home.category_deals', ['deals' => $frontDeals, 'color' => $category->color])--}}
            {{--</div>--}}
            {{--@endif--}}
            {{--@endforeach--}}
            <div class="row">
                <div class="col-sm-12 clearfix mTop-15">
                </div>
            </div>
            <div class="category-home orange">
                @include('frontend.contents.home.category_deals', ['deals' => $frontDeals, 'color' => 'orange'])
            </div>
        </div>
    </div>
@endsection
