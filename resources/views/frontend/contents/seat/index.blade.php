<div class="modal fade modal-setting-plan {{ str_replace('seat.', '', config('enums.seating_plans')[$deal->seating_plan_id]) }}" id="view-event-setting-plan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    {{ $deal->title }}
                </h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md12">
                        <div class="seat-container">
                            <div id="seat-map">
                                <div id="legend"></div>
                                <div class="front-indicator">Scène</div>
                                @if($deal->seating_plan_id == 1 || $deal->seating_plan_id == 4 || $deal->seating_plan_id == 5 || $deal->seating_plan_id == 8)
                                    <div class="governance">Régie</div>
                                @endif
                                @if($deal->seating_plan_id == 2)
                                    <div class="porte_1">Porte 1 Orchestre</div>
                                    <div class="porte_2">Porte 2 Orchestre</div>
                                @endif
                                @if($deal->seating_plan_id == 6 || $deal->seating_plan_id == 7)
                                    <div class="porte_1">Orchestre</div>
                                    <div class="porte_2">Balcon</div>
                                @endif
                            </div>
                            <div class="booking-details">
                                @if($deal->seating_plan_id == 8 || $deal->seating_plan_id == 10 || $deal->seating_plan_id == 13)
                                    <div style="color: red; margin: 10px 0; font-weight: bold; font-size: 12px">* Placements libres en Balcon</div>
                                @endif
                                <h4> Places sélectionnées (<span id="counter">0</span>) :</h4>
                                <ul id="selected-seats"></ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <a href="{{ route('frontend.cart.addSeats') }}" id="event-post-order" class="btn btn-success btn-raised ripple-effect btn-block">
                            @lang('cart.proceed_checkout')
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4 pull-right">
                        Total à payer : <b><span id="total">0</span> DH</b>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>