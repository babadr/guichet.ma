@extends('layouts.frontend')

@section('content')
    <div class="row">
        <div class="col-sm-6 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">
                        @lang('user.login')
                    </h1>
                </div>
                <div class="widget login-widget">
                    <form id="login-form" class="login-form clearfix form-validate"
                          action="{{ route('frontend.auth.login') }}" method="post"
                          novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">
                                        @lang('contact.email')
                                        <span class="required">*</span>
                                    </label>
                                    <input type="text" name="email" id="email" required data-rule-email="true"
                                           class="form-control" placeholder="@lang('contact.your_email')" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <small id="email-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('email') }}
                                        </small>
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password">
                                        @lang('user.password')
                                        <span class="required">*</span>
                                    </label>
                                    <input type="password" name="password" id="password" required
                                           class="form-control" placeholder="@lang('user.password')" value="{{ old('password') }}">
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                                        <input type="checkbox" value="1" name="remember" {{ old('remember') ? 'checked' : '' }}/>
                                        <span>@Lang('auth.remember_me')</span>
                                    </label>
                                </p>
                            </div>
                        </div>
                        <p class="login-button clearfix">
                            <input type="submit" class="btn btn-raised ripple-effect btn-success"
                                   id="submit-login" value="@lang('user.login')">
                            <a class="btn btn-link" href="{{ route('frontend.auth.password.showForgot') }}">
                                @lang('user.forget_your_password')
                            </a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-6 mTop-30">
            @include('frontend.auth.register')
        </div>
    </div>
@endsection
