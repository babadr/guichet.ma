<div class="inner-wrap frameLR bg-white shadow clearfix ">
    <div class="hr-link orange">
        <hr class="mBtm-50 mTop-30">
        <h1 class="list-title">
            @lang('user.registration')
        </h1>
    </div>
    <div class="widget registration-widget">
        <form id="registration-form" class="registration-form clearfix form-validate"
              action="{{ route('frontend.auth.showRegister') }}" method="post"
              novalidate="novalidate">
            {{ csrf_field() }}
            <p>@lang('user.registration_message')</p>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="input-block form-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
                        <label for="first_name">
                            @lang('user.first_name')
                            <span class="required">*</span>
                        </label>
                        <input type="text" name="first_name" id="first_name" required
                               class="form-control" placeholder="@lang('user.first_name')" value="{{ old('first_name') }}">
                        @if ($errors->has('first_name'))
                            <small id="first_name-error" class="has-error help-block help-block-error">
                                {{ $errors->first('first_name') }}
                            </small>
                        @endif
                    </p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="input-block form-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
                        <label for="last_name">
                            @lang('user.last_name')
                            <span class="required">*</span>
                        </label>
                        <input type="text" name="last_name" id="last_name" required
                               class="form-control" placeholder="@lang('user.last_name')" value="{{ old('last_name') }}">
                        @if ($errors->has('last_name'))
                            <small id="last_name-error" class="has-error help-block help-block-error">
                                {{ $errors->first('last_name') }}
                            </small>
                        @endif
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="input-block form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">
                            @lang('user.email')
                            <span class="required">*</span>
                        </label>
                        <input type="text" name="email" id="email" required data-rule-email="true"
                               class="form-control" placeholder="@lang('user.your_email')" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <small id="email-error" class="has-error help-block help-block-error">
                                {{ $errors->first('email') }}
                            </small>
                        @endif
                    </p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="input-block form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
                        <label for="phone">
                            @lang('user.phone')
                            <span class="required">*</span>
                        </label>
                        <input type="text" name="phone" id="phone" required data-role-phone="true"
                               class="form-control" placeholder="@lang('user.phone')" value="{{ old('phone') }}">
                        @if ($errors->has('phone'))
                            <small id="phone-error" class="has-error help-block help-block-error">
                                {{ $errors->first('phone') }}
                            </small>
                        @endif
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="input-block form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">
                            @lang('user.password')
                            <span class="required">*</span>
                        </label>
                        <input type="password" name="password" id="password" required
                               class="form-control" placeholder="@lang('user.password')" value="{{ old('password') }}">
                        @if ($errors->has('password'))
                            <small id="password-error" class="has-error help-block help-block-error">
                                {{ $errors->first('password') }}
                            </small>
                        @endif
                    </p>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <p class="input-block form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password_confirmation">
                            @lang('user.password_confirmation')
                            <span class="required">*</span>
                        </label>
                        <input type="password" name="password_confirmation" id="password_confirmation" required
                               class="form-control" placeholder="@lang('user.password_confirmation')" value="{{ old('password_confirmation') }}">
                        @if ($errors->has('password_confirmation'))
                            <small id="password_confirmation-error" class="has-error help-block help-block-error">
                                {{ $errors->first('password_confirmation') }}
                            </small>
                        @endif
                    </p>
                </div>
            </div>
            <p class="registration-button clearfix">
                <input type="submit" class="btn btn-raised ripple-effect btn-primary" id="submit-registration"
                       value="@lang('user.sign_up')">
            </p>
        </form>
    </div>
</div>