@extends('layouts.frontend')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-offset-3 mTop-30">
            <div class="inner-wrap frameLR bg-white shadow clearfix ">
                <div class="hr-link orange">
                    <hr class="mBtm-50 mTop-30">
                    <h1 class="list-title">
                        @lang('user.reset_password')
                    </h1>
                </div>
                <div class="widget login-widget">
                    <form id="login-form" class="login-form clearfix form-validate"
                          action="{{ route('frontend.auth.password.showForgot') }}" method="post"
                          novalidate="novalidate">
                        {{ csrf_field() }}
                        <p>@lang('user.forget_password_message')</p>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <p class="input-block form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">
                                        @lang('user.email')
                                        <span class="required">*</span>
                                    </label>
                                    <input type="text" name="email" id="email" required data-rule-email="true"
                                           class="form-control" placeholder="@lang('user.your_email')" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <small id="email-error" class="has-error help-block help-block-error">
                                            {{ $errors->first('email') }}
                                        </small>
                                    @endif
                                </p>
                            </div>
                        </div>
                        <p class="login-button clearfix">
                            <input type="submit" class="btn btn-raised ripple-effect btn-success"
                                   id="submit-login" value="@lang('user.send_reset_link')">
                            <a class="btn btn-link" href="{{ route('frontend.auth.showLogin') }}">
                                @lang('user.login')
                            </a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
