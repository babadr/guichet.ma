@extends('layouts.frontend')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-offset-3 mTop-30">
                <div class="inner-wrap frameLR bg-white shadow clearfix ">
                    <div class="hr-link orange">
                        <hr class="mBtm-50 mTop-30">
                        <h1 class="list-title">
                            @lang('user.reset_password')
                        </h1>
                    </div>
                    <div class="widget login-widget">
                        <form id="login-form" class="login-form clearfix form-validate"
                              action="{{ route('frontend.auth.password.reset') }}" method="post"
                              novalidate="novalidate">
                            {{ csrf_field() }}
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <p class="input-block form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email">
                                            @lang('user.email')
                                            <span class="required">*</span>
                                        </label>
                                        <input type="text" name="email" id="email" required data-rule-email="true"
                                               class="form-control" placeholder="@lang('user.your_email')" value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <small id="email-error" class="has-error help-block help-block-error">
                                                {{ $errors->first('email') }}
                                            </small>
                                        @endif
                                    </p>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <p class="input-block form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password">
                                            @lang('user.password')
                                            <span class="required">*</span>
                                        </label>
                                        <input type="password" name="password" id="password" required
                                               class="form-control" placeholder="@lang('user.password')" value="{{ old('password') }}">
                                        @if ($errors->has('password'))
                                            <small id="password-error" class="has-error help-block help-block-error">
                                                {{ $errors->first('password') }}
                                            </small>
                                        @endif
                                    </p>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <p class="input-block form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label for="password_confirmation">
                                            @lang('user.password_confirmation')
                                            <span class="required">*</span>
                                        </label>
                                        <input type="password" name="password_confirmation" id="password_confirmation" required
                                               class="form-control" placeholder="@lang('user.password_confirmation')" value="{{ old('password_confirmation') }}">
                                        @if ($errors->has('password_confirmation'))
                                            <small id="password_confirmation-error" class="has-error help-block help-block-error">
                                                {{ $errors->first('password_confirmation') }}
                                            </small>
                                        @endif
                                    </p>
                                </div>
                                <p class="login-button clearfix">
                                    <input type="submit" class="btn btn-raised ripple-effect btn-success"
                                           id="submit-login" value="@lang('user.reset_password')">
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection