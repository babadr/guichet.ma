<header>
    <div class="top-bar bg-light hdden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 list-inline list-unstyled no-margin hidden-xs">
                    <p class="no-margin">
                        @lang('front.have_question')
                        <strong>
                            {{--{{ Setting::get('phone') }}--}}
                            +212 6 45 765 765 / +212 6 62 08 48 40
                        </strong>
                        ou contact@guichet.ma
                        {{--@lang('front.or') {{ Setting::get('email') }}--}}
                    </p>
                </div>
                <div class="pull-right col-sm-6">
                    <div class="header-my-account">
                        @if (auth()->check())
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false">
                                    <img alt="{{ auth()->user()->full_name }}"
                                         src="{{ asset_cdn('/frontend/images/profile.png') }}" class="avatar"
                                         height="30"
                                         width="30">
                                    {{ auth()->user()->full_name }}
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('frontend.profile.index') }}">@lang('front.my_account')</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('frontend.order.list') }}">@lang('front.my_invoices')</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('frontend.auth.logout') }}">@lang('front.logout')</a>
                                    </li>
                                </ul>
                            </div>
                        @else
                            <a href="{{ route('frontend.auth.showLogin') }}">
                                <span>@lang('front.login_register')</span>
                            </a>
                        @endif
                    </div>
                    <ul class="list-inline list-unstyled pull-right" data-spy="affix" data-offset-top="100">
                        {{--<li class="active">--}}
                        {{--<a href="/foire-aux-questions">--}}
                        {{--<i class="fa fa-life-saver"></i>--}}
                        {{--@lang('front.help')--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        <li>
                            <a href="#" class="open-close-search">
                                <i class="fa fa-search"></i>
                                @lang('front.search')
                            </a>
                        </li>
                        <li>
                            <a href="/magazine">
                                <i class="fa fa-wordpress"></i>
                                Guichet Magazine
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.contact.index') }}">
                                <i class="fa fa-phone"></i>
                                @lang('front.contact')
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('frontend.cart.index') }}">
                                <i class="fa fa-shopping-cart"></i>
                                <span id="total-items">{{ Cart::count() }}</span> @lang('front.my_cart')
                            </a>
                        </li>
                    </ul>
                    <div class="nav-search justify-content-center align-items-center is-closed">
                        <form class="form-search form-validate" action="{{ route('frontend.search') }}" novalidate>
                            <div class="input-group">
                                <input type="text" name="q" value="{{ request()->get('q', '') }}" id="q" required
                                       class="search-control" placeholder="Rechercher un événement">
                                <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                            </div>
                            <a class="search-close open-close-search"><i class="fa fa-close"></i> Fermer</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="nav-wrap">
        <div class="nav-wrap-holder">
            <div class="container" id="nav_wrapper">
                <nav class="navbar navbar-static-top nav-white" id="main_navbar" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar">
                      <span class="sr-only">
                        @lang('front.toggle_navigation')
                                  </span>
                            <span class="icon-bar">
                                  </span>
                            <span class="icon-bar">
                                  </span>
                            <span class="icon-bar">
                                  </span>
                        </button>
                        <a href="{{ route('frontend.index') }}" class="navbar-brand logo col-sm-3">
                            @if(trim(Tools::activeMenu(['magazine', 'magazine/*'])) == 'active')
                                <img src="{{ asset_cdn('/frontend/images/guichet-magazine.png') }}"
                                     alt="Guichet Magazine" class="img-responsive">
                            @else
                                <img src="{{ asset_cdn('/frontend/images/logo-guichet.png') }}"
                                     alt="Guichet.ma" class="img-responsive">
                            @endif
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="Navbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="{{ Tools::activeMenu(['/']) }}">
                                <a href="{{ route('frontend.index') }}">
                                    <i class="fa fa-home"></i>
                                    @lang('front.home')
                                </a>
                            </li>
                            @if (isset($listCategories))
                                @foreach($listCategories as $cat)
                                    @if ($cat->show_on_menu)
                                        <li class="{{ Tools::activeMenu([$cat->seo->seo_alias, $cat->seo->seo_alias . '/*']) }}">
                                            <a href="/{{ $cat->seo->seo_alias }}">
                                                <i class="{{ $cat->icon }}"></i>
                                                <span>{{ $cat->title }}</span>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                            @endif
                            {{--<li class="{{ Tools::activeMenu(['deals/billetterie/theatre-humour', 'deals/billetterie/theatre-humour/*']) }}">--}}
                            {{--<a href="/deals/billetterie/theatre-humour">--}}
                            {{--<i class="fa fa-smile-o"></i>--}}
                            {{--Théâtre & Humour--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ Tools::activeMenu(['deals/billetterie/concerts-festivals', 'deals/billetterie/concerts-festivals/*']) }}">--}}
                            {{--<a href="/deals/billetterie/concerts-festivals">--}}
                            {{--<i class="fa fa-music"></i>--}}
                            {{--Concerts & Festivals--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ Tools::activeMenu(['deals/billetterie/formations', 'deals/billetterie/formations/*']) }}">--}}
                            {{--<a href="/deals/billetterie/formations">--}}
                            {{--<i class="fa fa-graduation-cap"></i>--}}
                            {{--Salons & Formations--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ Tools::activeMenu(['deals/billetterie/sport', 'deals/billetterie/sport/*']) }}">--}}
                            {{--<a href="/deals/billetterie/sport">--}}
                            {{--<i class="fa fa-soccer-ball-o"></i>--}}
                            {{--Sport--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--<li class="{{ Tools::activeMenu(['deals/loisirs', 'deals/loisirs/*']) }}">--}}
                            {{--<a href="/deals/loisirs">--}}
                            {{--<i class="fa fa-shopping-basket"></i>--}}
                            {{--Famille & Loisirs--}}
                            {{--</a>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>