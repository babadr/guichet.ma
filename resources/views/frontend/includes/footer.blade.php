<footer id="footer">
    <div class="container">
        <div class="col-sm-4 footer-widget">
            <img src="{{ asset_cdn('/frontend/images/logo-guichet-footer.png') }}" alt="Guichet.ma"
                 class="img-responsive logo">
            <p>
                Un site de billetterie nouvelle génération vous offrant la possibilité d’acheter en ligne des tickets
                pour les événements de votre choix. Sa vocation est de vous proposer l’offre de concerts, sports,
                spectacles, cinéma, festivals, théâtre, humour, voyages et evasions la plus large du Maroc en
                collaboration avec les organisateurs d’événements.
            </p>
        </div>
        <div class="col-sm-4 footer-widget">
{{--            <div id="fb-root"></div>--}}
{{--            <script>--}}
{{--                (function (d, s, id) {--}}
{{--                    var js, fjs = d.getElementsByTagName(s)[0];--}}
{{--                    if (d.getElementById(id)) {--}}
{{--                        return;--}}
{{--                    }--}}
{{--                    js = d.createElement(s);--}}
{{--                    js.id = id;--}}
{{--                    js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.11';--}}
{{--                    fjs.parentNode.insertBefore(js, fjs);--}}
{{--                }(document, 'script', 'facebook-jssdk'));--}}
{{--            </script>--}}
{{--            <div class="fb-page" data-href="https://www.facebook.com/guichet.ma/" data-height="300"--}}
{{--                 data-small-header="false" data-adapt-container-width="true" data-hide-cover="false"--}}
{{--                 data-show-facepile="true">--}}
{{--                <blockquote cite="https://www.facebook.com/guichet.ma/" class="fb-xfbml-parse-ignore">--}}
{{--                    <a href="https://www.facebook.com/guichet.ma/">guichet.ma</a>--}}
{{--                </blockquote>--}}
{{--            </div>--}}
        </div>
        <div class="col-sm-2 footer-widget">
            <h5>
                @lang('front.categories')
            </h5>
            <ul class="list-unstyled">
                @if (isset($listCategories))
                    @foreach($listCategories as $cat)
                        <li>
                            @php $count = $cat->children()->count() @endphp
                            @if(!$count)
                                <a href="/{{ $cat->seo->seo_alias }}">
                                    {{ $cat->title }}
                                </a>
                            @endif
                            {{--@php $count = $cat->children()->count() @endphp--}}
                            {{--@if($count)--}}
                            {{--<ul>--}}
                            {{--@foreach($cat->children as $child)--}}
                            {{--<li>--}}
                            {{--<a href="/{{ $child->seo->seo_alias }}">--}}
                            {{--{{ $child->title }}--}}
                            {{--</a>--}}
                            {{--</li>--}}
                            {{--@endforeach--}}
                            {{--</ul>--}}
                            {{--@endif--}}
                        </li>
                    @endforeach
                @endif
                {{--<li>--}}
                {{--<a href="/deals/billetterie/theatre-humour">Théâtre & Humour</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="/deals/billetterie/concerts-festivals">Concerts & Festivals</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="/deals/billetterie/formations">Salons & Formations</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="/deals/billetterie/sport">Sport</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="/deals/loisirs">Famille & Loisirs</a>--}}
                {{--</li>--}}
            </ul>
        </div>
        <div class="col-sm-2 footer-widget">
            <h5>
                Guichet.ma
            </h5>
            <ul class="list-unstyled">
                <li>
                    <a href="/qui-sommes-nous">@lang('front.about_us')</a>
                </li>
                <li>
                    <a href="{{ route('frontend.contact.index') }}">@lang('front.contact_us')</a>
                </li>
                <li>
                    <a href="/magazine">Guichet Magazine</a>
                </li>
                {{--<li>--}}
                {{--<a href="/foire-aux-questions">@lang('front.faq')</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<a href="/conditions-generales-de-vente">@lang('front.cgv')</a>--}}
                {{--</li>--}}
                <li>
                    <a href="/mentions-legales">@lang('front.legal_notice')</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="btmFooter">
        <div class="container">
            <div class="col-sm-4 copyright">
                <p>
                    <strong>
                        Copyright {{ date('Y') }} © Guichet.ma
                    </strong>
                    - @lang('front.copyright')
                </p>
            </div>
            <div class="col-sm-5 text-center">
                <img src="{{ asset_cdn('/frontend/images/logos-paiement.jpg') }}" class="payment-logo" alt="" title="">
            </div>
            <div class="col-sm-3">
                <ul class="social text-right">
                    <li><a href="https://www.facebook.com/guichet.ma/" target="_blank"
                           class="social-btn social-facebook"
                           title="Facebook"><i class="ti-facebook"></i></a>
                    </li>
                    <li><a href="https://twitter.com/guichet_ma" target="_blank" class="social-btn social-twitter"
                           title="Twitter"><i class="ti-twitter"></i></a>
                    </li>
                    <li><a href="https://www.instagram.com/guichet.ma/" target="_blank"
                           class="social-btn social-instagram"
                           title="Instagram"><i class="ti-instagram"></i></a>
                    </li>
                    <li><a href="https://plus.google.com/u/0/116600951703676552113" target="_blank"
                           class="social-btn social-google"
                           title="Google+"><i class="ti-google"></i></a>
                    </li>
                    <li><a href="https://www.linkedin.com/company/guichet-ma/" target="_blank"
                           class="social-btn social-linkedin"
                           title="LinkedIn"><i class="ti-linkedin"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>