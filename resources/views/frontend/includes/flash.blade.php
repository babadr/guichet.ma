@if (session()->has('flash_notification'))
    @foreach (session('flash_notification', collect())->toArray() as $message)
        <div class="alert alert-{{ $message['level'] }} alert-dismissible mTop-30" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">
                        &times;
                      </span>
            </button>
            {!! $message['message'] !!}
        </div>
    @endforeach
    {{ session()->forget('flash_notification') }}
@endif