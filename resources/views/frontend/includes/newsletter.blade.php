<div class="cta-box bg-blue-1 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <h3>
                    @lang('front.newsletter_title') {{ Setting::get('site_name') }}
                </h3>
                <p>
                    @lang('front.newsletter_description')
                </p>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="newsletter">
                    <form id="newsletter-form" class="form-validate"
                          action="{{ route('frontend.newsletter.store') }}" method="post"
                          novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="input-group">
                                <input name="email" id="newsletter-email" type="text" required data-rule-email="true" class="form-control" placeholder="@lang('front.newsletter_email')">
                                <span class="input-group-btn">
                                  <button class="btn btn-danger btn-raised ripple-effect" type="submit">
                                    @lang('front.newsletter_subscribe')
                                  </button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>