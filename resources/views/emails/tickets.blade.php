<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Message de {{ Setting::get('site_name') }}</title>
</head>
<body>
@php $key = 1 @endphp
@php $total = 0 @endphp
@foreach($order->lines as $line)
    @php $total += $line->tickets->count() @endphp
    @php $profiteers = $line->profiteers->toArray() @endphp
    @foreach($line->tickets as $index => $ticket)
        <section style="width:auto;margin:10px auto;font-family:Arial,Helvetica,sans-serif">
            <table style="border:3px dashed #181818;font-family:Arial,Helvetica,sans-serif;font-size:1em;color:#181818;margin:0 auto"
                   cellpadding="30" cellspacing="0" rules="groups" width="100%">
                <tbody>
                <tr>
                    <td style="padding:10px 20px;text-align:center;margin:0 auto;width:100%">

                        <table width="100%" cellpadding="30" cellspacing="0">
                            <tbody>
                            <tr>
                                <td width="70%" align="left" style="padding:10px 20px;border-bottom:2px solid #d0d0d0">
                                    <a href="https://www.guichet.ma/" target="_blank">
                                        <img src="{{ asset('email/logo-invert.png') }}" alt="Guichet.ma">
                                    </a>
                                </td>
                                <td width="30%" align="right"
                                    style="padding:10px 20px;border-bottom:2px solid #d0d0d0;font-size:30px"><b>{{ $ticket->number }}</b>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:10px 20px;text-align:center;margin:0 auto;width:100%">
                        <table width="100%" cellpadding="30" cellspacing="0">
                            <tbody>
                            <tr>
                                <td width="70%" align="left" style="padding:10px 20px">
                                    <p style="font-size:24px;font-weight:700;text-align:left;margin-bottom:0px">{{ $line->product_name }}</p>
                                </td>
                                <td width="30%" align="right" valign="top" style="padding:0;font-size:13px">
                                    <b>{{ $line->offer->deal->provider->title }}</b>
                                    <br>
                                    <b>{{ $line->offer->deal->address }}</b>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0px 50px 10px">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td width="80%" valign="top">
                                    <p>
                                        <span style="font-size:12px;font-weight:700">{{ $line->offer->deal->custom_field }}</span><br>
                                        <span style="font-size:12px">Ouverture des portes : {{ str_replace(':', 'h', $line->offer->deal->opening) }} / Heure du spectacle : {{ str_replace(':', 'h', $line->offer->deal->start_time) }}</span>
                                    </p>
                                    <p style="font-size:26px;font-weight:700;text-align:center">
                                        @if(isset($profiteers[$index]))
                                            {{ isset($profiteers[$index]['full_name']) ? $profiteers[$index]['full_name'] : $order->user->full_name }}<br>
                                            {{ (isset($profiteers[$index]['is_minor']) && $profiteers[$index]['is_minor'] == 1) ? 'N° CIN Tuteur' : 'N° CIN' }} {{ isset($profiteers[$index]['cin']) ? $profiteers[$index]['cin'] : '' }}
                                        @else
                                            {{ $order->user->full_name }}
                                        @endif
                                    </p>
                                    <p style="font-size:16px;font-weight:700;margin-bottom:0"> {{ number_format($line->product_price, 0) }} Dhs - {{ $line->offer_name }} </p>
                                    @if($ticket->seat)
                                        <div class="place">
                                            @if(($line->offer->deal->seating_plan_id == 8 || $line->offer->deal->seating_plan_id == 10) && strtolower($line->offer_name) == 'balcon')
                                                <p style="font-size:20px;font-weight:700;margin-bottom:0"> Placement libre </p>
                                            @else
                                                <p style="font-size:20px;font-weight:700;margin-bottom:0"> PLACE {{ $ticket->seat }} </p>
                                            @endif
                                        </div>
                                    @endif
                                </td>
                                <td width="20%" valign="middle" align="right">
                                    @if ($line->offer->deal->barcode_type)
                                        <div style="margin-bottom: 10px;">
                                            <img class="logo-partner"
                                                 src="{{ url('/imagecache/partner/' . $line->offer->deal->provider->logo) }}">
                                            @if ($line->offer->deal->category_id == 15)
                                                <br>Du 21 au 29 juin 2019
                                            @endif
                                        </div>
                                        <img src="{{ url('/tickets/barcode/'. $ticket->id . '/100.png') }}"><br>
                                        <span>{{ $ticket->hash }}</span>
                                    @else
                                        <img src="{{ url('/tickets/qrcode/'. $ticket->id . '/200.png') }}">
                                    @endif
                                    <br>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:0px 50px 10px">
                        <p style="font-size:10px;font-weight:bold">
                            Achat {{ $order->created_at }}<br>
                            {{ $line->offer->deal->address }}
                        </p>
                        <p style="font-size:11px;margin-bottom:0;margin-top:20px"><span style="color:red">Important&nbsp;: Ceci n’est pas votre billet, veuillez accéder à votre compte <a
                                        href="https://www.guichet.ma" target="_blank"><span
                                            class="il">Guichet</span>.<span class="il">ma</span></a> dans la rubrique "Mes Commandes" puis imprimer vos tickets.</span>
                        </p>
                        <p style="font-size:12px;font-weight:bold;text-align:right">{{ $key }} / {{ $total }}</p>
                    </td>
                </tr>
                </tbody>
            </table>
        </section>
        @php $key++ @endphp
    @endforeach
@endforeach
</body>
</html>