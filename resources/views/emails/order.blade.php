<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <title>Message de {{ Setting::get('site_name') }}</title>
    <style>
        @media only screen and (max-width: 300px) {
            body {
                width: 218px !important;
                margin: auto !important;
            }

            thead, tbody {
                width: 100%
            }

            .table {
                width: 195px !important;
                margin: auto !important;
            }

            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer {
                width: auto !important;
                display: block !important;
            }

            span.title {
                font-size: 20px !important;
                line-height: 23px !important
            }

            span.subtitle {
                font-size: 14px !important;
                line-height: 18px !important;
                padding-top: 10px !important;
                display: block !important;
            }

            td.box p {
                font-size: 12px !important;
                font-weight: bold !important;
            }

            .table-recap table, .table-recap thead, .table-recap tbody, .table-recap th, .table-recap td, .table-recap tr {
                display: block !important;
            }

            .table-recap {
                width: 200px !important;
            }

            .table-recap tr td, .conf_body td {
                text-align: center !important;
            }

            .address {
                display: block !important;
                margin-bottom: 10px !important;
            }

            .space_address {
                display: none !important;
            }
        }

        @media only screen and (min-width: 301px) and (max-width: 500px) {
            body {
                width: 425px !important;
                margin: auto !important;
            }

            thead, tbody {
                width: 100%
            }

            .table {
                margin: auto !important;
            }

            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer {
                width: auto !important;
                display: block !important;
            }

            .table-recap {
                width: 295px !important;
            }

            .table-recap tr td, .conf_body td {
                text-align: center !important;
            }

            .table-recap tr th {
                font-size: 10px !important
            }

        }

        @media only screen and (min-width: 501px) and (max-width: 768px) {
            body {
                width: 478px !important;
                margin: auto !important;
            }

            thead, tbody {
                width: 100%
            }

            .table {
                margin: auto !important;
            }

            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer {
                width: auto !important;
                display: block !important;
            }
        }

        @media only screen and (max-device-width: 480px) {
            body {
                width: 340px !important;
                margin: auto !important;
            }

            thead, tbody {
                width: 100%
            }

            .table {
                margin: auto !important;
            }

            .logo, .titleblock, .linkbelow, .box, .footer, .space_footer {
                width: auto !important;
                display: block !important;
            }

            .table-recap {
                width: 295px !important;
            }

            .table-recap tr td, .conf_body td {
                text-align: center !important;
            }

            .address {
                display: block !important;
                margin-bottom: 10px !important;
            }

            .space_address {
                display: none !important;
            }
        }
    </style>

</head>
<body style="-webkit-text-size-adjust:none;background-color:#fff;width:650px;font-family:Open-sans, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
<table class="table table-mail"
       style="width:100%;margin-top:10px;-moz-box-shadow:0 0 5px #afafaf;-webkit-box-shadow:0 0 5px #afafaf;-o-box-shadow:0 0 5px #afafaf;box-shadow:0 0 5px #afafaf;filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf,Direction=134,Strength=5)">
    <tr>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        <td align="center" style="padding:7px 0">
            <table class="table" bgcolor="#ffffff" style="width:100%">
                <tr>
                    <td align="center" class="logo" style="border-bottom:4px solid #333333;padding:7px 0">
                        <a title="{{ Setting::get('site_name') }}" href="{{ route('frontend.index') }}"
                           style="color:#337ff1">
                            <img src="{{ asset('email/logo-invert.png') }}" alt="{{ Setting::get('site_name') }}"/>
                        </a>
                    </td>
                </tr>

                <tr>
                    <td align="center" class="titleblock" style="padding:7px 0">
                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                            <span class="title"
                                  style="font-weight:500;font-size:28px;text-transform:uppercase;line-height:33px">Bonjour {{ $order->user->full_name }}
                                ,</span><br/>
                            <span class="subtitle"
                                  style="font-weight:500;font-size:16px;text-transform:uppercase;line-height:25px">Merci d'avoir effectué vos achats sur {{ Setting::get('site_name') }}
                                !</span>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                <tr>
                    <td class="box" style="border:1px solid #D6D4D4;background-color:#f8f8f8;padding:7px 0">
                        <table class="table" style="width:100%">
                            <tr>
                                <td width="10" style="padding:7px 0">&nbsp;</td>
                                <td style="padding:7px 0">
                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                        <p data-html-only="1"
                                           style="border-bottom:1px solid #D6D4D4;margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px">
                                            Détails de la commande </p>
                                        <span style="color:#777">
							                <span style="color:#333"><strong>Commande :</strong></span> {{ $order->reference }}
                                            passée le {{ date('m/d/Y H:i', strtotime($order->created_at)) }}<br/><br/>
                                            <span style="color:#333"><strong>Etat de la commande :</strong></span> {{ trans(config('enums.order_status')[$order->status]) }}<br/><br/>
							                <span style="color:#333"><strong>Mode de paiement :</strong></span>
                                            @if ($order->payment_method == 1)
                                                Carte bancaire
                                            @elseif ($order->payment_method == 2)
                                                PAYEXPRESS (Cash Plus)
                                            @elseif ($order->payment_method == 3)
                                                Virement / versement bancaire
                                            @elseif ($order->payment_method == 4)
                                                Espèce
                                            @endif
                                            <br/><br/>
                                            @if (in_array($order->status, [2, 3, 5]) && $order->payment_method == 1 && !empty($order->payment_transaction))
                                                <span style="color:#333"><strong>Transaction ID :</strong></span> {{ $order->payment_transaction }}
                                                <br/><br/>
                                                <span style="color:#333"><strong>Transaction Date :</strong></span> {{ date('m/d/Y H:i', strtotime($order->payment_transaction_date)) }}
                                                <br/><br/>
                                            @endif
                                            @if (in_array($order->status, [2, 3, 5]) && in_array($order->payment_method, [2, 3, 4]) && !empty($order->payment_transaction_date))
                                                <span style="color:#333"><strong>Date règlement :</strong></span> {{ date('m/d/Y H:i', strtotime($order->payment_transaction_date)) }}
                                                <br/><br/>
                                            @endif
                                            @if (!empty($order->user->address))
                                                <span style="color:#333"><strong>Adresse :</strong></span> {{ $order->address }}
                                            @endif
						                </span>
                                    </font>
                                </td>
                                <td width="10" style="padding:7px 0">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding:7px 0">
                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                            <table class="table table-recap" bgcolor="#ffffff"
                                   style="width:100%;border-collapse:collapse"><!-- Title -->
                                <tr>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">
                                        Produit
                                    </th>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;"
                                        width="17%">
                                        Prix unitaire
                                    </th>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;">
                                        Quantité
                                    </th>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #D6D4D4;background-color: #fbfbfb;color: #333;font-family: Arial;font-size: 13px;padding: 10px;"
                                        width="17%">
                                        Prix total
                                    </th>
                                </tr>
                                @foreach ($order->lines as $product)
                                    <tr>
                                        <td style="border:1px solid #D6D4D4;">
                                            <table class="table">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td>
                                                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                            <strong>{{ $product->product_name }}</strong><br>
                                                            {{ $product->offer_name }}
                                                            @if($product->seats)
                                                                <br>Places : {{ $product->seats }}
                                                            @endif
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="border:1px solid #D6D4D4;">
                                            <table class="table">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td align="right">
                                                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                            {{ number_format($product->product_price, 0, ',', ' ') }} DH
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="border:1px solid #D6D4D4;">
                                            <table class="table">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td align="right">
                                                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                            {{ $product->quantity }}
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="border:1px solid #D6D4D4;">
                                            <table class="table">
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td align="right">
                                                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                            {{ number_format($product->total_price, 0, ',', ' ') }} DH
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="conf_body">
                                    <td bgcolor="#f8f8f8" colspan="2"
                                        style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="2" face="Open-sans, sans-serif" color="#555454">
                                                        <strong>Total payé</strong>
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td bgcolor="#f8f8f8" colspan="2"
                                        style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0">
                                                    <font size="4" face="Open-sans, sans-serif" color="#555454">
                                                        {{ number_format($order->total_paid, 2, ',', ' ') }} DH
                                                    </font>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                <tr>
                    <td class="linkbelow" style="padding:7px 0">
                        <font size="2" face="Open-sans, sans-serif" color="#555454">
                            <span style="color: red"><strong>Important : Ceci n’est pas votre Ticket d'achat</strong></span><br>
                            <span><strong>Veuillez Télécharger votre Ticket d'achat, en cliquant sur ce lien <a
                                            href="{{ route('frontend.order.list') }}" style="color:#337ff1">Historique des commandes.</a></strong></span><br>
                            <span><strong>Il est strictement obligatoire d'imprimer votre ticket d'achat.</strong></span>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
                <tr>
                    <td class="linkbelow" style="padding:7px 0">
                        <font size="2" face="Open-sans, sans-serif" color="#555454">
			                <span>
				            Vous pouvez accéder à tout moment au suivi de votre commande dans <a
                                        href="{{ route('frontend.order.list') }}" style="color:#337ff1">"Historique des commandes"</a> de la rubrique "Mon compte"</a>
                                sur notre site.
                            </span>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td class="space_footer" style="padding:0!important">&nbsp;</td>
                </tr>
            </table>
        </td>
        <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
    </tr>
</table>
</body>
</html>