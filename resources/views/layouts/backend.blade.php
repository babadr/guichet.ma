<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"/>
    <title>@yield('title') - {{ Setting::get('site_name') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="robots" content="noindex,nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/backend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/backend/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    @yield('stylesheets')
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('/backend/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="{{ asset('/backend/css/plugins.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="{{ asset('/backend/css/layout.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/css/darkblue.min.css') }}" rel="stylesheet" type="text/css"
          id="style_color"/>
    <link href="{{ asset('/backend/css/custom.css?v=1.2.1') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/seat-charts.css') }}">
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{ asset('/frontend/images/favicon.png') }}"/>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white">
<div class="page-wrapper">
    @include('backend.includes.header')
    <div class="clearfix"></div>
    <div class="page-container">
        @include('backend.includes.sidebar')
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    @include('backend.includes.breadcrumb')
                </div>
                <h1 class="page-title">
                    @yield('title')
                    <small>@yield('smallTitle')</small>
                </h1>
                <div class="row">
                    <div class="col-md-12">
                        @include('backend.includes.flash')
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.includes.footer')
</div>
<!--[if lt IE 9]>
<script src="{{ asset('/backend/plugins/respond.min.js') }}"></script>
<script src="{{ asset('/backend/plugins/excanvas.min.js') }}"></script>
<script src="{{ asset('/backend/plugins/ie8.fix.min.js') }}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('/backend/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('/backend/plugins/jquery-validation/js/jquery.validate.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/jquery-validation/js/additional-methods.min.js') }}"
        type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/jquery-validation/js/localization/messages_fr.min.js') }}"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('/backend/js/form.validation.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/js/form.controls.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/js/app.js') }}" type="text/javascript"></script>
@yield('scripts')
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="{{ asset('/backend/js/layout.min.js') }}" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>