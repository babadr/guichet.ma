<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!-- Define Charset -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <!-- Responsive Meta Tag -->
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
            <title>@yield('title')</title><!-- Responsive Styles and Valid Styles -->
            <style type="text/css">
                body{
                    width: 100%;
                    background-color: #e8e8e8;
                    margin:0;
                    padding:0;
                    -webkit-font-smoothing: antialiased;
                }
                p,h1,h2,h3,h4{
                    margin-top:0;
                    margin-bottom:0;
                    padding-top:0;
                    padding-bottom:0;
                }
                html{
                    width: 100%;
                }
                table{
                    font-size: 14px;
                    border: 0;
                }
                /* ----------- responsivity ----------- */
                @media only screen and (max-width: 640px){
                    /*------ top header ------ */
                    .header-bg{width: 440px !important; height: 8px !important;}
                    .container{width: 440px !important;}
                    .container-middle{width: 420px !important;}
                    .mainContent{width: 400px !important;}
                    /*------- footer ------*/
                    .top-bottom-bg{width: 420px !important; height: auto !important;}
                }
                @media only screen and (max-width: 479px){
                    /*------ top header ------ */
                    .header-bg{width: 280px !important; height: 8px !important;}
                    .top-header-left{width: 260px !important; text-align: center !important;}
                    .top-header-right{width: 260px !important;}
                    .main-header{text-align: center !important;}
                    .main-subheader{text-align: center !important;}
                    /*------- header ----------*/
                    .logo, .logo a, .logo img {text-align: center !important; margin: 0 auto !important;}
                    .nav{width: 260px !important;}
                    .container{width: 280px !important;}
                    .container-middle{width: 260px !important;}
                    .mainContent{width: 240px !important;}
                    .button, .button a, .button img {text-align: center !important; margin: 0 auto !important;}
                    /*------- footer ------*/
                    .top-bottom-bg{width: 260px !important; height: auto !important;}
                }
            </style>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table border="0" width="100%" cellpadding="0" cellspacing="0">
            <tr bgcolor="#e8e8e8"><td height="30"></td></tr>
            <tr bgcolor="#e8e8e8">
                <td width="100%" align="center" valign="top" bgcolor="#e8e8e8">
                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container">
                        <tr><td height="40"></td></tr>
                        <tr class="logo">
                            <td>
                                <a href="{{ config('app.url') }}" title="{{ Setting::get('site_name') }}" style="display: block; width: 192px; border-style: none !important; border: 0 !important;">
                                    <img style="display: block;" src="{{ asset('email/logo-invert.png') }}" width="196" height="30" alt="{{ Setting::get('site_name') }}" />
                                </a>
                            </td>
                        </tr>
                        <tr><td height="25"></td></tr>
                        <tr>
                            <td style="line-height:8px;"><img style="display: block;" src="{{ asset('email/top-header-bg.png') }}" width="600" height="8" alt="" class="header-bg" /></td>
                        </tr>
                    </table>
                    <!----------   main content---------->
                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ffffff">
                        <!--------- Header  ---------->
                        <tr><td height="30"></td></tr>
                        <tr>
                            <td>
                                <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle">
                                    <tr>
                                        <td>
                                            <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent">
                                                <tr>
                                                    <td class="main-header" style="color: #4babe1; font-size: 24px; font-weight: normal; font-family: Tahoma, Arial, sans-serif;">
                                                        <singleline>
                                                            @yield('title')
                                                        </singleline>
                                                    </td>
                                                </tr>
                                                <tr><td height="25"></td></tr>
                                                <tr>
                                                    <td><img style="display: block;" src="{{ asset('email/sep.jpg') }}" height="1" width="100%" alt="" class="" /></td>
                                                </tr>
                                                <tr><td height="35"></td></tr>
                                                <tr>
                                                    <td class="main-header" style="color: #888888; font-size: 15px; line-height: 26px; font-weight: normal; font-family: Tahoma, Arial, sans-serif;">
                                                        <multiline>
                                                            @yield('content')
                                                        </multiline>
                                                    </td>
                                                </tr>
                                                <tr><td height="25"></td></tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr><!--------- end main section -------->
                        <tr><td height="25"></td></tr>
                    </table>
                    <!---------- footer  -------->
                    <table border="0" width="600" cellpadding="0" cellspacing="0" class="container">
                        <tr>
                            <td style="line-height: 10px;"><img style="display: block;" src="{{ asset('email/bottom-footer-bg.png') }}" alt="" class="header-bg" /></td>
                        </tr>
                        <tr><td height="25"></td></tr>
                        <tr>
                            <td style="color: #888888; font-size: 11px; font-weight: normal; font-family: Tahoma, Arial, sans-serif; line-height: 17px;">
                                <singleline>
                                    @Lang('email.auto_message')
                                </singleline>
                            </td>
                        </tr>
                    </table>
                    <!---------  end footer -------->
                </td>
            </tr>
            <tr><td height="30"></td></tr>
        </table>
    </body>
</html>
