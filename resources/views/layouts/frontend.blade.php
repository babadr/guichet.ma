<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google-site-verification" content="WznVICj03I9b12EpJbln9DT7Y4WTszUv1O4i0UqDH5s" />
    <meta name="robots" content="{{ isset($preview) ? 'noindex, nofollow' : Setting::get('meta_robots') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}
    <link href="{{ asset_cdn('/frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset_cdn('frontend/css/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset_cdn('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset_cdn('frontend/owl.carousel/assets/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset_cdn('frontend/css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset_cdn('frontend/css/animsition.css?v1.0') }}" rel="stylesheet">
    <link href="{{ asset_cdn('frontend/css/plugins.min.css') }}" rel="stylesheet">
    <link href="{{ asset_cdn('/backend/plugins/icheck/skins/all.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset_cdn('frontend/slick/slick.css') }}"/>
    @yield('stylesheets')
    <link href="{{ asset_cdn('/backend/plugins/bootstrap-toastr/toastr.min.css?v2.0') }}" rel="stylesheet" type="text/css" />
    {{--<link href="{{ asset_cdn('frontend/css/style.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{ mix_cdn('frontend/css/all.min.css') }}">
    {{--<link rel="stylesheet" type="text/css" href="{{ asset_cdn('frontend/css/seat-charts.css') }}">--}}
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{ asset_cdn('/frontend/images/favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset_cdn('/frontend/images/favicon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset_cdn('/frontend/images/favicon.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset_cdn('/frontend/images/favicon.png') }}">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115869491-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-115869491-1');
    </script>
{{--    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=web_widget/guichet.zendesk.com"></script>--}}
{{--    <script>--}}
{{--        !function(f,b,e,v,n,t,s)--}}
{{--        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?--}}
{{--            n.callMethod.apply(n,arguments):n.queue.push(arguments)};--}}
{{--            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';--}}
{{--            n.queue=[];t=b.createElement(e);t.async=!0;--}}
{{--            t.src=v;s=b.getElementsByTagName(e)[0];--}}
{{--            s.parentNode.insertBefore(t,s)}(window,document,'script',--}}
{{--            'https://connect.facebook.net/en_US/fbevents.js');--}}
{{--        fbq('init', '2177488325831299');--}}
{{--        fbq('track', 'PageView');--}}
{{--    </script>--}}
{{--    <noscript>--}}
{{--        <img height="1" width="1" src="https://www.facebook.com/tr?id=2177488325831299&ev=PageView&noscript=1"/>--}}
{{--    </noscript>--}}
</head>
<body class="@yield('body-class')">
    @if (Agent::isMobile())
        <div class="popup-instagram">
            <div class="btn-close">
                <img src="{{ asset_cdn('/frontend/images/clear-white.svg') }}" alt="">
            </div>
            <div class="pub-instagram">
                <img src="{{ asset_cdn('/frontend/images/pub-instagram.png') }}" alt="">
                <p>تابعوا حسابنا على</p>
            </div>
            <a class="link-instagram" href="https://www.instagram.com/guichet.ma/" target="_blank">Follow</a>
        </div>
    @endif
    <div class="site-wrapper animsition" data-animsition-in="fade-in" data-animsition-out="fade-out">
        @include('frontend.includes.header')
        @yield('slides')
        <section class="container">
            @include('frontend.includes.flash')
            @yield('content')
        </section>
        @include('frontend.includes.newsletter')
        @include('frontend.includes.footer')
    </div>
    <script src="{{ asset_cdn('/frontend/js/jquery.min.js') }}"></script>
    <script src="{{ asset_cdn('/frontend/js/jquery.downCount.min.js?v2.0') }}"></script>
    <script src="{{ asset_cdn('/frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset_cdn('/frontend/js/jquery.animsition.min.js?v2.0') }}"></script>
    <script src="{{ asset_cdn('/frontend/owl.carousel/owl.carousel.min.js?v2.0') }}"></script>
    <script src="{{ asset_cdn('/frontend/js/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset_cdn('/backend/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/backend/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/backend/plugins/jquery-validation/js/localization/messages_fr.min.js?v2.0') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/backend/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/backend/plugins/icheck/icheck.min.js?v2.0') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/frontend/js/jquery.seat-charts.min.js?v2.0') }}"></script>
    @yield('plugins')
    <script src="{{ asset_cdn('/backend/js/form.validation.js?v2.0') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/frontend/slick/slick.min.js') }}" type="text/javascript"></script>
    {{--<script src="https://cdn.ravenjs.com/3.22.2/raven.min.js" crossorigin="anonymous"></script>--}}
    {{--<script>Raven.config('https://c72b50c83b5444b3abf9f576802a2a6d@sentry.io/281560').install();</script>--}}
    <script src="{{ asset_cdn('/frontend/js/plugins.js') }}"></script>
    <script src="{{ asset_cdn('/frontend/js/guichet.min.js?v2.0') }}"></script>
    <script src="{{ asset_cdn('/frontend/js/app.js?v3.1') }}" type="text/javascript"></script>
    <script src="{{ asset_cdn('/frontend/js/cart.js?v2.0') }}" type="text/javascript"></script>
    @yield('scripts')
</body>
</html>
