<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>@yield('title') - {{ Setting::get('site_name') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta name="robots" content="noindex,nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{ asset('/backend/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="{{ asset('/backend/css/plugins.min.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="{{ asset('/backend/css/login.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('/backend/css/custom.css') }}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{ asset('/frontend/images/favicon.png') }}" />
</head>
<!-- END HEAD -->
<body class=" login">
<!-- BEGIN LOGO -->
<div class="logo">
    <a href="{{ route('backend.index') }}">
        <img src="/backend/img/logo-white-2.png" alt="{{ Setting::get('site_name') }}"/>
    </a>
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    @yield('content')
</div>
<div class="copyright"> {!! date('Y') !!} &copy; {{ config('app.name', 'Laravel 5.5') }}. Admin Panel</div>
<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="{{ asset('/backend/plugins/respond.min.js') }}"></script>
<script src="{{ asset('/backend/plugins/excanvas.min.js') }}"></script>
<script src="{{ asset('/backend/plugins/ie8.fix.min.js') }}"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="{{ asset('/backend/plugins/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="{{ asset('/backend/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/plugins/jquery-validation/js/localization/messages_fr.min.js') }}"
        type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="{{ asset('/backend/js/form.validation.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/js/form.controls.js') }}" type="text/javascript"></script>
<script src="{{ asset('/backend/js/app.js') }}" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
</body>
</html>