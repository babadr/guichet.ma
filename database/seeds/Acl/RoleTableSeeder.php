<?php

namespace Database\Seeds\Acl;

use Illuminate\Database\Seeder;
use App\Models\Acl\Role;

/**
 * RoleTableSeeder.
 *
 * Populate database with users examples.
 *
 */
class RoleTableSeeder extends Seeder
{
    protected $rows = [
        [
            'name' => 'superadmin',
            'display_name' => 'Super Admin',
            'description' => 'Full access',
        ],
        [
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Limited backend access',
        ],
        [
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'Frontend user role',
        ]
    ];

    public function run()
    {
        foreach ($this->rows as $data) {
            $role = Role::where("name", "=", $data['name'])->first();
            if (!$role) {
                $role = new Role;
                $role->fill($data);
                $role->save();
            } else {
                $role->update($data);
            }
        }
    }
}
