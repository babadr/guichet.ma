<?php

namespace Database\Seeds\Acl;

use Illuminate\Database\Seeder;
use App\Models\Acl\CategoryPermission;

class CategoryPermissionsTableSeeder extends Seeder
{
    protected $datas = [
        [
            'id' => '1',
            'name' => 'backend_generality',
            'display_name' => 'Généralités',
            'position' => '1',
            'type' => '0'
        ],
        [
            'id' => '2',
            'name' => 'backend_content_management',
            'display_name' => 'Gestion de contenu',
            'position' => '2',
            'type' => '0'
        ],
        [
            'id' => '3',
            'name' => 'backend_common',
            'display_name' => 'Commun',
            'position' => '3',
            'type' => '0'
        ],
        [
            'id' => '4',
            'name' => 'backend_administration',
            'display_name' => 'Administration',
            'position' => '4',
            'type' => '0'
        ],
        [
            'id' => '5',
            'name' => 'backend_permission',
            'display_name' => 'Permission',
            'position' => '5',
            'type' => '0'
        ],
        [
            'id' => '6',
            'name' => 'backend_deala',
            'display_name' => 'Deals',
            'position' => '6',
            'type' => '0'
        ],
        [
            'id' => '7',
            'name' => 'backend_store',
            'display_name' => 'Stores',
            'position' => '7',
            'type' => '0'
        ]
    ];

    public function run()
    {
        $position = 0;
        foreach ($this->datas as $data) {
            $position++;
            $data['position'] = $position;
            $category = CategoryPermission::where("id", "=", $data['id'])->first();
            if (!$category) {
                $category = new CategoryPermission;
                $category->fill($data);
                $category->save();
            } else {
                $category->update($data);
            }
        }
    }
}