<?php

namespace Database\Seeds\Acl;

use App\Models\Acl\User;
use App\Models\Acl\Role;
use Illuminate\Database\Seeder;

/**
 * UserTableSeeder.
 *
 * Populate database with users examples.
 *
 */
class UserTableSeeder extends Seeder
{

    protected $rows = [
        [
            'id' => 1,
            'first_name' => 'Wafek',
            'last_name' => 'Alaeddine',
            'phone' => '0664746317',
            'address' => 'Adresse de test',
            'city' => 'Fès',
            'country' => 'MAR',
            'email' => 'w.alaeddine@gmail.com',
            'password' => 'PASS2018',
            'active' => 1,
            'role' => 'superadmin',
        ]
    ];

    public function run()
    {
        foreach ($this->rows as $row) {
            $user = User::find($row['id']);
            $role = $row['role'];
            unset($row['role']);
            if (!$user) {
                $user = new User;
                $user->fill($row);
                $user->save();
            } else {
                $user->update($row);
            }
            $user->detachRoles($user->roles);
            $user->attachRole(Role::where('name', '=', $role)->first());
        }
    }

}
