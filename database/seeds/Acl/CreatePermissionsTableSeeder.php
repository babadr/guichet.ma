<?php

namespace Database\Seeds\Acl;

use App\Models\Acl\Permission;
use DB;

class CreatePermissionsTableSeeder extends AbstractPermissionsRolesSeeder
{
    /**
     * Element position
     *
     * @var int
     */
    protected $position = 0;

    /**
     * Permissions array
     *
     * @var array
     */
    protected $datas = [
        [
            'name' => 'backend.dashboard',
            'display_name' => 'app.Dashboard',
            'category_id' => 1,
            'access' => [
                'read' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.setting',
            'display_name' => 'app.settings',
            'category_id' => 1,
            'access' => [
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.page',
            'display_name' => 'page.pages',
            'category_id' => 2,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.post',
            'display_name' => 'post.posts',
            'category_id' => 2,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.faq',
            'display_name' => 'faq.faqs',
            'category_id' => 2,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.contact',
            'display_name' => 'contact.contacts',
            'category_id' => 3,
            'access' => [
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
                'export' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.newsletter',
            'display_name' => 'newsletter.newsletters',
            'category_id' => 3,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.user',
            'display_name' => 'user.users',
            'category_id' => 4,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.role',
            'display_name' => 'role.roles',
            'category_id' => 4,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.permission',
            'display_name' => 'permission.permissions',
            'category_id' => 5,
            'access' => [
                'read' => ['superadmin'],
                'update' => ['superadmin'],
            ],
        ],
        [
            'name' => 'backend.provider',
            'display_name' => 'provider.providers',
            'category_id' => 6,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.category',
            'display_name' => 'category.categories',
            'category_id' => 6,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.deal',
            'display_name' => 'deal.deals',
            'category_id' => 6,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.offer',
            'display_name' => 'offer.offers',
            'category_id' => 6,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.seller',
            'display_name' => 'deal.sellers',
            'category_id' => 6,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.current_event',
            'display_name' => 'deal.current_events',
            'category_id' => 6,
            'access' => [
                'read' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.order',
            'display_name' => 'offer.order',
            'category_id' => 7,
            'access' => [
                'create' => ['superadmin', 'admin'],
                'read' => ['superadmin', 'admin'],
                'update' => ['superadmin', 'admin'],
                'delete' => ['superadmin', 'admin'],
            ],
        ],
        [
            'name' => 'backend.validator',
            'display_name' => 'order.validator',
            'category_id' => 7,
            'access' => [
                'read' => ['validator'],
                'update' => ['validator'],
            ],
        ],
    ];

    public function run()
    {
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//        Permission::truncate();
//        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach ($this->datas as $data) {
            if (!empty($data['access'])) {
                $this->createAccessPermissions($data);
            } else {
                $this->addPermission($data);
            }
        }
    }

    /**
     * Create all access permission in access field, example: create,read,update,delete
     *
     * @param array $data
     */
    protected function createAccessPermissions(array $data)
    {
        foreach ($data['access'] as $key => $access) {
            $permission_access = !is_array($access) ? $access : $key;
            $info = $data;
            if (isset($info['access'][$permission_access])) {
                $info['roles'] = $info['access'][$permission_access];
            }
            unset($info['access']);
            $info['name'] = $info['name'] . '.' . $permission_access;
            $info['display_name'] = $info['display_name'] . ' / ' . 'permission.' . $permission_access;
            $this->addPermission($info);
        }
    }

    /**
     * Create permission
     *
     * @param array $info
     */
    protected function addPermission(array $info)
    {
        $roles = isset($info['roles']) ? $info['roles'] : null;
        if ($roles == '*') {
            $roles = $this->ALL_ROLES;
        }
        unset($info['roles']);
        $permission = Permission::where("name", "=", $info['name'])->first();
        $info['position'] = ++$this->position;
        if (!$permission) {
            $permission = new Permission;
            $permission->fill($info);
            $permission->save();
        }
        /*else {
            $permission->update($info);
        }*/
        if ($roles) {
            $this->attachPermissionToRoles($info['name'], $roles);
        }
    }
}