<?php

namespace Database\Seeds\Content;

use Illuminate\Database\Seeder;
use App\Models\Content\Faq;

/**
 * FaqTableSeeder
 *
 * Populate database with Faqs examples.
 *
 */
class FaqTableSeeder extends Seeder
{

    protected $rows = [
            [
            'id' => 1,
            'title' => "C'est la première fois que je me rends sur le site Guichet, comment puis-je m’inscrire?",
            'content' => "Pour vous inscrire, c'est très simple. Il suffit de vous rendre dans la barre de navigation en haut à droite et de cliquer sur \"Je m'inscris\" en renseignant le formulaire d’inscription.À chaque utilisation, vous n’aurez plus qu’à vous connecter en cliquant sur « Je me connecte ».",
        ],
            [
            'id' => 2,
            'title' => 'L\'inscription est-elle gratuite ?',
            'content' => "Oui, l'inscription est totalement gratuite !"
        ],
            [
            'id' => 3,
            'title' => 'Je recherche une personne pour effectuer un achat et/ou me livrer un produit, dois-je publier ma demande ou consulter la liste des voyageurs ?',
            'content' => "Il est préférable de commencer par publier votre demande. Vous pouvez ensuite consulter la liste des voyageurs et proposer votre annonce à ceux qui correspondentà vos critères de recherche (destination, date du voyage, etc.). Vous avez également la possibilité d’être contacté ultérieurement par des voyageurs intéressés par votre demande."
        ],
            [
            'id' => 4,
            'title' => 'Puis-je effectuer une recherche par produit ?',
            'content' => "Oui, il est possible d’effectuer une recherche avancée en consultant la liste des demandes."
        ],
            [
            'id' => 5,
            'title' => "J'ai publié une demande de livraison et/ou achat, combien de temps va-t-elle rester en ligne ?",
            'content' => "Votre demande reste en ligne pendant 30 jours après la date souhaitée de livraison. Lorsque les 30 jours sont écoulés, vous recevez un e-mail vous informant que votre demande a été désactivée. Elle reste néanmoins disponible dans votre profil privé. Vous pouvez l’actualiser et la remettre en ligne si vous le souhaitez."
        ],
            [
            'id' => 6,
            'title' => "Puis-je remettre en ligne une annonce ?",
            'content' => "Vous pouvez tout à fait réactiver une demande lorsqu'elle a été désactivée. Il vous suffit de cliquer sur le lien du mail \"je réactive ma demande\" envoyé par Guichet."
        ],
            [
            'id' => 7,
            'title' => "J'ai un excédent de bagage, comment profiter de la communauté des voyageurs sur le site Guichetet éviter les frais occasionnés par ma compagnie aérienne ?",
            'content' => "La procédure est la même qu'une demande de livraison ou d'achat. Commencez par créer un profil utilisateur puis publiez votre demande. Vous pouvez ensuite consulter la liste des voyageurs et proposer votre annonce à ceux qui correspondent à vos critères de recherche (destination, date du voyage, etc.). Vous avez également la possibilité d’être contacté ultérieurement par des voyageurs intéressés par votre demande."
        ],
            [
            'id' => 8,
            'title' => "Puis-je demander à un voyageur d'acheter un produit ?",
            'content' => "Vous avez la possibilité de demander à un voyageur d'acheter un produit. Généralement, le voyageur indique sur son profil s'il accepte ou non de réaliser des achats. Mais certains oublient parfois de le mentionner. N'hésitez donc pas à leur demander directement par messages privés."
        ],
            [
            'id' => 9,
            'title' => "Ma question ne figure pas dans cette liste",
            'content' => "Vous avez une autre question ? N'hésitez pas à nous contacter(lien contact) directement, nous sommes là pour vous guider."
        ],
        [
            'id' => 10,
            'title' => "J'ai trouvé un voyageur qui répond à mes critères (dates, destination, etc.), comment puis-je me mettre en relation avec lui ?",
            'content' => "Nous avons créé un système sécurisé de messagerie privée qui vous permet d'échanger avec les utilisateurs de la plateforme. Lorsque vous souhaitez communiquer avec l'un d'eux, vous pouvez vous rendre directement sur son profil et cliquer sur \"envoyer un message\"."
        ],
        [
            'id' => 11,
            'title' => "Quelles informations puis-je donner au voyageur ?",
            'content' => "Nous vous conseillons de donner un maximum d'informations utiles sur le produit : photos, taille, poids, prix, etc. Si c'est le voyageur qui l'achète, indiquez-lui précisément le lieu/magasin/site web où il peut le trouveret demandez-lui si des frais de livraison ou de déplacement sont à prévoir.Ne soyez pas avare de renseignements, la transaction n'en sera que plus simple pour tout le monde !"
        ],
        [
            'id' => 12,
            'title' => "Comment passer un accord avec le voyageur ?",
            'content' => "Une fois que vous vous êtes mis d'accord,le voyageur accepte le deal. Vous en êtes alors informé via un mail, vous demandant de procéder au paiement pour conclure l’accord."
        ],
        [
            'id' => 13,
            'title' => "A quel moment je réalise le paiement ?",
            'content' => "Vous êtes tenu de réaliser le paiement une fois que le deal estaccepté par le voyageur. Nous conservons alors le montant de la transaction via notre prestataire XXX, organisme de paiement certifié par la Banque de France. Le paiement ne sera effectif que lorsque le voyageur aura rempli sa mission."
        ],
        [
            'id' => 14,
            'title' => "Comment puis-je m'assurer que le tarif proposé par le voyageur pour la transaction est correct ?",
            'content' => "Il n'est pas toujours facile de bien évaluer le montant d'une transaction. C'est pourquoi nous avonsréalisé un simulateur pour vous aider à estimer le tarif en fonction des zones géographiques. Ce tarif servira de base pour engager les discussions entre les utilisateurs N'hésitez pas à vous y référer pour vérifier que le tarif proposé par le voyageur est cohérent."
        ],
        [
            'id' => 15,
            'title' => "Quels sont les moyens de paiement acceptés par Guichet ?",
            'content' => "La grande majorité des cartes bancaires internationales sont acceptées. Vous pouvez vous référer à la liste de notre prestataire de paiement en cliquant sur « <a href=\"#\">ce lien</a> »."
        ],
        [
            'id' => 16,
            'title' => "Quelles sont les devises acceptées sur Guichet ?",
            'content' => "La monnaie de référence de Guichet est l’Euro mais vous pouvez payer et être payé dans plusieurs autres devises acceptées par notre prestataire XXX que vous trouverez en cliquant sur « <a href=\"#\">ce lien</a> »."
        ],
        [
            'id' => 17,
            'title' => "Qu'en est-il de la protection de mes données bancaires ?",
            'content' => "Vos données bancaires sont entièrement sécurisées par notre prestataire XXX, agréé par la Banque de France. Pour en savoir plus sur notre charte de confiance et sécurité, n'hésitez pas à consulter <a href=\"#\">la page dédiée</a>."
        ],
        [
            'id' => 18,
            'title' => "Puis-je annuler une transaction après avoir procédé au paiement ?",
            'content' => "<p class=\"desc\">Des frais d'annulation ainsi qu'un dédommagement envers le voyageur sont à prévoir si vous annulez une demande. Ils sont plus ou moins importants en fonction de l'avancée et de la nature du deal : </p><ul><li> Si vous annulez une demande de livraison de colis ou d'excédent de bagageavant le départ du voyageur, vous devez vous acquitter de frais d'annulation envers Guichet, correspondant au montant des frais de services versés lors du paiement. Vous êtes également tenu de verser un dédommagement au voyageur de l'ordre 50% de la récompense initialement prévue lors du deal.</li><li> Si vous annulez une demande de livraison de colis ou d'excédent de bagage après le départ du voyageur, vous êtes tenu de verser un dédommagement au voyageur de l'ordre de 100% de la récompense initialement prévue lors du deal. Vous devez également vous acquitter de frais d'annulation envers Guichet, correspondant au montant des frais de services versés lors du paiement. </li><li> Dans le cas d'une annulation de demande d'achat une fois que le paiement a été réalisé, vous êtes redevable envers le voyageur du montant total de l'achat. Vous devrez également verser au voyageur un dédommagement de l'ordre de 50% de la récompense prévue lors du deal. </li><li> Dans le cas d’un achat, si le demandeur annule à n’importe quel moment après le paiement, il sera redevable du montant de l’achat engagé par le voyageur,sauf si un accord est trouvé accord avec le voyageur. De plus, des frais d'annulation correspondant au montant des Frais de Service versés lors du paiement sont dus à Guichet et le voyageur perçoit un dédommagement de 50% de la récompense prévue. En conséquence, le demandeur sera remboursé de la somme qu’il a payée diminuée du montant de l’achat engagé par le voyageur, du montant des Frais de Service dus à Guichet et du dédommagement de 50% de la récompense attendue par le voyageur.</li></ul>Pour en savoir plus sur nos conditions générales de vente, n'hésitez pas à consulter <a href=\"/fr/content/mentions-legales\">la page dédiée</a><p></p>"
        ],
        [
            'id' => 19,
            'title' => "Ma question ne figure pas dans cette liste",
            'content' => "Vous avez une autre question ? N'hésitez pas à nous <a href=\"/contact\">contacter</a> directement, nous sommes là pour vous guider."
        ],
    ];

    public function run()
    {
        foreach ($this->rows as $row) {
            $faq = Faq::where('id', '=', $row['id'])->first();
            if (!$faq) {
                $faq = new Faq;
                $faq->fill($row);
                $faq->save();
            } else {
                $faq->update($row);
            }
        }
    }

}
