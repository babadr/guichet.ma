<?php

namespace Database\Seeds\Deal;

use App\Models\Deal\Category;
use Illuminate\Database\Seeder;
use App\Models\Content\Seo;

/**
 * CategoryTableSeeder.
 *
 * Populate database with users examples.
 *
 */
class CategoryTableSeeder extends Seeder
{

    protected $rows = [
        [
            'id' => 1,
            'title' => 'Voyages',
            'parent_id' => null,
            'show_on_menu' => true,
            'show_on_home' => true,
            'order' => 1,
            'color' => 'blue',
            'icon' => 'fa fa-plane',
            'active' => true,
            'slug' => 'deals/voyages'
        ],
        [
            'id' => 2,
            'title' => 'Restauration',
            'parent_id' => null,
            'show_on_menu' => true,
            'show_on_home' => true,
            'order' => 2,
            'color' => 'green',
            'icon' => 'fa fa-cutlery',
            'active' => true,
            'slug' => 'deals/restauration'
        ],
        [
            'id' => 3,
            'title' => 'Loisirs',
            'parent_id' => null,
            'show_on_menu' => true,
            'show_on_home' => false,
            'order' => 3,
            'color' => 'yellow',
            'icon' => 'fa fa-shopping-basket',
            'active' => true,
            'slug' => 'deals/loisirs'
        ],
        [
            'id' => 4,
            'title' => 'Billetterie',
            'parent_id' => null,
            'show_on_menu' => true,
            'show_on_home' => true,
            'order' => 4,
            'color' => 'orange',
            'icon' => 'fa fa-ticket',
            'active' => true,
            'slug' => 'deals/billetterie'
        ],
        [
            'id' => 5,
            'title' => 'Sport',
            'parent_id' => 4,
            'show_on_menu' => true,
            'show_on_home' => false,
            'order' => 5,
            'color' => 'orange',
            'icon' => 'fa fa-ticket',
            'active' => true,
            'slug' => 'deals/billetterie/sport'
        ],
        [
            'id' => 6,
            'title' => 'Musique & Concert',
            'parent_id' => 4,
            'show_on_menu' => true,
            'show_on_home' => false,
            'order' => 6,
            'color' => 'orange',
            'icon' => 'fa fa-ticket',
            'active' => true,
            'slug' => 'deals/billetterie/musique-concert'
        ],
        [
            'id' => 7,
            'title' => 'Art & Théâtre',
            'parent_id' => 4,
            'show_on_menu' => true,
            'show_on_home' => false,
            'order' => 7,
            'color' => 'orange',
            'icon' => 'fa fa-ticket',
            'active' => true,
            'slug' => 'deals/billetterie/art-theatre'
        ],
    ];

    public function run()
    {
        foreach ($this->rows as $row) {
            $category = Category::where('id', '=', $row['id'])->first();
            $slug = $row['slug'];
            unset($row['slug']);
            if (!$category) {
                $category = new Category;
                $category->fill($row);
                $category->save();
            } else {
                $category->update($row);
            }
            $this->saveSeoMetas($category, $slug);
        }
    }

    /**
     * save SEO Metas
     *
     * @param Category $category
     * @param string $slug
     *
     * @return void
     */
    protected function saveSeoMetas(Category $category, $slug)
    {
        $attributes['model_id'] = $category->id;
        $attributes['model_class'] = get_class($category);
        $attributes['seo_alias'] = $slug;
        $attributes['seo_route'] = $category->route;
        $id = $category->seo ? $category->seo->id : null;
        if (empty($id)) {
            $seo = new Seo();
            $seo->fill($attributes);
            $seo->save();
        } else {
            $seo = Seo::find($id);
            $seo->update($attributes);
        }
    }

}
