<?php

namespace Database\Seeds\Deal;

use App\Models\Deal\City;
use Illuminate\Database\Seeder;

/**
 * UserTableSeeder.
 *
 * Populate database with users examples.
 *
 */
class CityTableSeeder extends Seeder
{

    protected $rows = [
        ["city" => "Casablanca"],
        ["city" => "Rabat"],
        ["city" => "Salé"],
        ["city" => "Tanger"],
        ["city" => "Marrakech"],
        ["city" => "Fès"],
        ["city" => "Meknès"],
        ["city" => "Oujda"],
        ["city" => "Kénitra"],
        ["city" => "Agadir"],
        ["city" => "Tétouan"],
        ["city" => "Témara"],
        ["city" => "Safi"],
        ["city" => "Laâyoune"],
        ["city" => "Mohammédia"],
        ["city" => "Khouribga"],
        ["city" => "ElJadida"],
        ["city" => "BéniMellal"],
        ["city" => "Taza"],
        ["city" => "Meknes"],
        ["city" => "Dakhla"],
        ["city" => "Khémisset"],
        ["city" => "Taourirt"],
    ];

    public function run()
    {
        foreach ($this->rows as $row) {
            $user = City::find($row['city']);
            if (!$user) {
                $user = new City;
                $user->fill($row);
                $user->save();
            } else {
                $user->update($row);
            }
        }
    }

}
