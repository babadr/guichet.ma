<?php

namespace Database\Seeds\Deal;

use App\Models\Deal\Scene;
use Illuminate\Database\Seeder;

/**
 * UserTableSeeder.
 *
 * Populate database with users examples.
 *
 */
class SceneTableSeeder extends Seeder
{

    protected $rows = [
        [
            'id' => 1,
            'name' => 'Salé',
            'code' => 'SAL',
            'export_code' => 'SALE',
            'quantity' => 100,
        ],
        [
            'id' => 2,
            'name' => 'Bouregreg',
            'code' => 'BRG',
            'export_code' => 'BRG',
            'quantity' => 100,
        ],
        [
            'id' => 3,
            'name' => 'OLM SOUISSI',
            'code' => 'OLM',
            'export_code' => 'OLM',
            'quantity' => 100,
        ],
        [
            'id' => 4,
            'name' => 'Nahda',
            'code' => 'NHD',
            'export_code' => 'NHD',
            'quantity' => 100,
        ],
        [
            'id' => 5,
            'name' => 'Théâtre Med V',
            'code' => 'TNV',
            'export_code' => 'TMV',
            'quantity' => 100,
        ],
        [
            'id' => 6,
            'name' => 'Chellah',
            'code' => 'CHL',
            'export_code' => 'CHL',
            'quantity' => 100,
        ]
    ];

    public function run()
    {
        foreach ($this->rows as $row) {
            $scene = Scene::find($row['id']);
            if (!$scene) {
                $scene = new Scene;
                $scene->fill($row);
                $scene->save();
            } else {
                $scene->update($row);
            }
        }
    }

}
