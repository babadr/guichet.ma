<?php

use Illuminate\Database\Seeder;
use Database\Seeds\Acl\UserTableSeeder;
use Database\Seeds\Acl\RoleTableSeeder;
use Database\Seeds\Acl\CategoryPermissionsTableSeeder;
use Database\Seeds\Acl\CreatePermissionsTableSeeder;
use Database\Seeds\Deal\CityTableSeeder;
use Database\Seeds\Deal\CategoryTableSeeder;
use Database\Seeds\Setting\SettingTableSeeder;
use Database\Seeds\Content\PageTableSeeder;
use Database\Seeds\Content\PostTableSeeder;
use Database\Seeds\Content\FaqTableSeeder;
use Database\Seeds\Deal\SceneTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(RoleTableSeeder::class);
        //$this->call(CategoryPermissionsTableSeeder::class);
        //$this->call(CreatePermissionsTableSeeder::class);
        /*$this->call(UserTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        $this->call(PageTableSeeder::class);
        $this->call(PostTableSeeder::class);
        $this->call(FaqTableSeeder::class);*/
        $this->call(SceneTableSeeder::class);
    }
}
