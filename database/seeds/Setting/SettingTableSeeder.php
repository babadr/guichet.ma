<?php

namespace Database\Seeds\Setting;

use App\Models\Setting\Setting;
use Illuminate\Database\Seeder;

/**
 * SettingTableSeeder
 *
 * Populate database with Settings examples.
 *
 */
class SettingTableSeeder extends Seeder
{

    protected $rows = [
        [
            'key' => 'site_name',
            'value' => 'Guichet.ma',
        ],
        [
            'key' => 'site_slogan',
            'value' => 'Deals & Billetterie',
        ],
        [
            'key' => 'site_description',
            'value' => 'Un site de billetterie nouvelle génération vous offrant la possibilité d’acheter en ligne des tickets pour les événements de votre choix. Sa vocation est de vous proposer l’offre de concerts, sports, spectacles, cinéma, festivals, théâtre, humour, voyages et evasions la plus large du Maroc en collaboration avec les organisateurs d’événements.',
        ],
        [
            'key' => 'address_1',
            'value' => '22, Bd Yacoub Al Mansour, MAARIF',
        ],
        [
            'key' => 'address_2',
            'value' => '',
        ],
        [
            'key' => 'zip_code',
            'value' => '20000',
        ],
        [
            'key' => 'city',
            'value' => 'Casablanca',
        ],
        [
            'key' => 'country',
            'value' => 'Maroc',
        ],
        [
            'key' => 'phone',
            'value' => '+212 522 23 39 34',
        ],
        [
            'key' => 'email',
            'value' => 'contact@guichet.ma',
        ],
        [
            'key' => 'location',
            'value' => '33.578071,-7.63768',
        ],
        [
            'key' => 'facebook_url',
            'value' => 'https://www.facebook.com/guichet.ma/',
        ],
        [
            'key' => 'twitter_url',
            'value' => 'https://twitter.com/guichet_ma',
        ],
        [
            'key' => 'instagram_url',
            'value' => 'https://www.instagram.com/guichet.ma/',
        ],
        [
            'key' => 'linkedin_url',
            'value' => 'https://www.linkedin.com/company/guichet-ma/',
        ],
        [
            'key' => 'google_url',
            'value' => 'https://plus.google.com/u/0/116600951703676552113',
        ],
        [
            'key' => 'pinterest_url',
            'value' => '',
        ],
        [
            'key' => 'youtube_url',
            'value' => '',
        ],
        [
            'key' => 'google_map_api_key',
            'value' => 'AIzaSyAwW6mYuFcyoKomDErbJhREBu7PkQuBKBE',
        ],
    ];

    public function run()
    {
        foreach ($this->rows as $row) {
            $setting = Setting::where('key', '=', $row['key'])->first();
            if (!$setting) {
                $setting = new Setting;
                $setting->fill($row);
                $setting->save();
            } else {
                $setting->update($row);
            }
        }
    }

}