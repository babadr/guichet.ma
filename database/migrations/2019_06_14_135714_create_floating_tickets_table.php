<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFloatingTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('floating_tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scene_id')->unsigned();
            $table->string('hash');
            $table->string('day', 5);
            $table->boolean('selected')->unsigned()->default(0);
            $table->timestamps();
            $table->foreign('scene_id')->references('id')->on('scenes')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('floating_tickets');
    }
}
