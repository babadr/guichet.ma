<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('reference', 30)->unique();
            $table->text('address');
            $table->tinyInteger('status')->unsigned()->default(1);
            $table->decimal('total_paid', 10, 2)->unsigned()->default(0.00);
            $table->string('payment_reference')->nullable();
            $table->string('payment_track')->nullable();
            $table->string('payment_transaction')->nullable();
            $table->dateTime('payment_transaction_date')->nullable();
            $table->string('payment_response_code')->nullable();
            $table->text('payment_reason_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}