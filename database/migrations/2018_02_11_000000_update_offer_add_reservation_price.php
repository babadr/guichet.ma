<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class UpdateOfferAddReservationPrice
 */
class UpdateOfferAddReservationPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('offers', function (Blueprint $table) {
            if(!Schema::hasColumn('offers', 'reservation_price')) {
                $table->double('reservation_price')->nullable();
            }
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            if(Schema::hasColumn('offers', 'reservation_price')) {
                $table->dropColumn('reservation_price');
            }
        });
    }
}
