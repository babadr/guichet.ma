<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 255)->unique();
            $table->string('password');
            $table->string('last_name', 65);
            $table->string('first_name', 65);
            $table->string('phone', 25);
            $table->string('address')->nullable();
            $table->string('city', 65)->nullable();
            $table->string('country', 15)->nullable();
            $table->string('avatar')->nullable();
            $table->boolean('active')->unsigned()->default(0);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
