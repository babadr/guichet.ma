<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_line_id')->unsigned();
            $table->string('full_name', 65);
            $table->string('email', 255);
            $table->string('phone', 25);
            $table->string('address')->nullable();
            $table->string('city', 65)->nullable();
            $table->string('zip_code', 20)->nullable();
            $table->string('avatar')->nullable();
            $table->string('cin')->nullable();
            $table->date('birth_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('order_line_id')->references('id')->on('order_lines')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipients');
    }
}
