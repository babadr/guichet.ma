<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNewslettersAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('newsletters', function (Blueprint $table) {
            $table->string('phone', 25)->nullable()->after('email');
            $table->unsignedTinyInteger('source')->nullable()->after('phone')->default(1);
            $table->unsignedTinyInteger('civility')->nullable()->after('source');
            $table->date('birth_date')->nullable()->after('civility');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('newsletters', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('source');
            $table->dropColumn('civility');
            $table->dropColumn('birth_date');
        });
    }
}
