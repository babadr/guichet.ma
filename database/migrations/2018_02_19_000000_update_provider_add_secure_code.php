<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class UpdateProviderAddSecureCode
 */
class UpdateProviderAddSecureCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('providers', function (Blueprint $table) {
            if(!Schema::hasColumn('providers', 'secure_code')) {
                $table->string('secure_code', 30)->unique();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('providers', function (Blueprint $table) {
            if(Schema::hasColumn('providers', 'number')) {
                $table->dropColumn('secure_code');
            }
        });
    }
}
