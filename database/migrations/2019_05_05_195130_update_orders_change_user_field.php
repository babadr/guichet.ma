<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOrdersChangeUserField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement('ALTER TABLE `orders` MODIFY `user_id` INTEGER UNSIGNED NULL;');
        \DB::statement('UPDATE `orders` SET `user_id` = NULL WHERE `user_id` = 0;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement('UPDATE `orders` SET `user_id` = 0 WHERE `user_id` IS NULL;');
        \DB::statement('ALTER TABLE `orders` MODIFY `user_id` INTEGER UNSIGNED NOT NULL;');
    }
}
