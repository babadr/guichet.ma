<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfiteersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiteers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_line_id')->unsigned();
            $table->string('full_name', 65);
            $table->string('cin', 20)->nullable();
            $table->timestamps();
            $table->foreign('order_line_id')->references('id')->on('order_lines')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiteers');
    }
}
