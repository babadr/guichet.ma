<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDealMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deal_medias', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('deal_id')->unsigned();
            $table->foreign('deal_id')->references('id')->on('deals')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('filename');
            $table->boolean('is_cover')->nullable()->default(0);
            $table->boolean('is_miniature')->nullable()->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('deal_medias');
	}

}
