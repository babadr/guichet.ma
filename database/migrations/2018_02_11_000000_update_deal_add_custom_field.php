<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class UpdateDealAddCustomField
 */
class UpdateDealAddCustomField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deals', function (Blueprint $table) {
            if(!Schema::hasColumn('deals', 'custom_field')) {
                $table->string('custom_field', 500)->nullable();
            }
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deals', function (Blueprint $table) {
            if(Schema::hasColumn('deals', 'custom_field')) {
                $table->dropColumn('custom_field');
            }
        });
    }
}
