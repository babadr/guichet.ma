/**
 Core script to handle the entire theme and core functions
 **/
var FrontApp = function () {
    var csrfToken = $('meta[name="csrf-token"]').attr('content');
    var initCsrfToken = function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        });
    };
    var initToastr = function () {
        if (window.toastr != undefined) {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
        }
    };
    // Handles custom checkboxes & radios using jQuery iCheck plugin
    var handleiCheck = function () {
        if (!$().iCheck) {
            return;
        }

        $('.icheck').each(function () {
            var checkboxClass = $(this).attr('data-checkbox') ? $(this).attr('data-checkbox') : 'icheckbox_minimal-grey';
            var radioClass = $(this).attr('data-radio') ? $(this).attr('data-radio') : 'iradio_minimal-grey';

            if (checkboxClass.indexOf('_line') > -1 || radioClass.indexOf('_line') > -1) {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass,
                    insert: '<div class="icheck_line-icon"></div>' + $(this).attr("data-label")
                });
            } else {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass
                });
            }
        });
    };
    /**
     * Initialize select2
     */
    var handleSelect2 = function () {
        if ($.fn.select2) {
            $('.select2').each(function () {
                $(this).select2();
            });
        }
    };
    var newsletterSubscription = function () {
        $(document).on('submit', '#newsletter-form', function (e) {
            e.preventDefault();
            var $this = $(this);
            $.ajax({
                method: 'POST',
                url: $this.attr('action'),
                data: $this.serialize(),
                success: function (response) {
                    if (response.success) {
                        toastr.success(response.message, '');
                        $('#newsletter-email').val('');
                    } else {
                        toastr.error(response.message, '');
                    }
                },
                error: function (response) {
                    toastr.error('Une erreur est survenue.', '');
                }
            });
            return false;
        });
    };
    var onTabToggled = function () {
        $('ul.nav-tabs a').on('click', function (event, tab) {
            var element = $(this).attr('href');
            if ($(element + ' .category-slick').length) {
                //console.log($(element + ' .category-slick').slick("getSlick"));
                //$(element + ' .category-slick').slick("getSlick").setPosition();
            }
        });
    };
    var initLazyloader = function () {
        $("img.lazy").lazyload();
    };
    var navSearch = function () {
        $('.open-close-search').on('click', function (e) {
            e.preventDefault();
            $('.nav-search').toggleClass('is-closed');
        });
    };
    var navPlus = function () {
        $('.close-nav-plus').on('click', function (e) {
            e.preventDefault();
            $('.nav-plus').toggleClass('is-closed');
        });
    };

    var initInstagram = function () {
        setTimeout(function(){
            $(".popup-instagram").addClass("is-active");
        }, 3000);
        $('.btn-close').on('click', function (e) {
            e.preventDefault();
            $(".popup-instagram").removeClass("is-active").hide(300);
        });
    };
    return {
        init: function () {
            initCsrfToken();
            initToastr();
            handleiCheck();
            handleSelect2();
            newsletterSubscription();
            navSearch();
            navPlus();
            initInstagram();
            //initLazyloader();
            //onTabToggled();
        },
        initAjax: function () {
        }
    };
}();

jQuery(document).ready(function () {
    FrontApp.init();
    AppFormValidation.init();
    FrontCart.init();
});