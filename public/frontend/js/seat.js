var SeatingPlan = function () {

    var sc;
    var selectedSeats = [];

    var handleViewPlan = function () {
        $(document).on("click", ".view-seating-plan", function (e) {
            e.preventDefault();
            var $this = $(this);
            $this.addClass('disabled');
            $.ajax({
                method: "POST",
                url: $this.attr("href"),
                success: function (response) {
                    if (response.success) {
                        $this.removeClass('disabled');
                        $(response.html).modal('show');
                    } else {
                        toastr.error(response.msg);
                        $this.removeClass('disabled');
                    }
                },
                error: function (response) {
                    toastr.error(response, "");
                    $this.removeClass('disabled');
                }
            });
        });
    };

    var onHideModal = function () {
        $(document).on('hidden.bs.modal', '#view-event-setting-plan', function () {
            $('#view-event-setting-plan').remove();
        });
    };

    var onShowModal = function () {
        $(document).on('shown.bs.modal', '#view-event-setting-plan', function () {
            initSeatCharts();
            initReservedSeats();
            initSelectedSeats();
        });
    };

    var initSeatCharts = function () {
        var $cart = $('#selected-seats'), $counter = $('#counter'), $total = $('#total');
        sc = $('#seat-map').seatCharts({
            map: window.mapCharts,
            seats: window.seats,
            naming: {
                top: false,
                left: true,
                right: true,
                getLabel: function (character, row, column) {
                    return row + column;
                },
                rows: window.rows,
            },
            legend: {
                node: $('#legend'),
                items: window.items
            },
            click: function () {
                if (this.status() == 'available') {
                    $('<li>' + this.data().category + ' Place #' + this.settings.label + ': <b>' + this.data().price + ' DH</b> <a href="#" class="cancel-cart-item"><i class="ti-trash"></i></a></li>')
                        .attr('id', 'cart-item-' + this.settings.id)
                        .attr('data-seatId', this.settings.id)
                        .appendTo($cart);

                    selectedSeats.push({id: this.data().offer_id, seat: this.settings.id});
                    $counter.text(sc.find('selected').length + 1);
                    $total.text(recalculateTotal(sc) + parseFloat(this.data().price));

                    return 'selected';
                } else if (this.status() == 'selected') {
                    var seat = this.settings.id;
                    $counter.text(sc.find('selected').length - 1);
                    $total.text(recalculateTotal(sc) - parseFloat(this.data().price));
                    $('#cart-item-' + this.settings.id).remove();

                    selectedSeats = selectedSeats.filter(function (place) {
                        return place.seat !== seat;
                    });
                    return 'available';
                } else if (this.status() == 'unavailable') {

                    return 'unavailable';
                } else {
                    return this.style();
                }
            }
        });

        $('#selected-seats').on('click', '.cancel-cart-item', function () {
            var seat = $(this).parents('li:first').attr('data-seatId');
            sc.get(seat).click();
            selectedSeats = selectedSeats.filter(function (place) {
                return place.seat !== seat;
            });
        });
    };

    var initReservedSeats = function () {
        $.ajax({
            method: "POST",
            url: window.reservedSeats,
            success: function (response) {
                if (response.success) {
                    sc.get(response.seats).status('unavailable');
                } else {
                    toastr.error(response.msg);
                }
            },
            error: function (response) {
                toastr.error(response, "");
            }
        });
    };

    var initSelectedSeats = function () {
        $.ajax({
            method: "POST",
            url: window.selectedCartSeats,
            success: function (response) {
                if (response.success) {
                    $.each(response.seats, function (index, item) {
                        $("#seat-map").find("div[data-seat=" + item + "]").click();
                    });
                } else {
                    toastr.error(response.msg);
                }
            },
            error: function (response) {
                toastr.error(response, "");
            }
        });
    };

    var recalculateTotal = function (sc) {
        var total = 0;
        sc.find('selected').each(function () {
            total += parseFloat(this.data().price);
        });

        return total;
    };

    var handleAddToCart = function () {
        $(document).on("click", "#event-post-order", function (e) {
            e.preventDefault();
            if (selectedSeats.length > 0) {
                var $this = $(this);
                $this.addClass('disabled');
                $.ajax({
                    method: "POST",
                    url: $this.attr("href"),
                    data: JSON.stringify({Seats: selectedSeats}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.success) {
                            $this.removeClass('disabled');
                            window.location = response.redirect;
                        } else {
                            toastr.error(response.msg);
                            $this.removeClass('disabled');
                        }
                    },
                    error: function (response) {
                        toastr.error(response, "");
                        $this.removeClass('disabled');
                    }
                });
            } else {
                toastr.error("Veuillez sélectionner une ou plusieurs places.", "");
            }
        });
    };

    return {
        init: function () {
            handleViewPlan();
            onHideModal();
            onShowModal();
            handleAddToCart();
        }
    };
}();