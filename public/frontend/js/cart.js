var FrontCart = function () {

    var addItemToCart = function () {
        $(document).on("click", ".add-to-cart", function (e) {
            e.preventDefault();
            var $this = $(this)
            $this.addClass('disabled');
            $.ajax({
                method: "POST",
                url: $this.attr("href"),
                data: {
                    offer_id: $this.attr("data-id-offer"),
                    quantity: $("#quantity-wanted").val()
                },
                success: function (response) {
                    if (response.success) {
                        $('#confirm-add-to-cart').remove();
                        $('#total-items').html(response.count);
                        $this.removeClass('disabled');
                        $(response.html).modal('show');
                    } else {
                        toastr.error(response.msg);
                        $this.removeClass('disabled');
                    }
                },
                error: function (response) {
                    toastr.error(response, "");
                    $this.removeClass('disabled');
                }
            });
        });
    };
    var deleteItemFromCart = function () {
        $(document).on("click", ".remove-cart-item", function (e) {
            e.preventDefault();
            var $this = $(this);
            bootbox.dialog({
                message: "Êtes-vous sûr de vouloir supprimer ce produit ?",
                title: "Confirmer la suppression",
                buttons: {
                    success: {
                        label: "Annuler",
                        callback: function () {
                            return;
                        }
                    },
                    danger: {
                        label: "Confirmer",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                method: 'delete',
                                url: $this.attr('data-href'),
                                success: function (response) {
                                    if (response.success) {
                                        toastr.success(response.msg);
                                        if (response.total == '0 DH') {
                                            window.location.reload();
                                        }
                                        $("#cart-container").html(response.cart);
                                        $("#total-items").html(response.count);
                                        $(".shop-total-paid").html(response.total);
                                    } else {
                                        toastr.error(response.msg);
                                    }
                                },
                                error: function (response) {
                                    toastr.error("Une erreur est survenue lors de la suppression de ce produit.");
                                }
                            });
                        }
                    }
                }
            });
        });
    };
    var updateCartItem = function (quantity, url, type) {
        $.ajax({
            method: 'post',
            url: url,
            data: {
                quantity: quantity,
                type: type
            },
            success: function (response) {
                if (response.success) {
                    toastr.success(response.msg);
                    $("#cart-container").html(response.cart);
                    $("#total-items").html(response.count);
                    $(".shop-total-paid").html(response.total);
                    return true;
                } else {
                    toastr.error(response.msg);
                }
            },
            error: function (response) {
                toastr.error("Une erreur est survenue lors de la mise à jour de votre panier.");
            }
        });

        return false;
    };
    var handleChangeQuantity = function () {
        $(document).on("click", ".product_quantity_up", function (e) {
            e.preventDefault();
            var fieldName = $(this).data("field-qty");
            var currentVal = parseInt($("input[name=" + fieldName + "]").val());
            if (!isNaN(currentVal)) {
                $("input[name=" + fieldName + "]").val(currentVal + 1).trigger("keyup");
            }
        });
        $(document).on("click", ".product_quantity_down", function (e) {
            e.preventDefault();
            var fieldName = $(this).data("field-qty");
            var currentVal = parseInt($("input[name=" + fieldName + "]").val());
            if (!isNaN(currentVal) && currentVal > 1) {
                $("input[name=" + fieldName + "]").val(currentVal - 1).trigger("keyup");
            } else {
                $("input[name=" + fieldName + "]").val(1);
            }
        });
        $(document).on('change', 'input[name=qty]', function (e) {
            var currentVal = parseInt($(this).val());
            if (isNaN(currentVal) || currentVal <= 1) {
                $(this).val(1);
            }
        });
    };
    var handleChangeOffer = function () {
        $(document).on('ifChecked', 'input.offer-icheck', function () {
            $('#ajax-add-to-cart').attr('data-id-offer', $(this).val());
        });
    };
    var handleChangeCartQuantity = function () {
        $(document).on("click", ".cart-quantity-up", function (e) {
            e.preventDefault();
            var $this = $(this);
            var rowID = $this.attr('data-id-cart');
            var $fieldName = $("#quantity-wanted-" + rowID);
            var currentVal = parseInt($fieldName.val());
            if (!isNaN(currentVal)) {
                $fieldName.val(currentVal + 1);
            }
            if (!updateCartItem($fieldName.val(), $this.attr('href'), 'up')) {
                $fieldName.val(currentVal);
            }
            //updateCartItem($fieldName.val(), $this.attr('href'), 'up');
        });
        $(document).on("click", ".cart-quantity-down", function (e) {
            e.preventDefault();
            var $this = $(this);
            var rowID = $this.attr('data-id-cart');
            var $fieldName = $("#quantity-wanted-" + rowID);
            var currentVal = parseInt($fieldName.val());
            if (!isNaN(currentVal) && currentVal > 1) {
                $fieldName.val(currentVal - 1);
            } else {
                $fieldName.val(1);
            }
            updateCartItem($fieldName.val(), $this.attr('href'), 'down');
        });
    };
    var handleCheckoutPayment = function () {
        $(document).on("submit", "#checkout-payment", function (e) {
            var validated = true;
            var cinList = [];
            $(document).find("#cart-items").find(".profiteer").each(function () {
                var $this = $(this);
                var eleValue = $.trim($this.val()).toLowerCase();
                if (eleValue === '') {
                    toastr.error("Vous devez renseigner tous les bénéficiaires de votre commande.");
                    $(this).focus();
                    validated = false;
                    return false;
                } else {
                    if ($this.hasClass('cin')) {
                        var eleCheckbox = $this.attr('name').replace("cin", "is_minor");
                        if ($.inArray(eleValue, cinList) !== -1 && $("input[name='" + eleCheckbox + "']:checked").val() === 'No') {
                            toastr.error("Le N° CIN " + eleValue + " a déjà été renseigné.");
                            $(this).focus();
                            validated = false;
                            return false;
                        }

                        if ($("input[name='" + eleCheckbox + "']:checked").val() === 'No') {
                            cinList.push(eleValue);
                        }
                    }
                }
            });

            if (!$("#user-form").valid()) {
                validated = false;
            }

            if (validated) {
                $(document).find("#cart-items").find(".profiteer").each(function () {
                    if ($.trim($(this).val()) !== '') {
                        $('<input />').attr("type", "hidden")
                            .attr("name", $(this).attr("name"))
                            .attr("value", $(this).val())
                            .appendTo('#checkout-payment');
                    }
                });

                $(document).find("#cart-items").find(".check-profiteer").each(function () {
                    if ($(this).is(':checked')) {
                        $('<input />').attr("type", "hidden")
                            .attr("name", $(this).attr("name"))
                            .attr("value", $(this).val())
                            .appendTo('#checkout-payment');
                    }
                });
            }

            return validated;
        });
    };
    var onHideModal = function () {
        $(document).on('hidden.bs.modal', '#confirm-add-to-cart', function () {
            $('#confirm-add-to-cart').remove();
        });
    };
    var inputsCollapse = function () {
        $('.inputs-collapse .collapse').collapse({
            toggle: false
        });
        $('.inputs-collapse').find('input').on('change', function () {
            $('.inputs-collapse').find('.collapse').collapse('hide');
            $(this).parent().next().collapse('show');
        });
    };
    var handleChangeRadio = function () {
        $(document).on("change", "input.check-profiteer", function () {
            var $this = $(this);
            if ($this.val() === "Yes") {
                $this.closest(".profiteer-group").find(".cin").attr("placeholder", "N° CIN du tuteur *");
            } else {
                $this.closest(".profiteer-group").find(".cin").attr("placeholder", "N° CIN *");
            }
        });
    };
    return {
        init: function () {
            addItemToCart();
            deleteItemFromCart();
            handleChangeQuantity();
            handleChangeCartQuantity();
            handleChangeOffer();
            handleCheckoutPayment();
            onHideModal();
            inputsCollapse();
            handleChangeRadio();
        }
    };
}();