function initMap() {
    var latValue = (window.lat) ? parseFloat(window.lat) : 33.570551,
        lngValue = (window.lng) ? parseFloat(window.lng) : -7.588025; // set default lat lng;

    var center = {lat: latValue, lng: lngValue};

    var map = new google.maps.Map(document.getElementById('map_canvas'), {
        zoom: 16,
        center: center
    });

    var marker = new google.maps.Marker({
        position: center,
        map: map
    });

    $('a[href="#tab-map"]').on('shown.bs.tab', function(e) {
        google.maps.event.trigger(map, 'resize');
        map.setCenter(center);
    });
}
