function initMap() {
    var lat = $('input[name="latitude"]'), lng = $('input[name="longitude"]');
    var latValue = (lat.val() != "") ? parseFloat(lat.val()) : 33.570551,
        lngValue = (lng.val() != "") ? parseFloat(lng.val()) : -7.588025; // set default lat lng;
    var center = {lat: latValue, lng: lngValue};

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: center
    });

    var marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        position: center
    });

    google.maps.event.addListener(marker, 'dragend', function(evt) {
        lat.val(evt.latLng.lat());
        lng.val(evt.latLng.lng());
    });

    google.maps.event.trigger(map, 'resize');
    map.setCenter(center);
    lat.val(latValue);
    lng.val(lngValue);

    var geocoder = new google.maps.Geocoder();
    document.getElementById('map_search').addEventListener('click', function() {
        var address = document.getElementById('map_address').value;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === 'OK') {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                lat.val(results[0].geometry.location.lat);
                lng.val(results[0].geometry.location.lng);
            } else {
                toastr.error("la détection d\'adresse n\'a pas réussi pour la raison suivante: " + status, "");
            }
        });
    });
}
