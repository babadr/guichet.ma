var Order = function () {
    var deals = [];
    /**
     *  Handle Add Deal
     */
    var handleAddDeal = function () {
        $(document).on("click", "#add-deal", function (e) {
            e.preventDefault();
            var $this = $(this);
            var dealID = $("#deal_id").val();
            if (dealID === '') {
                toastr.error("Veuillez sélectionner un produit.");
                return;
            }
            if ($.inArray(dealID, deals) !== -1) {
                toastr.error("Vous avez déjà sélectionné ce produit.");
                return;
            }
            $.ajax({
                method: "POST",
                url: "/admin/order/add/deal/" + dealID,
                data: {
                    deal_id: dealID,
                },
                success: function (response) {
                    if (response.success) {
                        $("#cart-container").html(response.html);
                        if (response.hasPlan) {
                            window.rows = response.rows;
                            window.mapCharts = response.mapCharts;
                            window.seats = response.seat_offers;
                            window.items = response.items;
                            window.reservedSeats = response.reservedSeats;
                            SeatingPlan.initSeatCharts();
                            SeatingPlan.initReservedSeats();
                        }
                        $("#total-order").show();
                        $(".sub-total").html('0 DH');
                        //deals.push(dealID);
                    } else {
                        toastr.error(response.msg);
                    }
                },
                error: function (response) {
                    toastr.error("Une erreur s'est produite.");
                }
            });
        });
    };

    var handleChangeCartQuantity = function () {
        $(document).on("click", ".cart-quantity-up", function (e) {
            e.preventDefault();
            var $this = $(this);
            var offerID = $this.attr('data-id-offer');
            var offerPrice = $this.attr('data-price');
            var $fieldName = $("#quantity-wanted-" + offerID);
            var currentVal = parseInt($fieldName.val());
            if (!isNaN(currentVal)) {
                $fieldName.val(currentVal + 1);
            }
            updateCartItem(offerID, offerPrice, $fieldName.val());
        });
        $(document).on("click", ".cart-quantity-down", function (e) {
            e.preventDefault();
            var $this = $(this);
            var offerID = $this.attr('data-id-offer');
            var offerPrice = $this.attr('data-price');
            var $fieldName = $("#quantity-wanted-" + offerID);
            var currentVal = parseInt($fieldName.val());
            if (!isNaN(currentVal) && currentVal > 1) {
                $fieldName.val(currentVal - 1);
            } else {
                $fieldName.val(0);
            }
            updateCartItem(offerID, offerPrice, $fieldName.val());
        });
    };

    var updateCartItem = function (id, price, quantity) {
        var total = 0.00;
        $("#total-price-" + id).html(formatNumber(price * quantity) + ' DH');
        $('.quantity-wanted').each(function () {
            var $this = $(this);
            var offerPrice = $this.attr('data-price');
            total = total + (offerPrice * $this.val());
        });
        $(".sub-total").html(formatNumber(total) + ' DH');
    };

    var formatNumber = function (str) {
        var parts = (str + "").split("."),
            main = parts[0],
            len = main.length,
            output = "",
            first = main.charAt(0),
            i;

        if (first === '-') {
            main = main.slice(1);
            len = main.length;
        } else {
            first = "";
        }
        i = len - 1;
        while (i >= 0) {
            output = main.charAt(i) + output;
            if ((len - i) % 3 === 0 && i > 0) {
                output = " " + output;
            }
            --i;
        }
        // put sign back
        output = first + output;
        // put decimal part back
        if (parts.length > 1) {
            output += "," + parts[1];
        }
        return output;
    };

    var onSubmitForm = function () {
        $('#app-form').submit(function () {
            $.each(window.selectedSeats, function (index, item) {
                $('<input />').attr('type', 'hidden')
                    .attr('name', "seats[" + item.id + "][]")
                    .attr('value', item.seat)
                    .appendTo('#app-form');
            });

            return true;
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleAddDeal();
            handleChangeCartQuantity();
            onSubmitForm();
        }
    };

}();

jQuery(document).ready(function () {
    Order.init();
});