(function (document, window, $) {
    'use strict';

    window.AppFormValidation = {
        formValidatorMethods: function () {
            // Email validation
            $.validator.addMethod('email', function (value, element, params) {
                if ($(element).prop('required') || value !== '') {
                    var re = new RegExp('^[a-z0-9]+([_|.|-]{1}[a-z0-9]+)*@[a-z0-9]+([.-]{1}[a-z0-9]+)*[.]{1}[a-z]{2,6}$', 'i');
                    return re.test(value);
                } else {
                    return true;
                }
            });
            // Date validation
            $.validator.addMethod('date', function (value, element, params) {
                if ($(element).prop('required') || value !== '') {
                    var re = new RegExp('^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$');
                    return re.test(value);
                } else {
                    return true;
                }
            }, "Veuillez fournir une date valide. Ex : jj/mm/aaaa");
            // Password
            $.validator.addMethod("custompassword", function (value, element) {
                var check = value.length >= 8 ;
                return this.optional(element) || check;
            }, "");
            // Slug
            $.validator.addMethod("slug", function (value, element) {
                if ($(element).prop('required') || value !== '') {
                    var re = new RegExp('^[A-Za-z\\d-/_]+$');
                    return re.test(value);
                } else {
                    return true;
                }
            }, "Format de l'alias invalide.");
            $.validator.addMethod("higher-date", function (value, element) {
                var $el = $(element);
                var format = 'DD/MM/YYYY';
                var toDay = $el.data('date-today');
                var check = moment(toDay, format) < moment(value, format);
                return this.optional(element) || check;
            }, "La date doit être supérieure ou égale à la date d'aujourd'hui.");
            $.validator.addMethod("higher-than", function (value, element) {
                var $el = $(element);
                var format = 'DD/MM/YYYY';
                var compare = $el.parents('.section-body').find('.departure_at').val();
                var check = moment(value, format) >= moment(compare.substring(0, 10), format);
                return this.optional(element) || check;
            }, "La date d'arrivée doit être supérieure ou égale à la date de départ.");
            // Phone validation
            $.validator.addMethod('phone', function (value, element, params) {
                if ($(element).prop('required') || value !== '') {
                    return (/^\d{7,}$/).test(value.replace(/[\s()+\-\.]|ext/gi, ''));
                } else {
                    return true;
                }
            }, "Veuillez fournir un numéro de téléphone valide.");
            // Summernote validation
            $.validator.addMethod('summernote', function (value, element, params) {
                if (value !== "" && value !== "<p><br></p>" && value !== "<br>" && value !== "<br><br>") {
                    return true;
                } else {
                    return false;
                }
            }, "Ce champ est obligatoire.");
            // Hexa color format
            $.validator.addMethod('color', function (value, element) {
                return this.optional(element) || /^(#)([a-f0-9]{3}){1,2}$/i.test(value);
            }, "Veuillez fournir un code couleur valide.");
            // only letters or number (case insensitive)
            $.validator.addMethod('alpha-numeric', function (value, element) {
                return this.optional(element) || /^[a-z0-9]+$/i.test(value);
            }, "Ce champ doit contenir seulement des caractères alphanumériques.");
            // only number
            $.validator.addMethod('numeric', function (value, element) {
                return this.optional(element) || /^[0-9]+$/i.test(value);
            }, "Ce champ doit contenir seulement des caractères numériques.");
        },
        formValidator: function () {
            $.validator.setDefaults({
                ignore: ["", ".hidden", ".note-editable"]
            });
            $('.form-validate').each(function () {
                $(this).validate({
                    errorElement: 'small',
                    onkeyup: false,
                    errorClass: 'has-error',
                    validClass: 'has-success',
                    errorPlacement: function (error, element) {
                        var formGroup = element.closest('.form-group');
                        var errorsContainer = formGroup.find('.error-container');
                        if (errorsContainer.length) {
                            errorsContainer.append(error);
                        } else {
                            formGroup.append(error);
                        }
                        $('small.has-error').addClass('help-block help-block-error');
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).parents('.form-group').addClass(errorClass).removeClass(validClass);
                        $(element).addClass(errorClass).removeClass(validClass);
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).parents('.form-group').removeClass(errorClass).addClass(validClass);
                        $(element).removeClass(errorClass).addClass(validClass);
                    },
                    invalidHandler: function (e, validator) {
                        if (validator.errorList.length) {
                            $('.display-hide').fadeIn();
                            $('.nav-tabs a[href="#' + $(validator.errorList[0].element).closest(".tab-pane").attr('id') + '"]').tab('show');
                        }
                    }
                });
            });
        },
        init: function () {
            this.formValidatorMethods();
            this.formValidator();
        }
    };

})(document, window, jQuery);
