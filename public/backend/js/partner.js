(function (document, window, $) {
    'use strict';

    window.PartnerCharts = {
        initSalesChart: function () {
            var start_at = $("#start_at").val();
            var end_at = $("#end_at").val();
            Highcharts.setOptions({
                lang: {
                    months: [
                        'Janvier', 'Février', 'Mars', 'Avril',
                        'Mai', 'Juin', 'Juillet', 'Août',
                        'Septembre', 'Octobre', 'Novembre', 'Décembre'
                    ],
                    weekdays: [
                        'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
                        'Jeudi', 'Vendredi', 'Samedi'
                    ],
                    thousandsSep: ' ',
                    decimalPoint: ','
                }
            });

            var mySeries = [];
            for (var i = 0; i < window.results.length; i++) {
                mySeries.push([window.results[i]["date"], parseFloat(window.results[i]["total"])]);
            }

            $('#highchart-sales').highcharts({
                chart: {
                    zoomType: 'x',
                    style: {
                        fontFamily: 'Open Sans'
                    }
                },
                title: {
                    text: 'Chiffre d\'affaires généré sur la période du ' + start_at + ' au ' + end_at
                },
                subtitle: {
                    text: document.ontouchstart === undefined ?
                        'Cliquez et faites glisser dans la zone pour zoomer' : 'Pincer le graphique pour zoomer'
                },
                xAxis: {
                    type: 'datetime'
                },
                yAxis: {
                    title: {
                        text: 'Chiffre d\'affaires'
                    },
                    labels: {
                        formatter: function () {
                            return this.value + ' DH';
                        }
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' DH'
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 1,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },
                series: [{
                    type: 'area',
                    name: 'Chiffre d\'affaires',
                    data: mySeries
                }]
            });
        },

        initAmountOfflineAmChart: function () {
            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amount_amchart').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("dashboard_amount_amchart", {
                "type": "pie",
                "theme": "light",
                "path": "/backend/plugins/amcharts/ammap/images/",
                "dataProvider": window.sellers,
                "labelText": "[[title]]: [[percents]]% ([[value]] DH)\n[[description]]",
                "valueField": "value",
                "titleField": "title",
                "outlineAlpha": 0.4,
                "depth3D": 15,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]] DH</b> ([[percents]]%)</span>",
                "angle": 30,
                "export": {
                    "enabled": true
                },
                "numberFormatter": {
                    "precision": 2,
                    "decimalSeparator": ",",
                    "thousandsSeparator": " "
                }
            });
        },

        init: function () {
            this.initSalesChart();
            this.initAmountOfflineAmChart();
        }
    };
})(document, window, jQuery);

jQuery(document).ready(function () {
    PartnerCharts.init();
});
