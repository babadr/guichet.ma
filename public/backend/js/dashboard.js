(function (document, window, $) {
    'use strict';

    window.DashboardCharts = {
        initSalesChart: function () {
            var start_at = $("#start_at").val();
            var end_at = $("#end_at").val();
            Highcharts.setOptions({
                lang: {
                    months: [
                        'Janvier', 'Février', 'Mars', 'Avril',
                        'Mai', 'Juin', 'Juillet', 'Août',
                        'Septembre', 'Octobre', 'Novembre', 'Décembre'
                    ],
                    weekdays: [
                        'Dimanche', 'Lundi', 'Mardi', 'Mercredi',
                        'Jeudi', 'Vendredi', 'Samedi'
                    ],
                    thousandsSep: ' ',
                    decimalPoint: ','
                }
            });
            $.getJSON('/admin/charts/sales/' + start_at + '/' + end_at, function (data) {
                var mySeries = [];
                for (var i = 0; i < data.length; i++) {
                    mySeries.push([data[i]["date"], parseFloat(data[i]["total"])]);
                }
                $('#highchart-sales').highcharts({
                    chart: {
                        zoomType: 'x',
                        style: {
                            fontFamily: 'Open Sans'
                        }
                    },
                    title: {
                        text: 'Chiffre d\'affaires généré par les commandes considérées comme validées.'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                            'Cliquez et faites glisser dans la zone pour zoomer' : 'Pincer le graphique pour zoomer'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Chiffre d\'affaires'
                        },
                        labels: {
                            formatter: function () {
                                return this.value + ' DH';
                            }
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        valueSuffix: ' DH'
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: 'Chiffre d\'affaires',
                        data: mySeries
                    }]
                });
            });
        },
        initCommissionChart: function () {
            var start_at = $("#start_at").val();
            var end_at = $("#end_at").val();
            $.getJSON('/admin/charts/commissions/' + start_at + '/' + end_at, function (data) {
                var mySeries = [];
                for (var i = 0; i < data.length; i++) {
                    mySeries.push([data[i]["date"], parseFloat(data[i]["total"])]);
                }
                $('#highchart-commission').highcharts({
                    chart: {
                        zoomType: 'x',
                        style: {
                            fontFamily: 'Open Sans'
                        }
                    },
                    title: {
                        text: 'Commission Guichet générée.'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                            'Cliquez et faites glisser dans la zone pour zoomer' : 'Pincer le graphique pour zoomer'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Commission Guichet'
                        },
                        labels: {
                            formatter: function () {
                                return this.value + ' DH';
                            }
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        valueSuffix: ' DH'
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[3]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[3]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: 'Commission',
                        data: mySeries
                    }]
                });
            });
        },
        initOrdersChart: function () {
            var start_at = $("#start_at").val();
            var end_at = $("#end_at").val();
            $.getJSON('/admin/charts/orders/' + start_at + '/' + end_at, function (data) {
                var mySeries = [];
                for (var i = 0; i < data.length; i++) {
                    mySeries.push([data[i]["date"], data[i]["total"]]);
                }
                $('#highchart-orders').highcharts({
                    chart: {
                        zoomType: 'x',
                        style: {
                            fontFamily: 'Open Sans'
                        }
                    },
                    title: {
                        text: 'Nombre total de commandes passées et considérées comme validées.'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                            'Cliquez et faites glisser dans la zone pour zoomer' : 'Pincer le graphique pour zoomer'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Commandes'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[7]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[7]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: 'Commandes',
                        data: mySeries
                    }]
                });
            });
        },
        initVisitorsChart: function () {
            var start_at = $("#start_at").val();
            var end_at = $("#end_at").val();
            $.getJSON('/admin/charts/inscriptions/' + start_at + '/' + end_at, function (data) {
                var mySeries = [];
                for (var i = 0; i < data.length; i++) {
                    mySeries.push([data[i]["date"], data[i]["total"]]);
                }
                $('#highchart-visitors').highcharts({
                    chart: {
                        zoomType: 'x',
                        style: {
                            fontFamily: 'Open Sans'
                        }
                    },
                    title: {
                        text: 'Nombre total des inscriptions.'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                            'Cliquez et faites glisser dans la zone pour zoomer' : 'Pincer le graphique pour zoomer'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Inscriptions'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[5]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[5]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },
                    series: [{
                        type: 'area',
                        name: 'Inscriptions',
                        data: mySeries
                    }]
                });
            });
        },
        onChangeTab: function () {
            $('#tab-charts a').on('click', function () {
                var element = $(this).attr('href');
                switch (element) {
                    case '#tab-chart-sales':
                        DashboardCharts.initSalesChart();
                        break;
                    case '#tab-chart-commission':
                        DashboardCharts.initCommissionChart();
                        break;
                    case '#tab-chart-orders':
                        DashboardCharts.initOrdersChart();
                        break;
                    case '#tab-chart-visitors':
                        DashboardCharts.initVisitorsChart();
                        break;
                }
            });
        },
        initDashboardDaterange: function () {
            moment.locale('fr', {
                months: 'janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre'.split('_'),
                monthsShort: 'janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.'.split('_'),
                monthsParseExact: true,
                weekdays: 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
                weekdaysShort: 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
                weekdaysMin: 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
                weekdaysParseExact: true,
                longDateFormat: {
                    LT: 'HH:mm',
                    LTS: 'HH:mm:ss',
                    L: 'DD/MM/YYYY',
                    LL: 'D MMMM YYYY',
                    LLL: 'D MMMM YYYY HH:mm',
                    LLLL: 'dddd D MMMM YYYY HH:mm'
                },
                calendar: {
                    sameDay: '[Aujourd’hui à] LT',
                    nextDay: '[Demain à] LT',
                    nextWeek: 'dddd [à] LT',
                    lastDay: '[Hier à] LT',
                    lastWeek: 'dddd [dernier à] LT',
                    sameElse: 'L'
                },
                relativeTime: {
                    future: 'dans %s',
                    past: 'il y a %s',
                    s: 'quelques secondes',
                    m: 'une minute',
                    mm: '%d minutes',
                    h: 'une heure',
                    hh: '%d heures',
                    d: 'un jour',
                    dd: '%d jours',
                    M: 'un mois',
                    MM: '%d mois',
                    y: 'un an',
                    yy: '%d ans'
                },
                dayOfMonthOrdinalParse: /\d{1,2}(er|e)/,
                ordinal: function (number) {
                    return number + (number === 1 ? 'er' : 'e');
                },
                meridiemParse: /PD|MD/,
                isPM: function (input) {
                    return input.charAt(0) === 'M';
                },
                // In case the meridiem units are not separated around 12, then implement
                // this function (look at locale/id.js for an example).
                // meridiemHour : function (hour, meridiem) {
                //     return /* 0-23 hour, given meridiem token and hour 1-12 */ ;
                // },
                meridiem: function (hours, minutes, isLower) {
                    return hours < 12 ? 'PD' : 'MD';
                },
                week: {
                    dow: 1, // Monday is the first day of the week.
                    doy: 4  // Used to determine first week of the year.
                }
            });

            moment.locale('fr');

            if (!jQuery().daterangepicker) {
                return;
            }

            $('#dashboard-report-range').daterangepicker({
                "ranges": {
                    "Aujourd'hui": [moment(), moment()],
                    'Hier': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Les 7 derniers jours': [moment().subtract('days', 6), moment()],
                    'Les 30 derniers jours': [moment().subtract('days', 29), moment()],
                    'Ce mois-ci': [moment().startOf('month'), moment().endOf('month')],
                    'Le mois dernier': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Appliquer",
                    "cancelLabel": "Annuler",
                    "fromLabel": "De",
                    "toLabel": "à",
                    "customRangeLabel": "Personnaliser",
                    "daysOfWeek": [
                        "Dim",
                        "Lun",
                        "Mar",
                        "Mer",
                        "Jeu",
                        "Ven",
                        "Sam"
                    ],
                    "monthNames": [
                        "Janvier",
                        "Février",
                        "Mars",
                        "Avril",
                        "Mai",
                        "Juin",
                        "Juillet",
                        "Août",
                        "Septembre",
                        "Octobre",
                        "Novembre",
                        "Décembre"
                    ],
                    "firstDay": 1
                },
                "startDate": moment($('#start_at').val(), 'YYYY-MM-DD').format('DD/MM/YYYY'),
                "endDate": moment($('#end_at').val(), 'YYYY-MM-DD').format('DD/MM/YYYY'),
                "setDate": new Date(),
                opens: (App.isRTL() ? 'right' : 'left'),
            }, function (start, end, label) {
                if ($('#dashboard-report-range').attr('data-display-range') != '0') {
                    $('#dashboard-report-range span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
                    $('#start_at').val(start.format('YYYY-MM-DD'));
                    $('#end_at').val(end.format('YYYY-MM-DD'));
                }
                $("#form-filter").submit();
            });
            if ($('#dashboard-report-range').attr('data-display-range') != '0') {
                $('#dashboard-report-range span').html(moment($('#start_at').val(), 'YYYY-MM-DD').format('D MMMM YYYY') + ' - ' + moment($('#end_at').val(), 'YYYY-MM-DD').format('D MMMM YYYY'));
            }
            $('#dashboard-report-range').show();
            $(document).on('click', '#reload-stats', function (e) {
                e.preventDefault();
                $("#form-filter").submit();
            });
        },
        initAmountAmChart: function () {
            if (typeof(AmCharts) === 'undefined' || $('#dashboard_amount_amchart').size() === 0) {
                return;
            }

            var chart = AmCharts.makeChart("dashboard_amount_amchart", {
                "type": "pie",
                "theme": "light",
                "path": "/backend/plugins/amcharts/ammap/images/",
                "dataProvider": [{
                    "title": "TOTAL DES VENTES",
                    "value": 853470.00
                }, {
                    "title": "TOTAL DES VENTES EN LIGNE\n",
                    "value": 428220.00
                }, {
                    "title": "TOTAL DES VENTES OFFLINE\n",
                    "value": 425250.00
                }],
                "labelText": "[[title]]: [[percents]]% ([[value]])\n[[description]]",
                "valueField": "value",
                "titleField": "title",
                "outlineAlpha": 0.4,
                "depth3D": 15,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "angle": 30,
                "export": {
                    "enabled": true
                }
            });

            jQuery('.chart-input').off().on('input change', function () {
                var property = jQuery(this).data('property');
                var target = chart;
                var value = Number(this.value);
                chart.startDuration = 0;

                if (property == 'innerRadius') {
                    value += "%";
                }

                target[property] = value;
                chart.validateNow();
            });
        },

        init: function () {
            this.initSalesChart();
            this.onChangeTab();
            this.initDashboardDaterange();
            this.initAmountAmChart();
        }
    };
})(document, window, jQuery);

jQuery(document).ready(function () {
    DashboardCharts.init();
});
