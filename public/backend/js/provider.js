(function (document, window, $) {
    'use strict';

    window.ProviderDashboard = {
        handleChangeEvent: function () {
            $('#event-list').on('change', function () {
                var $form = $('#form-select-event');
                var selected = $(this).val();
                window.location = $form.attr('action') + '/' + selected;
            });
        },
        init: function () {
            this.handleChangeEvent();
        }
    };
})(document, window, jQuery);

jQuery(document).ready(function () {
    ProviderDashboard.init();
});
