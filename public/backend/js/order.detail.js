var OrderDetail = function () {

    /**
     *  Handle Delete Ticket
     */
    var handleDeleteTicket = function () {
        $(document).on("click", ".delete-ticket", function (e) {
            e.preventDefault();
            var $this = $(this);
            bootbox.dialog({
                message: "Êtes-vous sûr de vouloir supprimer ce ticket ?",
                title: "Confirmer la suppression",
                buttons: {
                    success: {
                        label: "Annuler",
                        callback: function () {
                            return;
                        }
                    },
                    danger: {
                        label: "Confirmer",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                method: 'delete',
                                url: $this.attr('href'),
                                success: function (data) {
                                    if (data.success) {
                                        toastr.success(data.msg);
                                        location.reload();
                                    } else {
                                        toastr.error(data.msg);
                                    }
                                },
                                error: function (data) {
                                    toastr.error("Une erreur est survenue.");
                                }
                            });
                        }
                    }
                }
            });
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleDeleteTicket();
        }
    };

}();

jQuery(document).ready(function () {
    OrderDetail.init();
});