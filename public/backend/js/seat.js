var SeatingPlan = function () {

    var sc;

    var initSeatCharts = function () {
        var $cart = $('#selected-seats'), $counter = $('#counter'), $total = $('.sub-total');
        sc = $('#seat-map').seatCharts({
            map: window.mapCharts,
            seats: window.seats,
            naming: {
                top: false,
                left: true,
                right: true,
                getLabel: function (character, row, column) {
                    return row + column;
                },
                rows: window.rows,
            },
            legend: {
                node: $('#legend'),
                items: window.items
            },
            click: function () {
                if (this.status() == 'available') {
                    $('<li>' + this.data().category + ' Place #' + this.settings.label + ': <b>' + this.data().price + ' DH</b> <a href="#" class="cancel-cart-item"><i class="ti-trash"></i></a></li>')
                        .attr('id', 'cart-item-' + this.settings.id)
                        .attr('data-seatId', this.settings.id)
                        .appendTo($cart);

                    window.selectedSeats.push({id: this.data().offer_id, seat: this.settings.id});
                    $counter.text(sc.find('selected').length + 1);
                    $total.text(recalculateTotal(sc) + parseFloat(this.data().price) + ' DH');

                    return 'selected';
                } else if (this.status() == 'selected') {
                    var seat = this.settings.id;
                    $counter.text(sc.find('selected').length - 1);
                    $total.text(recalculateTotal(sc) - parseFloat(this.data().price) + ' DH');
                    $('#cart-item-' + this.settings.id).remove();

                    window.selectedSeats = window.selectedSeats.filter(function (place) {
                        return place.seat !== seat;
                    });
                    return 'available';
                } else if (this.status() == 'unavailable') {

                    return 'unavailable';
                } else {
                    return this.style();
                }
            }
        });

        $('#selected-seats').on('click', '.cancel-cart-item', function () {
            var seat = $(this).parents('li:first').attr('data-seatId');
            sc.get(seat).click();
            window.selectedSeats = window.selectedSeats.filter(function (place) {
                return place.seat !== seat;
            });
        });
    };

    var initReservedSeats = function () {
        sc.get(window.reservedSeats).status('unavailable');
    };

    var initBloquedSeats = function () {
        //sc.get(window.selectedSeats).status('selected');
        $.each(window.blockedSeats, function (index, item) {
            if (item !== '') {
                sc.get(item).click();
            }
        });
    };

    var recalculateTotal = function (sc) {
        var total = 0;
        sc.find('selected').each(function () {
            total += parseFloat(this.data().price);
        });

        return total;
    };

    var handleAddToCart = function () {
        $(document).on("click", "#event-post-order", function (e) {
            e.preventDefault();
            if (window.selectedSeats.length > 0) {
                var $this = $(this);
                $this.addClass('disabled');
                $.ajax({
                    method: "POST",
                    url: $this.attr("href"),
                    data: JSON.stringify({Seats: window.selectedSeats}),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.success) {
                            $this.removeClass('disabled');
                            window.location = response.redirect;
                        } else {
                            toastr.error(response.msg);
                            $this.removeClass('disabled');
                        }
                    },
                    error: function (response) {
                        toastr.error(response, "");
                        $this.removeClass('disabled');
                    }
                });
            } else {
                toastr.error("Veuillez sélectionner une ou plusieurs places.", "");
            }
        });
    };

    return {
        init: function () {

            //handleAddToCart();
        },
        initSeatCharts: function () {
            initSeatCharts();
        },
        initReservedSeats: function () {
            initReservedSeats();
        },
        initBloquedSeats: function () {
            initBloquedSeats();
        }
    };
}();