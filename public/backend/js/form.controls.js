(function (document, window, $) {
    'use strict';

    window.AppFormControls = {
        /**
         * Initialize Text Editor
         */
        handleEditor: function () {
            if ($.fn.wysihtml5) {
                $('.wysihtml5').each(function () {
                    var $element = $(this);
                    $element.wysihtml5();
                });
            }
            if ($.fn.summernote) {
                $('.summernote').each(function () {
                    var $element = $(this);
                    $element.summernote({
                        height: $element.data('height') ? $element.data('height') : 300,
                        lang: 'fr-FR',
                        disableResizeEditor: true,
                        onkeyup: function (e) {
                            $element.val($(this).code());
                            $element.change(); //To update any action binded on the control
                        },
                        toolbar: [
                            ['style', ['style']], // no style button
                            ['style', ['bold', 'italic', 'underline', 'clear']],
                            ['fontsize', ['fontsize']],
                            ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            ['height', ['height']],
                            ['insert', ['picture', 'video', 'link']], // no insert buttons
                            ['table', ['table']], // no table button
                            ['view', ['codeview']]
                        ]
                    });
                });
            }
        },
        /**
         *  Initialize date pickers
         */
        handleDatePickers: function () {
            if ($.fn.datepicker) {
                $('.datepicker').each(function () {
                    var $element = $(this);
                    $element.datepicker({
                        todayHighlight: true,
                        dateFormat: $element.attr('data-date-format') ? $element.attr('data-date-format').toLowerCase() : 'dd/mm/yy',
                        language: 'fr',
                        keyboardNavigation: false,
                        autoclose: true,
                        forceParse: false,
                        rtl: false,
                        orientation: "bottom auto",
                    });
                });
            }
        },
        /**
         *  Initialize datetime pickers
         */
        handleDateTimePickers: function () {
            if ($.fn.datetimepicker) {
                $('.datetimepicker').each(function () {
                    var $element = $(this);
                    $element.datetimepicker({
                        format: $element.attr('data-date-format') ? $element.attr('data-date-format') : 'dd/mm/yy hh:ii',
                        language: 'fr',
                        keyboardNavigation: false,
                        autoclose: true,
                        forceParse: false,
                        rtl: false,
                        orientation: "bottom auto",
                    });
                });
            }
        },
        /**
         *  Initialize Color Picker
         */
        handleColorPicker: function () {
            if ($.fn.colorPicker) {
                $('.colorpicker').each(function () {
                    var $element = $(this);
                    $element.colorPicker({
                        opacity: false,
                        doRender: false
                    });
                });
            }
        },
        /**
         * Initialize select2
         */
        handleSelect2: function () {
            if ($.fn.select2) {
                $('.select2').each(function () {
                    var $element = $(this);
                    $element.select2({
                        dropdownAutoWidth : true,
                        width: 'auto'
                    });
                });
            }
        },
        /**
         * Initialize Switch
         */
        handleSwitch: function () {
            if ($.fn.switchButton) {
                $('.make-switch, .switch-status').each(function () {
                    $(this).switchButton({
                        show_labels: false,
                        width: 50,
                        height: 25,
                        button_width: 23
                    });
                });
            }
        },
        /**
         * Initialize Rating
         */
        handleRating: function () {
            if ($.fn.barrating) {
                $('.barrating').each(function () {
                    $(this).barrating({
                        theme: 'fontawesome-stars'
                    });
                });
            }
        },
        /**
         * Initialize Slider Single
         */
        handleSliderSingle: function () {
            if ($.fn.slider) {
                $('.slider-single').each(function () {
                    var $element = $(this);
                    var $html = $element.find('.val');
                    var $input = $element.find('input[type=hidden]');
                    $element.slider({
                        min: $input.data('min'),
                        max: $input.data('max'),
                        step: $input.data('step'),
                        value: $input.val(),
                        change: function (event, ui) {
                            $html.html(ui.value);
                            $input.val(ui.value);
                        },
                        slide: function (event, ui) {
                            $html.html(ui.value);
                            $input.val(ui.value);
                        }
                    });
                });
            }
        },
        /**
         * Initialize the change handler on the .select-Select selector
         */
        initAjaxSelect: function () {
            if ($.fn.select2) {
                $('.ajax-select').each(function () {
                    var $el = $(this);
                    var url = $el.data('url');
                    var filter = $el.data('filter');
                    var placeHolder = $el.data('placeholder');
                    if (typeof url === 'undefined') {
                        return false;
                    }
                    $el.select2({
                        allowClear: true,
                        placeholder: placeHolder,
                        minimumInputLength: 0,
                        ajax: {
                            type: 'GET',
                            url: url,
                            delay: 250,
                            data: function (params) {
                                if (typeof filter !== "undefined") {
                                    return {
                                        search: params, // search term,
                                        filter: $(filter).val()
                                    };
                                }
                                return {
                                    search: params // search term
                                };
                            },
                            results: function (data) {
                                var results = [];
                                $.each(data.datas, function (index, item) {
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                                return {
                                    results: results
                                };
                            }
                        },
                        initSelection: function (element, callback) {
                            var text = element.data('initial-selection');
                            var value = element.val();
                            if (value !== '' && text !== '') {
                                callback({text: text, id: value});
                            }
                        }
                    });
                });
            }
        },
        /**
         * Initialize the google places autocomplete
         */
        initGooglePlaces: function () {
            $('.google-places').each(function () {
                window.AppFormControls.initGooglePlace(this);
            });
        },
        initGooglePlace: function (element) {
            var autocomplete = new google.maps.places.Autocomplete(element, {types: [element.getAttribute('data-type')]});
            google.maps.event.addListener(autocomplete, 'place_changed', function (el) {
                var result = this.getPlace();
                var target = element.getAttribute('data-target');
                if (target) {
                    for(var i = 0; i < result.address_components.length; i += 1) {
                        var addressObj = result.address_components[i];
                        for(var j = 0; j < addressObj.types.length; j += 1) {
                            if (addressObj.types[j] === 'country') {
                                $(target).val(addressObj.long_name);
                            }
                        }
                    }
                }
                element.value = result.name;
            });
        },
        /**
         * Init Select Tags
         */
        initInputTags: function () {
            var $selectTags = $('.select-tags');
            var tagContainer = $selectTags.next('.dev-tags');
            if (tagContainer.html()) {
                var langs = tagContainer.html().split(',');
                tagContainer.html('');
                langs.forEach(function (element) {
                    var item = element.split(',');
                    window.AppFormControls.initGenerateTags(element, tagContainer, 'langs');
                });
            }
            $selectTags.on('change', function () {
                var $el = $(this);
                var text = $el.children("option").filter(":selected").text();
                window.AppFormControls.initGenerateTags(text, $el.next('.dev-tags'), 'langs', $el.val());
            });
            var $inputTags = $('.input-tags');
            if ($inputTags.val()) {
                var tags = $inputTags.val().split(",");
                var tagContainer = $inputTags.next('.dev-tags');
                $inputTags.val('');
                tags.forEach(function (element) {
                    window.AppFormControls.initGenerateTags(element, tagContainer, 'interests');
                });
            }
            $inputTags.on('keydown', function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);
                /*key enter */
                if (keycode === 13)
                {
                    event.preventDefault();
                    var $el = $(this);
                    var text = $el.val();
                    window.AppFormControls.initGenerateTags(text, $el.next('.dev-tags'), 'interests');
                    $(this).val('');
                }
            });
        },
        /**
         * Init Tags Generation
         * @param {string} text
         * @param {object} tagContainer
         * @param {string} tagsName
         */
        initGenerateTags: function (text, tagContainer, tagsName, val) {
            if (text !== '')
            {
                var exist = false;
                tagContainer.find('span input').each(function () {
                    if ($(this).val() === text)
                        exist = true;
                });

                if (!exist)
                {
                    var tag = '<span>' + text + '<input type="text" name="' + tagsName + '[]" value="' + (typeof val !== 'undefined' ? val : text) + '" readonly> <i class="fa fa-times"></i></span>';
                    tagContainer.append(tag);
                }
            }

            tagContainer.find('span i').on('click', function () {
                $(this).parent().fadeOut(function () {
                    $(this).remove();
                });
            });
        },
        /**
         * Generate slug
         * @param {string} str
         * @returns {string} 
         */
        generateSlug: function (str) {
            str = str.replace(/^\s+|\s+$/g, ''); // trim
            str = str.toLowerCase();
            // remove accents, swap ñ for n, etc
            var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
            var to = "aaaaaeeeeeiiiiooooouuuunc------";
            for (var i = 0, l = from.length; i < l; i++) {
                str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
            }
            str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
                    .replace(/\s+/g, '-') // collapse whitespace and replace by -
                    .replace(/-+/g, '-'); // collapse dashes

            return str;
        },

        initToastr: function () {
            if (window.toastr != undefined) {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                }
            }
        },

        init: function () {
            this.handleEditor();
            this.handleDatePickers();
            this.handleDateTimePickers();
            this.handleColorPicker();
            this.handleSelect2();
            this.handleSwitch();
            this.handleSliderSingle();
            this.initAjaxSelect();
            this.initGooglePlaces();
            this.initInputTags();
            this.handleRating();
            this.initToastr();
        }
    };

})(document, window, jQuery);
