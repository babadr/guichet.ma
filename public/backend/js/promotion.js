(function (document, window, $) {

    var PromotionForm = function () {
        var self = this;

        this.handleChangeType = function () {
            $(document).on('change', '.radio-type', function () {
                self.handleToggleDiscount();
            });
        };

        this.handleToggleDiscount = function () {
            var checked = $('input[name=type]:checked', '#app-form').val();
            if (checked === undefined) {
                $('#percentage-type').hide();
                $('#amount-type').hide();
                $('#percentage').val('').removeAttr("required");
                $('#amount').val('').removeAttr("required");
            }
            if (checked == 1) {
                $('#percentage-type').show(400);
                $('#amount-type').hide(200);
                $('#amount').val('').removeAttr("required");
                $('#percentage').attr("required", true);
            }
            if (checked == 2) {
                $('#percentage-type').hide(200);
                $('#amount-type').show(400);
                $('#percentage').val('').removeAttr("required");
                $('#amount').attr("required", true);
            }
        };

        this.handleChangeTypeable = function () {
            $(document).on('change', '#typeable_type', function () {
                self.handleToggleTypeable();
            });
        };

        this.handleToggleTypeable = function () {
            var selected = $('#typeable_type', '#app-form').val();
            if (selected == '') {
                $('#typeable-category').hide();
                $('#typeable-event').hide();

                $('#category_id').val('').trigger('change').removeAttr("required");
                $('#event_id').val('').trigger('change').removeAttr("required");
            }
            if (selected == 'App\\Models\\Deal\\Category') {
                $('#typeable-category').show(400);
                $('#typeable-event').hide(200);

                $('#category_id').attr("required", true);
                $('#event_id').val('').trigger('change').removeAttr("required");
            }
            if (selected == 'App\\Models\\Deal\\Deal') {
                $('#typeable-category').hide(200);
                $('#typeable-event').show(400);

                $('#event_id').attr("required", true);
                $('#customer_id').val('').trigger('change').removeAttr("required");
            }
        };

        return {
            init: function () {
                self.handleChangeType();
                self.handleToggleDiscount();
                self.handleChangeTypeable();
                self.handleToggleTypeable();
            }
        };
    }();

    $(document).ready(function () {
        PromotionForm.init();
    });

})(document, window, jQuery);
