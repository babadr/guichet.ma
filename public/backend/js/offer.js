var Offer = function () {
    /**
     *  Handles Bootstrap switches
     */
    var handleBootstrapSwitch = function () {
        $('.make-switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
            var $this = $(this);
            $.ajax({
                method: 'PUT',
                url: $this.data('switch-url'),
                data: {
                    state: state ? 1 : 0,
                    field: $this.data('switch-field')
                },
                success: function (data) {
                    if (data.success) {
                        toastr.success("Changement d'étât effectué avec succès.", '');
                    } else {
                        toastr.error("Cette ligne n'existe plus.", '');
                    }
                },
                error: function () {
                    toastr.error("Une erreur est survenue.", '');
                }
            });
        });
    };

    /**
     * initialize confirmation on datatable
     * @returns
     */
    var handleDeleteModal = function () {
        $(document).on('click', '.delete-element', function (e) {
            e.preventDefault();
            var $this = $(this);
            bootbox.dialog({
                message: "Êtes-vous sûr de vouloir supprimer cet élément ?",
                title: "Confirmer la suppression",
                buttons: {
                    success: {
                        label: "Annuler",
                        callback: function () {
                            return false;
                        }
                    },
                    danger: {
                        label: "Confirmer",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                method: 'delete',
                                url: $this.attr('href'),
                                success: function (data) {
                                    if (data.success) {
                                        toastr.success(data.msg);
                                        location.reload();
                                        location.hash = '#tab_3';
                                    } else {
                                        toastr.error(data.msg);
                                    }
                                },
                                error: function () {
                                    toastr.error("Une erreur est survenue.");
                                }
                            });
                        }
                    }
                }
            });
        });
    };

    /**
     * handleSubmitForm
     * @returns
     */
    var handleSubmitForm = function () {
        $(document).on('submit', '#app-form-offer', function (e) {
            var form = $("#app-form-offer");
            form.validate();

            if (form.valid()) {
                var formData = new FormData(form[0]);

                if ($('#caneva').length) {
                    var files = $('#caneva')[0].files[0];
                    formData.append('caneva', files);
                }

                $.ajax({
                    method: 'post',
                    url: form.attr('action'),
                    data: formData,//form.serialize(),
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data.success) {
                            toastr.success(data.message);
                            location.reload();
                            location.hash = '#tab_3';
                        } else {
                            toastr.error(data.message);
                            $.each(data.errors, function (index, error) {
                                var element = form.find("#" + index);
                                var formGroup = element.closest('.form-group');
                                var errorsContainer = formGroup.find('.error-container');
                                errorsContainer.append('<small id="' + index + '-error" class="has-error help-block help-block-error">' + error + '</small>');
                                formGroup.addClass('has-error').removeClass('has-success');
                            });
                        }
                    },
                    error: function () {
                        toastr.error("Une erreur est survenue.");
                    }
                });
            }

            e.preventDefault();
            return false;
        });
    };

    /**
     * handleSubmitForm
     * @returns
     */
    var handleEditOffer = function () {
        $(document).on('click', '.edit-offer', function (e) {
            $.ajax({
                method: 'get',
                url: $(this).data('url'),
                success: function (data) {
                    var form = $("#app-form-offer");
                    form.find("#id").val(data.id);
                    form.find("#title").val(data.title);
                    form.find("#description").val(data.description);
                    form.find("#price").val(data.price);
                    form.find("#old_price").val(data.old_price);
                    form.find("#reservation_price").val(data.reservation_price);
                    form.find("#quantity").val(data.quantity);
                    form.find("#active").bootstrapSwitch('state', data.active);
                    form.find("#default").bootstrapSwitch('state', data.default);
                    form.find("#quantity_max").val(data.quantity_max);

                    $("#modal_offer").modal();
                },
                error: function () {
                    toastr.error("Une erreur est survenue.");
                }
            });

            $("#modal_offer").modal();
            e.preventDefault();
            return false;
        });
    };

    /**
     * switchers handller
     */
    var handleSwitchDefault = function () {
        $('.offer-switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
            var $this = $(this);
            $.ajax({
                method: 'PUT',
                url: $this.data('switch-url'),
                data: {
                    state: state ? 1 : 0,
                    field: $this.data('switch-field')
                },
                success: function (data) {
                    if (data.success) {
                        toastr.success("Changement d'étât effectué avec succès.", '');
                        location.reload();
                        location.hash = '#tab_3';
                    } else {
                        toastr.error("Cette ligne n'existe plus.", '');
                    }
                },
                error: function (data) {
                    toastr.error("Une erreur est survenue.", '');
                }
            });
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            handleBootstrapSwitch();
            handleDeleteModal();
            handleEditOffer();
            handleSubmitForm();
            handleSwitchDefault();
        }
    };

}();

jQuery(document).ready(function () {
    Offer.init();
});