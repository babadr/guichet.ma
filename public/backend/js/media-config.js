$(function(){
    $('#drag-and-drop-zone').dmUploader({ //
        url: window.uploadUrl,
        maxFileSize: 3000000, // 3 Megs
        allowedTypes: 'image/*',
        extFilter: ["jpg", "jpeg","png","gif"],
        onDragEnter: function(){
            // Happens when dragging something over the DnD area
            this.addClass('active');
        },
        onDragLeave: function(){
            // Happens when dragging something OUT of the DnD area
            this.removeClass('active');
        },
        onInit: function(){
            // Plugin is ready to use
            ui_add_log('Penguin initialized :)', 'info');
        },
        onComplete: function(){
            // All files in the queue are processed (success or error)
            ui_add_log('All pending tranfers finished');
            location.reload();
            location.hash = '#tab_4';
        },
        onNewFile: function(id, file){
            // When a new file is added using the file selector or the DnD area
            ui_add_log('New file added #' + id);
        },
        onBeforeUpload: function(id){
            // about tho start uploading a file
            ui_add_log('Starting the upload of #' + id);
            ui_multi_update_file_progress(id, 0, '', true);
            ui_multi_update_file_status(id, 'uploading', 'Uploading...');
        },
        onUploadProgress: function(id, percent){
            // Updating file progress
            ui_multi_update_file_progress(id, percent);
        },
        onUploadSuccess: function(id, data){
            // A file was successfully uploaded
            ui_add_log('Server Response for file #' + id + ': ' + JSON.stringify(data));
            ui_add_log('Upload of file #' + id + ' COMPLETED', 'success');
            ui_multi_update_file_status(id, 'success', 'Upload Complete');
            ui_multi_update_file_progress(id, 100, 'success', false);
        },
        onUploadError: function(id, xhr, status, message){
            ui_multi_update_file_status(id, 'danger', message);
            ui_multi_update_file_progress(id, 0, 'danger', false);
        },
        onFallbackMode: function(){
            // When the browser doesn't support this plugin :(
            ui_add_log('Plugin cant be used here, running Fallback callback', 'danger');
        },
        onFileSizeError: function(file){
            ui_add_log('File \'' + file.name + '\' cannot be added: size excess limit', 'danger');
        },
        onFileTypeError: function(file){
            ui_add_log('File \'' + file.name + '\' cannot be added: must be an image (type error)', 'danger');
        },
        onFileExtError: function(file){
            ui_add_log('File \'' + file.name + '\' cannot be added: must be an image (extension error)', 'danger');
        }
    });

    $(document).on('click', '.delete-media', function (e) {
        e.preventDefault();
        var $this = $(this);
        bootbox.dialog({
            message: "Êtes-vous sûr de vouloir supprimer cet élément ?",
            title: "Confirmer la suppression",
            buttons: {
                success: {
                    label: "Annuler",
                    callback: function() {
                        return false;
                    }
                },
                danger: {
                    label: "Confirmer",
                    className: "red",
                    callback: function() {
                        $.ajax({
                            method: 'delete',
                            url: $this.attr('href'),
                            success: function (data) {
                                if (data.success) {
                                    toastr.success("Image supprimer avec succès.");
                                    location.reload();
                                    location.hash = '#tab_4';
                                } else {
                                    toastr.error(data.msg);
                                }
                            },
                            error: function () {
                                toastr.error("Une erreur est survenue.");
                            }
                        });
                    }
                }
            }
        });
    });

    /**
     * switchers handller
     */
    $('.media-switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
        var $this = $(this);
        $.ajax({
            method: 'PUT',
            url: $this.data('switch-url'),
            data: {
                state: state ? 1 : 0,
                field: $this.data('switch-field')
            },
            success: function (data) {
                if (data.success) {
                    toastr.success("Changement d'étât effectué avec succès.", '');
                    location.reload();
                    location.hash = '#tab_4';
                } else {
                    toastr.error("Cette ligne n'existe plus.", '');
                }
            },
            error: function (data) {
                toastr.error("Une erreur est survenue.", '');
            }
        });
    });

});


// Creates a new file and add it to our list
function ui_multi_add_file(id, file)
{
    console.log(file);
    var template = $('#files-template').text();
    template = template.replace('%%filename%%', '/images/crop/' + file.filename + '?w=50&h=50');

    template = $(template);
    template.prop('id', 'uploaderFile' + id);
    template.data('file-id', id);

    $('#files').prepend(template);
}
