(function($) {
    $.fn.donetyping = function(callback){
        var _this = $(this);
        var x_timer;
        _this.keyup(function (){
            clearTimeout(x_timer);
            x_timer = setTimeout(clear_timer, 1000);
        });

        function clear_timer(){
            clearTimeout(x_timer);
            callback.call(_this);
        }
    }
})(jQuery);

var TableDatatablesAjax = function () {

    var grid = new Datatable();
    var typingTimer;                //timer identifier
    var doneTypingInterval = 3000;  //time in ms, 5 second for example

    var initDataTable = function () {

        grid.init({
            src: $("#app-datatable"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function (grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Chargement...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                //"dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
                // save datatable state(pagination, sort, etc) in cookie.
                "bStateSave": true,

                // save custom filters to the state
                "fnStateSaveParams": function (oSettings, sValue) {
                    $("#datatable_ajax tr.filter .form-control").each(function () {
                        sValue[$(this).attr('name')] = $(this).val();
                    });

                    return sValue;
                },

                // read the custom filters from saved state and populate the filter inputs
                "fnStateLoadParams": function (oSettings, oData) {
                    //Load custom filters
                    $("#app-datatable tr.filter .form-control").each(function () {
                        var element = $(this);
                        if (oData[element.attr('name')]) {
                            element.val(oData[element.attr('name')]);
                        }
                    });

                    return true;
                },
                "columnDefs": [{"targets": -1, "orderable": false, className: "footable-visible footable-last-column"}],
                "lengthMenu": [
                    [10, 20, 50, 100, 150],
                    [10, 20, 50, 100, 150] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": "", // ajax source
                },
                "ordering": true,
                "order": [
                    [0, "desc"]
                ],// set first column as a default sort by asc
                "drawCallback": function (settings) {
                    handleBootstrapSwitch();
                },
                responsive: {
                    details: {
                        type: 'column',
                        target: 'tr'
                    }
                },
            }
        });

        // handle group actionsubmit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'Please select an action',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            } else if (grid.getSelectedRowsCount() === 0) {
                App.alert({
                    type: 'danger',
                    icon: 'warning',
                    message: 'No record selected',
                    container: grid.getTableWrapper(),
                    place: 'prepend'
                });
            }
        });

        //grid.setAjaxParam("customActionType", "group_action");
        //grid.getDataTable().ajax.reload();
        //grid.clearAjaxParams();
    };

    /**
     *  Handles Bootstrap switches
     */
    var handleBootstrapSwitch = function () {
        if ($.fn.bootstrapSwitch) {
            $('.make-switch').bootstrapSwitch();
        }
        $('.make-switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
            var $this = $(this);
            $.ajax({
                method: 'PUT',
                url: $this.data('switch-url'),
                data: {
                    state: state ? 1 : 0,
                    field: $this.data('switch-field')
                },
                success: function (data) {
                    if (data.success) {
                        toastr.success("Changement d'étât effectué avec succès.", '');
                        grid.getDataTable().ajax.reload();
                    } else {
                        var message = data.message ? data.message : "Cette ligne n'existe plus.";
                        toastr.error(message, '');
                        $this.bootstrapSwitch('toggleState');
                    }
                },
                error: function (data) {
                    toastr.error("Une erreur est survenue.", '');
                }
            });
        });
    };

    /**
     * initialize confirmation on datatable
     * @returns
     */
    var handleDeleteModal = function () {
        $(document).on('click', '.delete-element', function (e) {
            e.preventDefault();
            var $this = $(this);
            bootbox.dialog({
                message: "Êtes-vous sûr de vouloir supprimer cet élément ?",
                title: "Confirmer la suppression",
                buttons: {
                    success: {
                        label: "Annuler",
                        callback: function () {
                            return;
                        }
                    },
                    danger: {
                        label: "Confirmer",
                        className: "red",
                        callback: function () {
                            $.ajax({
                                method: 'delete',
                                url: $this.attr('href'),
                                success: function (data) {
                                    if (data.success) {
                                        toastr.success(data.msg);
                                        grid.getDataTable().ajax.reload();
                                    } else {
                                        toastr.error(data.msg);
                                    }
                                },
                                error: function (data) {
                                    toastr.error("Une erreur est survenue.");
                                }
                            });
                        }
                    }
                }
            });
        });
    };

    var handleValidateTicket = function () {
        $(document).on('click', '.validate-ticket', function (e) {
            e.preventDefault();
            var $this = $(this);
            bootbox.dialog({
                message: "Êtes-vous sûr de vouloir valider ce ticket ?",
                title: "Confirmer la validation",
                buttons: {
                    success: {
                        label: "Annuler",
                        callback: function () {
                            return;
                        }
                    },
                    danger: {
                        label: "Confirmer",
                        className: "green",
                        callback: function () {
                            $.ajax({
                                method: 'POST',
                                url: $this.attr('href'),
                                success: function (data) {
                                    if (data.success) {
                                        toastr.success(data.msg);
                                        grid.getDataTable().ajax.reload();
                                    } else {
                                        toastr.error(data.msg);
                                    }
                                },
                                error: function (data) {
                                    toastr.error("Une erreur est survenue.");
                                }
                            });
                        }
                    }
                }
            });
        });
    };

    var onSubmitValidateTicket = function () {
        $(document).on('submit', '#form-validate-ticket', function () {
            validateTicket();
            return false;
        });
    };

    var onChangeToken = function () {
        $("#ticket-token").donetyping(function(callback){
            if ($(this).val() !== '') {
                validateTicket();
            }
        });
    };

    var validateTicket = function () {
        $.ajax({
            method: 'POST',
            url: '/admin/validator/ticket/validate/' + $('#ticket-token').val(),
            success: function (data) {
                if (data.success) {
                    toastr.success(data.msg);
                    grid.getDataTable().ajax.reload();
                    $("#ticket-token").val('');
                } else {
                    toastr.error(data.msg);
                    $("#ticket-token").val('');
                }
            },
            error: function (data) {
                toastr.error("Une erreur est survenue.");
            }
        });
    };

    var handleExportData = function () {
        $('a#export-data').on('click', function (e) {
            e.preventDefault();
            var url = $("#export-data").attr("href") + "?"
            $.each($(".form-filter"), function () {
                url = url + $(this).attr("name") + "=" + $(this).val() + "&";
                $("#export-data").attr("href", url);

            });
            location.href = $("#export-data").attr("href");
        });
    };
    return {

        //main function to initiate the module
        init: function () {
            initDataTable();
            handleDeleteModal();
            handleValidateTicket();
            onSubmitValidateTicket();
            onChangeToken();
        }

    };

}();

jQuery(document).ready(function () {
    TableDatatablesAjax.init();
});