<?php

return [
	'mode'              => 'utf-8',
    'format'            => [158, 82],
    'author'            => 'Guichet.Ma',
    'subject'           => 'Guichet.Ma - votre ticket',
    'keywords'          => 'Guichet.Ma',
    'creator'           => 'Guichet.Ma',
    'display_mode'      => 'fullpage',
    'tempDir'           => storage_path('framework/pdf'),
    'font_path'         => base_path('public/frontend/fonts/ticket/'),
    'font_data' => [
        'moskmedium'    => [ 'R'  => 'MoskMedium500.ttf' ],
        'mosklight'     => [ 'R'  => 'MoskLight300.ttf' ],
        'moskextrabold' => [ 'R'  => 'MoskExtraBold800.ttf' ]
    ]
];
