<?php

return [

    'use_cdn' => env('USE_CDN', false),

    'cdn_url' => 'https://guichet.imgix.net/',

    'filesystem' => [
        'disk' => 'asset-cdn',

        'options' => [
            'ACL' => 'public-read', // File is available to the public, independent of the S3 Bucket policy
            'CacheControl' => 'max-age=31536000, public', // Sets HTTP Header 'cache-control'. The client should cache the file for max 1 year
        ],
    ],

    'files' => [
        'ignoreDotFiles' => true,

        'ignoreVCS' => true,

        'include' => [
            'paths' => [
                'frontend/js',
                'frontend/css',
                'frontend/fonts',
                'frontend/images',
                'frontend/slick',
                'frontend/owl.carousel',
                'backend/plugins/select2',
                'backend/plugins/icheck',
                //'medias',
                //'avatars',
                //'posts',
                //'providers',
                //'settings'
            ],
            'files' => [
                'backend/plugins/bootstrap-toastr/toastr.min.css',
                'backend/plugins/jquery-validation/js/jquery.validate.min.js',
                'backend/plugins/jquery-validation/js/additional-methods.min.js',
                'backend/plugins/jquery-validation/js/localization/messages_fr.min.js',
                'backend/plugins/bootstrap-toastr/toastr.min.js',
                'backend/plugins/icheck/icheck.min.js',
                'backend/js/form.validation.js',
                'backend/plugins/bootbox/bootbox.min.js'
            ],
            'extensions' => [
                //
            ],
            'patterns' => [
                //
            ],
        ],

        'exclude' => [
            'paths' => [
                //
            ],
            'files' => [
                //
            ],
            'extensions' => [
                //
            ],
            'patterns' => [
                //
            ],
        ],
    ],

];
