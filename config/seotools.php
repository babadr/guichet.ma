<?php

return [
    'meta' => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults' => [
            'title' => false,//"Guichet.ma - Ticket et Deals au Maroc", // set false to total remove
            'description' => 'Guichet.ma: Tickets & Billetterie concerts, spectacles, cinéma, festivals, sport et théâtre au Maroc', // set false to total remove
            'separator' => ' - ',
            'keywords' => ['Guichet.ma', 'ticket', 'billetterie', 'concerts', 'casablanca', 'rabat', 'marrakech', 'agadir', 'tanger', 'spectacles', 'festivals', 'sport', 'theatre', 'humour', 'maroc'],
            'canonical' => false, // Set null for using Url::current(), set false to total remove
        ],
        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google' => null,
            'bing' => null,
            'alexa' => null,
            'pinterest' => null,
            'yandex' => null,
        ],
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title' => 'Guichet.ma - Ticket et Billetterie au Maroc', // set false to total remove
            'description' => 'Guichet.ma: Tickets & Billetterie concerts, spectacles, cinéma, festivals, sport et théâtre au Maroc', // set false to total remove
            'url' => false, // Set null for using Url::current(), set false to total remove
            'type' => false,
            'site_name' => false,
            'images' => [],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            'card'        => 'summary',
            'site'        => '@guichet_ma',
        ],
    ],
];
