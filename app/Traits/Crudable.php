<?php

namespace App\Traits;

use App\Models\Content\Seo;

trait Crudable
{

    /**
     * Handle create request.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->getBreadcrumbs();
        $view = $this->getView('backend.contents.' . $this->viewPrefix . '.form');

        return $view->with('title', trans($this->modelName . '.new'))
            ->with('route', route('backend.' . $this->modelName . '.store'))
            ->with('method', 'post')
            ->with('model', $this->currentModel);
    }

    /**
     * Store a newly created resource
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->beforeSave();
        $validator = $this->currentModel->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans($this->modelName . '.cannot_save'));

            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }

        $files = $this->saveFiles($this->currentRequest);
        $data = array_merge($attributes, $files);
        $this->currentModel->fill($data);
        if ($this->currentModel->save()) {
            $this->afterSave($data, $this->currentModel);
            if ($this->seo) {
                $saved = $this->saveSeoMetas($attributes, $this->currentModel->id);
                if ($saved !== true) {
                    return $saved;
                }
            }
            flash()->success(trans($this->modelName . '.new_created'));
            if (array_key_exists('saveandcontinue', $attributes)) {
                return redirect(route('backend.' . $this->modelName . '.edit', $this->currentModel->id));
            } else {
                return redirect(route('backend.' . $this->modelName . '.index'));
            }
        }
    }

    /**
     * Handle edit request
     *
     * @param int $id => the id to edit
     *
     * @return Response
     */
    public function edit($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect(route('backend.' . $this->modelName . '.index'));
        }
        $this->getBreadcrumbs($model);
        $view = $this->getView('backend.contents.' . $this->viewPrefix . '.form');

        return $view->with('title', trans($this->modelName . '.edit', ['name' => $model->{$this->title}]))
            ->with('route', route('backend.' . $this->modelName . '.update', $id))
            ->with('method', 'post')
            ->with('model', $model);
    }

    /**
     * Update the specified resource
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect()->back();
        }
        $attributes = $this->beforeSave($id);
        $validator = $model->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans($this->modelName . '.cannot_save'));

            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }

        $files = $this->saveFiles($this->currentRequest);
        $data = array_merge($attributes, $files);
        if ($model->update($data)) {
            $this->afterSave($data, $model);
            if ($this->seo) {
                $saved = $this->saveSeoMetas($attributes, $model->id, false);
                if ($saved !== true) {
                    return $saved;
                }
            }
            flash()->success(trans($this->modelName . '.updated'));
            if (array_key_exists('saveandcontinue', $attributes)) {
                return redirect(route('backend.' . $this->modelName . '.edit', $id));
            } else {
                return redirect(route('backend.' . $this->modelName . '.index'));
            }
        }
    }

    /**
     * Before Save Model
     *
     * @param int $id
     *
     * @return array
     */
    protected function beforeSave($id = null)
    {
        return $this->currentRequest->all();
    }

    /**
     * save SEO Metas
     *
     * @param array $attributes
     * @param int $id
     * @param boolean $isNew
     *
     * @return void|boolean
     */
    protected function saveSeoMetas(array $attributes, $id, $isNew = true)
    {
        $attributes['model_id'] = $id;
        $attributes['model_class'] = get_class($this->currentModel);
        $attributes['seo_alias'] = !empty($attributes['seo_alias']) ? trim($attributes['seo_alias'], '/') : $this->generateAlias($attributes);
        $attributes['seo_route'] = $this->currentModel->route;
        if (empty($attributes['seo_id'])) {
            $seo = new Seo();
        } else {
            $seo = Seo::find($attributes['seo_id']);
        }
        $validator = $seo->validator($attributes);
        if ($validator->fails()) {
            flash()->success(trans($isNew ? $this->modelName . '.new_created' : $this->modelName . '.updated'));
            flash()->error(trans('seo.cannot_save'));

            return redirect(route('backend.' . $this->modelName . '.edit', $id))->withErrors($validator)->withInput($attributes);
        }

        if (empty($attributes['seo_id'])) {
            $seo->fill($attributes);
            $seo->save();
        } else {
            $seo->update($attributes);
        }

        return true;
    }

    /**
     * Generate URL alis
     *
     * @param array $attributes
     *
     * @return string
     */
    protected function generateAlias($attributes)
    {
        $alias = str_slug($attributes[$this->title]);

        return trim($alias);
    }

    /**
     * After save Model
     *
     * @param array $attributes
     * @param Model $model
     *
     * @return void
     */
    protected function afterSave(array $attributes, $model)
    {

    }

    /**
     * Generate current Breadcrumbs
     *
     * @return void
     */
    protected function getBreadcrumbs($model = null)
    {
        $this->breadcrumbs->addCrumb(trans($this->listTitle), route('backend.' . $this->modelName . '.index'));
        if ($model) {
            $this->breadcrumbs->addCrumb(trans($this->modelName . '.edit', ['name' => $model->{$this->title}]));
        } else {
            $this->breadcrumbs->addCrumb(trans($this->modelName . '.new'));
        }
    }

    /**
     * Save uploaded files
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    protected function saveFiles($request)
    {
        $files = [];
        if (isset($this->files) && is_array($this->files)) {
            foreach ($this->files as $file) {
                if ($request->hasFile($file)) {
                    $files[$file] = $request->file($file)->store($this->currentModel->path, ['disk' => 's3']);
//                    $directories = [
//                        'avatars' => ['original', 'thumb'],
//                        'medias' => ['original', 'slide', 'small', 'big', 'thumb'],
//                        'posts' => ['original', 'post'],
//                        'providers' => ['original', 'partner'],
//                        'settings' => ['original']
//                    ];
//                    $path = $this->getImagePath($files[$file]);
//                    //\ImageOptimizer::optimize($path, str_replace('.png', '_test.png', $path));
//                    if (isset($directories[$this->currentModel->path])) {
//                        foreach ($directories[$this->currentModel->path] as $template) {
//                            $image = \Image::make($path);
//                            switch ($template) {
//                                case 'slide':
//                                    $this->resize($image, 900, 580);
//                                    break;
//                                case 'small':
//                                    $this->resize($image, 480, 240);
//                                    break;
//                                case 'big':
//                                    $this->resize($image, 900, 600);
//                                    break;
//                                case 'partner':
//                                    $this->resize($image, 200, 150);
//                                    break;
//                                case 'post':
//                                    $this->resize($image, 845, 475);
//                                    break;
//                                case 'thumb':
//                                    $image = $image->fit(150, 90);
//                                    break;
//                                case 'original':
//                                    break;
//                            }
//                            \Storage::disk('s3')->put('imagecache' . DIRECTORY_SEPARATOR . $template . DIRECTORY_SEPARATOR . $files[$file], $image->stream()->__toString());
//                        }
//                    }
                }
            }
        }

        return $files;
    }

    /*
     * Returns full image path from given filename
     *
     * @param  string $filename
     * @return string
     */
    private function getImagePath($filename)
    {
        // find file
        foreach (config('imagecache.paths') as $path) {
            // don't allow '..' in filenames
            $image_path = $path . '/' . str_replace('..', '', $filename);
            if (file_exists($image_path) && is_file($image_path)) {
                // file found
                return $image_path;
            }
        }
    }

    /*
    * Resize image
    *
    * @param $image
    * @param int $w
    * @param int $h
     *
    * @return void
    */
    private function resize(&$image, $w, $h)
    {
        $image->resize($w, $h, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }

}
