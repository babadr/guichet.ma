<?php

namespace App\Traits;

trait Datatableable
{

    /**
     * Add conditions into data query to display list
     *
     * @var array
     */
    protected $conditions = [];

    /**
     * Request Length
     *
     * @var int
     */
    protected $length = 10;

    /**
     * Request Start
     *
     * @var int
     */
    protected $start = 0;

    /**
     * Request Draw
     *
     * @var int
     */
    protected $draw = 1;

    /**
     * Order by field
     *
     * @var string
     */
    protected $orderBy = '';

    /**
     * Order way
     *
     * @var string
     */
    protected $orderWay = 'asc';

    /**
     * Display a listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->currentRequest->ajax()) {
            return $this->createAjaxResponse();
        }

        $this->breadcrumbs->addCrumb(trans($this->listTitle), '/');

        return $this->getView('backend.contents.' . $this->viewPrefix . '.index');
    }

    /**
     * Handle delete request
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            return response()->json(['success' => false, 'msg' => trans('app.not_found')]);
        }

        $result = $model->delete();

        return response()->json(['success' => $result, 'msg' => trans($result ? 'app.delete_success' : 'app.cannot_delete')]);
    }

    /**
     * Handle a generic switch request.
     * used for switch a boolean field status
     *
     * @param int $id
     *
     * @return json
     */
    public function executeSwitch($id)
    {
        $request = $this->currentRequest->all();
        $model = $this->currentModel->find($id);
        if ($model == null) {
            return response()->json(['success' => false]);
        }
        $model->update([$request['field'] => $request['state']]);

        return response()->json(['success' => true]);
    }

    /**
     * Create ajax response
     *
     * @return \Illuminate\Http\Response
     */
    protected function createAjaxResponse()
    {
        $request = $this->currentRequest->all();
        $this->initFilter($request);
        $this->createOrder($request);
        $this->conditions = array_merge($this->conditions, $this->customConditions());
        $recordsTotal = $this->currentModel->where($this->conditions)->count();
        $this->createConditions($request);
        $this->currentModel = $this->currentModel->where($this->conditions);
        $this->initJoins();
        $recordsFiltered = $this->currentModel->count();
        $output = [
            'data' => $this->buildRows(),
            'draw' => $this->draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
        ];

        return response()->json($output);
    }

    /**
     * Build listing rows
     *
     * @return array
     */
    protected function buildRows()
    {
        $this->initRows();
        $rows = $this->currentModel->get($this->select);
        $data = [];
        foreach ($rows as $key => $object) {
            foreach ($this->columns as $field => $params) {
                if (isset($params['callBack'])) {
                    $str = $this->{$params['callBack']}($object, $field);
                } else {
                    $str = $object->{$field};
                }
                $data[$key][] = $str;
            }
            $data[$key][] = $this->addRowActions($object);
        }

        return $data;
    }

    /**
     * Creates conditions array from url parameters
     *
     * @param array $request
     *
     * @return void
     */
    protected function createConditions(array $request)
    {
        foreach ($this->columns as $field => $params) {
            $filter = isset($params['filterKey']) ? $params['filterKey'] : $field;
            if ((isset($request[$field]) && trim($request[$field]) != '' || !empty($request[$field . '_from']) || !empty($request[$field . '_from'])) && $filter) {
                if (in_array($params['type'], ['int', 'select', 'bool'])) {
                    $this->conditions[] = [$params['prefix'] . $filter, '=', (int)$request[$field]];
                } elseif ($params['type'] == 'text') {
                    $this->conditions[] = [$params['prefix'] . $filter, 'LIKE', '%' . trim($request[$field]) . '%'];
                } elseif ($params['type'] == 'select_string') {
                    $this->conditions[] = [$params['prefix'] . $filter, 'LIKE', '%' . str_replace('\\', '\\\\', trim($request[$field])) . '%'];
                } elseif ($params['type'] == 'date') {
                    if (!empty($request[$field . '_from'])) {
                        $this->conditions[] = [$params['prefix'] . $filter, '>=', date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $request[$field . '_from'])))];
                    }
                    if (!empty($request[$field . '_to'])) {
                        $this->conditions[] = [$params['prefix'] . $filter, '<=', date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $request[$field . '_to'])))];
                    }
                } elseif ($params['type'] == 'money') {
                    if (!empty($request[$field . '_from'])) {
                        $this->conditions[] = [$params['prefix'] . $filter, '>=', str_replace(',', '.', $request[$field . '_from'])];
                    }
                    if (!empty($request[$field . '_to'])) {
                        $this->conditions[] = [$params['prefix'] . $filter, '<=', str_replace(',', '.', $request[$field . '_to'])];
                    }
                }
            }
        }
    }

    /**
     * Initialize length, start and draw
     *
     * @param array $request
     *
     * @return void
     */
    protected function initFilter(array $request)
    {
        if (isset($request['length'])) {
            $this->length = (int)$request['length'];
        }
        if (isset($request['start'])) {
            $this->start = (int)$request['start'];
        }
        if (isset($request['draw'])) {
            $this->draw = (int)$request['draw'];
        }
    }

    /**
     * Creates order array from post parameters
     * order[0][column]:2
     * order[0][dir]:asc
     *
     * @param array $request
     *
     * @return void
     */
    protected function createOrder(array $request)
    {
        if (isset($request['order']) && count($request['order'])) {
            $o = $request['order'][0];
            $fieldName = $this->getFieldNameFromIndex($o['column']);
            if ($fieldName != null) {
                $this->orderWay = isset($o['dir']) ? $o['dir'] : 'asc';
                $this->orderBy = $fieldName;
            }
        }
    }

    /**
     * Creates order array from post parameters
     *
     * @return string|null
     */
    protected function getFieldNameFromIndex($index)
    {
        $fieldsName = array_keys($this->columns);
        if (array_key_exists($index, $fieldsName)) {
            return $this->columns[$fieldsName[$index]]['orderKey'];
        }

        return null;
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.elements.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Init Joins
     *
     * @return void
     */
    protected function initJoins()
    {
        if (!empty($this->joins)) {
            foreach ($this->joins as $join) {
                $this->currentModel->join($join[0], $join[1], $join[2], $join[3]);
            }
        }
        if (!empty($this->leftJoins)) {
            foreach ($this->leftJoins as $join) {
                $this->currentModel->leftJoin($join[0], $join[1], $join[2], $join[3]);
            }
        }
    }

    /**
     * Init Rows
     *
     * @return void
     */
    protected function initRows()
    {
        if ($this->select) {
            $this->currentModel->select($this->select);
        }
        if ($this->orderBy) {
            $this->currentModel->orderBy($this->orderBy, $this->orderWay);
        }
        if ($this->start) {
            $this->currentModel->skip((int)$this->start);
        }
        if ($this->length) {
            $this->currentModel->take((int)$this->length);
        }
    }

    /**
     * Format given date
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $field
     *
     * @return string
     */
    protected function printDate($model, $field)
    {
        if (is_object($model->{$field})) {
            return $model->{$field}->format('d/m/Y');
        } else {
            return $model->{$field} != '0000-00-00' && $model->{$field} != null ? date('d/m/Y', strtotime($model->{$field})) : '---';
        }
    }

    /**
     * Format given date
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $field
     *
     * @return string
     */
    protected function printDateTime($model, $field)
    {
        if (is_object($model->{$field})) {
            return $model->{$field}->format('d/m/Y - H:i');
        } else {
            return $model->{$field} != '0000-00-00 00:00:00' && $model->{$field} != null ? date('d/m/Y - H:i', strtotime($model->{$field})) : '---';
        }
    }

    /**
     * Generate checkbox for active field
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $field
     *
     * @return string
     */
    protected function printStatus($model, $field)
    {
        return view('backend.elements.status')
            ->with('model', $model)
            ->with('field', $field)
            ->with('modelName', $this->modelName)
            ->with('checked', $model->{$field})->render();
    }

    /**
     * Print Boolean
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $field
     *
     * @return string
     */
    protected function printBoolean($model, $field)
    {
        return view('backend.elements.boolean')
            ->with('model', $model)
            ->with('field', $field)->render();
    }

    /**
     * Print Price
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $field
     *
     * @return string
     */
    protected function printPrice($model, $field)
    {
        return \Tools::displayPrice($model->{$field});
    }

    /**
     * Create custom conditions
     *
     * @return array
     */
    protected function customConditions()
    {
        return [];
    }

}
