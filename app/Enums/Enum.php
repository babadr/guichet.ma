<?php

namespace App\Enums;

use Artisaninweb\Enum\Enum as EnumBase;

class Enum extends EnumBase
{

    /**
     * Returns all enum's values as array : [value => name]
     * @return array
     */
    public static function getValues()
    {
        $enumValues = self::getEnumValues();

        $values = [];
        foreach ($enumValues as $enumValue) {
            $values[$enumValue->getConstValue()] = $enumValue->getConstName();
        }
        return $values;
    }

    /**
     * Returns all enum's values as array : [name => value]
     * @return array
     */
    public static function getConstValues()
    {
        $enumValues = self::getEnumValues();

        $values = [];
        foreach ($enumValues as $enumValue) {
            $values[$enumValue->getConstName()] = $enumValue->getConstValue();
        }
        return $values;
    }

    /**
     *
     * @param integer $value
     * @return string the name of the enum's instance corresponding to $value
     */
    public static function getNameFromValue($value)
    {
        $enumValues = self::getEnumValues();

        foreach ($enumValues as $enumValue) {
            if ($enumValue->getConstValue() == $value) {
                return $enumValue->getConstName();
            }
        }
        return '';
    }

    /**
     * Get only the values from the enum
     *
     * example : Ticket::getJustValues() => [0 ,1, 2]
     * @return array
     */
    public static function getJustValues()
    {
        return array_values(self::getConstValues());
    }

    /**
     * Get the translated name by value
     *
     * @param string $transFile
     * @param int $value
     * @return string
     */
    public static function getTransNameFromValue($transFile, $value)
    {
        $enumValues = self::getEnumValues();
        foreach ($enumValues as $enumValue) {
            if ($enumValue->getConstValue() == $value) {
                return trans($transFile . '.' . $enumValue->getConstName());
            }
        }
        return '';
    }

    /**
     * Get the translated values
     *
     * @param $transFile
     * @return \Illuminate\Support\Collection
     */
    public static function getTransValues($transFile, $useValue = false)
    {
        return collect(self::getConstValues())->flip()->map(function ($key, $value) use ($transFile, $useValue) {
            return $useValue ? trans($transFile . '.' . $value) : trans($transFile . '.' . $key);
        });
    }
}