<?php

namespace App\Enums\Deal;

use App\Enums\Enum;

/**
 * Class Type
 *
 * @package App\Enums\Deal
 * @method static DEAL()
 * @method static TICKET()
 */
class Type extends Enum
{
    const DEAL    = "deal";
    const TICKET  = "ticket";
}
