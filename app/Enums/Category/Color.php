<?php

namespace App\Enums\Category;

use App\Enums\Enum;

/**
 * Class Color
 *
 * @package App\Enums\Category
 * @method static BOOKED()
 * @method static CANCELLED()
 */
class Color extends Enum
{
    const BLACK     = "black";
    const BLUE      = "blue";
    const BROWN     = "brown";
    const GREEN     = "green";
    const RED       = "red";
    const YELLOW    = "yellow";
    const GRAY      = "gray";
    const ORANGE    = "orange";
    const PINK      = "pink";
    const PURPLE    = "purple";
}
