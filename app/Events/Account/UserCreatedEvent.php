<?php

namespace App\Events\Account;

class UserCreatedEvent
{
    /**
     * @var array
     */
    public $datas;


    /**
     * UserCreatedEvent constructor.
     * @param array $datas
     */
    public function __construct(array $datas)
    {
        $this->datas = $datas;
        
    }
    
}