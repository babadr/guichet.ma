<?php

namespace App\Templates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Crop implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        $w = (int)request()->query('w');
        $h = (int)request()->query('h');
        if (empty($w) || empty($h)) {
            return $image;
        }

        return $image->fit($w, $h);
    }
}