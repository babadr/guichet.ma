<?php

namespace App\Templates;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Resize implements FilterInterface
{

    public function applyFilter(Image $image)
    {
        $w = (int)request()->query('w');
        $h = (int)request()->query('h');
        if (empty($w) || empty($h)) {
            return $image;
        }

        return $image->resize($w, $h, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }
}