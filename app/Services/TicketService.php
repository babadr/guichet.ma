<?php

namespace App\Services;

use App\Models\Acl\User;
use App\Models\Deal\Provider;
use App\Models\Store\Order;
use App\Models\Deal\Scene;
use App\Models\Store\Ticket;
use App\Models\Store\FloatingTicket;
use App\Models\Store\FootballTicket;

class TicketService
{
    /**
     * generate tickets for order
     *
     * @param $order_id
     */
    public function generate($order_id)
    {
        /** @var Order $order , find order or throw fail error */
        $order = (new Order)->findOrFail($order_id);

        // generate tickets for each order line
        foreach ($order->lines as $line) {
            $this->generateTickets($line);
        }
    }

    /**
     * validate ticket
     *
     * @param $hash
     * @return int|Ticket
     */
    public function validate($hash)
    {
        /** @var User $user */
        $user = auth()->user();
        if (!$user->hasRole(['validator'])) {
            return 404;
        }

        /** @var Ticket $ticket */
        $ticket = (new Ticket)->where('hash', '=', $hash)->first();

        if (!$ticket) {
            return 404;
        }

        /** @var Provider $provider */
        $provider = $ticket->line->offer->deal->provider;
        if ($provider->id != $user->provider_id || !in_array($ticket->line->order->status, [2, 3, 5])) {
            return 404;
        }

        if ($ticket->status) {
            return 401;
        }

        return $ticket;
    }

    /**
     * generate tickets
     *
     * @param $line
     */
    protected function generateTickets($line)
    {
        /** @var Ticket $countTicket , count existing ticket for given order line */
        $countTicket = (new Ticket)->where('order_line_id', '=', $line->id)->count();

        /** @var int $quantity , calculate quantity to generate */
        $quantity = $line->quantity - $countTicket;

        if (!empty($line->seats)) {
            $seats = explode(',', $line->seats);
        }

        // generate tickets by quantity
        for ($i = 0; $i < $quantity; $i++) {
            $deal = $line->offer->deal;
            $hash = str_random(21);
            if ($deal->barcode_type) {
                $days = [
                    '21' => 'J01',
                    '22' => 'J02',
                    '23' => 'J03',
                    '24' => 'J04',
                    '25' => 'J05',
                    '26' => 'J06',
                    '27' => 'J07',
                    '28' => 'J08',
                    '29' => 'J09'
                ];
                if (in_array(date('d'), array_keys($days)) && $deal->category_id === 15) {
                    $code = substr($deal->barcode_prefix, 0, 3);
                    $scene = Scene::where('code', $code)->first();
                    if ($scene) {
                        $floatingTicket = FloatingTicket::where('scene_id', $scene->id)
                            ->where('day', $days[date('d')])
                            ->where('selected', false)
                            ->first();
                        if ($floatingTicket) {
                            $hash = $floatingTicket->hash;
                            $floatingTicket->selected = true;
                            $floatingTicket->save();
                        } else {
                            $hash = Ticket::generateBarcode($deal->barcode_prefix);
                        }
                    } else {
                        $hash = Ticket::generateBarcode($deal->barcode_prefix);
                    }
                } elseif ($deal->is_football) {
                    $footballTicket = FootballTicket::where('offer_id', $line->offer_id)
                        ->where('selected', false)
                        ->first();
                    if ($footballTicket) {
                        $hash = $footballTicket->hash;
                        $footballTicket->selected = true;
                        $footballTicket->save();
                    } else {
                        $hash = Ticket::generateBarcode($deal->barcode_prefix);
                    }
                } else {
                    $hash = Ticket::generateBarcode($deal->barcode_prefix);
                }
            }
            $ticket = new Ticket([
                'order_line_id' => $line->id,
                'hash' => strtoupper($hash),
                'status' => false,
                'number' => $this->generateTicketNumber(),
                'seat' => isset($seats) ? $seats[$i] : null
            ]);
            $ticket->save();
        }
    }

    /**
     * generate auto ticket number
     * @return string
     */
    protected function generateTicketNumber()
    {
        $year_part = '#GU' . date("y");
        $countTickets = (new Ticket)->where('number', 'LIKE', $year_part . '%')->count();
        return $year_part . str_pad($countTickets + 1, 7, "0", STR_PAD_LEFT);
    }
}
