<?php

namespace App\Services;

use Setting;
use Mail;
use App\Models\Acl\User;
use App\Models\Common\Contact;
use App\Models\Store\Order;
use App\Models\Store\OrderLine;
use App\Models\Store\Recipient;

class MailService
{

    protected $prefix;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->prefix = strpos(request()->url(), '/admin') === false ? 'frontend.' : 'backend.';
    }

    /**
     * Send the mail to the user for resetting the password
     *
     * @param User $user
     * @param string $token
     *
     * @return void
     */
    public function sendResetPasswordRequest(User $user, $token)
    {
        $action = route($this->prefix . 'auth.password.showReset', $token);
        Mail::send('emails.reset', compact('user', 'action'), function ($message) use ($user) {
            $message
                ->to($user->email, $user->nice_name)
                ->subject(trans('email.reset', ['sitename' => Setting::get('site_name')]));
        });
    }

    /**
     * Send the mail to the user after account creation
     *
     * @param User $user
     *
     * @return void
     */
    public function sendUserWelcome(User $user)
    {
        Mail::send('emails.welcome', compact('user'), function ($message) use ($user) {
            $message
                ->to($user->email, $user->nice_name)
                ->subject(trans('email.welcome', ['sitename' => Setting::get('site_name')]));
        });
    }

    /**
     * Send notification for new contact request
     *
     * @param Contact $contact
     *
     * @return void
     */
    public function sendContactRequest(Contact $contact)
    {
        Mail::send('emails.contact', compact('contact'), function ($message) use ($contact) {
            $message->replyTo($contact->email, $contact->full_name)
                ->to(\Setting::get('email'), \Setting::get('site_name'))
                ->subject(trans('contact.subject_email'));
        });
    }

    /**
     * Send notification for new order to customer
     *
     * @param Order $order
     *
     * @return void
     */
    public function sendOrderConfirmation(Order $order)
    {
        Mail::send('emails.order', compact('order'), function ($message) use ($order) {
            $message->to($order->user->email, $order->user->full_name)->subject('Votre commande sur Guichet.ma');
        });
    }

    public function sendOrderTickets(Order $order)
    {
        Mail::send('emails.tickets', compact('order'), function ($message) use ($order) {
            $message->to($order->user->email, $order->user->full_name)->subject('Vos tickets sur Guichet.ma');
        });
    }

    public function sendNewSubscription(Recipient $recipient, OrderLine $line)
    {
        Mail::send('emails.subscription', compact('recipient', 'line'), function ($message) use ($line, $recipient) {
            $filePath = storage_path('app') . '/' . $recipient->avatar;

            $message->to(['karim.boukamoum@gmail.com', 'abonnements@rajaclubathletic.ma'])
                ->subject('Nouvel abonnement CARTE ' . $line->offer_name . ' - Guichet.ma')
                ->attach($filePath, [
                    'as' => str_slug($recipient->full_name, '_') . \File::extension($filePath),
                    'mime' => \File::mimeType($filePath)
                ]);
        });
    }

}
