<?php

namespace App\Services;

use App\Models\Setting\Setting;

class SettingService
{

    /**
     * Cache variable containing all Setting values by key
     *
     * @var Setting[]|Collection
     */
    protected $keyedSettings;

    /**
     * Cache variable containing all Setting values
     *
     * @var Setting[]|Collection
     */
    protected $settings;

    public function __construct()
    {
        $this->setting = \App::make(Setting::class);
        $this->settings = $this->setting->all();
        $this->keyedSettings = $this->settings->keyBy('key');
    }

    /**
     * Get value of config name
     *
     * @param string $configName
     * @param string $default
     *
     * @return string
     */
    public function get($configName, $default = null)
    {
        $setting = $this->keyedSettings->get($configName);
        if ($setting != null) {
            return $setting->value;
        }

        return $default ? $default : trans('config.' . $configName);
    }

    /**
     * Check if config name exists
     *
     * @param string $configName
     * @param bool|string $different
     *
     * @return String
     */
    public function has($configName, $different = false)
    {
        $setting = $this->keyedSettings->get($configName);
        if ($setting != null && !empty($setting->value)) {
            if ($different == $setting->value) {
                return false;
            }

            return true;
        }

        return false;
    }

    /**
     * Replace variables with
     *
     * @param String $text
     *
     * @return String
     */
    public function fillConfig($text)
    {
        if (empty($this->settings)) {
            return $text;
        }
        foreach ($this->settings as $setting) {
            $param = '{{' . $setting->key . '}}';
            $text = str_replace($param, $setting->value, $text);
        }

        return $text;
    }

    /**
     * @return Collection|\Illuminate\Support\Collection
     */
    public function all()
    {
        return $this->settings;
    }

}
