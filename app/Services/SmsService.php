<?php

namespace App\Services;

use App\Models\Store\Order;
use Illuminate\Support\Str;
use infobip\api\client\SendSingleTextualSms;
use infobip\api\configuration\BasicAuthConfiguration;
use infobip\api\model\sms\mt\send\textual\SMSTextualRequest;

class SmsService
{

    protected $client;

    protected $requestBody;

    /**
     * Create a new service instance.
     *
     * @return void
     */
    public function __construct()
    {
        $config = new BasicAuthConfiguration('guichet.ma', 'mnatsofiahanaa1201');
        $this->client = new SendSingleTextualSms($config);
        $this->requestBody = new SMSTextualRequest();
    }

    /**
     * Send sms for new order to customer
     *
     * @param Order $order
     *
     * @return void
     */
    public function sendOrderConfirmation(Order $order)
    {
        $phone = $order->user->phone;
        if (!empty($phone) && env('APP_ENV') === 'production') {
            $this->formatPhone($phone);
            $reference = $order->reference;
            $amount = \Tools::displayPrice($order->total_paid, 0);
            $message = sprintf('Cher(e) client(e) %s, votre commande guichet.ma N° %s de %s a été confirmée! Voir email pour plus de détails. Merci pour votre confiance.', $order->user->first_name, $reference, $amount);
            $this->requestBody->setFrom('Guichet.ma');
            $this->requestBody->setTo($phone);
            $this->requestBody->setText($message);
            try {
                $response = $this->client->execute($this->requestBody);
                $sentMessageInfo = $response->getMessages()[0];
                return $sentMessageInfo->getStatus()->getName();
            } catch (\Exception $ex) {
                return $ex->getMessage();
            }
        }
    }

    private function formatPhone(&$phone)
    {
        $phone = trim(str_replace(['(', ')', '-', ' ', '.'], '', $phone));
        if (starts_with($phone, '+')) {
            $phone = str_replace('+', '', $phone);
        } elseif (starts_with($phone, '00')) {
            $phone = Str::replaceFirst('00', '', $phone);
        } elseif (starts_with($phone, '0')) {
            $phone = Str::replaceFirst('0', '212', $phone);
        } elseif (starts_with($phone, '212')) {
            $phone = $phone;
        } else {
            $phone = '212' . $phone;
        }

        return $phone;
    }

}
