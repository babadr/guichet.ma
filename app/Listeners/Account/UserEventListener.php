<?php

namespace App\Listeners\Account;

use App\Models\Acl\Role;
use App\Models\Acl\User;
use App\Services\MailService;
use Illuminate\Auth\Events\Registered;

/**
 * Description of UserEventListener
 */
class UserEventListener 
{
    /**
     *  @var MailService $mailService 
     */
    protected $mailService;

    /**
     * UserEventListener constructor.
     * @param MailService $mailService
     */
    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * @param Registered $event
     */
    public function handle(Registered $event)
    {
        /** @var User $event */
        $this->mailService->sendUserWelcome($event->user);
        $event->user->attachRole((new Role)->where('name', '=', 'user')->first());
    }
}
