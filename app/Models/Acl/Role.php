<?php

namespace App\Models\Acl;

use Zizaco\Entrust\EntrustRole;
use App\Traits\Validatorable;

class Role extends EntrustRole
{

    use Validatorable;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['name', 'display_name', 'description'];

    /**
     * Disable Created_at and Updated_at
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'name' => 'required|max:255|unique:roles,name' . $exceptId,
            'display_name' => 'required|max:255|unique:roles,display_name' . $exceptId,
            'description' => 'max:255',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany('App\Models\Acl\Permission');
    }

    /**
     * @return \App\Models\Acl\User
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\Acl\User');
    }
}