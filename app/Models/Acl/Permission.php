<?php

namespace App\Models\Acl;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'display_name', 'active', 'category_id'];

    /**
     * Disable Created_by and Updated_by
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'name' => 'required|max:255|unique:permissions,name' . $exceptId,
            'display_name' => 'required|max:255|unique:permissions,display_name' . $exceptId
        ];
    }

    /**
     * return the name of the linked category
     * @return String
     */
    public function getCategoryNameAttribute()
    {
        if ($this->category != null) {
            return $this->category->display_name;
        }
    }

    public function category()
    {
        return $this->hasOne('App\Models\Acl\CategoryPermission', 'id', 'category_id');
    }
}