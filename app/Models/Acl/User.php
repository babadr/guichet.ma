<?php

namespace App\Models\Acl;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Traits\Validatorable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,
        EntrustUserTrait,
        Validatorable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'phone', 'address', 'city', 'country', 'email', 'password', 'avatar', 'active', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Storage path name
     *
     * @var string
     */
    public $path = 'avatars';

    /**
     * Default Super Admin ID
     *
     * @var int
     */
    const SUPER_ADMIN = 1;

    /**
     * Default Customer ID
     *
     * @var int
     */
    const CUSTOMER_ID = 3;

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        $passwordRequired = !$this->id ? '|required|min:8|' : '';
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'email' => 'required|max:255|email|unique:users,email' . $exceptId,
            'last_name' => 'required|max:65',
            'first_name' => 'required|max:65',
            'phone' => 'required|max:14|regex:/^\+?\d+( \d+)*$/',
            'address' => 'max:255',
            'city' => 'max:64',
            'country' => 'max:15',
            'password' => 'confirmed' . $passwordRequired,
            'password_confirmation' => 'same:password' . $passwordRequired,
            'avatar' => 'image|mimes:jpeg,png,jpg,gif|max:1024',
            'active' => 'boolean',
        ];
    }

    /**
     * Crypt password before save
     *
     * @param string $password
     * @return void
     */
    public function setPasswordAttribute($password)
    {
        if (!empty($password)) {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    /**
     * Concat user first and last name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

    public function provider()
    {
        return $this->hasOne('App\Models\Deal\Provider');
    }

    public function seller()
    {
        return $this->hasOne('App\Models\Deal\Seller');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
