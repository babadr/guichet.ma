<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class Faq extends Model
{

    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['title', 'content', 'active'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'content' => 'required',
            'active' => 'boolean',
        ];
    }

    /**
     * Scope a query to only include active faqs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}