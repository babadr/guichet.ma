<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;

class Seo extends Model
{

    use Validatorable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'seo';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['model_id', 'model_class', 'seo_route', 'seo_alias', 'seo_title', 'seo_description', 'seo_keywords', 'seo_robots'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'model_id' => 'required',
            'model_class' => 'required',
            'seo_route' => 'required|max:255',
            'seo_alias' => 'required|max:255|unique:seo,seo_alias' . $exceptId,
            'seo_title' => 'max:255',
            'seo_description' => '',
            'seo_keywords' => '',
            'seo_robots' => '',
        ];
    }

}