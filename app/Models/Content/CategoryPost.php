<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class CategoryPost extends Model
{

    use Validatorable,
        SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['name', 'active'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Dynamic model route
     *
     * @var string
     */
    public $route = 'Frontend\Content\PostController@category';

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'name' => 'required|max:255|unique:category_posts,name' . $exceptId,
            'active' => 'boolean',
        ];
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Content\Post', 'category_id');
    }

    /**
     * Get SEO
     */
    public function seo()
    {
        return $this->hasOne('App\Models\Content\Seo', 'model_id')->where('model_class', self::class);
    }
}
