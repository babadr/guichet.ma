<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class Post extends Model
{

    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['title', 'category_id', 'content', 'image', 'active'];

    /**
     * Storage path name
     *
     * @var string
     */
    public $path = 'posts';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Dynamic model route
     *
     * @var string
     */
    public $route = 'Frontend\Content\PostController@view';

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'content' => 'required',
            'active' => 'boolean',
            'category_id' => 'nullable|integer',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:1024',
        ];
    }

    /**
     * Scope a query to only include active faqs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
     * Get SEO
     */
    public function seo()
    {
        return $this->hasOne('App\Models\Content\Seo', 'model_id')->where('model_class', self::class);
    }

    public function category()
    {
        return $this->hasOne('App\Models\Content\CategoryPost', 'id', 'category_id');
    }
}