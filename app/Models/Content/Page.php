<?php

namespace App\Models\Content;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class Page extends Model
{

    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['title', 'content', 'active'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Dynamic model route
     *
     * @var string
     */
    public $route = 'Frontend\Content\PageController@view';

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'content' => '',
            'active' => 'boolean',
        ];
    }

    /**
     * Get SEO
     */
    public function seo()
    {
        return $this->hasOne('App\Models\Content\Seo', 'model_id')->where('model_class', self::class);
    }

    /**
     * Scope a query to only include active faqs.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

}