<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;

class FootballTicket extends Model
{

    use Validatorable;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'offer_id',
        'zone',
        'access',
        'sector',
        'seat',
        'hash',
        'selected'
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'offer_id' => 'required|integer',
            'hash' => 'required',
            'zone' => 'nullable',
            'access' => 'nullable',
            'sector' => 'nullable',
            'seat' => 'nullable',
            'selected' => 'required|boolean',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo(\App\Models\Deal\Offer::class, 'offer_id');
    }

    /*
    * Scope a query to only get order by reference
    *
    * @param string $reference
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public static function getByReference($reference)
    {
        return self::where('hash', '=', $reference)->first();
    }
}
