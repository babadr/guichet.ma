<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipient extends Model
{

    use Validatorable, SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Storage path name
     *
     * @var string
     */
    public $path = 'photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'order_line_id',
        'full_name',
        'email',
        'phone',
        'address',
        'city',
        'zip_code',
        'avatar',
        'cin',
        'birth_date'
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'order_line_id' => 'required|integer',
            'full_name' => 'required|max:65',
            'email' => 'required|max:255|email',
            'phone' => 'required|max:14|regex:/^\+?\d+( \d+)*$/',
            'address' => 'required|max:255',
            'city' => 'required|max:64',
            'zip_code' => 'required|integer|between:1,99999',
            'birth_date' => 'required|date',
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif|max:500',
            'cin' => 'required|string',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function line()
    {
        return $this->belongsTo(OrderLine::class, 'order_line_id');
    }
}
