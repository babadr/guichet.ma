<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;

class Ticket extends Model
{

    use Validatorable;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'order_line_id',
        'hash',
        'status',
        'number',
        'seat'
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'order_line_id' => 'required',
            'hash' => 'required',
            'status' => 'required|boolean',
            'seat' => 'nullable|string'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function line()
    {
        return $this->belongsTo(OrderLine::class, 'order_line_id');
    }

    public static function generateBarcode($prefix = null)
    {
        do {
            $barcode = $prefix . \Tools::generateReference(11, $prefix ? 'NUMERIC' : 'ALPHANUMERIC');
        } while (self::getByReference($barcode) || FloatingTicket::getByReference($barcode) || FootballTicket::getByReference($barcode));

        return $barcode;
    }

    /*
    * Scope a query to only get order by reference
    *
    * @param string $reference
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public static function getByReference($reference)
    {
        return self::where('hash', '=', $reference)->first();
    }
}
