<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class Promotion extends Model
{

    use Validatorable,
        SoftDeletes;

    const MORPH_CATEGORY = 'App\Models\Deal\Category';

    //const MORPH_CUSTOMER = 'App\Models\Acl\User';

    const MORPH_EVENT = 'App\Models\Deal\Deal';


    public static $morphs = [
        self::MORPH_CATEGORY => 'promotion.category',
        //self::MORPH_CUSTOMER => 'promotion.customer',
        self::MORPH_EVENT => 'promotion.event',
    ];

    const TYPE_PERCENTAGE = 1;

    const TYPE_AMOUNT = 2;

    public static $types = [
        self::TYPE_PERCENTAGE => 'promotion.percentage',
        self::TYPE_AMOUNT => 'promotion.amount',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['name', 'code', 'discount', 'start_at', 'end_at', 'type', 'active', 'typeable_type', 'typeable_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';
        return [
            'name' => 'required|max:70',
            'code' => 'required|max:70|unique:promotions,code' . $exceptId,
            'start_at' => 'required|date_format:"Y-m-d"|after_or_equal:' . date('Y-m-d'),
            'end_at' => 'required|date_format:"Y-m-d"|after:start_at',
            'type' => 'required|integer',
            'typeable_type' => 'required',
            'typeable_id' => 'required|integer',
            'discount' => 'required|numeric|between:0.1,99999.99',
            'active' => 'boolean',
        ];
    }

    public function typeable()
    {
        return $this->morphTo();
    }

}