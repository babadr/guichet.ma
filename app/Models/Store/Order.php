<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{

    use Validatorable,
        SoftDeletes;

    protected $table = 'orders';
    protected $dates = ['payment_transaction_date'];

    const STATUS_WAITING = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_VALIDATED = 3;
    const STATUS_CANCELED = 4;
    const STATUS_CAPTURED = 5;
    const STATUS_EXPIRED = 6;
    const STATUS_REFUSED = 7;
    const STATUS_RESERVED = 8;
    const STATUS_DEFRAUDED = 9;

    protected $fillable = [
        'user_id',
        'seller_id',
        'reference',
        'address',
        'total_paid',
        'status',
        'paid_at',
        'offered',
        'payment_method',
        'comment',
        'payment_transaction_date',
        'offered_by'
    ];

    public function lines()
    {
        return $this->hasMany('App\Models\Store\OrderLine', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Deal\Seller');
    }

    /*
    * Scope a query to only get order by reference
    *
    * @param string $reference
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public function getByReference($reference)
    {
        return $this->where('reference', '=', $reference)->first();
    }


    /**
     * Generate a unique reference for order
     *
     * @return String
     */
    public function generateReference()
    {
        do {
            $reference = \Tools::generateReference();
        } while ($this->getByReference($reference));

        return $reference;
    }

    /**
     * Concat user first and last name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->user ? ucfirst($this->user->first_name) . ' ' . ucfirst($this->user->last_name) : '--';
    }

}
