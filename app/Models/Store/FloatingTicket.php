<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;

class FloatingTicket extends Model
{

    use Validatorable;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'scene_id',
        'hash',
        'day',
        'selected'
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'scene_id' => 'required',
            'hash' => 'required',
            'selected' => 'required|boolean',
            'day' => 'required|string'
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function scene()
    {
        return $this->belongsTo(\App\Models\Deal\Scene::class, 'order_line_id');
    }

    public static function generateBarcode($prefix = null)
    {
        do {
            $barcode = $prefix . \Tools::generateReference(11, 'NUMERIC');
        } while (self::getByReference($barcode) || Ticket::getByReference($barcode));

        return $barcode;
    }

    /*
    * Scope a query to only get order by reference
    *
    * @param string $reference
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public static function getByReference($reference)
    {
        return self::where('hash', '=', $reference)->first();
    }
}
