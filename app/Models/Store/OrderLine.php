<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;

class OrderLine extends Model
{

    protected $table = 'order_lines';

    protected $fillable = ['order_id', 'offer_id', 'reference', 'product_name', 'offer_name', 'product_price', 'quantity', 'total_price', 'seats'];

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Store\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offer()
    {
        return $this->belongsTo('App\Models\Deal\Offer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets()
    {
        return $this->hasMany('App\Models\Store\Ticket', 'order_line_id');
    }

    public function recipients()
    {
        return $this->hasMany('App\Models\Store\Recipient', 'order_line_id');
    }

    public function profiteers()
    {
        return $this->hasMany('App\Models\Store\Profiteer', 'order_line_id');
    }

    /**
     * Generate a unique reference for order
     *
     * @return String
     */
    public function generateReference()
    {
        do {
            $reference = \Tools::generateReference(8, 'NUMERIC');
        } while ($this->getByReference($reference));

        return $reference;
    }

}
