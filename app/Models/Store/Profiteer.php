<?php

namespace App\Models\Store;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;

class Profiteer extends Model
{

    use Validatorable;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'order_line_id',
        'full_name',
        'cin',
        'is_minor'
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'order_line_id' => 'required|integer',
            'full_name' => 'required|max:65',
            'cin' => 'nullable|string|max:20',
            'is_minor' => 'boolean',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function line()
    {
        return $this->belongsTo(OrderLine::class, 'order_line_id');
    }

    public static function checkExist($cin, $deal_id)
    {
        $exist = Profiteer::join('order_lines', 'order_lines.id', '=', 'profiteers.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where('deals.is_football', true)
            ->where('profiteers.cin', $cin)
            ->where('deals.id', $deal_id)
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->where('profiteers.is_minor', false)
            ->first();

        return $exist ? true : false;
    }
}
