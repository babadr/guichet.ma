<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class Newsletter extends Model
{

    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['last_name', 'first_name', 'email', 'phone', 'source', 'birth_date', 'civility'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'last_name' => 'max:65',
            'first_name' => 'max:65',
            'phone' => 'max:14|regex:/^\+?\d+( \d+)*$/',
            'source' => 'numeric|in:1,2,3,4',
            'civility' => 'numeric|in:1,2,3',
            'birth_date' => 'date',
            'email' => 'required|max:255|email|unique:newsletters,email' . $exceptId,
        ];
    }

    /**
     * Concat user first and last name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

}