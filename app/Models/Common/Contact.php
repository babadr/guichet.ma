<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class Contact extends Model
{

    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = ['last_name', 'first_name', 'email', 'phone', 'subject', 'message'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'last_name' => 'required|max:65',
            'first_name' => 'required|max:65',
            'email' => 'required|max:255|email',
            'phone' => 'max:16|regex:/^\+?\d+( \d+)*$/',
            'subject' => 'required|max:255',
            'message' => 'required',
        ];
    }

    /**
     * Concat user first and last name
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return ucfirst($this->first_name) . ' ' . ucfirst($this->last_name);
    }

}