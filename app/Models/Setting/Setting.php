<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;

class Setting extends Model
{

    use Validatorable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    /**
     * Disable Created_at and Updated_at
     *
     * @var boolean
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'value'];

    /**
     * Storage path name
     *
     * @var string
     */
    public $path = 'settings';

    /**
     * Validation rules
     *
     * @return array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'key' => 'required|max:255|key|unique:settings,key' . $exceptId,
        ];
    }

}
