<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property mixed id
 * @property mixed children
 */
class Category extends Model
{
    use Validatorable,
        SoftDeletes;

    /**
     * Dynamic model route
     *
     * @var string
     */
    public $route = 'Frontend\Deal\CategoryController@view';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'title',
        'parent_id',
        'show_on_menu',
        'show_on_home',
        'order',
        'color',
        'icon',
        'active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'show_on_menu' => 'boolean',
            'show_on_home' => 'boolean',
            'active' => 'boolean',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')->orderBy('order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * Get SEO
     */
    public function seo()
    {
        return $this->hasOne('App\Models\Content\Seo', 'model_id')->where('model_class', self::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deals()
    {
        return $this->hasMany("App\Models\Deal\Deal");
    }

    /**
     * get category deals
     *
     * @param null $count
     * @return \Illuminate\Support\Collection
     */
    public function findDeals($count = null, $isSlider = false)
    {
        $ids = $this->children->pluck("id")->merge([$this->id])->all();
        $deals = (new Deal())->whereIn("category_id", $ids)
            ->select('*')
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) > 0 AS date_diff')
            ->where('active', '=', 1)
            ->where('is_private', '=', 0)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->orderBy('show_on_slider', 'desc')
            ->orderBy('order', 'ASC');

        if ($isSlider) {
            $deals->orderBy('created_at', 'DESC');
        } else {
            $deals->orderBy('date_diff', 'DESC')->orderBy('end_at', 'ASC');
        }

        if ($count) {
            $deals->take($count);
        }

        return $deals;
    }

    public function currentEvents()
    {
        $ids = $this->children->pluck("id")->merge([$this->id])->all();
        $deals = (new Deal())->whereIn("category_id", $ids)
            ->select('*')
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) AS date_diff')
            ->where('active', '=', 1)
            ->where('is_private', '=', 0)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->where('end_at', '>=', date('Y-m-d H:i:s'))
            ->orderBy('date_diff', 'ASC')
            //->orderBy('order', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->get();

        return $deals;
    }

    public function pastEvents()
    {
        $ids = $this->children->pluck("id")->merge([$this->id])->all();
        $deals = (new Deal())->whereIn("category_id", $ids)
            ->select('*')
            ->where('active', '=', 1)
            ->where('is_private', '=', 0)
            ->where('end_at', '<=', date('Y-m-d H:i:s'))
            ->orderBy('created_at', 'DESC');

        return $deals;
    }

    /*
    * Scope a query to only include active categories.
    *
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @return \Illuminate\Database\Eloquent\Builder
    */
    public function scopeCategories($query)
    {
        return $query->where('active', true)->whereNull('parent_id')->orderBy('order', 'asc');
    }

    /**
     * check if category is ticketing
     *
     * @return bool
     */
    public function isTicketing()
    {
        return $this->id == 4 || $this->parent_id == 4;
    }

    public function getFrontDeals($limit = 6)
    {
        $ids = $this->children->pluck("id")->merge([$this->id])->all();
        $deals = (new Deal())->whereIn("category_id", $ids)
            ->select('*')
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) AS date_diff')
            ->where('active', '=', 1)
            ->where('expired', '=', 0)
            ->where('is_private', '=', 0)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->orderBy('date_diff', 'ASC')
            //->orderBy('order', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->take($limit)->get();

        return $deals;
    }
}
