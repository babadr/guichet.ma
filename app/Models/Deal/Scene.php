<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Validatorable;

class Scene extends Model
{
    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'name', 'code', 'export_code', 'quantity'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';

        return [
            'code' => 'required|max:5|unique:scenes,code' . $exceptId,
            'export_code' => 'required|max:5|unique:scenes,export_code' . $exceptId,
            'name' => 'required|max:70',
            'quantity' => 'required|integer',
        ];
    }

    public function tickets()
    {
        return $this->hasMany(\App\Models\Store\FloatingTicket::class, 'scene_id');
    }
}
