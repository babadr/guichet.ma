<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Store\Order;

class Provider extends Model
{
    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'user_id',
        'title',
        'description',
        'address',
        'email',
        'phone',
        'fax',
        'logo',
        'active',
        'secure_code',
        'commission'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Storage path name
     *
     * @var string
     */
    public $path = 'providers';

    /**
     * Dynamic model route
     *
     * @var string
     */
    public $route = 'Frontend\Deal\ProviderController@view';

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        $exceptId = $this->id ? ',' . $this->id : '';
        $exceptUser = $this->user_id ? ',' . $this->user_id : '';
        $passwordRequired = !$this->user ? '|required|min:8|' : '';
        return [
            'title' => 'required|max:255',
            'secure_code' => 'required|max:30|unique:providers,secure_code' . $exceptId,
            'active' => 'boolean',
            'logo' => 'image|mimes:jpeg,png,jpg,gif|max:1024',
            'email' => 'required|max:255|email|unique:users,email' . $exceptUser,
            'last_name' => 'required|max:65',
            'first_name' => 'required|max:65',
            'phone' => 'required|max:14|regex:/^\+?\d+( \d+)*$/',
            'address' => 'max:255',
            'city' => 'max:64',
            'password' => 'confirmed' . $passwordRequired,
            'password_confirmation' => 'same:password' . $passwordRequired,
            'commission' => 'numeric',
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }

    public function deals()
    {
        return $this->hasMany('App\Models\Deal\Deal');
    }

    public function sellers()
    {
        return $this->hasMany('App\Models\Deal\Seller');
    }

    public function getTotalAmount($event_id = null)
    {
        $orderedAmount = 0;
        $deals = $this->deals()->get();
        if ($event_id) {
            if (is_array($event_id)) {
                $deals = $deals->whereIn('id', $event_id);
            } else {
                $deals = $deals->where('id', $event_id);
            }

        }
        foreach ($deals as $deal) {
            $orderedAmount += $deal->getTotalOrderedAmount(false) + $deal->getTotalOrderedAmountOffline(false);
        }

        return $orderedAmount;
    }

    public function getOrderedQuantity($event_id = null)
    {
        $orderedQuantity = 0;
        $deals = $this->deals()->get();
        if ($event_id) {
            if (is_array($event_id)) {
                $deals = $deals->whereIn('id', $event_id);
            } else {
                $deals = $deals->where('id', $event_id);
            }
        }
        foreach ($deals as $deal) {
            $orderedQuantity += $deal->getOrderedQuantity(false);
        }

        return $orderedQuantity;
    }

    public function getTickets($event_id = null)
    {
        $tickets = $this->select(['tickets.id', 'tickets.status', 'tickets.number', 'order_lines.product_price'])
            ->join('deals', 'deals.provider_id', '=', 'providers.id')
            ->join('offers', 'offers.deal_id', '=', 'deals.id')
            ->join('order_lines', 'order_lines.offer_id', '=', 'offers.id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('tickets', 'tickets.order_line_id', '=', 'order_lines.id');

        if ($event_id) {
            $tickets->where('offers.deal_id', $event_id);
        } else {
            $tickets->where('deals.provider_id', $this->id);
        }

        $tickets->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->orderBy('tickets.id', 'ASC')
            ->get();

        return $tickets;
    }
}
