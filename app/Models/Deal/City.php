<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'city',
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'city'  => 'required|max:255',
        ];
    }
}
