<?php

namespace App\Models\Deal;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Store\Order;
use App\Models\Store\Ticket;
use App\Models\Store\OrderLine;

/**
 * @property mixed offers
 * @property mixed price
 * @property mixed old_price
 * @property mixed medias
 * @property mixed saving
 * @property mixed quantity
 */
class Deal extends Model
{
    use Validatorable,
        SoftDeletes;

    /**
     * Storage path name
     *
     * @var string
     */
    public $path = 'medias';

    /**
     * Dynamic model route
     *
     * @var string
     */
    public $route = 'Frontend\Deal\DealController@view';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'title',
        'type',
        'category_id',
        'provider_id',
        'short_description',
        'description',
        'address',
        'latitude',
        'longitude',
        'start_at',
        'end_at',
        'quantity',
        'bought',
        'city_id',
        'show_on_slider',
        'active',
        'opening',
        'start_time',
        'custom_field',
        'custom_ticket',
        'commission',
        'order',
        'seating_plan_id',
        'reserved_seats',
        'offline_commission',
        'expired',
        'barcode_type',
        'barcode_prefix',
        'offered_price',
        'is_football',
        'is_private',
        'private_token'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'start_at', 'end_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'category_id' => 'required',
            'provider_id' => 'required',
            'show_on_slider' => 'boolean',
            'active' => 'boolean',
            'barcode_type' => 'boolean',
            'expired' => 'boolean',
            'opening' => 'date_format:H:i',
            'start_time' => 'date_format:H:i',
            'custom_ticket' => 'image|mimes:jpeg,png,jpg,gif|max:1024',
            'commission' => 'numeric|required',
            'offline_commission' => 'numeric|required',
            'offered_price' => 'numeric|required',
            'is_football' => 'boolean',
            'is_private' => 'boolean',
        ];
    }

    /**
     * date start accessor
     *
     * @param $timestamp
     * @return mixed
     */
    public function getOpeningAttribute($timestamp)
    {
        return Carbon::parse($timestamp)->format('H:i');
    }

    /**
     * date end accessor
     *
     * @param $timestamp
     * @return mixed
     */
    public function getStartTimeAttribute($timestamp)
    {
        return Carbon::parse($timestamp)->format('H:i');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offers()
    {
        return $this->hasMany('App\Models\Deal\Offer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function provider()
    {
        return $this->belongsTo('App\Models\Deal\Provider');
    }

    /**
     * return default offer, or first
     * @return mixed
     */
    public function defaultOffer()
    {
        $default = $this->offers->where('default', 1)->where('active', 1)->first();
        if ($default) {
            return $default;
        }

        return $this->offers->where('active', 1)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medias()
    {
        return $this->hasMany('App\Models\Deal\DealMedia');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Deal\Category');
    }

    /**
     * Get SEO
     */
    public function seo()
    {
        return $this->hasOne('App\Models\Content\Seo', 'model_id')->where('model_class', self::class);
    }

    public function city()
    {
        return $this->belongsTo('App\Models\Deal\City');
    }

    /**
     * Get first offer price.
     *
     * @return string
     */
    public function getPriceAttribute()
    {
        return $this->defaultOffer() ? $this->defaultOffer()->price : 0;
    }

    /**
     * Get first offer old price.
     *
     * @return string
     */
    public function getOldPriceAttribute()
    {
        return $this->defaultOffer() ? $this->defaultOffer()->old_price : 0;
    }

    /**
     * Get first offer saving price.
     *
     * @return string
     */
    public function getSavingAttribute()
    {
        $saving = ($this->old_price > 0) ? $this->old_price - $this->price : 0;
        return number_format($saving, 0);
    }

    /**
     * Get first offer discount price.
     *
     * @return string
     */
    public function getDiscountAttribute()
    {
        $saving = ($this->old_price > 0) ? $this->old_price - $this->price : 0;
        $discount = ($saving > 0) ? $saving * 100 / $this->old_price : 0;
        return number_format($discount, 0);
    }

    /**
     * Get deal cover image.
     *
     * @return string
     */
    public function getCoverAttribute()
    {
        $media = $this->medias->where('is_cover', 1)->first();
        //return $media ? '/imagecache/slide/' . $media->filename : 'http://placehold.it/900x580';
        return $media ? $media->filename : 'http://placehold.it/900x580';
    }

    /**
     * Get deal miniature image.
     *
     * @return string
     */
    public function getMiniatureAttribute()
    {
        $media = $this->medias->where('is_miniature', 1)->first();
        if (!$media) {
            return str_replace('slide', 'small', $this->cover);
        }
        //return $media ? '/imagecache/small/' . $media->filename : 'http://placehold.it/480x240';
        return $media ?  $media->filename : 'http://placehold.it/480x240';
    }

    public function getCurrentDeals($start_at, $end_at)
    {
        return $this->where('active', '=', 1)
            //->where('start_at', '<=', date('Y-m-d H:i:s'))
            //->where('end_at', '>=', date('Y-m-d H:i:s'))
            ->where(function ($query) use ($start_at, $end_at) {
                $query->whereBetween('start_at', [$start_at, $end_at])
                    ->orWhereBetween('end_at', [$start_at, $end_at]);
            })
            //->whereRaw('DATE(end_at) >= ?', [$start_at])
            //->whereRaw('DATE(end_at) <= ?', [$end_at])
            ->orderBy('order', 'asc')
            ->orderBy('created_at', 'desc')
            ->get();
    }

    /**
     * get deals to show on slider
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getLastSliderDeals()
    {
        return $this->where('show_on_slider', '=', 1)
            ->with(['seo', 'category', 'city'])
            ->where('active', '=', 1)
            ->where('expired', '=', 0)
            ->where('is_private', '=', 0)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            //->where('end_at', '>=', date('Y-m-d H:i:s'))
            //->orderBy('show_on_slider', 'desc')
            ->orderBy('order', 'asc')
            ->orderBy('created_at', 'desc')
            ->limit(12)
            ->get();
    }

    /**
     * get last three active deals by category
     *
     * @param int $category_id
     * @param int $count
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getLastDealsByCategory($category_id, $count = null)
    {
        return $this->where('category_id', '=', $category_id)
            ->where('active', '=', 1)
            ->where('is_private', '=', 0)
            ->orderBy('id', 'desc')
            ->take($count)
            ->get();
    }

    public function getRealBought()
    {
        return OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.deal_id', $this->id)
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED])
            ->count();

//        $orderedQuantity = 0;
//        $offers = $this->offers()->get();
//        foreach ($offers as $offer) {
//            $total = $offer->lines()->get()->filter(function ($line) {
//                return in_array($line->order->status, [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED]);
//            })->count();
//            $orderedQuantity += $total;
//        }
//
//        return $this->bought + $orderedQuantity;
    }

    public function getRealBoughtOffline($seller_id = null)
    {
        $lines = OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.deal_id', $this->id)
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED]);

        if ($seller_id) {
            $lines->where('orders.seller_id', $seller_id);
        } else {
            $lines->whereNotNull('orders.seller_id');
        }

        return $lines->count();

//        $orderedQuantity = 0;
//        $offers = $this->offers()->get();
//        foreach ($offers as $offer) {
//            $total = $offer->lines()->get()->filter(function ($line) use ($seller_id) {
//                return in_array($line->order->status, [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED]) && ($seller_id ? $line->order->seller_id == $seller_id : !empty($line->order->seller_id));
//            })->count();
//            $orderedQuantity += $total;
//        }
//
//        return $this->bought + $orderedQuantity;
    }

    public function getOfferedTickets()
    {
        return OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.deal_id', $this->id)
            ->where('offered', 1)
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->sum('order_lines.quantity');
    }

    public function getOrderedQuantity($all = true)
    {
        $status = $all ? [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED] : [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED];

        return OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.deal_id', $this->id)
            ->whereIn('orders.status', $status)
            ->sum('order_lines.quantity');

//        $orderedQuantity = 0;
//        $offers = $this->offers()->get();
//        foreach ($offers as $offer) {
//            $orderedQuantity += $offer->getOrderedQuantity($all);
//        }
//
//        return $this->bought + $orderedQuantity;
    }

    public function getOrderedQuantityOffline($all = true, $seller_id = null)
    {
        $status = $all ? [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED] : [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED];

        $lines = OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.deal_id', $this->id)
            ->whereIn('orders.status', $status);

        if ($seller_id) {
            $lines->where('orders.seller_id', $seller_id);
        } else {
            $lines->whereNotNull('orders.seller_id');
        }

        return $lines->sum('order_lines.quantity');
//        $orderedQuantity = 0;
//        $offers = $this->offers()->get();
//        foreach ($offers as $offer) {
//            $orderedQuantity += $offer->getOrderedQuantityOffline($all, $seller_id);
//        }
//
//        return $this->bought + $orderedQuantity;
    }

    public function checkAvailability()
    {
        $offers = $this->offers()->get();
        foreach ($offers as $offer) {
            if ($offer->quantity > $offer->getOrderedQuantity()) {
                return true;
            }
        }

        return false;
    }

    public function getTotalOrderedAmount($all = true)
    {
        $status = $all ? [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED] : [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED];

        return OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.deal_id', $this->id)
            ->whereIn('orders.status', $status)
            ->whereNull('orders.seller_id')
            ->sum('order_lines.total_price');

//        $orderedAmount = 0;
//        $offers = $this->offers()->get();
//        foreach ($offers as $offer) {
//            $line = $offer->lines()->get()->filter(function ($line) use ($status) {
//                return in_array($line->order->status, $status) && empty($line->order->seller_id);
//            })->sum('total_price');
//            $orderedAmount += $line;
//        }
//
//        return $orderedAmount;
    }

    public function getTotalOrderedAmountOffline($all = true, $seller_id = null)
    {
        $status = $all ? [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED] : [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED];

        $lines = OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.deal_id', $this->id)
            ->whereIn('orders.status', $status);

        if ($seller_id) {
            $lines->where('orders.seller_id', $seller_id);
        } else {
            $lines->whereNotNull('orders.seller_id');
        }

        return $lines->sum('order_lines.total_price');

//        $orderedAmount = 0;
//        $offers = $this->offers()->get();
//        foreach ($offers as $offer) {
//            $line = $offer->lines()->get()->filter(function ($line) use ($status, $seller_id) {
//                return in_array($line->order->status, $status) && ($seller_id ? $line->order->seller_id == $seller_id : !empty($line->order->seller_id));
//            })->sum('total_price');
//            $orderedAmount += $line;
//        }
//
//        return $orderedAmount;
    }

    public function getTotalOfferedCommission($all = true)
    {
        $status = $all ? [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED] : [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED];

        $totalCommission = (new OrderLine())
            ->selectRaw('(order_lines.quantity * deals.offered_price) AS total_commission')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where('deals.id', $this->id)
            ->where('orders.offered', 1)
            ->whereIn('orders.status', $status)
            ->get()->sum('total_commission');

        return $totalCommission;
    }

    /**
     * Get provider name
     *
     * @return string
     */
    public function getCategoryNameAttribute()
    {
        return $this->category->title;
    }

    /**
     * Get category name
     *
     * @return string
     */
    public function getProviderNameAttribute()
    {
        return $this->provider->title;
    }

    public function getReservedSeats($onlyOrdered = false)
    {
        $offers = $this->offers()->where('active', true)->orderBy('id')->get()->pluck('id');

        $tickets = Ticket::select('tickets.id', 'tickets.seat')
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->whereIn('order_lines.offer_id', $offers)
            ->whereNotNull('tickets.seat')
            ->get()->pluck('seat')->toArray();

        if ($onlyOrdered) {
            return $tickets;
        } else {
            $seats = array_merge($tickets, explode(',', $this->reserved_seats));
        }

        return $seats;
    }

    public function getAssociatedEvents()
    {
        return $this->select('id', 'title', 'end_at')
            ->where('active', '=', 1)
            ->where('expired', '=', 0)
            ->where('id', '<>', $this->id)
            ->where('category_id', '=', $this->category_id)
            ->orderByRaw('(provider_id != ' . $this->provider_id . ') asc')
            ->orderBy('order', 'asc')
            ->orderBy('created_at', 'desc')
            ->limit(3)
            ->get();
    }

}
