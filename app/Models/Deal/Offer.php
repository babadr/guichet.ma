<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Store\Order;
use App\Models\Store\OrderLine;
use App\Models\Store\Ticket;

class Offer extends Model
{
    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'title',
        'description',
        'price',
        'old_price',
        'reservation_price',
        'quantity',
        'quantity_max',
        'deal_id',
        'default',
        'active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'price' => 'required',
            'deal_id' => 'required',
            'active' => 'boolean',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deal()
    {
        return $this->belongsTo('App\Models\Deal\Deal');
    }

    public function lines()
    {
        return $this->hasMany('App\Models\Store\OrderLine', 'offer_id');
    }

    public function checkMaxQuantity($quantity)
    {
        if (empty($this->quantity_max)) {
            return true;
        }

        return $quantity <= $this->quantity_max;
    }

    public function checkAvailability($quantity)
    {
        $orderedQuantity = $this->getOrderedQuantity();

        return $quantity <= ($this->quantity - $orderedQuantity);
    }

    public function getOrderedQuantity($all = true)
    {
        $status = $all ? [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED] : [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED];

        return OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.id', $this->id)
            ->whereIn('orders.status', $status)
            ->sum('order_lines.quantity');

//        return $this->lines()->get()->filter(function ($line) use ($status) {
//            return in_array($line->order->status, $status);
//        })->sum('quantity');
    }

    public function getOrderedQuantityOffline($all = true, $seller_id = null)
    {
        $status = $all ? [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED, Order::STATUS_RESERVED] : [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED];

        $lines = OrderLine::join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where('offers.id', $this->id)
            ->whereIn('orders.status', $status);

        if ($seller_id) {
            $lines->where('orders.seller_id', $seller_id);
        } else {
            $lines->whereNotNull('orders.seller_id');
        }

        return $lines->sum('order_lines.quantity');

//        return $this->lines()->get()->filter(function ($line) use ($status, $seller_id) {
//            return in_array($line->order->status, $status) && ($seller_id ? $line->order->seller_id == $seller_id : !empty($line->order->seller_id));
//        })->sum('quantity');
    }

    public function getReservedSeats()
    {
        $seats = Ticket::select('tickets.id', 'tickets.seat')
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->where('order_lines.offer_id', $this->id)
            ->whereNotNull('tickets.seat')
            ->get()->pluck('seat')->toArray();

        return $seats;
    }
}
