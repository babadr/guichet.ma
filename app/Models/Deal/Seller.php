<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Store\Order;

class Seller extends Model
{
    use Validatorable,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'user_id',
        'provider_id',
        'title',
        'address',
        'active',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        $exceptUser = $this->user_id ? ',' . $this->user_id : '';
        $passwordRequired = !$this->user ? '|required|min:8|' : '';
        return [
            'title' => 'required|max:255',
            'active' => 'boolean',
            'email' => 'required|max:255|email|unique:users,email' . $exceptUser,
            'last_name' => 'required|max:65',
            'first_name' => 'required|max:65',
            'phone' => 'required|max:14|regex:/^\+?\d+( \d+)*$/',
            'address' => 'max:255',
            'city' => 'max:64',
            'password' => 'confirmed' . $passwordRequired,
            'password_confirmation' => 'same:password' . $passwordRequired,
        ];
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Deal\Provider');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Store\Order');
    }

    /**
     * Get category name
     *
     * @return string
     */
    public function getProviderNameAttribute()
    {
        return $this->provider->title;
    }
}
