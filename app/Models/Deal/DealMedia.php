<?php

namespace App\Models\Deal;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Validatorable;

class DealMedia extends Model
{
    use Validatorable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'deal_medias';

    /**
     * The attributes that are mass assignable.
     *
     * @var  array
     */
    protected $fillable = [
        'filename',
        'is_cover',
        'is_miniature',
        'deal_id'
    ];

    /**
     * Validation rules
     *
     * @return  array
     */
    public function rules()
    {
        return [
            'filename'  => 'required|max:255',
            'deal_id'  => 'required',
        ];
    }
}
