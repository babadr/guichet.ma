<?php

namespace App\Helpers;

use App\Models\Deal\Offer;
use App\Models\Store\FootballTicket;

class Tools
{


    public function checkRecipient($order)
    {
        foreach ($order->lines as $line) {
            if ($line->offer->deal->category_id == 17 && !$line->recipients->count()) {
                return true;
            }
        }

        return false;
    }

    public function checkFootball($offer_id)
    {
        $offer = Offer::find($offer_id);
        if ($offer) {
            return $offer->deal->is_football;
        }

        return false;
    }

    public function getTicket($offer_id, $hash)
    {
        $ticket = FootballTicket::where('offer_id', $offer_id)->where('hash', $hash)->first();
        if ($ticket) {
            return $ticket->toArray();
        }

        return [];
    }

    /**
     * Permits to include some routes files using the parameters
     *
     * @param String $dir The path or the dir to the routes directory or the route file to include
     * @param Boolean $recursive : if true try to read the sub directories and try to include routes files.
     * @return void
     */
    public function includeRoutes($dir = null, $recursive = false)
    {
        $path = base_path('routes') . ($dir ? '/' . $dir : '');
        if (is_dir($path) && $handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    $filename = $path . '/' . $entry;
                    if (is_file($filename) && Tools::isCorrectFileExt($entry, array('php'))) {
                        include_once($filename);
                    } elseif ($recursive) {
                        Tools::includeRoutes(($dir ? '/' . $dir . '/' : '') . $entry, $recursive);
                    }
                }
            }
            closedir($handle);
        } elseif (is_file($path) && file_exists($path) && Tools::isCorrectFileExt($path, array('php'))) {
            include_once($path);
        }
    }

    /**
     * Convert CSV file to array
     *
     * @param string $filename
     * @param string $delimiter
     * @return array|boolean
     */
    public function csvToArray($filename, $delimiter = ';')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * Check if file extension is correct
     *
     * @param string $filename Real filename
     * @param array $authorized_extensions
     * @return bool True if it's correct
     */
    public function isCorrectFileExt($filename, $authorized_extensions = null)
    {
        if ($authorized_extensions === null) {
            $authorized_extensions = array('gif', 'jpg', 'jpeg', 'png');
        }

        $name_explode = explode('.', $filename);

        $count = count($name_explode);
        if ($count >= 2) {
            $current_extension = strtolower($name_explode[$count - 1]);
            if (in_array($current_extension, $authorized_extensions)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Add active class to current menu page
     *
     * @param array $paths
     * @param array $options
     * @return string
     */
    public function activeMenu($paths, $options = [])
    {
        foreach ($paths as $path) {
            if (request()->is($path)) {
                return 'active' . implode(' ', $options);
            }
        }

        return '';
    }

    /**
     * Transform a string in the CamelCase to under_scored_case
     *
     * @param string $string
     * @return string
     */
    function camelCaseToUnderscoredCase($string)
    {
        return trim(strtolower(preg_replace('/[A-Z]/', '_$0', $string)), '_');
    }

    /**
     * Generate a unique reference for cart
     * This references, is useful for check payment
     *
     * @param int $length
     * @param string $flag
     *
     * @return String
     */
    public function generateReference($length = 9, $flag = 'ALPHANUMERIC')
    {
        if ((int)$length <= 0) {
            $length = 9;
        }

        switch ($flag) {
            case 'NUMERIC':
                $str = '0123456789';
                break;
            case 'NO_NUMERIC':
                $str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
            case 'RANDOM':
                $num_bytes = ceil($length * 0.75);
                $bytes = $this->getBytes($num_bytes);

                return substr(rtrim(base64_encode($bytes), '='), 0, $length);
            case 'ALPHANUMERIC':
            default:
                $str = 'abcdefghijkmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                break;
        }

        $bytes = $this->getBytes($length);
        $position = 0;
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $position = ($position + ord($bytes[$i])) % strlen($str);
            $result .= $str[$position];
        }

        return strtoupper($result);
    }

    /**
     * Random bytes generator
     *
     * @param int $length Desired length of random bytes
     *
     * @return bool|string Random bytes
     */
    protected function getBytes($length)
    {
        if ((int)$length <= 0) {
            $length = 9;
        }

        if (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length, $crypto_strong);
            if ($crypto_strong === true) {
                return $bytes;
            }
        }

        if (function_exists('mcrypt_create_iv')) {
            $bytes = mcrypt_create_iv($length, MCRYPT_DEV_URANDOM);
            if ($bytes !== false && strlen($bytes) === $length) {
                return $bytes;
            }
        }

        $result = '';
        $entropy = '';
        $msec_per_round = 400;
        $bits_per_round = 2;
        $total = $length;
        $hash_length = 20;

        while (strlen($result) < $length) {
            $bytes = ($total > $hash_length) ? $hash_length : $total;
            $total -= $bytes;

            for ($i = 1; $i < 3; $i++) {
                $t1 = microtime(true);
                $seed = mt_rand();

                for ($j = 1; $j < 50; $j++) {
                    $seed = sha1($seed);
                }

                $t2 = microtime(true);
                $entropy .= $t1 . $t2;
            }

            $div = (int)(($t2 - $t1) * 1000000);

            if ($div <= 0) {
                $div = 400;
            }

            $rounds = (int)($msec_per_round * 50 / $div);
            $iter = $bytes * (int)(ceil(8 / $bits_per_round));

            for ($i = 0; $i < $iter; $i++) {
                $t1 = microtime();
                $seed = sha1(mt_rand());

                for ($j = 0; $j < $rounds; $j++) {
                    $seed = sha1($seed);
                }

                $t2 = microtime();
                $entropy .= $t1 . $t2;
            }

            $result .= sha1($entropy, true);
        }

        return substr($result, 0, $length);
    }

    /**
     * Convert price to french price format
     *
     * @param float $price
     * @param int $after
     * @return string
     */
    public function displayPrice($price, $after = 2)
    {
        return sprintf('%s DH', number_format($price, $after, ',', ' '));
    }

    /**
     * Convert reduction to french reduction format
     *
     * @param float $reduction
     * @param int $after
     * @return string
     */
    public function displayReduction($reduction, $after = 2)
    {
        return sprintf('%s', number_format($reduction, $after, ',', '')) . '%';
    }

}
