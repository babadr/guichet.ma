<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LinkMedia extends Command
{
    /**
     * The console command signature.
     *
     * @var string
     */
    protected $signature = 'media:link';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a symbolic link from "public/medias" to "storage/app/medias"';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        if (file_exists(public_path('medias'))) {
            return $this->error('The "public/medias" directory already exists.');
        }

        $this->laravel->make('files')->link(
            storage_path('app/medias'), public_path('medias')
        );

        $this->info('The [public/medias] directory has been linked.');
    }
}
