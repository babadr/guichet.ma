<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ClearDump extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dump:clear {--local}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete older database dumps';


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $isLocal = $this->option('local');
        $dumps = Storage::disk($isLocal ? 'local' : 'backups')->allFiles('backups');
        foreach ($dumps as $key => $dump) {
            $file = new File(storage_path('app/' . $dump));
            $createdAt = date('Y-m-d H:i:s', $file->getCTime());
            $currentDate = date("Y-m-d H:i:s", strtotime("-5 days"));
            if ($createdAt < $currentDate) {
                $this->info($file->getFilename());
                Storage::delete($dump);
            }
        }

        $this->info('*********** end ***********');
    }


}
