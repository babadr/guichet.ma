<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Store\Order;
use Carbon\Carbon;

class CheckOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check reserved orders to update status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = (new Order())->where('status', Order::STATUS_RESERVED)->orderBy('created_at')->get();
        foreach ($orders as $order) {
            $start = Carbon::parse($order->created_at);
            $end = Carbon::now();
            $hours = $end->diffInHours($start);
            if ($hours >= 24) {
                $order->status = Order::STATUS_EXPIRED;
                $order->save();
            }
        }
    }

}
