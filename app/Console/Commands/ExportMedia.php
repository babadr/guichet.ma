<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ExportMedia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:media {--last}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all media to S3';


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $last = $this->option('last');
        $directories = [
            'avatars' => ['original', 'thumb'],
            'medias' => ['original', 'slide', 'small', 'big', 'thumb'],
            'posts' => ['original', 'post'],
            'providers' => ['original', 'partner'],
            'settings' => ['original']
        ];
        foreach ($directories as $directory => $templates) {
            $images = Storage::allFiles($directory);
            foreach ($images as $key => $image) {
                $file = new File(storage_path('app/' . $image));
                if (!\Tools::isCorrectFileExt($file->getFilename())) {
                    continue;
                }
                if ($last) {
                    $createdAt = date('Y-m-d H:i:s', $file->getCTime());
                    $currentDate = date("Y-m-d H:i:s", strtotime("-5 minutes"));
                    if ($createdAt < $currentDate) {
                        continue;
                    }
                }
                $path = $this->getImagePath($directory . DIRECTORY_SEPARATOR . $file->getFilename());
                foreach ($templates as $template) {
                    $imageFile = \Image::make($path);
                    switch ($template) {
                        case 'slide':
                            $this->resize($imageFile, 900, 580);
                            break;
                        case 'small':
                            $this->resize($imageFile, 480, 240);
                            break;
                        case 'big':
                            $this->resize($imageFile, 900, 600);
                            break;
                        case 'partner':
                            $this->resize($imageFile, 200, 150);
                            break;
                        case 'post':
                            $this->resize($imageFile, 845, 475);
                            break;
                        case 'thumb':
                            $imageFile = $imageFile->fit(150, 90);
                            break;
                        case 'original':
                            break;
                    }
                    Storage::disk('s3')->put('imagecache' . DIRECTORY_SEPARATOR . $template . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $file->getFilename(), $imageFile->stream()->__toString());
                }
                $this->info($directory . '/' . $file->getFilename());
            }
        }

        $this->info('*********** end ***********');
    }

    /*
     * Returns full image path from given filename
     *
     * @param  string $filename
     * @return string
     */
    private function getImagePath($filename)
    {
        // find file
        foreach (config('imagecache.paths') as $path) {
            // don't allow '..' in filenames
            $image_path = $path . '/' . str_replace('..', '', $filename);
            if (file_exists($image_path) && is_file($image_path)) {
                // file found
                return $image_path;
            }
        }
    }

    /*
    * Resize image
    *
    * @param $image
    * @param int $w
    * @param int $h
     *
    * @return void
    */
    private function resize(&$imageFile, $w, $h)
    {
        $imageFile->resize($w, $h, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }


}
