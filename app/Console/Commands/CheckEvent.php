<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Deal\Deal;
use Carbon\Carbon;

class CheckEvent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'event:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if events are expired';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Deal::where('expired', 0)
            ->where('end_at', '<', date('Y-m-d H:i:s'))
            ->orderBy('created_at')->update(['expired' => 1]);

        $this->info("End process");
    }

}
