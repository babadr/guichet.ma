<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        '\App\Console\Commands\CheckOrder',
        '\App\Console\Commands\CheckEvent',
        //'\App\Console\Commands\MysqlDump',
        //'\App\Console\Commands\MysqlRestore',
        //'\App\Console\Commands\ClearDump',
        //'\App\Console\Commands\LinkMedia',
        //'\App\Console\Commands\ExportMedia',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('order:check')->everyTenMinutes()->withoutOverlapping();
        $schedule->command('event:check')->everyThirtyMinutes()->withoutOverlapping();
        //$schedule->command('backup:mysql-dump --compress')->twiceDaily(1, 13)->emailOutputTo('w.alaeddine@gmail.com');
        //$schedule->command('dump:clear')->daily()->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
