<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Common\Contact;
use App\Services\MailService;

class ContactController extends Controller
{

    /**
     * @var MailService
     */
    protected $mailService;

    /**
     * Create a new controller instance.
     *
     * @param MailService $mailService
     *
     */
    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * API Create
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $contact = new Contact();
        $attributes = $request->only('last_name', 'first_name', 'email', 'phone', 'subject', 'message');
        $validator = $contact->validator($attributes);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()->first()], Response::HTTP_BAD_REQUEST);
        }

        $contact->fill($attributes);
        if ($contact->save()) {
            $this->mailService->sendContactRequest($contact);
        }

        return response()->json(['success' => true, 'message' => trans('contact.contact_success')], Response::HTTP_OK);
    }

}
