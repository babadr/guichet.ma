<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Deal\Deal;

class HomeController extends Controller
{

    /**
     * GET Home Slides
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function slides()
    {
        $events = Deal::where('show_on_slider', '=', 1)
            ->with(['category', 'city'])
            ->where('active', '=', 1)
            ->where('expired', '=', 0)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->orderBy('order', 'asc')
            ->orderBy('created_at', 'desc')
            ->limit(12)
            ->get();

        $data = [];

        foreach ($events as $key => $event) {
            $data[$key]['id'] = $event->id;
            $data[$key]['title'] = $event->title;
            $data[$key]['city'] = $event->city->city;
            $data[$key]['price'] = \Tools::displayPrice($event->price, 0);
            $data[$key]['discount'] = \Tools::displayReduction($event->discount, 0);
            $data[$key]['sold_out'] = $event->end_at->isPast() || !$event->checkAvailability();
            $data[$key]['end_at'] = $event->end_at->format('Y-m-d H:i:s');
            $data[$key]['image'] = asset_cdn($event->cover) . '?w=900&h=580&fit=clip&auto=format,compress&q=80';
            $data[$key]['category']['id'] = $event->category->id;
            $data[$key]['category']['name'] = $event->category->title;
        }

        return response()->json(['success' => true, 'events' => $data], Response::HTTP_OK);
    }

    /**
     * Search Events
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $key = $request->get('q', null);

        $events = (new Deal())->select('*')
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) AS date_diff')
            ->where('active', '=', 1)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->where('end_at', '>=', date('Y-m-d H:i:s'))
            ->orderBy('date_diff', 'ASC')
            ->orderBy('created_at', 'DESC');

        if ($key) {
            $events = $events->where(function ($query) use ($key) {
                $query->where('title', 'like', '%' . $key . '%')
                    ->orWhere('short_description', 'like', '%' . $key . '%')
                    ->orWhere('description', 'like', '%' . $key . '%');
            });
        }

        $events = $events->paginate(10);

        $data = [
            'total' => $events->total(),
            'lastPage' => $events->lastPage(),
            'perPage' => $events->perPage(),
            'currentPage' => $events->currentPage(),
            'nextPage' => $events->nextPageUrl(),
            'events' => []
        ];

        foreach ($events->items() as $key => $event) {
            $data['events'][$key]['id'] = $event->id;
            $data['events'][$key]['title'] = $event->title;
            $data['events'][$key]['city'] = $event->city->city;
            $data['events'][$key]['price'] = \Tools::displayPrice($event->price, 0);
            $data['events'][$key]['discount'] = \Tools::displayReduction($event->discount, 0);
            $data['events'][$key]['sold_out'] = $event->end_at->isPast() || !$event->checkAvailability();
            $data['events'][$key]['end_at'] = $event->end_at->format('Y-m-d H:i:s');
            $data['events'][$key]['image'] = asset_cdn($event->cover) . '?w=340&h=200&fit=clip&auto=format,compress&q=80';
            $data['events'][$key]['category']['id'] = $event->category->id;
            $data['events'][$key]['category']['name'] = $event->category->title;
        }

        return response()->json(['success' => true, 'events' => $data], Response::HTTP_OK);
    }

}
