<?php

namespace App\Http\Controllers\Api\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Acl\User;
use JWTAuth;
use Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Services\MailService;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{

    /**
     * @var MailService
     */
    protected $mailService;

    /**
     * Create a new controller instance.
     *
     * @param MailService $mailService
     *
     */
    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $credentials = $request->only('first_name', 'last_name', 'email', 'phone', 'password', 'password_confirmation');
        if (empty($data['password_confirmation'])) {
            $credentials['password_confirmation'] = $credentials['password'];
        }
        $credentials['active'] = 1;

        $user = new User();
        $validator = $user->validator($credentials);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()->first()], Response::HTTP_BAD_REQUEST);
        }

        $user->fill($credentials);
        if ($user->save()) {
            $user->attachRole(User::CUSTOMER_ID);
            $token = JWTAuth::attempt($credentials);
            $this->mailService->sendUserWelcome($user);
        }

        return response()->json(['success' => true, 'user' => $this->getCurrentUser(), 'token' => $token, 'message' => 'Merci pour votre inscription ! Veuillez vérifier votre email pour compléter votre inscription.'], Response::HTTP_OK);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(['success' => true, 'user' => $this->getCurrentUser()], Response::HTTP_OK);
    }

    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $attributes = $request->all();

        $credentials = $request->only('email', 'password');

        $rules = [
            'email' => 'required|email',
            'password' => 'required',
            'role' => 'nullable'
        ];

        $validator = Validator::make($attributes, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()->first()], Response::HTTP_BAD_REQUEST);
        }

        $credentials['active'] = 1;

        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'error' => trans('auth.failed')], Response::HTTP_NOT_FOUND);
            }
            if (isset($attributes['role']) && $attributes['role'] === 'validator') {
                if (!auth()->user()->hasRole(['validator'])) {
                    auth()->logout();
                    JWTAuth::invalidate($request->input('token'));

                    return response()->json(['success' => false, 'error' => trans('auth.failed')], Response::HTTP_NOT_FOUND);
                }
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => trans('auth.failed')], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        // all good so return the token
        return response()->json(['success' => true, 'user' => $this->getCurrentUser(), 'token' => $token], Response::HTTP_OK);
    }

    /**
     * API Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        try {
            auth()->logout();
            JWTAuth::invalidate($request->input('token'));
            return response()->json(['success' => true, 'message' => 'Vous vous êtes déconnecté avec succès.'], Response::HTTP_OK);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Échec de la déconnexion, veuillez réessayer.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Refresh a token.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        try {
            JWTAuth::refresh($request->input('token'));
            return response()->json(['success' => true, 'message' => 'Jeton actualisé avec succès.']);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['success' => false, 'error' => 'Échec de l\'actualisation du jeton, veuillez réessayer.'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * API Recover Password
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recover(Request $request)
    {
        $validator = Validator::make($request->only('email'), ['email' => 'required|email']);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()->first()], Response::HTTP_BAD_REQUEST);
        }

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['success' => false, 'error' => 'Votre adresse email n\'a pas été trouvée.'], Response::HTTP_NOT_FOUND);
        }
        try {
            $this->mailService->sendResetPasswordRequest($user, Password::broker()->createToken($user));
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'success' => true,
            'message' => 'Un email de réinitialisation a été envoyé ! Merci de consulter vos emails.'
        ], Response::HTTP_OK);
    }

    /**
     * Get User Info
     *
     * @return array
     */
    private function getCurrentUser()
    {
        $user = auth()->user();

        $data['id'] = $user->id;
        $data['email'] = $user->email;
        $data['phone'] = $user->phone;
        $data['last_name'] = $user->last_name;
        $data['first_name'] = $user->first_name;
        $data['avatar'] = $user->avatar ? asset_cdn($user->avatar) . '?w=200&h=200&fit=crop&crop=entropy&auto=format,compress&q=80' : '';
        $data['address'] = $user->address;
        $data['city'] = $user->city;
        $data['country'] = $user->country;

        return $data;
    }
}
