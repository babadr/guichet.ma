<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Models\Store\Ticket;
use App\Models\Store\Order;
use App\Models\Deal\Deal;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class TicketController extends Controller
{

    /*
     * validate Ticket
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function check(Request $request)
    {
        $user = auth()->user();

        if (!$user->hasRole(['validator'])) {
            return response()->json(['success' => false, 'error' => 'Not authorized.'], Response::HTTP_UNAUTHORIZED);
        }

        $attributes = $request->only('event_id', 'code');

        $rules = [
            'event_id' => 'required|integer',
            'code' => 'required',
        ];

        $validator = Validator::make($attributes, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()->first()], Response::HTTP_BAD_REQUEST);
        }

        $event = Deal::find($attributes['event_id']);

        $ticket = Ticket::where('hash', '=', $attributes['code'])->first();

        if (!$event || !$ticket || $event->provider_id !== $user->provider_id || $ticket->line->offer->deal_id !== $event->id || !in_array($ticket->line->order->status, [2, 3, 5])) {
            return response()->json(['success' => false, 'error' => 'Le ticket demandé est introuvable.'], Response::HTTP_NOT_FOUND);
        }

        if ($ticket->status) {
            return response()->json(['success' => false, 'error' => 'Ce ticket a déjà été validé.'], Response::HTTP_NOT_FOUND);
        }

        $ticket->update(['status' => 1]);

        return response()->json(['success' => true, 'message' => 'Validation effectuée avec succès.'], Response::HTTP_OK);
    }

    /*
     * Download Tickets
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $user = auth()->user();
        $order = Order::find($id);

        if ($user->id !== $order->user_id || !in_array($order->status, [2, 3, 5])) {
            return response()->json(['success' => false, 'error' => 'Commande introuvable.'], Response::HTTP_NOT_FOUND);
        }

        config(['pdf.format' => 'A4']);
        $pdf = \PDF::loadView('frontend.partials.ticket_online', ['order' => $order, 'isOffile' => false]);

        return $pdf->download('guichet.ma-tickets-' . $order->reference . '.pdf');
    }

}
