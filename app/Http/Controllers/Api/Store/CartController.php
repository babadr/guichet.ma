<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Deal\Offer;
use App\Models\Deal\Deal;
use Carbon\Carbon;
use Validator;
use Cart;

class CartController extends Controller
{


    /*
     * Collection of Cart Items
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['success' => true, 'cart' => $this->getCartContent()], Response::HTTP_OK);
    }

    /**
     * Adding an item to the cart
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        $attributes = $request->only('offer_id', 'quantity');
        $rules = [
            'offer_id' => 'required|integer|min:1',
            'quantity' => 'required|integer|min:1|max:30',
        ];

        $validator = Validator::make($attributes, $rules);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'error' => $validator->messages()->first()], Response::HTTP_BAD_REQUEST);
        }

        $offer = Offer::where('id', $attributes['offer_id'])->where('active', true)->first();
        if (!$offer) {
            return response()->json(['success' => false, 'error' => 'L\'événement choisi n\'existe pas.'], Response::HTTP_NOT_FOUND);
        }

        $qty = $this->getCartOfferQuantity($attributes['offer_id']) + $attributes['quantity'];

        if (!$offer->checkAvailability($qty)) {
            return response()->json(['success' => false, 'error' => 'La quantité demandée n\'est plus disponible en stock.'], Response::HTTP_BAD_REQUEST);
        }

        if (!$offer->checkMaxQuantity($qty)) {
            return response()->json(['success' => false, 'error' => 'La quantité max autorisée pour cette offre est de ' . $offer->quantity_max . ' tickets.'], Response::HTTP_BAD_REQUEST);
        }

        $deal = $offer->deal;

        $price = empty($offer->reservation_price) ? $offer->price : $offer->reservation_price;

        Cart::add($offer->id, $deal->title, $attributes['quantity'], $price, [
            'event_id' => $deal->id,
            'offer_title' => $offer->title,
            'image' => asset_cdn($deal->cover) . '?w=340&h=200&fit=clip&auto=format,compress&q=80',
            'reservation_price' => !empty($offer->reservation_price) ? $offer->price : null
        ]);

        return response()->json(['success' => true, 'cart' => $this->getCartContent()], Response::HTTP_OK);
    }

    /*
     * Update cart item
     *
     * @param string $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        try {
            $attributes = $request->only('quantity', 'type');
            $rules = [
                'quantity' => 'required|integer|min:1|max:30',
                'type' => 'required|string|in:up,down',
            ];

            $validator = Validator::make($attributes, $rules);

            if ($validator->fails()) {
                return response()->json(['success' => false, 'error' => $validator->messages()->first()], Response::HTTP_BAD_REQUEST);
            }

            $quantity = $attributes['quantity'];

            $item = Cart::get($id);

            $qty = $item->qty;

            if ($attributes['type'] == 'up') {
                $qty = $quantity;
            } elseif ($attributes['type'] == 'down') {
                $qty -= $quantity;
            }

            $offer = Offer::where('id', $item->id)->where('active', true)->first();

            if (!$offer) {
                return response()->json(['success' => false, 'error' => 'L\'événement choisi n\'existe pas.'], Response::HTTP_NOT_FOUND);
            }

            if (!$offer->checkAvailability($qty)) {
                return response()->json(['success' => false, 'error' => 'La quantité demandée n\'est plus disponible en stock.'], Response::HTTP_BAD_REQUEST);
            }

            if (!$offer->checkMaxQuantity($qty)) {
                return response()->json(['success' => false, 'error' => 'La quantité max autorisée pour cette offre est de ' . $offer->quantity_max . ' tickets.'], Response::HTTP_BAD_REQUEST);
            }

            Cart::update($id, $quantity);

            return response()->json([
                'success' => true,
                'message' => 'Panier mis à jour avec succès.',
                'cart' => $this->getCartContent(),
            ], Response::HTTP_OK);

        } catch (\Exception $ex) {
            return response()->json(['success' => false, 'error' => 'Votre panier ne contient pas ce produit. Veuillez recharger la page.'], Response::HTTP_NOT_FOUND);
        }
    }

    /*
     * Remove a given item from the cart
     *
     * @param string $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        try {
            Cart::remove($id);

            return response()->json([
                'success' => true,
                'message' => 'Evénement supprimé avec succès.',
                'cart' => $this->getCartContent(),
            ], Response::HTTP_OK);

        } catch (\Exception $ex) {
            return response()->json(['success' => false, 'error' => 'Votre panier ne contient pas cet événement. Veuillez recharger la page.'], Response::HTTP_NOT_FOUND);
        }
    }

    /*
     * Get offer quantity in cart
     *
     * @param int $offer_id
     * @return int
     */
    private function getCartOfferQuantity($offer_id)
    {
        $offer = Cart::search(function ($cartItem, $rowId) use ($offer_id) {
            return $cartItem->id == $offer_id;
        })->first();

        return $offer ? $offer->qty : 0;
    }

    /*
     * Get cart content
     *
     * @return array
     */
    private function getCartContent()
    {
        $cart = [
            'count' => Cart::count(),
            'sub_total' => Cart::total(2, ',', ' ') . ' DH',
            'items' => []
        ];

        $items = Cart::content();

        $key = 0;

        foreach ($items as $item) {
            $cart['items'][$key]['item_id'] = $item->rowId;

            $cart['items'][$key]['event']['id'] = $item->options->event_id;
            $cart['items'][$key]['event']['title'] = $item->name;
            $cart['items'][$key]['event']['image'] = $item->options->image;

            $cart['items'][$key]['offer']['id'] = $item->id;
            $cart['items'][$key]['offer']['title'] = $item->options->offer_title;
            $cart['items'][$key]['offer']['price'] = $item->options->reservation_price ? \Tools::displayPrice($item->options->reservation_price) : \Tools::displayPrice($item->price);
            $cart['items'][$key]['is_reservation'] = !empty($item->options->reservation_price) ? true : false;

            $cart['items'][$key]['price'] = \Tools::displayPrice($item->price);
            $cart['items'][$key]['quantity'] = $item->qty;
            $cart['items'][$key]['total'] = \Tools::displayPrice($item->subtotal);

            $key++;
        }

        return $cart;
    }

}
