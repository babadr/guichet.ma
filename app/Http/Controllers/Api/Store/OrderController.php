<?php

namespace App\Http\Controllers\Api\Store;

use App\Http\Controllers\Controller;
use App\Models\Store\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{

    /*
     * GET Orders List
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();

        $orders = Order::where('user_id', $user->id)->orderBy('created_at', 'desc')->get();

        $data = [];

        foreach ($orders as $key => $order) {
            $data[$key]['id'] = $order->id;
            $data[$key]['reference'] = $order->reference;
            $data[$key]['created_at'] = date('d/m/Y H:i', strtotime($order->created_at));
            $data[$key]['status'] = trans(config('enums.order_status')[$order->status]);
            $data[$key]['total_paid'] = $order->offered ? 'Commande offerte' : \Tools::displayPrice($order->total_paid, 0);
            $data[$key]['payment_method'] = $this->getPaymentMethod($order->payment_method);
            $data[$key]['payment_reference'] = $order->payment_reference;
            $data[$key]['payment_transaction'] = $order->payment_transaction;
            $data[$key]['payment_transaction_date'] = $order->payment_transaction_date ? date('m/d/Y H:i', strtotime($order->payment_transaction_date)) : null;
        }

        return response()->json(['success' => true, 'orders' => $data], Response::HTTP_OK);
    }

    /**
     * GET Order By ID
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $user = auth()->user();

        $order = Order::where('id', $id)->where('user_id', $user->id)->first();

        if (!$order) {
            return response()->json(['success' => false, 'error' => 'Not found.'], Response::HTTP_NOT_FOUND);
        }

        $data = [
            'id' => $order->id,
            'reference' => $order->reference,
            'created_at' => date('d/m/Y H:i', strtotime($order->created_at)),
            'status' => trans(config('enums.order_status')[$order->status]),
            'total_paid' => $order->offered ? 'Commande offerte' : \Tools::displayPrice($order->total_paid, 0),
            'payment_method' => $this->getPaymentMethod($order->payment_method),
            'payment_reference' => $order->payment_reference,
            'payment_transaction' => $order->payment_transaction,
            'payment_transaction_date' => $order->payment_transaction_date ? date('m/d/Y H:i', strtotime($order->payment_transaction_date)) : null,
            'lines' => []
        ];

        foreach ($order->lines as $key => $line) {
            $data['lines'][$key]['id'] = $line->id;
            $data['lines'][$key]['product_name'] = $line->product_name;
            $data['lines'][$key]['offer_name'] = $line->offer_name;
            $data['lines'][$key]['product_image'] = asset_cdn($line->offer->deal->cover) . '?w=340&h=200&fit=clip&auto=format,compress&q=80';
            $data['lines'][$key]['seats'] = $line->seats;
            $data['lines'][$key]['product_price'] = \Tools::displayPrice($line->product_price, 0);
            $data['lines'][$key]['quantity'] = $line->quantity;
            $data['lines'][$key]['total_price'] = \Tools::displayPrice($line->total_price, 0);
        }

        return response()->json(['success' => true, 'order' => $data], Response::HTTP_OK);
    }

    private function getPaymentMethod($key)
    {
        $str = '';
        switch ($key) {
            case 1:
                $str = 'Carte bancaire';
                break;
            case 2:
                $str = 'PAYEXPRESS (Cash Plus)';
                break;
            case 3:
                $str = 'Virement / versement bancaire';
                break;
            case 4:
                $str = 'Espèce';
                break;
            default:
                break;
        }

        return $str;
    }

}
