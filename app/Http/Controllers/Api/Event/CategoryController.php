<?php

namespace App\Http\Controllers\Api\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Deal\Category;
use App\Models\Deal\Deal;

class CategoryController extends Controller
{
    /**
     * GET Category List
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $categories = Category::select('id', 'parent_id', 'show_on_menu', 'title', 'icon', 'color', 'order')
            ->where('active', true)
            ->orderBy('order', 'asc')
            ->get()->toArray();

        return response()->json(['success' => true, 'categories' => $categories], Response::HTTP_OK);
    }

    /**
     * GET Category By ID
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $category = Category::find($id);
        if (!$category || !$category->active) {
            return response()->json(['success' => false, 'error' => 'Not found.'], Response::HTTP_NOT_FOUND);
        }

        $data = [
            'parent_id' => $category->parent_id,
            'id' => $category->id,
            'title' => $category->title,
            'icon' => $category->icon,
            'color' => $category->color,
            'events' => [],
        ];

        $events = $category->findDeals(6, true)->where('expired', '=', 0)->get();
        foreach ($events as $key => $event) {
            $data['events'][$key]['id'] = $event->id;
            $data['events'][$key]['title'] = $event->title;
            $data['events'][$key]['city'] = $event->city->city;
            $data['events'][$key]['price'] = \Tools::displayPrice($event->price, 0);
            $data['events'][$key]['old_price'] = $event->old_price ? \Tools::displayPrice($event->old_price, 0) : null;
            $data['events'][$key]['discount'] = \Tools::displayReduction($event->discount, 0);
            $data['events'][$key]['sold_out'] = $event->end_at->isPast() || !$event->checkAvailability();
            $data['events'][$key]['end_at'] = $event->end_at->format('Y-m-d H:i:s');
            $data['events'][$key]['image'] = asset_cdn($event->cover) . '?w=900&h=580&fit=clip&auto=format,compress&q=80';
        }

        return response()->json(['success' => true, 'category' => $data], Response::HTTP_OK);
    }

    /**
     * GET current Events By Category ID
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function currentEvents($id)
    {
        $category = Category::find($id);
        if (!$category || !$category->active) {
            return response()->json(['success' => false, 'error' => 'Not found.'], Response::HTTP_NOT_FOUND);
        }

        $ids = $category->children->pluck('id')->merge([$category->id])->all();

        $events = Deal::select('*')
            ->whereIn('category_id', $ids)
            ->with(['city', 'offers'])
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) AS date_diff')
            ->where('active', '=', 1)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->where('end_at', '>=', date('Y-m-d H:i:s'))
            ->orderBy('date_diff', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        $data = [
            'total' => $events->total(),
            'lastPage' => $events->lastPage(),
            'perPage' => $events->perPage(),
            'currentPage' => $events->currentPage(),
            'nextPage' => $events->nextPageUrl(),
            'events' => []
        ];

        foreach ($events->items() as $key => $event) {
            $data['events'][$key]['id'] = $event->id;
            $data['events'][$key]['title'] = $event->title;
            $data['events'][$key]['city'] = $event->city->city;
            $data['events'][$key]['price'] = \Tools::displayPrice($event->price, 0);
            $data['events'][$key]['discount'] = \Tools::displayReduction($event->discount, 0);
            $data['events'][$key]['sold_out'] = $event->end_at->isPast() || !$event->checkAvailability();
            $data['events'][$key]['end_at'] = $event->end_at->format('Y-m-d H:i:s');
            $data['events'][$key]['image'] = asset_cdn($event->cover) . '?w=340&h=200&fit=clip&auto=format,compress&q=80';
        }

        return response()->json(['success' => true, 'events' => $data], Response::HTTP_OK);
    }

    /**
     * GET past Events By Category ID
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function pastEvents($id)
    {
        $category = Category::find($id);
        if (!$category || !$category->active) {
            return response()->json(['success' => false, 'error' => 'Not found.'], Response::HTTP_NOT_FOUND);
        }

        $ids = $category->children->pluck('id')->merge([$category->id])->all();

        $events = Deal::select('*')
            ->whereIn('category_id', $ids)
            ->with(['city', 'offers'])
            ->where('active', '=', 1)
            ->where('end_at', '<=', date('Y-m-d H:i:s'))
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        $data = [
            'total' => $events->total(),
            'lastPage' => $events->lastPage(),
            'perPage' => $events->perPage(),
            'currentPage' => $events->currentPage(),
            'nextPage' => $events->nextPageUrl(),
            'events' => []
        ];

        foreach ($events->items() as $key => $event) {
            $data['events'][$key]['id'] = $event->id;
            $data['events'][$key]['title'] = $event->title;
            $data['events'][$key]['city'] = $event->city->city;
            $data['events'][$key]['price'] = \Tools::displayPrice($event->price, 0);
            $data['events'][$key]['discount'] = \Tools::displayReduction($event->discount, 0);
            $data['events'][$key]['sold_out'] = $event->end_at->isPast() || !$event->checkAvailability();
            $data['events'][$key]['end_at'] = $event->end_at->format('Y-m-d H:i:s');
            $data['events'][$key]['image'] = asset_cdn($event->cover) . '?w=340&h=200&fit=clip&auto=format,compress&q=80';
        }

        return response()->json(['success' => true, 'events' => $data], Response::HTTP_OK);
    }


}
