<?php

namespace App\Http\Controllers\Api\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use App\Models\Deal\Deal;
use App\Models\Deal\Provider;

class EventController extends Controller
{
    /**
     * GET Event List
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $events = Deal::select('*')
            ->with(['category', 'city', 'offers'])
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) AS date_diff')
            ->where('active', '=', 1)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->where('end_at', '>=', date('Y-m-d H:i:s'))
            ->orderBy('date_diff', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->paginate(10);

        $data = [
            'total' => $events->total(),
            'lastPage' => $events->lastPage(),
            'perPage' => $events->perPage(),
            'currentPage' => $events->currentPage(),
            'nextPage' => $events->nextPageUrl(),
            'events' => []
        ];

        foreach ($events->items() as $key => $event) {
            $data['events'][$key]['id'] = $event->id;
            $data['events'][$key]['title'] = $event->title;
            $data['events'][$key]['city'] = $event->city->city;
            $data['events'][$key]['price'] = \Tools::displayPrice($event->price, 0);
            $data['events'][$key]['old_price'] = $event->old_price ? \Tools::displayPrice($event->old_price, 0) : null;
            $data['events'][$key]['discount'] = \Tools::displayReduction($event->discount, 0);
            $data['events'][$key]['sold_out'] = $event->end_at->isPast() || !$event->checkAvailability();
            $data['events'][$key]['end_at'] = $event->end_at->format('Y-m-d H:i:s');
            $data['events'][$key]['image'] = asset_cdn($event->cover) . '?w=340&h=200&fit=clip&auto=format,compress&q=80';
            $data['events'][$key]['category']['id'] = $event->category->id;
            $data['events'][$key]['category']['name'] = $event->category->title;
        }

        return response()->json(['success' => true, 'events' => $data], Response::HTTP_OK);
    }

    /**
     * GET Event By ID
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $event = Deal::find($id);
        if (!$event || !$event->active) {
            return response()->json(['success' => false, 'error' => 'Not found.'], Response::HTTP_NOT_FOUND);
        }

        $data = [
            'id' => $event->id,
            'title' => $event->title,
            'city' => $event->city->city,
            'short_description' => $event->short_description,
            'description' => $event->description,
            'address' => $event->address,
            'latitude' => $event->latitude,
            'longitude' => $event->longitude,
            'discount' => \Tools::displayReduction($event->discount, 0),
            'start_at' => $event->start_at->format('Y-m-d H:i:s'),
            'end_at' => $event->end_at->format('Y-m-d H:i:s'),
            'custom_field' => $event->custom_field,
            'opening_time' => $event->opening,
            'start_time' => $event->start_time,
            'sold_out' => $event->end_at->isPast() || !$event->checkAvailability(),
            'category' => [
                'id' => $event->category->id,
                'name' => $event->category->title
            ],
            'provider' => [
                'id' => $event->provider->id,
                'name' => $event->provider->title,
                'address' => $event->provider->address,
                'phone' => $event->provider->phone,
                'logo' => asset_cdn($event->provider->logo) . '?w=200&h=150&fit=clip&auto=format,compress&q=80'
            ],
            'offers' => [],
            'medias' => []
        ];

        $offers = $event->offers()->where('active', true)->orderBy('price')->get();
        foreach ($offers as $key => $offer) {
            $data['offers'][$key]['id'] = $offer->id;
            $data['offers'][$key]['name'] = $offer->title;
            $data['offers'][$key]['price'] = \Tools::displayPrice($offer->price, 0);
            $data['offers'][$key]['old_price'] = $offer->old_price ? \Tools::displayPrice($offer->old_price, 0) : null;
            $data['offers'][$key]['quantity'] = $offer->quantity;
            $data['offers'][$key]['quantity_max'] = $offer->quantity_max;
            $data['offers'][$key]['is_default'] = $offer->default;
            $data['offers'][$key]['sold_out'] = $event->end_at->isPast() || !$offer->checkAvailability(1);
        }

        foreach ($event->medias as $key => $media) {
            $data['medias'][$key] = asset_cdn($media->filename) . '?w=900&h=600&fit=clip&auto=format,compress&q=80';
        }

        return response()->json(['success' => true, 'event' => $data], Response::HTTP_OK);
    }

    /**
     * GET Validator associated events
     *
     * @return \Illuminate\Http\Response
     * */
    public function validator()
    {
        $user = auth()->user();
        if (!$user->hasRole(['validator'])) {
            return response()->json(['success' => false, 'error' => 'Not authorized'], Response::HTTP_UNAUTHORIZED);
        }

        $data = [];
        $provider = Provider::find($user->provider_id);
        $events = $provider->deals;
        foreach ($events as $key => $event) {
            $data[$key]['id'] = $event->id;
            $data[$key]['title'] = $event->title;
            $data[$key]['category'] = $event->category->title;
            $data[$key]['image'] = asset_cdn($event->cover) . '?w=900&h=600&fit=clip&auto=format,compress&q=80';
            $data[$key]['date'] = $event->end_at->format('Y-m-d H:i:s');
        }

        return response()->json(['success' => true, 'events' => $data], Response::HTTP_OK);
    }

}
