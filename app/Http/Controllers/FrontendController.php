<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Models\Deal\Category;

class FrontendController extends Controller
{

    /**
     * @var \Illuminate\Http\Request current Request
     */
    protected $currentRequest;

    /**
     * @var Model current Model
     */
    protected $currentModel;

    /**
     * Front controller construct
     * 
     * @param Request $request
     * @param Model $model
     */
    public function __construct(Request $request, Model $model = null)
    {
        $this->currentRequest = $request;
        $this->currentModel = $model;

        $listCategories = (new Category())->with('seo')->where('active', true)->orderBy('order', 'asc')->get();
        view()->share('listCategories', $listCategories);
    }

}
