<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Creitive\Breadcrumbs\Breadcrumbs;

class BackendController extends Controller
{

    /**
     * @var \Illuminate\Http\Request current Request
     */
    protected $currentRequest;

    /**
     * @var Model current Model
     */
    protected $currentModel;

    /**
     * Breadcrumbs facade
     */
    public $breadcrumbs;

    /**
     * Current model path
     *
     * @var string
     */
    protected $modelName = '';

    /**
     * Current view path
     *
     * @var string
     */
    protected $viewPrefix = '';

    /**
     * SEO associated
     *
     * @var boolean
     */
    protected $seo = false;

    /**
     * Save the $request in this->_request
     *
     * @param Request $request
     * @param Model $model
     */
    public function __construct(Request $request, Model $model = null)
    {
        $this->currentRequest = $request;
        $this->currentModel = $model;
        $this->modelName = $model !== null ? $model->getName() : '';
        $this->includeBreadcrumbs();
    }

    /**
     * Include and init the Breadcrumbs
     *
     * @params string $namespace
     * @return void
     */
    protected function includeBreadcrumbs()
    {
        $this->breadcrumbs = new Breadcrumbs();
        $this->breadcrumbs->setCssClasses('page-breadcrumb');
        $this->breadcrumbs->setDivider('<i class="fa fa-circle"></i>');
        $this->breadcrumbs->setListElement('ul');
        $this->breadcrumbs->addCrumb(\Lang::get('menu.dashboard'), route('backend.index'));
        \View::composer('backend.includes.breadcrumb', function() {
            view()->share('breadcrumbs', $this->breadcrumbs->render());
        });
    }

    /**
     * Get the evaluated view contents for the given view.
     *
     * @param string $name
     *
     * @return \Illuminate\View\View
     */
    protected function getView($name)
    {
        $view = view($name);
        $vars = $this->getViewVars();
        foreach ($vars as $name => $var) {
            $view->with($name, $var);
        }

        return $view;
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [];
    }

}
