<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageCacheController as BaseController;

class ImageCacheController extends BaseController
{
    /*
     * Get HTTP response of template applied image file
     *
     * @param  string $template
     * @param  string $filename
     * @return \Illuminate\Http\Response
     */
    public function getImage($template, $filename)
    {
        $path = $this->getImagePath($filename);
        $cachePath = $this->createDir(public_path("imagecache/$template/$filename"));
        $image = Image::make($path);
        switch ($template) {
            case 'slide':
                $this->resize($image, 900, 580);
                break;
            case 'small':
                $this->resize($image, 480, 240);
                break;
            case 'big':
                $this->resize($image, 900, 600);
                break;
            case 'partner':
                $this->resize($image, 200, 150);
                break;
            case 'post':
                $this->resize($image, 845, 475);
                break;
            case 'thumb':
                $image = $image->fit(150, 90);
                break;
            case 'original':
                break;
            default:
                abort(404);
                break;
        }

        return $this->buildResponse($image->save($cachePath));
    }

    /*
     * Returns corresponding template object from given template name
     *
     * @param  string $template
     * @return mixed
     */
    private function getTemplate($template)
    {
        $template = config("imagecache.templates.{$template}");

        switch (true) {
            // closure template found
            case is_callable($template):
                return $template;
            // filter template found
            case class_exists($template):
                return new $template;
            default:
                // template not found
                abort(404);
                break;
        }
    }

    /*
     * Returns full image path from given filename
     *
     * @param  string $filename
     * @return string
     */
    private function getImagePath($filename)
    {
        // find file
        foreach (config('imagecache.paths') as $path) {
            // don't allow '..' in filenames
            $image_path = $path . '/' . str_replace('..', '', $filename);
            if (file_exists($image_path) && is_file($image_path)) {
                // file found
                return $image_path;
            }
        }
        // file not found
        abort(404);
    }

    /**
     * Builds HTTP response from given image data
     *
     * @param  string $content
     * @return \Illuminate\Http\Response
     */
    private function buildResponse($content)
    {
        // define mime type
        $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $content);

        return response($content, 200, ['Content-Type' => $mime])
            ->setMaxAge(604800)
            ->setPublic();
    }

    /*
     * Create cache directories
     *
     * @param string $directory
     * @param int $mode
     * @return string
     */
    private function createDir($directory, $mode = 0777)
    {
        $dir = pathinfo($directory, PATHINFO_EXTENSION) ? dirname($directory) : $directory;

        if (!is_dir($dir)) {
            if (false === mkdir($dir, $mode, true) && !is_dir($dir)) {
                throw new FileException(sprintf('Unable to create the "%s" directory', $dir));
            }
        } elseif (!is_writable($dir)) {
            throw new FileException(sprintf('Unable to write in the "%s" directory', $dir));
        }

        return $directory;
    }

    /*
    * Resize image
    *
    * @param $image
    * @param int $w
    * @param int $h
     *
    * @return void
    */
    private function resize(&$image, $w, $h)
    {
        $image->resize($w, $h, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
    }
}