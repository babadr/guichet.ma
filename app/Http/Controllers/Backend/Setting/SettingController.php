<?php

namespace App\Http\Controllers\Backend\Setting;

use App\Http\Controllers\BackendController;
use App\Models\Setting\Setting;
use Illuminate\Http\Request;

class SettingController extends BackendController
{
    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $keys = [
        'app_parameters' => [
            'site_info' => 'title',
            'site_name',
            'site_slogan',
            'site_description' => [
                'type' => 'textarea',
            ],
            'logo_home' => [
                'type' => 'image',
                'form_columns' => 4,
            ],
            'logo_footer' => [
                'type' => 'image',
                'form_columns' => 4,
            ],
            'site_contact' => 'title',
            'address_1',
            'address_2',
            'zip_code',
            'city',
            'country',
            'phone',
            'email',
            'location' => [
                'help' => 'help_location',
            ],
            'google_code_site' => 'title',
            'google_analytics_code' => [
                'type' => 'textarea',
            ],
            'google_verification_code' => [
                'type' => 'textarea',
            ],
            'google_map_api_key',
            'facebook_pixel_code' => [
                'type' => 'textarea',
            ],
        ],
        'networking' => [
            'facebook_url' => [
                'placeholder' => 'Exemple : https://www.facebook.com/laravel',
            ],
            'twitter_url' => [
                'placeholder' => 'Exemple : https://twitter.com/laravel',
            ],
            'instagram_url' => [
                'placeholder' => '#',
            ],
            'linkedin_url' => [
                'placeholder' => 'Exemple : https://fr.linkedin.com/laravel',
            ],
            'google_url' => [
                'placeholder' => 'Exemple : https://plus.google.com/laravel',
            ],
            'pinterest_url' => [
                'placeholder' => '#',
            ],
            'youtube_url' => [
                'placeholder' => '#',
            ],
        ],
        'seo' => [
            'meta_parameters' => 'title',
            'meta_title' => [
                'help' => 'help_meta_home',
            ],
            'meta_description' => [
                'type' => 'textarea',
                'help' => 'help_meta_home',
            ],
            'meta_keywords' => [
                'help' => 'help_meta_home',
            ],
            'meta_robots' => [
                'type' => 'select',
                'form_columns' => 3,
                'list' => [
                    'index, follow' => 'index, follow',
                    'noindex, follow' => 'noindex, follow',
                    'index, nofollow' => 'index, nofollow',
                    'noindex, nofollow' => 'noindex, nofollow',
                ],
            ],
        ],
        'payment_fpay' => [
            'payment_merchant_id' => [
                'help' => 'help_payment_merchant_id',
            ],
            'payment_url' => [
                'help' => 'help_payment_url',
            ],
            'payment_key_hmac' => [
                'help' => 'help_payment_key_hmac',
            ]
        ],
    ];


    /**
     * @param Request $request
     * @param Setting $setting
     */
    public function __construct(Request $request, Setting $setting)
    {
        parent::__construct($request, $setting);
    }

    /**
     * Display Setting parameters
     *
     * @return View
     */
    public function index()
    {
        $settings = $this->currentModel->all();
        $values = [];
        if ($settings->count()) {
            foreach ($settings as $setting) {
                $values[$setting->key] = $setting->value;
            }
        }

        $this->breadcrumbs->addCrumb(trans('setting.settings'), '');

        return view('backend.contents.setting.index')
            ->with('parameters', $this->keys)
            ->with('settingsValues', $values);
    }

    /**
     * Handle post settings
     *
     * @return Response
     */
    public function update()
    {
        $attributes = $this->currentRequest->all();
        foreach ($attributes as $key => $value) {
            if ($key !== '_token' && $key !== 'save') {
                if ($this->currentRequest->hasFile($key)) {
                    $value = $this->currentRequest->file($key)->store($this->currentModel->path, ['disk' => 'local']);
                }
                $setting = $this->currentModel->where('key', $key)->first();
                if ($setting) {
                    $setting->value = $value;
                    $setting->update();
                } else {
                    $setting = new Setting;
                    $setting->fill(['key' => $key, 'value' => $value]);
                    $setting->save();
                }
            }
        }

        flash()->success(trans('setting.updated'));

        return redirect(route('backend.setting.index'));
    }

}
