<?php

namespace App\Http\Controllers\Backend\Acl;

use App\Http\Controllers\BackendController;
use App\Models\Acl\User;
use App\Models\Acl\Role;
use App\Models\Deal\Provider;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class UserController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['users.*', 'roles.display_name'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['role_user', 'users.id', '=', 'role_user.user_id'],
        ['roles', 'roles.id', '=', 'role_user.role_id']
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $files = ['avatar'];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'full_name';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'user.ManageUsers';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'users.id',
            'prefix' => 'users.'
        ],
        'last_name' => [
            'title' => 'user.last_name',
            'type' => 'text',
            'filterKey' => 'last_name',
            'orderKey' => 'users.last_name',
            'prefix' => 'users.'
        ],
        'first_name' => [
            'title' => 'user.first_name',
            'type' => 'text',
            'filterKey' => 'first_name',
            'orderKey' => 'users.first_name',
            'prefix' => 'users.'
        ],
        'email' => [
            'title' => 'user.email',
            'type' => 'text',
            'filterKey' => 'email',
            'orderKey' => 'users.email',
            'prefix' => 'users.'
        ],
        'display_name' => [
            'title' => 'user.role_id',
            'type' => 'select',
            'list' => 'roles',
            'orderKey' => 'roles.display_name',
            'filterKey' => 'id',
            'prefix' => 'roles.'
        ],
        'created_at' => [
            'title' => 'user.since_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'users.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'users.'
        ],
        'active' => [
            'title' => 'user.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'users.active',
            'callBack' => 'printStatus',
            'prefix' => 'users.'
        ]
    ];


    /**
     * @param Request $request
     * @param User $user
     */
    public function __construct(Request $request, User $user)
    {
        $this->viewPrefix = 'acl.user';
        parent::__construct($request, $user);
    }

    /**
     * Handle edit request
     *
     * @param int $id => the id to edit
     *
     * @return Response
     */
    public function edit($id)
    {
        if (auth()->user()->hasRole('partner') && auth()->user()->id != $id) {
            throw (new ModelNotFoundException)->setModel(User::class, $id);
        }
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect(route('backend.' . $this->modelName . '.index'));
        }
        $this->getBreadcrumbs($model);
        $view = $this->getView('backend.contents.' . $this->viewPrefix . '.form');

        return $view->with('title', trans($this->modelName . '.edit', ['name' => $model->{$this->title}]))
            ->with('route', route('backend.' . $this->modelName . '.update', $id))
            ->with('method', 'post')
            ->with('model', $model);
    }

    /**
     * Handle delete request
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            return response()->json(['success' => false, 'msg' => trans('app.not_found')]);
        }

        if ($id == auth()->user()->id || $id == User::SUPER_ADMIN) {
            return response()->json(['success' => false, 'msg' => trans('user.cannot_delete_user')]);
        }

        $result = $model->delete();

        return response()->json(['success' => $result, 'msg' => trans($result ? 'app.delete_success' : 'app.cannot_delete')]);
    }

    /**
     * Before Save user
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();
        if ($id != auth()->user()->id) {
            $attributes['active'] = isset($attributes['active']) ? true : false;
        }

        return $attributes;
    }

    /**
     * After save User
     *
     * @param array $attributes
     * @param User $model
     * @return void
     */
    protected function afterSave(array $attributes, $model)
    {
        if (isset($attributes['role_id'])) {
            $model->detachRoles();
            $model->attachRole($attributes['role_id']);
        }
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        $roles = (new Role)->select(['id', 'display_name'])->get();
        $rows = [];
        foreach ($roles as $role) {
            $rows[$role->id] = $role->display_name;
        }

        return [
            'roles' => $rows,
            'providers' => (new Provider())->select(['id', 'title'])->where('active', 1)->get()->pluck('title', 'id')->toArray(),
            'columns' => $this->columns
        ];
    }

    /**
     * Create custom conditions
     *
     * @return array
     */
    protected function customConditions()
    {
        return [
            ['users.id', '!=', auth()->user()->id],
            ['users.id', '!=', User::SUPER_ADMIN]
        ];
    }

    /**
     * export all Users
     *
     */
    public function export()
    {
        if (ob_get_level() && ob_get_length() > 0) {
            ob_clean();
        }

        $handle = fopen('php://memory', 'r+');
        $header = [
            'Num',
            'Nom',
            'Prenom',
            'Email',
            'Telephone',
            'Ville',
            'Date_Creation',
        ];
        fputcsv($handle, $header, ';');
        //fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));
        $users = User::select('id', 'last_name', 'first_name', 'email', 'phone', 'city', 'created_at')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_id', User::CUSTOMER_ID)
            ->orderBy('created_at', 'desc')->get();
        foreach ($users as $key => $user) {
            $content[$key] = [];
            $content[$key][] = $user->id;
            $content[$key][] = $user->last_name;
            $content[$key][] = $user->first_name;
            $content[$key][] = $user->email;
            $content[$key][] = '"' . $user->phone . '"';
            $content[$key][] = $user->city;
            $content[$key][] = date('d/m/Y H:i:s', strtotime($user->created_at));
            fputcsv($handle, $content[$key], ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        header('Content-type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-disposition: attachment; filename="export_users_' . date('Y-m-d_His') . '.csv"');

        print $content;
        exit;
    }

}
