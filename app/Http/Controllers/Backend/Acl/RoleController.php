<?php

namespace App\Http\Controllers\Backend\Acl;

use App\Http\Controllers\BackendController;
use App\Models\Acl\Role;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class RoleController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['roles.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'display_name';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'role.manage_roles';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'roles.id',
            'prefix' => 'roles.'
        ],
        'name' => [
            'title' => 'role.name',
            'type' => 'text',
            'filterKey' => 'name',
            'orderKey' => 'roles.name',
            'prefix' => 'roles.'
        ],
        'display_name' => [
            'title' => 'role.display_name',
            'type' => 'text',
            'filterKey' => 'display_name',
            'orderKey' => 'roles.display_name',
            'prefix' => 'roles.'
        ],
        'description' => [
            'title' => 'role.description',
            'type' => 'text',
            'filterKey' => 'description',
            'orderKey' => 'roles.description',
            'prefix' => 'roles.'
        ]
    ];


    /**
     * @param Request $request
     * @param Role $role
     */
    public function __construct(Request $request, Role $role)
    {
        $this->viewPrefix = 'acl.role';
        parent::__construct($request, $role);
    }

    /**
     * Handle delete request
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            return response()->json(['success' => false, 'msg' => trans('app.not_found')]);
        }

        if ($model->users()->count()) {
            return response()->json(['success' => false, 'msg' => trans('role.role_associated_users')]);
        }

        $result = $model->delete();

        return response()->json(['success' => $result, 'msg' => trans($result ? 'app.delete_success' : 'app.cannot_delete')]);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns
        ];
    }

}
