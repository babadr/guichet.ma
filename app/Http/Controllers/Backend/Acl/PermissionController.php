<?php

namespace App\Http\Controllers\Backend\Acl;

use Illuminate\Http\Request;
use App\Http\Controllers\BackendController;
use App\Models\Acl\Role;
use App\Models\Acl\CategoryPermission;
use App\Models\Acl\Permission;


class PermissionController extends BackendController
{

    protected $category;

    protected $permission;

    protected $role;

    /**
     * MatrixController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->category = new CategoryPermission();
        $this->permission = new Permission();
        $this->role = new Role();

        parent::__construct($request);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $this->breadcrumbs->addCrumb(trans('permission.matrix'), '');

        $roles = $this->role->all();
        $permissions = $this->permission->all();
        $categories = $this->category->all();

        return view('backend.contents.acl.permission.index', compact('categories', 'roles', 'permissions'));

    }

    /**
     * @return Response
     */
    public function postIndex()
    {
        $services = $this->currentRequest->get('roles', null);

        if ($services) {
            $allRoles = $this->role->all();
            foreach ($allRoles as $role) {
                $permissionsSync = [];
                if (!isset($services[$role->id])) {
                    continue;
                }
                $serviceData = $services[$role->id];
                foreach ($serviceData['permissions'] as $permissionId => $permissionData) {
                    $permissionsSync[] = $permissionId;
                }
                $role->perms()->sync($permissionsSync);
            }
        }

        flash()->success(trans('permission.updated'));

        return redirect(route('backend.permission.index'));
    }


}
