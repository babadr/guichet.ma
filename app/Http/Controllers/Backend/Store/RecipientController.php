<?php

namespace App\Http\Controllers\Backend\Store;

use App\Http\Controllers\BackendController;
use App\Models\Store\Order;
use App\Models\Store\Recipient;
use Illuminate\Http\Request;
use App\Traits\Datatableable;

class recipientController extends BackendController
{

    use Datatableable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['recipients.*', 'order_lines.offer_name', 'order_lines.offer_id', 'orders.status'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['order_lines', 'order_lines.id', '=', 'recipients.order_line_id'],
        ['orders', 'orders.id', '=', 'order_lines.order_id'],
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'recipient.manage_recipients';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'recipients.id',
            'prefix' => 'recipients.'
        ],
        'cin' => [
            'title' => 'recipient.cin',
            'type' => 'text',
            'filterKey' => 'cin',
            'orderKey' => 'recipients.cin',
            'prefix' => 'recipients.'
        ],
        'full_name' => [
            'title' => 'recipient.full_name',
            'type' => 'text',
            'filterKey' => 'full_name',
            'orderKey' => 'recipients.full_name',
            'prefix' => 'recipients.'
        ],
        'email' => [
            'title' => 'recipient.email',
            'type' => 'text',
            'filterKey' => 'email',
            'orderKey' => 'recipients.email',
            'prefix' => 'recipients.'
        ],
        'phone' => [
            'title' => 'recipient.phone',
            'type' => 'text',
            'filterKey' => 'phone',
            'orderKey' => 'recipients.phone',
            'prefix' => 'recipients.'
        ],
        'city' => [
            'title' => 'recipient.city',
            'type' => 'text',
            'filterKey' => 'city',
            'orderKey' => 'recipients.city',
            'prefix' => 'recipients.'
        ],
        'offer_name' => [
            'title' => 'recipient.offer_name',
            'type' => 'select',
            'list' => 'cards',
            'filterKey' => 'offer_id',
            'orderKey' => 'order_lines.offer_name',
            'prefix' => 'order_lines.'
        ],
        'created_at' => [
            'title' => 'recipient.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'recipients.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'recipients.'
        ],
    ];

    /**
     * @param Request $request
     * @param Recipient $recipient
     */
    public function __construct(Request $request, Recipient $recipient)
    {
        $this->viewPrefix = 'store.recipient';
        $this->select = [
            'recipients.*', 'order_lines.offer_name', 'order_lines.offer_id', 'orders.status'
        ];
        parent::__construct($request, $recipient);
    }

    public function export()
    {
        $recipients = Recipient::select('recipients.*', 'order_lines.offer_name', 'order_lines.offer_id', 'orders.status')
            ->join('order_lines', 'order_lines.id', '=', 'recipients.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->get();

        $rows = [];
        foreach ($recipients as $recipient) {
            $row = [];
            $row['ID'] = $recipient->id;
            $row['Num CIN'] = $recipient->cin;
            $row['Nom & Prénom'] = $recipient->full_name;
            $row['E-mail'] =  $recipient->email;
            $row['Téléphone'] = $recipient->phone;
            $row['Date de naissance'] =  date('d/m/Y', strtotime($recipient->birth_date));
            $row['Adresse'] =  $recipient->address;
            $row['Ville'] =  $recipient->city;
            $row['Code Postal'] =  $recipient->zip_code;
            $row['Type d\'abonnemet'] = 'Carte ' . $recipient->offer_name;
            $row['Date inscription'] = date('d/m/Y H:i', strtotime($recipient->created_at));
            $row['Photo'] = url('/imagecache/original/' . $recipient->avatar);
            $rows[] = $row;
        }

        return \Excel::create('export_abonnements_raja_' .  date('Y-m-d_His'), function ($excel) use ($rows) {
            $excel->sheet('Liste des abonnés', function ($sheet) use ($rows) {
                $sheet->fromArray($rows);
            });
        })->download('xls');
    }

    protected function createAjaxResponse()
    {
        $request = $this->currentRequest->all();
        $this->initFilter($request);
        $this->createOrder($request);
        $this->conditions = array_merge($this->conditions, $this->customConditions());
        $modelTotal = $this->currentModel;
        $recordsTotal = $modelTotal
            ->join('order_lines', 'order_lines.id', '=', 'recipients.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();

        $this->createConditions($request);
        $modelFilter = $this->currentModel;
        $recordsFiltered = $modelFilter->join('order_lines', 'order_lines.id', '=', 'recipients.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();

        $output = [
            'data' => $this->buildRows(),
            'draw' => $this->draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
        ];

        return response()->json($output);
    }

    /**
     * Build listing rows
     *
     * @return array
     */
    protected function buildRows()
    {
        //$this->initRows();
        $model = $this->currentModel->join('order_lines', 'order_lines.id', '=', 'recipients.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED]);
        if ($this->select) {
            $model->select($this->select);
        }
        if ($this->orderBy) {
            $model->orderBy($this->orderBy, $this->orderWay);
        }
        if ($this->start) {
            $model->skip((int)$this->start);
        }
        if ($this->length) {
            $model->take((int)$this->length);
        }
        $rows = $model->get($this->select);
        //$rows = $this->currentModel->get($this->select);
        $data = [];
        foreach ($rows as $key => $object) {
            foreach ($this->columns as $field => $params) {
                if (isset($params['callBack'])) {
                    $str = $this->{$params['callBack']}($object, $field);
                } else {
                    $str = $object->{$field};
                }
                $data[$key][] = $str;
            }
            $data[$key][] = $this->addRowActions($object);
        }

        return $data;
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
            'cards' => [954 => 'BRONZE', 955 => 'SILVER', 956 => 'GOLD'],
        ];
    }

    protected function addRowActions($model)
    {
        return '';
    }


}
