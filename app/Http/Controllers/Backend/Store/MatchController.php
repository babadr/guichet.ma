<?php

namespace App\Http\Controllers\Backend\Store;

use App\Http\Controllers\BackendController;
use App\Models\Deal\City;
use App\Models\Deal\Deal;
use Illuminate\Http\Request;
use App\Traits\Datatableable;

class MatchController extends BackendController
{

    use Datatableable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['deals.*', 'cities.city'];

    /**
     * Join associated models
     *
     * @var array
     */
    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['cities', 'cities.id', '=', 'deals.city_id'],
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'Gestion des Matchs';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'deals.id',
            'prefix' => 'deals.'
        ],
        'title' => [
            'title' => 'deal.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'deals.title',
            'prefix' => 'deals.',
            'width' => '300px'
        ],
        'city' => [
            'title' => 'deal.city_id',
            'type' => 'select',
            'list' => 'cities',
            'filterKey' => 'city_id',
            'orderKey' => 'cities.city',
            'prefix' => 'deals.'
        ],
        'end_at' => [
            'title' => 'Date et heure du Match',
            'type' => 'date',
            'filterKey' => 'end_at',
            'orderKey' => 'deals.end_at',
            'callBack' => 'printDateTime',
            'prefix' => 'deals.',
        ],
        'total_ticket' => [
            'title' => 'Nombre de tickets vendus',
            'type' => 'text',
            'filterKey' => false,
            'orderKey' => false,
            'callBack' => 'printCountTickets',
            'prefix' => 'deals.',
        ],
    ];


    /**
     * @param Request $request
     * @param Deal $deal
     */
    public function __construct(Request $request, Deal $deal)
    {
        $this->viewPrefix = 'store.match';
        parent::__construct($request, $deal);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
            'cities' => (new City)->select(['id', 'city'])->get()->pluck('city', 'id'),
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.store.match.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Create custom conditions
     *
     * @return array
     */
    protected function customConditions()
    {
        return [
            ['deals.is_football', '=', true],
        ];
    }

    protected function printCountTickets($model, $field)
    {
        return $model->getOrderedQuantity(false);
    }
}
