<?php

namespace App\Http\Controllers\Backend\Store;

use App\Http\Controllers\BackendController;
use App\Models\Store\Ticket;
use App\Models\Store\Order;
use App\Models\Deal\Deal;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use PDF;

class TicketsController extends BackendController
{

    use Datatableable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = [];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['order_lines', 'order_lines.id', '=', 'tickets.order_line_id'],
        ['orders', 'orders.id', '=', 'order_lines.order_id'],
        ['offers', 'offers.id', '=', 'order_lines.offer_id'],
        ['deals', 'deals.id', '=', 'offers.deal_id'],
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'ticket.manage_tickets';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'tickets.id',
            'prefix' => 'tickets.'
        ],
        'reference' => [
            'title' => 'Réf. Commande',
            'type' => 'text',
            'filterKey' => 'reference',
            'orderKey' => 'orders.reference',
            'prefix' => 'orders.'
        ],
        'number' => [
            'title' => 'ticket.number',
            'type' => 'text',
            'filterKey' => 'number',
            'orderKey' => 'tickets.number',
            'prefix' => 'tickets.'
        ],
        'title' => [
            'title' => 'ticket.offer',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'offers.title',
            'prefix' => 'offers.'
        ],
        'product_price' => [
            'title' => 'ticket.product_price',
            'type' => 'text',
            'filterKey' => 'product_price',
            'orderKey' => 'order_lines.product_price',
            'callBack' => 'printPrice',
            'prefix' => 'order_lines.'
        ],
        'created_at' => [
            'title' => 'order.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'orders.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'orders.'
        ],
    ];


    /**
     * @param Request $request
     * @param Ticket $ticket
     */
    public function __construct(Request $request, Ticket $ticket)
    {
        $this->viewPrefix = 'store.ticket';
        $this->select = [
            'tickets.id',
            'tickets.seat',
            'tickets.number',
            'order_lines.product_price',
            'orders.created_at',
            'orders.reference',
            'orders.status',
            'order_lines.order_id',
            'offers.title',
        ];
        parent::__construct($request, $ticket);
    }

    /**
     * Display a listing
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->currentRequest->ajax()) {
            return $this->createAjaxResponse();
        }

        $this->breadcrumbs->addCrumb('Gestion des tickets');

        return view('backend.contents.' . $this->viewPrefix . '.list')
            ->with('columns', $this->columns);
    }

    protected function addRowActions($model)
    {
        return view('backend.contents.store.ticket.links')
            ->with('ticket_id', $model->id)
            ->with('status', $model->status)
            ->with('order_id', $model->order_id)
            ->render();
    }

    /**
     * Create ajax response
     *
     * @return \Illuminate\Http\Response
     */
    protected function createAjaxResponse()
    {
        $request = $this->currentRequest->all();
        $this->initFilter($request);
        $this->createOrder($request);
        $this->conditions = array_merge($this->conditions, $this->customConditions());
        $modelTotal = $this->currentModel;
        $recordsTotal = $modelTotal
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();

        $this->createConditions($request);
        $modelFilter = $this->currentModel;
        $recordsFiltered = $modelFilter->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();

        $output = [
            'data' => $this->buildRows(),
            'draw' => $this->draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
        ];

        return response()->json($output);
    }

    /**
     * Build listing rows
     *
     * @return array
     */
    protected function buildRows()
    {
        $model = $this->currentModel->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED]);
        if ($this->select) {
            $model->select($this->select);
        }
        if ($this->orderBy) {
            $model->orderBy($this->orderBy, $this->orderWay);
        }
        if ($this->start) {
            $model->skip((int)$this->start);
        }
        if ($this->length) {
            $model->take((int)$this->length);
        }
        $rows = $model->get($this->select);
        $data = [];
        foreach ($rows as $key => $object) {
            foreach ($this->columns as $field => $params) {
                if (isset($params['callBack'])) {
                    $str = $this->{$params['callBack']}($object, $field);
                } else {
                    $str = $object->{$field};
                }
                $data[$key][] = $str;
            }
            $data[$key][] = $this->addRowActions($object);
        }

        return $data;
    }

    /**
     * Print Price
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $field
     *
     * @return string
     */
    protected function printPrice($model, $field)
    {
        $ticket = $this->currentModel->where('id', $model->id)->first();

        return $ticket->line->order->offered ? 'Ticket offert' : \Tools::displayPrice($model->{$field});
    }

}
