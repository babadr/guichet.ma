<?php

namespace App\Http\Controllers\Backend\Store;

use App\Http\Controllers\BackendController;
use App\Models\Store\Profiteer;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Models\Store\Order;
use App\Models\Deal\Deal;

class ProfiteerController extends BackendController
{

    use Datatableable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['profiteers.*', 'orders.reference'];

    /**
     * Join associated models
     *
     * @var array
     */
    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['order_lines', 'order_lines.id', '=', 'profiteers.order_line_id'],
        ['orders', 'orders.id', '=', 'order_lines.order_id'],
        ['offers', 'offers.id', '=', 'order_lines.offer_id']
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'Gestion des bénéficiaires';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'profiteers.id',
            'prefix' => 'profiteers.'
        ],
        'reference' => [
            'title' => 'Référence commande',
            'type' => 'text',
            'filterKey' => 'reference',
            'orderKey' => 'orders.reference',
            'prefix' => 'orders.'
        ],
        'full_name' => [
            'title' => 'profiteer.full_name',
            'type' => 'text',
            'filterKey' => 'full_name',
            'orderKey' => 'profiteers.full_name',
            'prefix' => 'profiteers.',
        ],
        'cin' => [
            'title' => 'profiteer.cin',
            'type' => 'text',
            'list' => 'cities',
            'filterKey' => 'cin',
            'orderKey' => 'profiteers.cin',
            'prefix' => 'profiteers.'
        ],
        'is_minor' => [
            'title' => 'profiteer.is_minor',
            'type' => 'bool',
            'filterKey' => 'is_minor',
            'callBack' => 'printBoolean',
            'orderKey' => 'profiteers.is_minor',
            'prefix' => 'profiteers.'
        ],
        'created_at' => [
            'title' => 'profiteer.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'profiteers.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'profiteers.'
        ],
    ];


    /**
     * @param Request $request
     * @param Profiteer $profiteer
     */
    public function __construct(Request $request, Profiteer $profiteer)
    {
        $this->viewPrefix = 'store.profiteer';
        parent::__construct($request, $profiteer);
    }

    public function index($event_id)
    {
        $deal = (new Deal)->find($event_id);
        if (!$deal) {
            abort(404);
        }

        $this->conditions['offers.deal_id'] = $event_id;

        if ($this->currentRequest->ajax()) {
            return $this->createAjaxResponse();
        }

        $this->breadcrumbs->addCrumb(trans($this->listTitle, ['name' => $deal->title]), '/');

        return view('backend.contents.' . $this->viewPrefix . '.index')->with('columns', $this->columns);
    }


    public function edit($event_id, $id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans('profiteer.not_found'));

            return redirect(route('backend.match.profiteers.index', ['event_id' => $event_id]));
        }

        $this->breadcrumbs->addCrumb('Gestion des Matchs', route('backend.match.profiteers.index', ['event_id' => $event_id]));
        $this->breadcrumbs->addCrumb(trans('profiteer.edit', ['name' => $model->cin]));

        $view = $this->getView('backend.contents.' . $this->viewPrefix . '.form');

        return $view->with('title', trans('profiteer.edit', ['name' => $model->cin]))
            ->with('route', route('backend.match.profiteers.update', ['event_id' => $event_id, 'id' => $id]))
            ->with('method', 'post')
            ->with('event_id', $event_id)
            ->with('model', $model);
    }

    public function update($event_id, $id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans('profiteer.not_found'));

            return redirect()->back();
        }

        $attributes = $this->currentRequest->all();
        $attributes['is_minor'] = isset($attributes['is_minor']) ? true : false;
        $attributes['order_line_id'] = $model->order_line_id;

        $validator = $model->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans('profiteer.cannot_save'));

            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }

        if ($model->update($attributes)) {
            flash()->success(trans('profiteer.updated'));
            if (array_key_exists('saveandcontinue', $attributes)) {
                return redirect(route('backend.match.profiteers.edit', ['event_id' => $event_id, 'id' => $id]));
            } else {
                return redirect(route('backend.match.profiteers.index', ['event_id' => $event_id]));
            }
        }
    }

    /**
     * Create ajax response
     *
     * @return \Illuminate\Http\Response
     */
    protected function createAjaxResponse()
    {
        $request = $this->currentRequest->all();
        $this->initFilter($request);
        $this->createOrder($request);
        $this->conditions = array_merge($this->conditions, $this->customConditions());
        $modelTotal = $this->currentModel;

        $recordsTotal = $modelTotal
            ->join('order_lines', 'order_lines.id', '=', 'profiteers.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();

        $this->createConditions($request);

        $modelFilter = $this->currentModel;

        $recordsFiltered = $modelFilter->join('order_lines', 'order_lines.id', '=', 'profiteers.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();

        $output = [
            'data' => $this->buildRows(),
            'draw' => $this->draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
        ];

        return response()->json($output);
    }

    /**
     * Build listing rows
     *
     * @return array
     */
    protected function buildRows()
    {
        //$this->initRows();
        $model = $this->currentModel->join('order_lines', 'order_lines.id', '=', 'profiteers.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED]);

        if ($this->select) {
            $model->select($this->select);
        }
        if ($this->orderBy) {
            $model->orderBy($this->orderBy, $this->orderWay);
        }
        if ($this->start) {
            $model->skip((int)$this->start);
        }
        if ($this->length) {
            $model->take((int)$this->length);
        }
        $rows = $model->get($this->select);
        //$rows = $this->currentModel->get($this->select);
        $data = [];
        foreach ($rows as $key => $object) {
            foreach ($this->columns as $field => $params) {
                if (isset($params['callBack'])) {
                    $str = $this->{$params['callBack']}($object, $field);
                } else {
                    $str = $object->{$field};
                }
                $data[$key][] = $str;
            }
            $data[$key][] = $this->addRowActions($object);
        }

        return $data;
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.store.profiteer.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

}
