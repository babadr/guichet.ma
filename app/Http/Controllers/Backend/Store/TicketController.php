<?php

namespace App\Http\Controllers\Backend\Store;

use App\Http\Controllers\BackendController;
use App\Models\Store\Ticket;
use App\Models\Store\Order;
use App\Models\Deal\Deal;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use PDF;

class TicketController extends BackendController
{

    use Datatableable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = [];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['order_lines', 'order_lines.id', '=', 'tickets.order_line_id'],
        ['orders', 'orders.id', '=', 'order_lines.order_id'],
        ['offers', 'offers.id', '=', 'order_lines.offer_id'],
        ['deals', 'deals.id', '=', 'offers.deal_id'],
        ['users', 'users.id', '=', 'orders.user_id'],
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'ticket.manage_tickets';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'tickets.id',
            'prefix' => 'tickets.'
        ],
        'number' => [
            'title' => 'ticket.number',
            'type' => 'text',
            'filterKey' => 'number',
            'orderKey' => 'tickets.number',
            'prefix' => 'tickets.'
        ],
        'hash' => [
            'title' => 'Code',
            'type' => 'text',
            'filterKey' => 'hash',
            'orderKey' => 'tickets.hash',
            'prefix' => 'tickets.'
        ],
        'seat' => [
            'title' => 'ticket.seat',
            'type' => 'text',
            'filterKey' => 'seat',
            'orderKey' => 'tickets.seat',
            'prefix' => 'tickets.'
        ],
        'full_name' => [
            'title' => 'ticket.full_name',
            'type' => 'text',
            'filterKey' => 'last_name',
            'orderKey' => 'full_name',
            'prefix' => 'users.'
        ],
        'email' => [
            'title' => 'ticket.email',
            'type' => 'text',
            'filterKey' => 'email',
            'orderKey' => 'email',
            'prefix' => 'users.'
        ],
        'title' => [
            'title' => 'ticket.offer',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'offers.title',
            'prefix' => 'offers.'
        ],
        'product_price' => [
            'title' => 'ticket.product_price',
            'type' => 'text',
            'filterKey' => 'product_price',
            'orderKey' => 'order_lines.product_price',
            'callBack' => 'printPrice',
            'prefix' => 'order_lines.'
        ],
        'created_at' => [
            'title' => 'order.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'orders.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'orders.'
        ],
        'status' => [
            'title' => 'ticket.status',
            'type' => 'select',
            'list' => 'status',
            'filterKey' => 'status',
            'orderKey' => 'tickets.status',
            'callBack' => 'printStatus',
            'prefix' => 'tickets.'
        ],
    ];


    /**
     * @param Request $request
     * @param Ticket $ticket
     */
    public function __construct(Request $request, Ticket $ticket)
    {
        $this->viewPrefix = 'store.ticket';
        $this->select = [
            'tickets.id',
            'tickets.seat',
            'tickets.hash',
            'tickets.status',
            'tickets.number',
            'order_lines.product_price',
            'orders.created_at',
            'offers.title',
            \DB::raw('CONCAT(last_name, " ", first_name) AS full_name'),
            'users.email'
        ];
        parent::__construct($request, $ticket);
    }

    /**
     * Display a listing
     *
     * @param int $deal_id
     * @param int $partner_id
     * @return \Illuminate\Http\Response
     */
    public function index($deal_id, $partner_id = null)
    {
        $user = auth()->user();
        $deal = (new Deal)->find($deal_id);
        if (!$deal) {
            abort(404);
        }
        if ($user->hasRole('partner') || $partner_id) {
            $partner_id = $partner_id ? $partner_id : $user->provider->id;
            if ($deal->provider_id != $partner_id) {
                abort(404);
            }
        }
        $this->conditions['deals.id'] = $deal_id;
        $this->conditions['deals.provider_id'] = $partner_id;

        if ($this->currentRequest->ajax()) {
            return $this->createAjaxResponse();
        }

        $this->breadcrumbs->addCrumb(trans($this->listTitle, ['name' => $deal->title]), '/');

        return view('backend.contents.' . $this->viewPrefix . '.index')
            ->with('deal', $deal)
            ->with('columns', $this->columns)
            ->with('partner_id', $partner_id)
            ->with('status', [0 => 'En attende', 1 => 'Validé']);
    }

    protected function addRowActions($model)
    {
        return view('backend.contents.store.ticket.actions')
            ->with('token', $model->hash)
            ->with('status', $model->status)
            ->render();
    }

    protected function printStatus($object, $field)
    {
        return view('backend.contents.store.ticket.status')
            ->with('model', $object)
            ->with('field', $field)->render();
    }

    /**
     * Create ajax response
     *
     * @return \Illuminate\Http\Response
     */
    protected function createAjaxResponse()
    {
        $request = $this->currentRequest->all();
        $this->initFilter($request);
        $this->createOrder($request);
        $this->conditions = array_merge($this->conditions, $this->customConditions());
        $modelTotal = $this->currentModel;
        $recordsTotal = $modelTotal
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();

        $this->createConditions($request);
        $modelFilter = $this->currentModel;
        $recordsFiltered = $modelFilter->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->count();
        //$this->currentModel = $this->currentModel->where($this->conditions);
        //$this->initJoins();
        //$recordsFiltered = $modelFilter->where($this->conditions)->count();
        $output = [
            'data' => $this->buildRows(),
            'draw' => $this->draw,
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
        ];

        return response()->json($output);
    }

    /**
     * Build listing rows
     *
     * @return array
     */
    protected function buildRows()
    {
        //$this->initRows();
        $model = $this->currentModel->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->where($this->conditions)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED]);
        if ($this->select) {
            $model->select($this->select);
        }
        if ($this->orderBy) {
            $model->orderBy($this->orderBy, $this->orderWay);
        }
        if ($this->start) {
            $model->skip((int)$this->start);
        }
        if ($this->length) {
            $model->take((int)$this->length);
        }
        $rows = $model->get($this->select);
        //$rows = $this->currentModel->get($this->select);
        $data = [];
        foreach ($rows as $key => $object) {
            foreach ($this->columns as $field => $params) {
                if (isset($params['callBack'])) {
                    $str = $this->{$params['callBack']}($object, $field);
                } else {
                    $str = $object->{$field};
                }
                $data[$key][] = $str;
            }
            $data[$key][] = $this->addRowActions($object);
        }

        return $data;
    }

    public function update($token)
    {
        $model = $this->currentModel->where('hash', $token)->first();
        if (empty($model) || !in_array($model->line->order->status, [2, 3, 5])) {
            return response()->json(['success' => false, 'msg' => 'Le ticket demandé est introuvable.']);
        }

        if ($model->status == 1) {
            return response()->json(['success' => false, 'msg' => 'Ce ticket a déjà été validé.']);
        }

        $model->update(['status' => 1]);

        return response()->json(['success' => true, 'msg' => 'Validation effectuée avec succès.']);
    }

    /**
     * Print Price
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @param string $field
     *
     * @return string
     */
    protected function printPrice($model, $field)
    {
        $ticket = $this->currentModel->where('id', $model->id)->first();

        return $ticket->line->order->offered ? 'Ticket offert' : \Tools::displayPrice($model->{$field});
    }

    /*
     * Download Ticket
     */
    public function download($id, $ticket_id = null)
    {
        /** @var Order $order */
        $order = (new Order)->findOrFail($id);
        if (!auth()->user()->hasRole(['superadmin', 'admin', 'seller'])) {
            abort(404);
        }
        if (auth()->user()->hasRole(['seller']) && auth()->user()->seller->id !== $order->seller_id) {
            abort(404);
        }
        if ($order->status == Order::STATUS_ACCEPTED || $order->status == Order::STATUS_CAPTURED) {
            $isOffile = true;
            foreach($order->lines as $line) {
                if ($line->offer->deal->category_id == 15 || $line->offer->deal->is_football || $line->offer->deal->barcode_type) {
                    $isOffile = false;
                    break;
                }
            }
            if ($isOffile) {
                $pdf = PDF::loadView('frontend.partials.ticket', ['order' => $order, 'isOffile' => $isOffile, 'ticket_id' => $ticket_id]);
            } else {
                config(['pdf.format' => 'A4']);
                $pdf = PDF::loadView('frontend.partials.ticket_online', ['order' => $order, 'isOffile' => $isOffile, 'ticket_id' => $ticket_id]);
            }

            return $pdf->download('guichet.ma-tickets-' . $order->reference . '.pdf');
        }

        abort(404);
    }

    /*
     * Download Ticket
     */
    public function downloadFront($id)
    {
        /** @var Order $order */
        $order = (new Order)->findOrFail($id);
        if (!auth()->user()->hasRole(['superadmin', 'admin', 'seller'])) {
            abort(404);
        }
        if (auth()->user()->hasRole(['seller']) && auth()->user()->seller->id !== $order->seller_id) {
            abort(404);
        }
        if ($order->status == Order::STATUS_ACCEPTED || $order->status == Order::STATUS_CAPTURED) {
            config(['pdf.format' => 'A4']);
            $pdf = PDF::loadView('frontend.partials.ticket_online', ['order' => $order, 'isOffile' => false, 'ticket_id' => null]);

            return $pdf->download('guichet.ma-tickets-' . $order->reference . '.pdf');
        }

        abort(404);
    }

    /**
     * Handle delete request
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $ticket = $this->currentModel->find($id);
        if ($ticket == null) {
            return response()->json(['success' => false, 'msg' => trans('app.not_found')]);
        }

        $line = $ticket->line;
        $order = $line->order;
        $seat = $ticket->seat;

        //if (auth()->user()->hasRole(['superadmin', 'admin']) && ($order->seller_id || empty($order->user_id)) && !$ticket->status && $line->quantity > 1) {
        if (auth()->user()->hasRole(['superadmin', 'admin']) && !$ticket->status && $line->quantity > 1) {
            $result = $ticket->delete();
            if ($result) {
                $line->quantity =  $line->quantity - 1;
                $line->total_price =  $order->offered ? 0.00 : $line->quantity * $line->product_price;
                if ($seat) {
                    $line->seats = str_replace(',' . $seat, '', $line->seats);
                    $line->seats = str_replace($seat . ',', '', $line->seats);
                }
                if($line->save()) {
                    $order->total_paid = $order->offered ? 0.00 : $order->total_paid -  $line->product_price;
                    $order->save();
                }
                flash()->success('Le ticket ' . $ticket->number . ' a bien été supprimé avec succès.');
            }

            return response()->json(['success' => $result, 'msg' => trans($result ? 'app.delete_success' : 'app.cannot_delete')]);
        }

        return response()->json(['success' => false, 'msg' => trans('Impossible de supprimer ce ticket')]);
    }

}
