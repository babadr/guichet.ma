<?php

namespace App\Http\Controllers\Backend\Store;

use App\Http\Controllers\BackendController;
use App\Models\Store\Order;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Services\TicketService;
use App\Services\SmsService;
use App\Services\MailService;
use App\Models\Deal\Deal;
use App\Models\Deal\Offer;
use App\Models\Acl\User;
use App\Models\Store\OrderLine;
use App\Models\Store\Profiteer;

class OrderController extends BackendController
{

    use Datatableable;

    /**
     * The TicketService instance.
     *
     * @var TicketService
     */
    public $ticketService;

    /**
     * The SmsService instance.
     *
     * @var SmsService
     */
    public $smsService;

    /**
     * The MailService instance.
     *
     * @var MailService
     */
    public $mailService;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['orders.*', 'users.first_name', 'users.email'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $leftJoins = [
        ['users', 'users.id', '=', 'orders.user_id'],
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'reference';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'order.manage_orders';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'orders.id',
            'prefix' => 'orders.'
        ],
        'reference' => [
            'title' => 'order.reference',
            'type' => 'text',
            'filterKey' => 'reference',
            'orderKey' => 'orders.reference',
            'prefix' => 'orders.'
        ],
        'full_name' => [
            'title' => 'order.user',
            'type' => 'text',
            'filterKey' => 'last_name',
            'orderKey' => 'users.last_name',
            'prefix' => 'users.'
        ],
        'email' => [
            'title' => 'order.email',
            'type' => 'text',
            'filterKey' => 'email',
            'orderKey' => 'users.email',
            'prefix' => 'users.'
        ],
        'total_paid' => [
            'title' => 'order.total_paid',
            'type' => 'text',
            'filterKey' => 'total_paid',
            'orderKey' => 'orders.total_paid',
            'callBack' => 'priceFormat',
            'prefix' => 'orders.'
        ],
        'payment_method' => [
            'title' => 'order.payment_method',
            'type' => 'select',
            'list' => 'payments',
            'filterKey' => 'payment_method',
            'orderKey' => 'orders.payment_method',
            'callBack' => 'printOrderPayment',
            'prefix' => 'orders.'
        ],
        'status' => [
            'title' => 'order.status',
            'type' => 'select',
            'list' => 'status',
            'filterKey' => 'status',
            'orderKey' => 'orders.status',
            'callBack' => 'printOrderStatus',
            'prefix' => 'orders.'
        ],
        'created_at' => [
            'title' => 'order.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'orders.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'orders.'
        ],
    ];


    /**
     * @param Request $request
     * @param Order $order
     * @param TicketService $ticketService
     * @param SmsService $smsService
     * @param MailService $mailService
     */
    public function __construct(Request $request, Order $order, TicketService $ticketService, SmsService $smsService, MailService $mailService)
    {
        $this->viewPrefix = 'store.order';
        $this->ticketService = $ticketService;
        $this->smsService = $smsService;
        $this->mailService = $mailService;

        parent::__construct($request, $order);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        $status = [];
        foreach (config('enums.order_status') as $key => $value) {
            $status[$key] = trans($value);
        }
        return [
            'columns' => $this->columns,
            'status' => $status,
            'payments' => [1 => 'FPay', 2 => 'PAYEXPRESS', 3 => 'Virement / Versement', 4 => 'Espèce'],
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.store.order.actions')
            ->with('order', $model)
            ->render();
    }

    /**
     * @param $object
     * @param $field
     * @return string
     */
    protected function priceFormat($object, $field)
    {
        return !$object->offered ? number_format($object->{$field}, 0, ',', ' ') . ' DH' : 'Commande offerte';
    }


    protected function printOrderStatus($object, $field)
    {
        return view('backend.contents.store.order.status')
            ->with('model', $object)
            ->with('field', $field)->render();
    }

    protected function printOrderPayment($object, $field)
    {
        $payment = '';
        if ($object->payment_method == 1) {
            $payment = 'Carte bancaire (FPay)';
        } elseif ($object->payment_method == 2) {
            $payment = 'PAYEXPRESS (Cash Plus)';
        } elseif ($object->payment_method == 3) {
            $payment = 'Virement / versement bancaire';
        } elseif ($object->payment_method == 4) {
            $payment = 'Espèce';
        }

        return $payment;
    }

    /**
     * Handle create request.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->breadcrumbs->addCrumb(trans($this->listTitle), route('backend.' . $this->modelName . '.index'));
        $this->breadcrumbs->addCrumb(trans($this->modelName . '.new'));

        $view = $this->getView('backend.contents.' . $this->viewPrefix . '.form');
        $deals = (new Deal)->select('id', 'title')
            ->where('active', '=', 1)
            ->whereRaw('DATE(end_at) >= CURDATE()')
            ->orderBy('created_at', 'desc');

        if (auth()->user()->hasRole(['seller'])) {
            $deals->where('provider_id', '=', auth()->user()->seller->provider_id);
        }

        return $view->with('title', trans($this->modelName . '.new'))
            ->with('route', route('backend.' . $this->modelName . '.store'))
            ->with('method', 'post')
            ->with('model', $this->currentModel)
            ->with('deals', $deals->get());
    }

    /**
     * Store a newly created resource
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->currentRequest->all();

        if (empty($attributes['quantity']) && empty($attributes['seats'])) {
            flash()->error('Veuillez sélectionner un produit.');
            return redirect()->back()->withInput($attributes);
        }
        $attributes['offered'] = isset($attributes['offered']) ? true : false;
        //$attributes['last_name'] = empty($attributes['last_name']) ? $this->generateRandomString() : $attributes['last_name'];
        //$attributes['first_name'] = empty($attributes['first_name']) ? $this->generateRandomString() : $attributes['first_name'];
        //$attributes['phone'] = empty($attributes['phone']) ? '0606060606' : $attributes['phone'];

//        $user = (new User)->where('email', '=', $attributes['email'])->first();
//        if ($user) {
//            $attributes['user_id'] = $user->id;
//        } else {
//            $user = new User();
//            $user->fill($attributes);
//            $user->save();
//            $user->attachRole(3);
//            $attributes['user_id'] = $user->id;
//            $this->mailService->sendUserWelcome($user);
//        }
        if (auth()->user()->hasRole(['seller'])) {
            $attributes['seller_id'] = auth()->user()->seller->id;
        }
        $created = false;
        if (isset($attributes['seats'])) {
            $orderedSeats = [];
            foreach ($attributes['seats'] as $id => $seat) {
                $offer = (new Offer)->where('id', $id)->where('active', true)->first();
                if (!$offer) {
                    flash()->error("L\'événement \"{$offer->deal->title}\" n'est plus disponible. Veuillez revoir votre commande.");
                    return redirect()->back()->withInput($attributes);
                }
                $qty = count($seat);
                if (!$offer->checkAvailability($qty)) {
                    flash()->error('La quantité demandée pour l\'offre "' . $offer->title . '" n\'est plus disponible en stock.');
                    return redirect()->back()->withInput($attributes);
                }

                $reservedSeats = $offer->getReservedSeats();
                foreach ($seat as $s) {
                    if (in_array($s, $reservedSeats)) {
                        flash()->error('La place "' . $s . '" n\'est plus disponible, Veuillez sélectionner une autre place.');
                        return redirect()->back()->withInput($attributes);
                    }
                    if (in_array($s, $orderedSeats)) {
                        flash()->error('Une erreur s\'est produite, veuillez réessayer à nouveau.');
                        return redirect()->back()->withInput($attributes);
                    }
                    $orderedSeats[] = $s;
                }
            }
            $created = true;
        } else {
            foreach ($attributes['quantity'] as $id => $quantity) {
                if ($quantity == 0) {
                    continue;
                }
                $created = true;
                $offer = (new Offer)->where('id', $id)->where('active', true)->first();
                if (!$offer || $offer->deal->end_at->isPast()) {
                    flash()->error("L\'événement \"{$offer->deal->title}\" n'est plus disponible. Veuillez revoir votre commande.");
                    return redirect()->back()->withInput($attributes);
                }
                if (!$offer->checkAvailability($quantity)) {
                    flash()->error("L\'événement \"{$offer->deal->title}\" n'est plus disponible en stock. Veuillez revoir votre commande.");
                    return redirect()->back()->withInput($attributes);
                }
            }
        }

        if (!$created) {
            flash()->error('Veuillez renseigner la quantité.');
            return redirect()->back()->withInput($attributes);
        }
        $attributes['reference'] = $this->currentModel->generateReference();
        $attributes['address'] = '';//$user->address . ', ' . $user->city . ' - ' . $user->country;
        $attributes['status'] = Order::STATUS_CAPTURED;
        $attributes['comment'] = '';
        $attributes['payment_method'] = 4;
        $attributes['payment_transaction_date'] = date('Y-m-d H:i:s');
        $this->currentModel->fill($attributes);
        if ($this->currentModel->save()) {
            $total = 0.00;

            if (isset($attributes['seats'])) {
                foreach ($attributes['seats'] as $id => $seat) {
                    $quantity = count($seat);
                    $offer = (new Offer)->where('id', $id)->where('active', true)->first();
                    $price = empty($offer->reservation_price) ? $offer->price : $offer->reservation_price;
                    $total += $price * $quantity;
                    $line = new OrderLine([
                        'offer_id' => $id,
                        'product_name' => $offer->deal->title,
                        'offer_name' => $offer->title,
                        'product_price' => $price,
                        'quantity' => $quantity,
                        'total_price' => $attributes['offered'] ? 0.00 : $price * $quantity,
                        'seats' => implode(',', $seat),
                    ]);
                    $this->currentModel->lines()->save($line);
                }
            } else {
                foreach ($attributes['quantity'] as $id => $quantity) {
                    if ($quantity == 0) {
                        continue;
                    }
                    $offer = (new Offer)->where('id', $id)->where('active', true)->first();
                    $price = empty($offer->reservation_price) ? $offer->price : $offer->reservation_price;
                    $total += $price * $quantity;
                    $line = new OrderLine([
                        'offer_id' => $id,
                        'product_name' => $offer->deal->title,
                        'offer_name' => $offer->title,
                        'product_price' => $price,
                        'quantity' => $quantity,
                        'total_price' => $attributes['offered'] ? 0.00 : $price * $quantity,
                    ]);
                    $this->currentModel->lines()->save($line);
                }
            }

            $this->currentModel->total_paid = $attributes['offered'] ? 0.00 : $total;
            $this->currentModel->update();
        }
        $this->ticketService->generate($this->currentModel->id);
        //$this->mailService->sendOrderConfirmation($this->currentModel);
        //$this->smsService->sendOrderConfirmation($this->currentModel);
        flash()->success('Nouvelle commande créée avec succès.');
        return redirect()->route('backend.order.show', $this->currentModel->id);
    }

    /**
     * Add new deal to cart
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function addDeal($id)
    {
        $deal = (new Deal)
            ->where('active', '=', 1)
            ->where('id', '=', $id)
            ->first();

        if (!$deal) {
            return response()->json(['success' => false, 'msg' => 'L\'événement choisi n\'existe pas.']);
        }

        $offers = (new Offer)
            ->where('active', '=', 1)
            ->where('deal_id', '=', $id)
            ->get();

        if (!empty($deal->seating_plan_id)) {
            $seat_offers = [];
            $items = [];
            $rows = [0 => 'a', 1 => 'b', 2 => 'c', 3 => 'd', 4 => 'e', 5 => 'f', 6 => 'g', 7 => 'h', 8 => 'i', 9 => 'j', 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p'];
            $results = $deal->offers()->where('active', true)->orderBy('price', 'desc')->get();;
            foreach ($results as $key => $offer) {
                $seat_offers[$rows[$key]]['price'] = number_format($offer->price, 0);
                $seat_offers[$rows[$key]]['classes'] = 'color_' . ($key + 1);
                $seat_offers[$rows[$key]]['category'] = $offer->title;
                $seat_offers[$rows[$key]]['offer_id'] = $offer->id;
                array_push($items, [$rows[$key], 'available', $offer->title . ' (' . \Tools::displayPrice($offer->price, 0) . ')']);
            }

            array_push($items, [$rows[$offers->count()], 'unavailable', 'Vendue']);
            array_push($items, [$rows[$offers->count() + 1], 'selected', 'Sélectionnée']);

            return response()->json([
                'success' => true,
                'hasPlan' => true,
                'items' => $items,
                'seat_offers' => $seat_offers,
                'reservedSeats' => $deal->getReservedSeats(),
                'rows' => config('enums.seating_rows')[$deal->seating_plan_id],
                'mapCharts' => config('enums.seating_map')[$deal->seating_plan_id],
                'html' => view('backend.contents.store.order.seats')
                    ->with('deal', $deal)
                    ->with('offers', $offers)
                    ->render()
            ]);
        }

        return response()->json([
            'success' => true,
            'hasPlan' => false,
            'html' => view('backend.contents.store.order.offers')->with('deal', $deal)->with('offers', $offers)->render()
        ]);
    }

    public function show($id)
    {
        $order = $this->currentModel->findOrFail($id);
        $this->breadcrumbs->addCrumb(trans($this->listTitle), route('backend.' . $this->modelName . '.index'));
        $this->breadcrumbs->addCrumb(trans('order.show', ['reference' => $order->reference]));
        
        return view('backend.contents.store.order.show')
            ->with('order', $order)
            ->with('status', config('enums.order_status'))
            ->with('payments', [1 => 'FPay', 2 => 'PAYEXPRESS', 3 => 'Virement / Versement', 4 => 'Espèce']);
    }

    public function update($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans('Impossible de retrouver cette commande.'));
            return redirect()->back();
        }
        $attributes = $this->currentRequest->all();
        $attributes['payment_transaction_date'] = date('Y-m-d H:i:s');

        if ($model->update($attributes)) {
            if (in_array($model->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED])) {
                $this->ticketService->generate($id);
                $this->mailService->sendOrderConfirmation($model);
                $this->mailService->sendOrderTickets($model);
                $this->smsService->sendOrderConfirmation($model);
                foreach ($model->lines as $line) {
                    if ($line->offer->deal->category_id == 17 && $line->recipients->count()) {
                        foreach ($line->recipients as $recipient) {
                            $this->mailService->sendNewSubscription($recipient, $line);
                        }
                    }
                }
            }
            flash()->success(trans('Commande mise à jour avec succès.'));
            return redirect(route('backend.order.show', $id));
        }
    }

    /**
     * Handle delete request
     *
     * @param int $id
     *
     * @return void
     */
    public function delete($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            return response()->json(['success' => false, 'msg' => trans('app.not_found')]);
        }

        $result = $model->forceDelete();

        return response()->json(['success' => $result, 'msg' => trans($result ? 'app.delete_success' : 'app.cannot_delete')]);
    }

    /**
     * Create custom conditions
     *
     * @return array
     */
    protected function customConditions()
    {
        if (auth()->user()->hasRole(['seller'])) {
            return [
                ['orders.seller_id', '=', auth()->user()->seller->id]
            ];
        }

        return [];
    }

    private function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x = 'abcdefghijklmnopqrstuvwxyz', ceil($length / strlen($x)))), 1, $length);
    }

    public function editProfiteers($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans('Impossible de retrouver cette commande.'));
            return redirect()->route('backend.order.index');
        }

        $this->breadcrumbs->addCrumb(trans($this->listTitle), route('backend.' . $this->modelName . '.index'));
        $this->breadcrumbs->addCrumb('Bénéficiaires de la commande :  #Ref. ' . $model->reference);

        $lines = [];
        foreach ($model->lines as $line) {
            $lines[$line->id]['total'] = $line->quantity;
            $lines[$line->id]['offer'] = $line->offer_name;
            $lines[$line->id]['offer_id'] = $line->offer_id;
            $lines[$line->id]['profiteers'] = $line->profiteers->toArray();
        }

        return view('backend.contents.store.order.profiteers')->with('order', $model)->with('lines', $lines);
    }

    public function updateProfiteers($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans('Impossible de retrouver cette commande.'));
            return redirect()->route('backend.order.index');
        }

        $attributes = $this->currentRequest->all();
        if (isset($attributes['rows']) && is_array($attributes['rows'])) {
            $ids = $model->lines->pluck('id')->toArray();
            foreach ($attributes['rows'] as $id => $row) {
                if (in_array($id, $ids)) {
                    $line = OrderLine::find($id);
                    $line->profiteers()->delete();
                    foreach ($row as $values) {
                        if (!is_array($values)) {
                            continue;
                        }
                        $profiteer = new Profiteer;
                        $profiteer->order_line_id = $line->id;
                        $profiteer->full_name = $values['full_name'];
                        if (isset($values['cin'])) {
                            $profiteer->cin = $values['cin'];
                        }
                        if (isset($values['is_minor'])) {
                            $profiteer->is_minor = true;
                        }
                        $profiteer->save();
                    }
                }
            }
        }

        flash()->success('Bénéficiaires enregistrés avec succès.');

        return redirect()->back();
    }
}
