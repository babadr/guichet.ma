<?php

namespace App\Http\Controllers\Backend\Store;

use App\Http\Controllers\BackendController;
use App\Models\Store\Promotion;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;
use App\Models\Deal\Category;
use App\Models\Deal\Deal;
use Carbon\Carbon;

class PromotionController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['promotions.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'name';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'promotion.manage_promotions';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'promotions.id',
            'prefix' => 'promotions.'
        ],
        'name' => [
            'title' => 'promotion.name',
            'type' => 'text',
            'filterKey' => 'name',
            'orderKey' => 'promotions.name',
            'prefix' => 'promotions.'
        ],
        'code' => [
            'title' => 'promotion.code',
            'type' => 'text',
            'filterKey' => 'code',
            'orderKey' => 'promotions.code',
            'prefix' => 'promotions.'
        ],
        'typeable_type' => [
            'title' => 'promotion.typeable_type',
            'type' => 'select_string',
            'list' => 'typeable_types',
            'filterKey' => 'typeable_type',
            'orderKey' => 'promotions.typeable_type',
            'callBack' => 'printTypeable',
            'prefix' => 'promotions.'
        ],
        'type' => [
            'title' => 'promotion.type',
            'type' => 'select',
            'list' => 'types',
            'filterKey' => 'type',
            'orderKey' => 'promotions.type',
            'prefix' => 'promotions.',
            'callBack' => 'printType',
        ],
        'active' => [
            'title' => 'promotion.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'promotions.active',
            'callBack' => 'printStatus',
            'prefix' => 'promotions.'
        ],
        'end_at' => [
            'title' => 'promotion.expires_on',
            'type' => 'date',
            'filterKey' => 'end_at',
            'orderKey' => 'promotions.end_at',
            'callBack' => 'printDate',
            'prefix' => 'promotions.'
        ],
        'created_at' => [
            'title' => 'promotion.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'promotions.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'promotions.'
        ],
    ];


    /**
     * @param Request $request
     * @param Promotion $promotion
     */
    public function __construct(Request $request, Promotion $promotion)
    {
        $this->viewPrefix = 'store.promotion';

        parent::__construct($request, $promotion);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        $types = [];
        foreach (Promotion::$types as $key => $type) {
            $types[$key] = trans($type);
        }

        $typeable_types = [];
        foreach (Promotion::$morphs as $key => $morph) {
            $typeable_types[$key] = trans($morph);
        }

        $categories = (new Category())->select(['id', 'title'])->where('active', 1);
        $events = (new Deal())->select(['id', 'title'])->where('active', 1)->where('expired', '=', 0);

        return [
            'columns' => $this->columns,
            'types' => $types,
            'typeable_types' => $typeable_types,
            'categories' => $categories->get()->pluck('title', 'id'),
            'events' => $events->get()->pluck('title', 'id'),
        ];
    }

    /**
     * Before Save promotion
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();

        $attributes['active'] = isset($attributes['active']) ? true : false;
        $attributes['start_at'] = Carbon::createFromFormat('d/m/Y', $attributes['start_at'])->toDateString();
        $attributes['end_at'] = Carbon::createFromFormat('d/m/Y', $attributes['end_at'])->toDateString();

        if (!empty($attributes['percentage_discount'])) {
            $attributes['discount'] = $attributes['percentage_discount'];
        } elseif (!empty($attributes['amount_discount'])) {
            $attributes['discount'] = $attributes['amount_discount'];
        }

        if (!empty($attributes['category_id'])) {
            $attributes['typeable_id'] = $attributes['category_id'];
        } elseif (!empty($attributes['event_id'])) {
            $attributes['typeable_id'] = $attributes['event_id'];
        }

        return $attributes;
    }

    /**
     * Create custom conditions
     *
     * @return array
     */
    protected function customConditions()
    {
        return [];
    }

    protected function printType($model, $field)
    {
        return trans(Promotion::$types[$model->type]);
    }

    protected function printTypeable($model, $field)
    {
        $content = trans(Promotion::$morphs[$model->typeable_type]);
        $content .= '<ul><li>' . $model->typeable->title . '</li></ul>';

        return $content;
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.store.promotion.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

}
