<?php

namespace App\Http\Controllers\Backend\Deal;

use App\Http\Controllers\BackendController;
use App\Models\Deal\Provider;
use App\Models\Deal\Seller;
use App\Models\Acl\User;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class SellerController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['sellers.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['providers', 'providers.id', '=', 'sellers.provider_id']
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'seller.manage_sellers';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'sellers.id',
            'prefix' => 'sellers.'
        ],
        'title' => [
            'title' => 'seller.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'sellers.title',
            'prefix' => 'sellers.'
        ],
        'provider_name' => [
            'title' => 'seller.provider_id',
            'type' => 'select',
            'list' => 'providers',
            'filterKey' => 'provider_id',
            'orderKey' => 'providers.title',
            'prefix' => 'sellers.'
        ],
        'created_at' => [
            'title' => 'seller.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'sellers.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'sellers.'
        ],
        'active' => [
            'title' => 'seller.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'sellers.active',
            'callBack' => 'printStatus',
            'prefix' => 'sellers.'
        ]
    ];


    /**
     * @param Request $request
     * @param Seller $seller
     */
    public function __construct(Request $request, Seller $seller)
    {
        $this->viewPrefix = 'deal.seller';
        parent::__construct($request, $seller);
    }

    /**
     * Store a newly created resource
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->beforeSave();
        $validator = $this->currentModel->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans($this->modelName . '.cannot_save'));
            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }
        $user = new User();
        $user->fill($attributes);
        if ($user->save()) {
            $user->attachRole(7);
            $attributes['user_id'] = $user->id;
        }
        $this->currentModel->fill($attributes);
        if ($this->currentModel->save()) {
            $this->afterSave($attributes, $this->currentModel);
            flash()->success(trans($this->modelName . '.new_created'));
            if (array_key_exists('saveandcontinue', $attributes)) {
                return redirect(route('backend.' . $this->modelName . '.edit', $this->currentModel->id));
            } else {
                return redirect(route('backend.' . $this->modelName . '.index'));
            }
        }
    }

    /**
     * Update the specified resource
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect()->back();
        }
        $attributes = $this->beforeSave($id);
        $validator = $model->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans($this->modelName . '.cannot_save'));
            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }
        $user = $model->user;
        if (empty($user)) {
            $user = new User();
            $user->fill($attributes);
            if ($user->save()) {
                $user->attachRole(7);
                $attributes['user_id'] = $user->id;
            }
        } else {
            $user->update($attributes);
        }

        if ($model->update($attributes)) {
            $this->afterSave($attributes, $model);
            flash()->success(trans($this->modelName . '.updated'));
            if (array_key_exists('saveandcontinue', $attributes)) {
                return redirect(route('backend.' . $this->modelName . '.edit', $id));
            } else {
                return redirect(route('backend.' . $this->modelName . '.index'));
            }
        }
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
            'providers' => (new Provider)->where('active', '=', 1)->select(['id', 'title'])->get()->pluck('title', 'id'),
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.deal.seller.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Before Save provider
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();
        $attributes['active'] = isset($attributes['active']) ? true : false;

        return $attributes;
    }

}
