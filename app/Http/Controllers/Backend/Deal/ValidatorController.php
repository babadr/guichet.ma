<?php

namespace App\Http\Controllers\Backend\Deal;

use App\Http\Controllers\BackendController;
use App\Models\Deal\Provider;
use App\Models\Store\Ticket;

class ValidatorController extends BackendController
{

    public function index()
    {
        $provider = Provider::find(auth()->user()->provider_id);
        $deals = $provider->deals;
        return view('backend.contents.validator.events')
            ->with('deals', $deals)
            ->with('provider', $provider);
    }

    public function update($token)
    {
        $ticket = Ticket::where('hash', $token)->orWhere('number', '#' . $token)->first();
        if (empty($ticket)) {
            return response()->json(['success' => false, 'msg' => 'Le ticket demandé est introuvable.']);
        }
        $provider = $ticket->line->offer->deal->provider;
        if ($provider->id != auth()->user()->provider_id) {
            return response()->json(['success' => false, 'msg' => 'Le ticket demandé est introuvable.']);
        }
        if ($ticket->status == 1) {
            return response()->json(['success' => false, 'msg' => 'Ce ticket a déjà été validé.']);
        }

        $ticket->update(['status' => 1]);

        return response()->json(['success' => true, 'msg' => 'Validation effectuée avec succès.']);
    }
}
