<?php

namespace App\Http\Controllers\Backend\Deal;

use App\Enums\Category\Color;
use App\Http\Controllers\BackendController;
use App\Models\Deal\Category;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class CategoryController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['categories.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'category.manage_categories';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'categories.id',
            'prefix' => 'categories.'
        ],
        'title' => [
            'title' => 'category.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'categories.title',
            'prefix' => 'categories.'
        ],
        'parent_id' => [
            'title' => 'category.parent_id',
            'type' => 'select',
            'list' => 'categories',
            'callBack' => 'showCategory',
            'filterKey' => 'parent_id',
            'orderKey' => 'categories.parent_id',
            'prefix' => 'categories.'
        ],
        'show_on_menu' => [
            'title' => 'category.show_on_menu',
            'type' => 'bool',
            'filterKey' => 'show_on_menu',
            'orderKey' => 'categories.show_on_menu',
            'callBack' => 'printStatus',
            'prefix' => 'categories.'
        ],
        'show_on_home' => [
            'title' => 'category.show_on_home',
            'type' => 'bool',
            'filterKey' => 'show_on_home',
            'orderKey' => 'categories.show_on_home',
            'callBack' => 'printStatus',
            'prefix' => 'categories.'
        ],
        'active' => [
            'title' => 'category.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'categories.active',
            'callBack' => 'printStatus',
            'prefix' => 'categories.'
        ]
    ];


    /**
     * @param Request $request
     * @param Category $category
     */
    public function __construct(Request $request, Category $category)
    {
        $this->viewPrefix = 'deal.category';
        $this->seo = true;
        parent::__construct($request, $category);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
            'categories' => (new Category)->select(['id', 'title'])
                ->get()->pluck('title', 'id'),
            'colors' =>  Color::getTransValues('category.colors', true)
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.content.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Before Save category
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();
        $attributes['active'] = isset($attributes['active']) ? true : false;
        $attributes['show_on_menu'] = isset($attributes['show_on_menu']) ? true : false;
        $attributes['show_on_home'] = isset($attributes['show_on_home']) ? true : false;

        return $attributes;
    }


    protected function showCategory($object, $field)
    {
        $id = $object->{$field};
        $category = (new Category)->find($id);
        return $category ? $category->title : '';
    }

    /**
     * Generate URL alis
     *
     * @param array $attributes
     *
     * @return string
     */
    protected function generateAlias($attributes)
    {
        $parentID = $attributes['parent_id'];
        $prefix = '';
        if ($parentID) {
            $parent = (new Category)->find($parentID);
            $prefix = trim(str_slug($parent->title)) . '/';
        }
        $alias = $prefix . str_slug($attributes['title']);

        return trim($alias);
    }
}
