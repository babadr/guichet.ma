<?php

namespace App\Http\Controllers\Backend\Deal;

use App\Http\Controllers\BackendController;
use App\Models\Deal\Provider;
use App\Models\Acl\User;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;
use App\Models\Store\Order;
use App\Models\Store\OrderLine;
use App\Models\Store\Ticket;
use App\Models\Store\Profiteer;

class ProviderController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['providers.*'];

    /**
     * Current model path
     *
     * @var string
     */
    protected $files = ['logo'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'provider.manage_providers';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'providers.id',
            'prefix' => 'providers.'
        ],
        'title' => [
            'title' => 'provider.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'providers.title',
            'prefix' => 'providers.'
        ],
        'created_at' => [
            'title' => 'provider.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'providers.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'providers.'
        ],
        'active' => [
            'title' => 'provider.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'providers.active',
            'callBack' => 'printStatus',
            'prefix' => 'providers.'
        ]
    ];


    /**
     * @param Request $request
     * @param Provider $provider
     */
    public function __construct(Request $request, Provider $provider)
    {
        $this->viewPrefix = 'deal.provider';
        parent::__construct($request, $provider);
    }

    /**
     * Store a newly created resource
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->beforeSave();
        $validator = $this->currentModel->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans($this->modelName . '.cannot_save'));
            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }
        $user = new User();
        $user->fill($attributes);
        if ($user->save()) {
            $user->attachRole(6);
            $attributes['user_id'] = $user->id;
        }
        $files = $this->saveFiles($this->currentRequest);
        $data = array_merge($attributes, $files);
        $this->currentModel->fill($data);
        if ($this->currentModel->save()) {
            $this->afterSave($data, $this->currentModel);
            flash()->success(trans($this->modelName . '.new_created'));
            if (array_key_exists('saveandcontinue', $attributes)) {
                return redirect(route('backend.' . $this->modelName . '.edit', $this->currentModel->id));
            } else {
                return redirect(route('backend.' . $this->modelName . '.index'));
            }
        }
    }

    /**
     * Update the specified resource
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect()->back();
        }
        $attributes = $this->beforeSave($id);
        $validator = $model->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans($this->modelName . '.cannot_save'));
            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }
        $user = $model->user;
        if (empty($user)) {
            $user = new User();
            $user->fill($attributes);
            if ($user->save()) {
                $user->attachRole(6);
                $attributes['user_id'] = $user->id;
            }
        } else {
            $user->update($attributes);
        }

        $files = $this->saveFiles($this->currentRequest);
        $data = array_merge($attributes, $files);
        if ($model->update($data)) {
            $this->afterSave($data, $model);
            flash()->success(trans($this->modelName . '.updated'));
            if (array_key_exists('saveandcontinue', $attributes)) {
                return redirect(route('backend.' . $this->modelName . '.edit', $id));
            } else {
                return redirect(route('backend.' . $this->modelName . '.index'));
            }
        }
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.deal.provider.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Before Save provider
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();
        $attributes['active'] = isset($attributes['active']) ? true : false;

        return $attributes;
    }

    public function exportSubscriptions($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect(route('backend.' . $this->modelName . '.index'));
        }

        $lines = (new OrderLine())->select('order_lines.*')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where('deals.provider_id', $id)
            ->where('deals.category_id', 17)
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->get();

        $rows = [];
        foreach ($lines as $line) {
            if (!$line->recipients->count()) {
                $order = $line->order;
                $user = $order->user;
                $row = [];
                $row['Référence commande'] = $order->reference;
                $row['Date commande'] = date('d/m/Y H:i:s', strtotime($order->created_at));
                $row['Client'] = $user->full_name;
                $row['E-mail'] =  $user->email;
                $row['Téléphone'] = $user->phone;
                $row['Qté vendue'] =  $line->quantity;
                $row['PU (Dhs TTC)'] = \Tools::displayPrice($line->product_price);
                $row['Total (Dhs TTC)'] = \Tools::displayPrice($line->total_price);
                $row['Catégorie'] = $line->offer_name;
                $rows[] = $row;
            }
        }

        return \Excel::create('export_achat_sana_abonnements_raja_' .  date('Y-m-d_His'), function ($excel) use ($rows) {
            $excel->sheet('Clients', function ($sheet) use ($rows) {
                $sheet->fromArray($rows);
            });
        })->download('xls');
    }

    public function exportOasis($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect(route('backend.' . $this->modelName . '.index'));
        }

        $tickets = Ticket::select('tickets.*', 'orders.offered', 'order_lines.product_name', 'order_lines.offer_name', 'order_lines.product_price', 'users.first_name', 'users.last_name', 'users.email', 'users.phone')
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->where('deals.id', 332)
            ->where('deals.provider_id', 123)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->get()->toArray();

        $rows = [];
        $ids = [];
        foreach ($tickets as $key => $ticket) {
            $profiteer = Profiteer::where('order_line_id', $ticket['order_line_id'])->whereNotIn('id', $ids)->first();
            if ($profiteer) {
                $ids[] = $profiteer->id;
            }
            $row = [];
            $row['company'] = 'guichet_ma';
            $row['ticket_type'] = $ticket['offer_name'];
            $row['reference'] = strtoupper($ticket['hash']);
            $row['first_name'] = $profiteer ? $profiteer->full_name : $ticket['first_name'];
            $row['last_name'] = $profiteer ?  '' : $ticket['last_name'];
            $row['email'] = $ticket['email'];
            $row['banned'] = 'false';
            $rows[] = $row;
        }

        return \Excel::create('export_tickets_oasis_' . date('Y-m-d_His'), function ($excel) use ($rows) {
            $excel->sheet('Tickets', function ($sheet) use ($rows) {
                $sheet->fromArray($rows);
            });
        })->download('xls');
    }

}
