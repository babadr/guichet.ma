<?php

namespace App\Http\Controllers\Backend\Deal;

use App\Http\Controllers\BackendController;
use App\Models\Deal\Deal;
use App\Models\Deal\Offer;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;
use Illuminate\Validation\Validator;
use App\Models\Store\FootballTicket;

class OfferController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['offers.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'offer.manage_offers';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'offers.id',
            'prefix' => 'offers.'
        ],
        'title' => [
            'title' => 'offer.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'offers.title',
            'prefix' => 'offers.'
        ],
        'created_at' => [
            'title' => 'offer.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'offers.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'offers.'
        ],
        'active' => [
            'title' => 'offer.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'offers.active',
            'callBack' => 'printStatus',
            'prefix' => 'offers.'
        ]
    ];


    /**
     * @param Request $request
     * @param Offer $offer
     */
    public function __construct(Request $request, Offer $offer)
    {
        $this->viewPrefix = 'deal.offer';
        parent::__construct($request, $offer);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
            'deals' => (new Deal)->where('active', '=', 1)->select(['id', 'title'])
                ->get()->pluck('title', 'id')
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.content.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * store
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store()
    {
        /** @var array $response */
        $response = [
            'success' => true,
            'message' => '',
            'errors' => []
        ];

        /** @var array $data , get request data */
        $data = $this->currentRequest->all();
        $data['active'] = isset($data['active']) ? true : false;
        $data['default'] = isset($data['default']) ? true : false;

        /** @var Validator $validator , validate data */
        $validator = $this->currentModel->validator($data);
        if ($validator->fails()) {
            $response['success'] = false;
            $response['message'] = trans('offer.cannot_save');
            $response['errors'] = $validator->errors();
        } else if (0 < $data['id']) {
            $model = $this->currentModel->findOrFail($data['id']);
            if ($model->update($data)) {
                if ($this->currentRequest->hasFile('caneva')) {
                    $this->importTickets(\Excel::load(request()->file('caneva'), function ($reader) {
                    })->get(), $model->id);
                }
                $response['message'] = trans('offer.updated');
            }
        } else {
            if ($data['default']) {
                $this->currentModel
                    ->where('deal_id', '=', $data['deal_id'])
                    ->where('default', '=', 1)
                    ->update(['default' => 0]);
            }
            $this->currentModel->fill($data);
            if ($this->currentModel->save()) {
                if ($this->currentRequest->hasFile('caneva')) {
                    $this->importTickets(\Excel::load(request()->file('caneva'), function ($reader) {
                    })->get(), $this->currentModel->id);
                }
                $response['message'] = trans('offer.new_created');
            }
        }

        return response()->json($response);
    }

    /**
     * store
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function find($id)
    {
        $model = $this->currentModel->findOrFail($id);
        return response()->json($model);
    }

    /**
     * switch default offer
     *
     * @param int $id
     * @return array
     */
    protected function switchDefault($id)
    {
        $offer = $this->currentModel->findOrFail($id);

        $this->currentModel
            ->where('deal_id', '=', $offer->deal_id)
            ->where('default', '=', 1)
            ->update(['default' => 0]);

        $offer = $this->currentModel->findOrFail($id);
        $offer->update(['default' => 1]);

        return response()->json([
            'success' => true,
            'message' => ''
        ]);
    }

    protected function importTickets($rows, $offer_id)
    {
        foreach ($rows as $row) {
            try {
                if (!empty($row->zone)) {
                    $ticket = new FootballTicket();
                    $ticket->offer_id = $offer_id;
                    $ticket->zone = (string)trim($row->zone);
                    $ticket->access = (string)trim($row->access);
                    $ticket->sector = (string)trim($row->secteur);
                    $ticket->seat = (string)trim($row->place);
                    $ticket->hash = (string)trim($row->cb);
                    $ticket->selected = 0;
                    $ticket->save();
                }
            } catch (\Exception $e) {
                continue;
            }
        }
    }

}
