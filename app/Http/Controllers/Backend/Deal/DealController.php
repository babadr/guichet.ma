<?php

namespace App\Http\Controllers\Backend\Deal;

use App\Enums\Deal\Type;
use App\Http\Controllers\BackendController;
use App\Models\Deal\Category;
use App\Models\Deal\City;
use App\Models\Deal\Deal;
use App\Models\Deal\DealMedia;
use App\Models\Deal\Provider;
use App\Models\Store\Ticket;
use App\Models\Store\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class DealController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['deals.*', 'cities.city'];

    /**
     * Current model path
     *
     * @var string
     */
    protected $files = ['file', 'custom_ticket'];

    /**
     * Join associated models
     *
     * @var array
     */
    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [
        ['categories', 'categories.id', '=', 'deals.category_id'],
        ['cities', 'cities.id', '=', 'deals.city_id'],
        ['providers', 'providers.id', '=', 'deals.provider_id']
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'deal.manage_deals';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'deals.id',
            'prefix' => 'deals.'
        ],
        'title' => [
            'title' => 'deal.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'deals.title',
            'prefix' => 'deals.',
            'width' => '300px'
        ],
        'category_name' => [
            'title' => 'deal.category_id',
            'type' => 'select',
            'list' => 'categoriesList',
            'filterKey' => 'category_id',
            'orderKey' => 'categories.title',
            'prefix' => 'deals.'
        ],
        'city' => [
            'title' => 'deal.city_id',
            'type' => 'select',
            'list' => 'cities',
            'filterKey' => 'city_id',
            'orderKey' => 'cities.city',
            'prefix' => 'deals.'
        ],
        'provider_name' => [
            'title' => 'deal.provider_id',
            'type' => 'select',
            'list' => 'providers',
            'filterKey' => 'provider_id',
            'orderKey' => 'providers.title',
            'prefix' => 'deals.'
        ],
        'created_at' => [
            'title' => 'deal.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'deals.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'deals.',
        ],
        'show_on_slider' => [
            'title' => 'deal.show_on_slider',
            'type' => 'bool',
            'filterKey' => 'show_on_slider',
            'orderKey' => 'deals.show_on_slider',
            'callBack' => 'printStatus',
            'prefix' => 'deals.'
        ],
        'active' => [
            'title' => 'deal.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'deals.active',
            'callBack' => 'printStatus',
            'prefix' => 'deals.'
        ]
    ];


    /**
     * @param Request $request
     * @param Deal $deal
     */
    public function __construct(Request $request, Deal $deal)
    {
        $this->viewPrefix = 'deal.deal';
        $this->seo = true;
        parent::__construct($request, $deal);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        $seats = config('enums.seating_plans');
        $seats[9] = 'seat.megarama_casa_s8_2';
        $seats[10] = 'seat.theatre_med_5_vip_rabat';
        $seats[16] = 'seat.megarama_casa_s8_3';
		
        return [
            'columns' => $this->columns,
            'categoriesList' => (new Category)->where('active', '=', 1)->get()->pluck('title', 'id'),
            'categories' => (new Category)->where('active', '=', 1)->where('parent_id', '=', NULL)->get(),
            'providers' => (new Provider)->where('active', '=', 1)->select(['id', 'title'])
                ->get()->pluck('title', 'id'),
            'types' => Type::getTransValues('deal.types', true),
            'cities' => (new City)->select(['id', 'city'])->get()->pluck('city', 'id'),
            'seats' => $seats
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.deal.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Before Save deal
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();
        $attributes['active'] = isset($attributes['active']) ? true : false;
        $attributes['is_football'] = isset($attributes['is_football']) ? true : false;
        $attributes['is_private'] = isset($attributes['is_private']) ? true : false;
        $attributes['barcode_type'] = isset($attributes['barcode_type']) ? true : false;
        $attributes['show_on_slider'] = isset($attributes['show_on_slider']) ? true : false;
        $attributes['bought'] = isset($attributes['bought']) ? $attributes['bought'] : 0;
        $attributes['start_at'] = Carbon::createFromFormat('d/m/Y H:i', $attributes['start_at'])->toDateTimeString();
        $attributes['end_at'] = Carbon::createFromFormat('d/m/Y H:i', $attributes['end_at'])->toDateTimeString();

        // disable deal if no related offer
        $deal = $this->currentModel->find($id);
        if (!$deal || !$deal->offers->count()) {
            $attributes['active'] = false;
        }

        if ($attributes['is_private'] && (!$deal || empty($deal->private_token))) {
            $attributes['private_token'] = sha1(time());
        }

        return $attributes;
    }

    /**
     * Handle a generic switch request.
     * used for switch a boolean field status
     *
     * @param int $id
     *
     * @return json
     */
    public function executeSwitch($id)
    {
        $request = $this->currentRequest->all();
        $model = $this->currentModel->find($id);
        if ($model == null) {
            return response()->json(['success' => false]);
        }

        // disable deal if no related offer
        if ($request['state'] && !$model->offers->count()) {
            return response()->json([
                'success' => false,
                'message' => 'Ce deal ne contient aucune offre, impossible de l\'activer'
            ]);
        }

        $model->update([$request['field'] => $request['state']]);
        return response()->json(['success' => true]);
    }

    /**
     * Before Save deal
     *
     * @param int $id
     * @return array
     */
    public function upload($id = null)
    {
        $deal = $this->currentModel->findOrFail($id);
        $files = $this->saveFiles($this->currentRequest);
        $media = new DealMedia();
        $media->fill(["filename" => $files["file"], "deal_id" => $deal->id]);
        $media->save();

        return response()->json($deal->medias);
    }

    /**
     * Before Save deal
     *
     * @param int $id
     * @return array
     */
    protected function deleteMedia($id = null)
    {
        $media = DealMedia::findOrFail($id);
        $media->delete();

        return response()->json([
            'success' => true,
            'message' => ''
        ]);
    }

    /**
     * Before Save deal
     *
     * @param int $id
     * @return array
     */
    protected function switchCover($id, $deal_id)
    {
        DealMedia::where('deal_id', '=', $deal_id)
            ->update(['is_cover' => 0]);

        $media = DealMedia::findOrFail($id);
        $media->update(['is_cover' => 1]);

        return response()->json([
            'success' => true,
            'message' => ''
        ]);
    }

    /**
     * Before Save deal
     *
     * @param int $id
     * @return array
     */
    protected function switchMiniature($id, $deal_id)
    {
        DealMedia::where('deal_id', '=', $deal_id)
            ->update(['is_miniature' => 0]);

        $media = DealMedia::findOrFail($id);
        $media->update(['is_miniature' => 1]);

        return response()->json([
            'success' => true,
            'message' => ''
        ]);
    }

    /**
     * Generate URL alis
     *
     * @param array $attributes
     *
     * @return string
     */
    protected function generateAlias($attributes)
    {
        $categoryID = $attributes['category_id'];
        $prefix = '';
        if ($categoryID) {
            $category = (new Category)->find($categoryID);
            $prefix = $category->seo->seo_alias . '/';
        }
        $alias = $prefix . str_slug($attributes['title']);

        return trim($alias);
    }

    public function currentEvents()
    {
        $currentDeals = (new Deal())->getCurrentDeals(date('Y-m-01'), date('Y-m-t'));

        return view('backend.contents.deal.currents')->with('currentDeals', $currentDeals);
    }

    public function exportTickets($id)
    {
        if (ob_get_level() && ob_get_length() > 0) {
            ob_clean();
        }

        $handle = fopen('php://memory', 'r+');
        $header = [
            'Num',
            'Num_Ticket',
            'Num_Place',
            'Code_Validation',
            'Nom_Prenom_Client',
            'Email_Client',
            'Telephone_Client',
            'Titre_Offre',
            'Prix_Offre',
            'Date_Creation',
        ];
        fputcsv($handle, $header, ';');
        //fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));
        $tickets = Ticket::select('tickets.*', 'orders.offered', 'order_lines.offer_name', 'order_lines.product_price', 'users.first_name', 'users.last_name', 'users.email', 'users.phone')
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->where('deals.id', $id)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->get()->toArray();

        foreach ($tickets as $key => $ticket) {
            $content[$key] = [];
            $content[$key][] = $ticket['id'];
            $content[$key][] = $ticket['number'];
            $content[$key][] = $ticket['seat'];
            $content[$key][] = $ticket['hash'];
            $content[$key][] = $ticket['last_name'] . ' ' . $ticket['first_name'];
            $content[$key][] = $ticket['email'];
            $content[$key][] = $ticket['phone'] ? '"' . $ticket['phone'] . '"' : '';
            $content[$key][] = $ticket['offer_name'];
            $content[$key][] = $ticket['offered'] ? 'Ticket Offert' : $ticket['product_price'];
            $content[$key][] = date('d/m/Y H:i:s', strtotime($ticket['created_at']));
            fputcsv($handle, $content[$key], ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        header('Content-type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-disposition: attachment; filename="export_tickets_event_' . $id . '_' . date('Y-m-d_His') . '.csv"');

        print $content;
        exit;
    }

    public function reserveSeats($id)
    {
        $event = $this->currentModel->find($id);
        if ($event == null) {
            flash()->error('Evénement sélectionné est introuvable.');
            return redirect()->route('backend.deal.index');
        }

        if (empty($event->seating_plan_id) || !$event->active) {
            flash()->error('Vous ne pouvez pas bloquer des places pour l\'événement sélectionné.');
            return redirect()->route('backend.deal.index');
        }

        $user = auth()->user();
        if ($user->hasRole(['partner']) && $event->provider_id != $user->provider->id) {
            flash()->error('Evénement sélectionné est introuvable.');
            return redirect()->route('backend.index');
        }
        if (!$user->hasRole(['partner'])) {
            $this->breadcrumbs->addCrumb(trans($this->listTitle), route('backend.deal.index'));
        }
        $this->breadcrumbs->addCrumb('Bloquer des places');

        $seat_offers = [];
        $items = [];
        $rows = [0 => 'a', 1 => 'b', 2 => 'c', 3 => 'd', 4 => 'e', 5 => 'f', 6 => 'g', 7 => 'h', 8 => 'i', 9 => 'j', 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p'];
        $offers = $event->offers()->where('active', true)->orderBy('price', 'desc')->get();;
        foreach ($offers as $key => $offer) {
            $seat_offers[$rows[$key]]['price'] = number_format($offer->price, 0);
            $seat_offers[$rows[$key]]['classes'] = 'color_' . ($key + 1);
            $seat_offers[$rows[$key]]['category'] = $offer->title;
            $seat_offers[$rows[$key]]['offer_id'] = $offer->id;
            array_push($items, [$rows[$key], 'available', $offer->title . ' (' . \Tools::displayPrice($offer->price, 0) . ')']);
        }

        array_push($items, [$rows[$offers->count()], 'unavailable', 'Vendue']);
        array_push($items, [$rows[$offers->count() + 1], 'selected', 'Bloquée']);

        return view('backend.contents.deal.seats')
            ->with('items', $items)
            ->with('bloqued', explode(',', $event->reserved_seats))
            ->with('seat_offers', $seat_offers)
            ->with('reservedSeats', $event->getReservedSeats(true))
            ->with('rows', config('enums.seating_rows')[$event->seating_plan_id])
            ->with('mapCharts', config('enums.seating_map')[$event->seating_plan_id])
            ->with('event', $event);
    }

    public function saveReservedSeats($id)
    {
        $event = $this->currentModel->find($id);
        if ($event == null) {
            flash()->error('Evénement sélectionné est introuvable.');
            return redirect()->route('backend.deal.index');
        }

        if (empty($event->seating_plan_id) || !$event->active) {
            flash()->error('Vous ne pouvez pas bloquer des places pour l\'événement sélectionné.');
            return redirect()->route('backend.deal.index');
        }

        $user = auth()->user();
        if ($user->hasRole(['partner']) && $event->provider_id != $user->provider->id) {
            flash()->error('Evénement sélectionné est introuvable.');
            return redirect()->route('backend.index');
        }

        $attributes = $this->currentRequest->all();
        $event->reserved_seats = $attributes['seats'];
        $event->save();
        flash()->success('Modification effectuée avec succès.');

        return redirect()->back();
    }
}
