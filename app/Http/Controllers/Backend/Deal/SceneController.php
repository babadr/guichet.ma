<?php

namespace App\Http\Controllers\Backend\Deal;

use App\Http\Controllers\BackendController;
use App\Models\Deal\Scene;
use App\Models\Store\Ticket;
use App\Models\Store\FloatingTicket;
use App\Models\Store\Order;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class SceneController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['scenes.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'name';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'scene.manage_scenes';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'scenes.id',
            'prefix' => 'scenes.'
        ],
        'name' => [
            'title' => 'scene.name',
            'type' => 'text',
            'filterKey' => 'name',
            'orderKey' => 'scenes.name',
            'prefix' => 'scenes.'
        ],
        'code' => [
            'title' => 'scene.code',
            'type' => 'text',
            'filterKey' => 'code',
            'orderKey' => 'scenes.code',
            'prefix' => 'scenes.'
        ],
        'export_code' => [
            'title' => 'scene.export_code',
            'type' => 'text',
            'filterKey' => 'export_code',
            'orderKey' => 'scenes.export_code',
            'prefix' => 'scenes.'
        ],
        'quantity' => [
            'title' => 'scene.quantity',
            'type' => 'text',
            'filterKey' => 'quantity',
            'orderKey' => 'scenes.quantity',
            'prefix' => 'scenes.'
        ],
    ];


    /**
     * @param Request $request
     * @param Scene $scene
     */
    public function __construct(Request $request, Scene $scene)
    {
        $this->viewPrefix = 'deal.scene';
        parent::__construct($request, $scene);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.deal.scene.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function exportAll($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect(route('backend.' . $this->modelName . '.index'));
        }

        $tickets = Ticket::select('tickets.hash', 'deals.end_at', 'deals.start_time', 'deals.title', 'orders.seller_id')
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where('deals.barcode_type', 1)
            ->where('deals.category_id', 15)
            ->where('deals.barcode_prefix', 'LIKE', $model->code . '%')
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->get()->toArray();

        if (count($tickets) === 0) {
            flash()->warning('Aucun ticket n\'est disponible pour l\'export.');

            return redirect()->back();
        }

        $rows = [];
        foreach ($tickets as $ticket) {
            $row = [];
            $row['Date et heure spectacle'] = date('d/m/Y', strtotime($ticket['end_at'])) . ' - ' . str_replace(':', 'H', substr($ticket['start_time'], 0, 5));
            $row['Spectacle'] = $ticket['title'];
            $row['Code à barres'] = $ticket['hash'];
            $row['Scène'] = $model->export_code;
            $row['Type'] = empty($ticket['seller_id']) ? 'ONLINE' : 'POS';
            $rows[] = $row;
        }

        return \Excel::create('scene_' . $model->export_code . '_guichet_all_' . date('Y-m-d_His'), function ($excel) use ($rows) {
            $excel->sheet('Tickets', function ($sheet) use ($rows) {
                $sheet->fromArray($rows);
            });
        })->download('xls');
    }

    public function export($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect(route('backend.' . $this->modelName . '.index'));
        }

        $days = [
            '21' => 'J01',
            '22' => 'J02',
            '23' => 'J03',
            '24' => 'J04',
            '25' => 'J05',
            '26' => 'J06',
            '27' => 'J07',
            '28' => 'J08',
            '29' => 'J09'
        ];

        if (!in_array(date('d'), array_keys($days))) {
            flash()->error('Aucun export n\'est disponible pour ajourd\'hui.');

            return redirect(route('backend.' . $this->modelName . '.index'));
        }

        $tickets = Ticket::select('tickets.hash', 'deals.end_at', 'deals.start_time', 'deals.title', 'orders.seller_id')
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where('deals.barcode_type', 1)
            ->where('deals.category_id', 15)
            ->whereRaw('DATE(deals.end_at) = ?', [date('Y-m-d')])
            ->where('deals.barcode_prefix', 'LIKE', $model->code . '%')
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->get()->toArray();

        $rows = [];
        foreach ($tickets as $ticket) {
            $row = [];
            $row['Date et heure spectacle'] = date('d/m/Y', strtotime($ticket['end_at'])) . ' - ' . str_replace(':', 'H', substr($ticket['start_time'], 0, 5));
            $row['Spectacle'] = $ticket['title'];
            $row['Code à barres'] = $ticket['hash'];
            $row['Scène'] = $model->export_code;
            $row['Type'] = empty($ticket['seller_id']) ? 'ONLINE' : 'POS';
            $row['Ticket Flottant'] = 'Non';
            $rows[] = $row;
        }

        $floatingTickets = FloatingTicket::where('scene_id', $id)->where('day', $days[date('d')])->get();

        if ($floatingTickets->count() === 0) {
            $prefix = $model->code . $days[date('d')] . 'GUI';
            for ($i = 1; $i <= $model->quantity; $i++) {
                $hash = FloatingTicket::generateBarcode($prefix);
                $floatingTicket = new FloatingTicket();
                $floatingTicket->scene_id = $id;
                $floatingTicket->hash = $hash;
                $floatingTicket->selected = false;
                $floatingTicket->day = $days[date('d')];
                if ($floatingTicket->save()) {
                    $row = [];
                    $row['Date et heure spectacle'] = '---';
                    $row['Spectacle'] = '---';
                    $row['Code à barres'] = $hash;
                    $row['Scène'] = $model->export_code;
                    $row['Type'] = 'ONLINE';
                    $row['Ticket Flottant'] = 'Oui';
                    $rows[] = $row;
                }
            }
        } else {
            foreach ($floatingTickets as $ticket) {
                $row = [];
                $row['Date et heure spectacle'] = '---';
                $row['Spectacle'] = '---';
                $row['Code à barres'] = $ticket->hash;
                $row['Scène'] = $model->export_code;
                $row['Type'] = 'ONLINE';
                $row['Ticket Flottant'] = 'Oui';
                $rows[] = $row;
            }
        }

        return \Excel::create('scene_' . $model->export_code . '_guichet_' . $days[date('d')] . '_' . date('Y-m-d_His'), function ($excel) use ($rows) {
            $excel->sheet('Tickets', function ($sheet) use ($rows) {
                $sheet->fromArray($rows);
            });
        })->download('xls');
    }


}
