<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\BackendController;
use App\Models\Common\Contact;
use Illuminate\Http\Request;
use App\Traits\Datatableable;

class ContactController extends BackendController
{

    use Datatableable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['contacts.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'subject';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'contact.manage_contacts';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'contacts.id',
            'prefix' => 'contacts.'
        ],
        'full_name' => [
            'title' => 'contact.full_name',
            'type' => 'text',
            'filterKey' => 'first_name',
            'orderKey' => 'contacts.first_name',
            'prefix' => 'contacts.'
        ],
        'email' => [
            'title' => 'contact.email',
            'type' => 'text',
            'filterKey' => 'email',
            'orderKey' => 'contacts.email',
            'prefix' => 'contacts.'
        ],
        'subject' => [
            'title' => 'contact.subject',
            'type' => 'text',
            'filterKey' => 'subject',
            'orderKey' => 'contacts.subject',
            'prefix' => 'contacts.'
        ],
        'read' => [
            'title' => 'contact.read',
            'type' => 'bool',
            'filterKey' => 'read',
            'orderKey' => 'contacts.read',
            'prefix' => 'contacts.',
            'callBack' => 'printBoolean'
        ],
        'created_at' => [
            'title' => 'contact.received_on',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'contacts.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'contacts.'
        ],
    ];


    /**
     * @param Request $request
     * @param Contact $contact
     */
    public function __construct(Request $request, Contact $contact)
    {
        $this->viewPrefix = 'common.contact';
        parent::__construct($request, $contact);
    }

    /**
     * Handle view request
     *
     * @param int $id => the id to view
     *
     * @return Response
     */
    public function view($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            flash()->error(trans($this->modelName . '.not_found'));

            return redirect(route('backend.' . $this->modelName . '.index'));
        }
        $model->read = 1;
        $model->save();
        $this->breadcrumbs->addCrumb(trans($this->listTitle), route('backend.' . $this->modelName . '.index'));
        $this->breadcrumbs->addCrumb(trans($this->modelName . '.view', ['name' => $model->{$this->title}]));
        $view = $this->getView('backend.contents.' . $this->viewPrefix . '.view');

        return $view->with('title', trans($this->modelName . '.view', ['name' => $model->{$this->title}]))
            ->with('model', $model);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.common.contact.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

}
