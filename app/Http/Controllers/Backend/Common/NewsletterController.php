<?php

namespace App\Http\Controllers\Backend\Common;

use App\Http\Controllers\BackendController;
use App\Models\Common\Newsletter;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class NewsletterController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['newsletters.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'email';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'newsletter.manage_newsletters';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'newsletters.id',
            'prefix' => 'newsletters.'
        ],
        'civility' => [
            'title' => 'newsletter.civility',
            'type' => 'select',
            'list' => 'civilities',
            'filterKey' => 'civility',
            'orderKey' => 'newsletters.civility',
            'prefix' => 'newsletters.',
            'callBack' => 'printCivility'
        ],
        'last_name' => [
            'title' => 'newsletter.last_name',
            'type' => 'text',
            'filterKey' => 'last_name',
            'orderKey' => 'newsletters.last_name',
            'prefix' => 'newsletters.'
        ],
        'first_name' => [
            'title' => 'newsletter.first_name',
            'type' => 'text',
            'filterKey' => 'first_name',
            'orderKey' => 'newsletters.first_name',
            'prefix' => 'newsletters.'
        ],
        'email' => [
            'title' => 'newsletter.email',
            'type' => 'text',
            'filterKey' => 'email',
            'orderKey' => 'newsletters.email',
            'prefix' => 'newsletters.'
        ],
        'phone' => [
            'title' => 'newsletter.phone',
            'type' => 'text',
            'filterKey' => 'phone',
            'orderKey' => 'newsletters.phone',
            'prefix' => 'newsletters.'
        ],
        'source' => [
            'title' => 'newsletter.source',
            'type' => 'select',
            'list' => 'sources',
            'filterKey' => 'source',
            'orderKey' => 'newsletters.source',
            'prefix' => 'newsletters.',
            'callBack' => 'printSource'
        ],
        'birth_date' => [
            'title' => 'newsletter.birth_date',
            'type' => 'date',
            'filterKey' => 'birth_date',
            'orderKey' => 'newsletters.birth_date',
            'callBack' => 'printDate',
            'prefix' => 'newsletters.'
        ],
        'created_at' => [
            'title' => 'newsletter.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'newsletters.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'newsletters.'
        ],
    ];


    /**
     * @param Request $request
     * @param Newsletter $newsletter
     */
    public function __construct(Request $request, Newsletter $newsletter)
    {
        $this->viewPrefix = 'common.newsletter';
        parent::__construct($request, $newsletter);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'civilities' => [1 => 'Mlle.', 2 => 'Mme.', 3 => 'Mr.'],
            'sources' => [1 => 'Site web', 2 => 'LP Champions league', 3 => 'LP PS4', 4 => 'LP Women'],
            'columns' => $this->columns
        ];
    }

    protected function printSource($model, $field)
    {
        $sources = [1 => 'Site web', 2 => 'LP Champions league', 3 => 'LP PS4', 4 => 'LP Women'];

        return empty($model->{$field}) ? '--' : $sources[$model->{$field}];
    }

    protected function printCivility($model, $field)
    {
        $civilities = [1 => 'Mlle.', 2 => 'Mme.', 3 => 'Mr.'];

        return empty($model->{$field}) ? '--' : $civilities[$model->{$field}];
    }

    /**
     * export all subscriptions
     *
     */
    public function export()
    {
        if (ob_get_level() && ob_get_length() > 0) {
            ob_clean();
        }

        $handle = fopen('php://memory', 'r+');
        $header = [
            'ID',
            'Civilite',
            'Nom',
            'Prenom',
            'Email',
            'Telephone',
            'Date_Naissance',
            'Date_Creation',
            'Source',
        ];
        fputcsv($handle, $header, ';');
        fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));

        $civilities = [1 => 'Mlle.', 2 => 'Mme.', 3 => 'Mr.'];
        $sources = [1 => 'Site web', 2 => 'LP Champions league', 3 => 'LP PS4', 4 => 'LP Women'];
        $users = Newsletter::all();
        foreach ($users as $key => $user) {
            $content[$key] = [];
            $content[$key][] = $user->id;
            $content[$key][] = empty($user->civility) ? '--' : $civilities[$user->civility];
            $content[$key][] = $user->last_name;
            $content[$key][] = $user->first_name;
            $content[$key][] = $user->email;
            $content[$key][] = $user->phone;
            $content[$key][] = empty($user->source) ? '--' : $sources[$user->source];
            $content[$key][] = empty($user->birth_date) ? '--' : date('d/m/Y', strtotime($user->birth_date));
            $content[$key][] = date('d/m/Y H:i:s', strtotime($user->created_at));
            fputcsv($handle, $content[$key], ';');
        }

        rewind($handle);
        $content = stream_get_contents($handle);
        fclose($handle);

        header('Content-type: text/csv');
        header('Content-Type: application/force-download; charset=UTF-8');
        header('Cache-Control: no-store, no-cache');
        header('Content-disposition: attachment; filename="export_newsletters_' . date('Y-m-d_His') . '.csv"');

        print $content;
        exit;
    }

}
