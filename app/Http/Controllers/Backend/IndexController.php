<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Models\Deal\Deal;
use App\Models\Deal\Provider;
use App\Models\Acl\User;
use App\Models\Store\Order;
use App\Models\Store\OrderLine;
use App\Models\Store\Ticket;
use App\Models\Common\Newsletter;
use App\Models\Common\Contact;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

class IndexController extends BackendController
{

    /**
     * Display Homepage
     *
     * @param int $partner_id
     * @param int $event_id
     * @return \Illuminate\Http\Response
     */
    public function index($partner_id = null, $event_id = null)
    {
        $user = auth()->user();
        if ($user->hasRole(['seller'])) {
            return redirect()->route('backend.order.index');
        }

        if ($user->hasRole(['validator'])) {
            return redirect()->route('backend.validator.index');
        }

        if ($user->hasRole(['partner'])) {
            return redirect()->route('backend.dashboard', ['id' => $user->provider->id]);
        }

        if (!$user->hasRole(['superadmin', 'admin', 'partner', 'seller', 'validator'])) {
            return redirect()->route('backend.deal.index');
        }

        if ($user->hasRole('partner') || $partner_id) {
            if ($partner_id) {
                $provider = (new Provider())->find($partner_id);
                $this->breadcrumbs->addCrumb('Gestion des producteurs', route('backend.provider.index'));
                $this->breadcrumbs->addCrumb($provider->title . ' (événements et rapports)', '');
            } else {
                $provider = $user->provider;
            }
            $partner_id = $provider->id;
            $deals = $provider->deals;
            $listDeals = $deals->pluck('title', 'id')->toArray();
            if ($event_id) {
                $deals = $deals->where('id', $event_id);
                $event = $deals->first();
                if (!$event) {
                    abort(404);
                }
                $this->breadcrumbs->addCrumb($event->title, '');
            }
            $tickets = $provider->getTickets($event_id)->where('tickets.status', 0);

            $totalAmount = $provider->getTotalAmount($event_id);
            $totalQuantity = $provider->getOrderedQuantity($event_id);
            $totalCommission = 0.00;

            $totalAmountOnLine = 0.00;
            $totalAmountOffLine = 0.00;
            $totalCommissionOnLine = 0.00;
            $totalCommissionOffLine = 0.00;

            foreach ($deals as $deal) {
                $totalCommission += (($deal->getTotalOrderedAmount(false) * $deal->commission) / 100) + (($deal->getTotalOrderedAmountOffline(false) * $deal->offline_commission) / 100) + $deal->getTotalOfferedCommission(false);

                $totalCommissionOnLine += ($deal->getTotalOrderedAmount(false) * $deal->commission) / 100;
                $totalCommissionOffLine += (($deal->getTotalOrderedAmountOffline(false) * $deal->offline_commission) / 100) + $deal->getTotalOfferedCommission(false);

                $totalAmountOnLine += $deal->getTotalOrderedAmount(false);
                $totalAmountOffLine += $deal->getTotalOrderedAmountOffline(false);
            }

            $totalOfferedTickets = (new Order())
                ->join('order_lines', 'order_lines.order_id', '=', 'orders.id')
                ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
                ->join('deals', 'deals.id', '=', 'offers.deal_id')
                ->where('offered', 1);

            if ($event_id) {
                $totalOfferedTickets->where('deals.id', $event_id);
            } else {
                $totalOfferedTickets->where('deals.provider_id', $partner_id);
            }

            $totalOfferedTickets = $totalOfferedTickets->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                ->sum('order_lines.quantity');

            return view('backend.contents.dashboard.partner')
                ->with('deals', $deals)
                ->with('listDeals', $listDeals)
                ->with('event_id', $event_id)
                ->with('tickets', $tickets->count())
                ->with('provider', $provider)
                ->with('totalAmount', $totalAmount)
                ->with('partner_id', $partner_id)
                ->with('totalQuantity', $totalQuantity)
                ->with('totalCommission', $totalCommission)
                ->with('totalCommissionOnLine', $totalCommissionOnLine)
                ->with('totalCommissionOffLine', $totalCommissionOffLine)
                ->with('totalAmountOnLine', $totalAmountOnLine)
                ->with('totalAmountOffLine', $totalAmountOffLine)
                ->with('totalOfferedTickets', $totalOfferedTickets);
        }

        $start_at = $this->currentRequest->get('start_at');
        $end_at = $this->currentRequest->get('end_at');

        if (empty($start_at)) {
            $start_at = date('Y-m-01');
        }

        if (empty($end_at)) {
            $end_at = date('Y-m-t');
        }

        $totalUsers = (new User())
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            //->where('active', 1)
            ->where('role_id', 3)
            ->whereRaw('DATE(users.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(users.created_at) <= ?', [$end_at])
            ->count();

        $totalOrder = (new Order())
            ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->count();

        $totalAmount = (new Order())
            ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->get();

        $totalAmountOnLine = $totalAmount->where('seller_id','=', null)->sum('total_paid');

        $totalAmountOffLine = $totalAmount->where('seller_id', '!=', null)->sum('total_paid');

        $totalAmount = $totalAmount->sum('total_paid');

        $totalCommission = (new OrderLine())->select(['orders.seller_id', 'orders.created_at'])
            ->selectRaw('IF(seller_id IS NULL, ((total_price * deals.commission) / 100), IF(offered = 1,(order_lines.quantity * deals.offered_price),((total_price * deals.offline_commission) / 100))) AS total_commission')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->get();

        $totalCommissionOnline = $totalCommission->where('seller_id','=', null)->sum('total_commission');

        $totalCommissionOffline = $totalCommission->where('seller_id', '!=', null)->sum('total_commission');

        $totalCommission = $totalCommission->sum('total_commission');

        $totalCommissionNET = $totalCommissionOnline - (($totalCommissionOnline * 2) / 100);

        $lastOrders = (new Order())
            ->select(['id', 'status', 'created_at', 'total_paid', 'user_id', 'offered'])
            ->orderBy('created_at', 'DESC')
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->take(10)->get();

        $bestSellers = (new OrderLine())
            ->select('offers.deal_id')
            ->selectRaw('SUM(order_lines.quantity) as total')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->groupBy('offers.deal_id')
            ->orderBy('total', 'DESC')
            ->take(10)
            ->get();

        $deals = (new Deal())->select('id', 'title', 'category_id', 'commission', 'offline_commission')->whereIn('id', $bestSellers->pluck('deal_id'))->get();

        $pendingOrders = (new Order())->where('status', Order::STATUS_WAITING)
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->count();

        $abandonedOrders = (new Order())->whereIn('status', [Order::STATUS_EXPIRED, Order::STATUS_CANCELED, Order::STATUS_RESERVED])
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->count();

        $pendingTickets = (new Ticket())->where('status', 0)
            ->whereRaw('DATE(tickets.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(tickets.created_at) <= ?', [$end_at])
            ->count();

        $validatedTickets = (new Ticket())->where('status', 1)
            ->whereRaw('DATE(tickets.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(tickets.created_at) <= ?', [$end_at])
            ->count();

        $activeDeals = (new Deal())->where('active', 1)
            ->whereRaw('DATE(deals.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(deals.created_at) <= ?', [$end_at])
            ->count();

        $disabledDeals = (new Deal())->where('active', 0)
            ->whereRaw('DATE(deals.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(deals.created_at) <= ?', [$end_at])
            ->count();

        $expiredDeals = (new Deal())->where('active', 1)->whereRaw('DATE(end_at) >= ?', [$start_at])->count();

        $totalSubscriptions = (new Newsletter())
            ->whereRaw('DATE(newsletters.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(newsletters.created_at) <= ?', [$end_at])
            ->count();

        $totalMessages = (new Contact())->where('read', 0)
            ->whereRaw('DATE(contacts.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(contacts.created_at) <= ?', [$end_at])
            ->count();

        $newUsers = (new User())
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_id', 3)
            //->where('created_at', '>=', Carbon::now()->subDay(1)->toDateString())
            ->whereRaw('DATE(users.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(users.created_at) <= ?', [$end_at])
            ->count();

        $newSubscriptions = (new Newsletter())
            //->where('created_at', '>=', Carbon::now()->subDay(1)->toDateString())
            ->whereRaw('DATE(newsletters.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(newsletters.created_at) <= ?', [$end_at])
            ->count();

        $currentOrders = (new Order())->whereRaw('created_at >= (NOW() - INTERVAL 90 MINUTE)')->whereRaw('DATE(created_at) = CURDATE()')->count();

        //$currentDeals = (new Deal())->getCurrentDeals($start_at, $end_at);

        return view('backend.contents.dashboard.index')
            ->with('totalUsers', $totalUsers)
            ->with('totalOrder', $totalOrder)
            ->with('totalAmount', $totalAmount)
            ->with('totalAmountOnLine', $totalAmountOnLine)
            ->with('totalAmountOffLine', $totalAmountOffLine)
            ->with('totalCommission', $totalCommission)
            ->with('totalCommissionOnline', $totalCommissionOnline)
            ->with('totalCommissionOffline', $totalCommissionOffline)
            ->with('totalCommissionNET', $totalCommissionNET)
            ->with('lastOrders', $lastOrders)
            ->with('bestSellers', $bestSellers)
            ->with('deals', $deals)
            ->with('pendingOrders', $pendingOrders)
            ->with('abandonedOrders', $abandonedOrders)
            ->with('pendingTickets', $pendingTickets)
            ->with('validatedTickets', $validatedTickets)
            ->with('activeDeals', $activeDeals)
            ->with('disabledDeals', $disabledDeals)
            ->with('expiredDeals', $expiredDeals)
            ->with('totalSubscriptions', $totalSubscriptions)
            ->with('totalMessages', $totalMessages)
            ->with('newUsers', $newUsers)
            ->with('newSubscriptions', $newSubscriptions)
            ->with('currentOrders', $currentOrders)
            //->with('currentDeals', $currentDeals)
            ->with('start_at', $start_at)
            ->with('end_at', $end_at);
    }

    public function charts($type, $start_at, $end_at)
    {
        $results = [];

        switch ($type) {
            case 'sales':
                $results = (new Order())
                    ->selectRaw('UNIX_TIMESTAMP(DATE(created_at))*1000 as date')
                    ->selectRaw('SUM(total_paid) as total')
                    ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                    ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                    ->groupBy('date')
                    ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                    ->get()
                    ->toArray();
                break;
            case 'commissions':
                $results = (new OrderLine())
                    ->selectRaw('UNIX_TIMESTAMP(DATE(orders.created_at))*1000 as date')
                    //->selectRaw('FORMAT(SUM((order_lines.total_price * deals.commission) / 100),2) AS total')
                    ->selectRaw('SUM(IF(seller_id IS NULL, ((total_price * deals.commission) / 100), IF(offered = 1,(order_lines.quantity * deals.offered_price),((total_price * deals.offline_commission) / 100)))) AS total')
                    ->join('orders', 'orders.id', '=', 'order_lines.order_id')
                    ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
                    ->join('deals', 'deals.id', '=', 'offers.deal_id')
                    ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                    ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                    ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                    ->groupBy('date')
                    ->get()
                    ->toArray();
                break;
            case 'orders':
                $results = (new Order())
                    ->selectRaw('UNIX_TIMESTAMP(DATE(created_at))*1000 as date')
                    ->selectRaw('COUNT(*) as total')
                    ->groupBy('date')
                    ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                    ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                    ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                    ->get()
                    ->toArray();
                break;
            case 'inscriptions':
                $results = (new User())
                    ->selectRaw('UNIX_TIMESTAMP(DATE(created_at))*1000 as date')
                    ->selectRaw('COUNT(*) as total')
                    ->join('role_user', 'users.id', '=', 'role_user.user_id')
                    ->where('role_id', 3)
                    ->whereRaw('DATE(users.created_at) >= ?', [$start_at])
                    ->whereRaw('DATE(users.created_at) <= ?', [$end_at])
                    ->groupBy('date')
                    ->get()
                    ->toArray();
            default:
                break;
        }

        return $results;
    }

    public function export($provider_id)
    {
        $user = auth()->user();

        if (!$user->hasRole(['superadmin', 'admin', 'partner'])) {
            return redirect()->route('backend.index');
        }

        if ($user->hasRole('partner') && $user->provider->id != $provider_id) {
            return redirect()->route('backend.index');
        }

        $lines = (new OrderLine())->select('order_lines.*')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            //->with('order', 'offer')
            ->where('deals.provider_id', $provider_id)
            ->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->get();

        $rows = [];
        foreach ($lines as $line) {
            $deal = $line->offer->deal;
            if ($deal) {
                $order = $line->order;
                $user = $line->order->user;
                $row = [];
                $row['Scène'] = $deal->barcode_prefix ? substr($deal->barcode_prefix, 0, 3) : '---';
                $row['Date Spectacle'] = date('d/m/Y', strtotime($deal->end_at)) . ' - ' . str_replace(':', 'H', substr($deal->start_time, 0, 5));
                $row['Spectacle'] = $deal->title;
                $row['Date vente'] = date('d/m/Y H:i:s', strtotime($order->created_at));
                $row['Client'] = $user ? $user->full_name : '---';
                $row['Qté vendue'] = $line->quantity;
                $row['PU (Dhs TTC)'] = \Tools::displayPrice($line->product_price);
                $row['Total (Dhs TTC)'] = $order->offered ? 'Offert' : \Tools::displayPrice($line->total_price);
                $row['Catégorie'] = $line->offer->title;
                $row['Type'] = empty($order->seller_id) ? 'ONLINE' : 'POS';
                $row['Point de vente'] = $order->seller_id ? $order->seller->title : '---';
                $rows[] = $row;
            } else {
                \Log::info($line->order_id);
            }
        }

        return \Excel::create('export_ventes_guichet_' . date('Y-m-d_His'), function ($excel) use ($rows) {
            $excel->sheet('Ventes', function ($sheet) use ($rows) {
                $sheet->fromArray($rows);
            });
        })->download('xls');
    }

    public function generateReport()
    {
        $months = [
            '01' => 'janvier',
            '02' => 'février',
            '03' => 'mars',
            '04' => 'avril',
            '05' => 'mai',
            '06' => 'juin',
            '07' => 'juillet',
            '08' => 'août',
            '09' => 'septembre',
            '10' => 'octobre',
            '11' => 'novembre',
            '12' => 'décembre',
        ];

        $header = [
            '' => null,
            'TOTAL DES VENTES' => null,
            'TOTAL COMMISSION GUICHET' => null,
            'TOTAL DES COMMANDES' => null,
            'TOTAL DES UTILISATEURS' => null,
        ];

        $data = [];
        $period = CarbonPeriod::create('2018-03-01', '1 month', '2018-11-30');
        foreach ($period as $index => $date) {
            $row = $header;
            if ($index === 0) {
                $row[''] = '10MENTIONS';
                array_push($data, $row);
            }
            $row[''] = $months[$date->format('m')] . '-' . $date->format('Y');

            $start_at = $date->format('Y-m-01');
            $end_at = $date->format('Y-m-t');

            $totalUsers = (new User())
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                //->where('active', 1)
                ->where('role_id', 3)
                ->whereRaw('DATE(users.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(users.created_at) <= ?', [$end_at])
                ->count();

            $totalOrder = (new Order())
                ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                ->count();

            $totalAmount = (new Order())
                ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                ->sum('total_paid');

            $totalCommission = (new OrderLine())
                ->selectRaw('IF(seller_id IS NULL, ((total_price * deals.commission) / 100), IF(offered = 1,(order_lines.quantity * deals.offered_price),((total_price * deals.offline_commission) / 100))) AS total_commission')
                ->join('orders', 'orders.id', '=', 'order_lines.order_id')
                ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
                ->join('deals', 'deals.id', '=', 'offers.deal_id')
                ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                ->get()->sum('total_commission');

            $row['TOTAL DES VENTES'] = $totalAmount + 0.0;
            $row['TOTAL COMMISSION GUICHET'] = $totalCommission;
            $row['TOTAL DES COMMANDES'] = $totalOrder;
            $row['TOTAL DES UTILISATEURS'] = $totalUsers;

            array_push($data, $row);
        }

        array_push($data, $header);

        $period = CarbonPeriod::create('2018-12-01', '1 month', date('Y-m-d'));
        foreach ($period as $index => $date) {
            $row = $header;
            if ($index === 0) {
                $row[''] = 'GUICHET MAROC';
                array_push($data, $row);
            }
            $row[''] = $months[$date->format('m')] . '-' . $date->format('Y');

            $start_at = $date->format('Y-m-01');
            $end_at = $date->format('Y-m-t');

            $totalUsers = (new User())
                ->join('role_user', 'users.id', '=', 'role_user.user_id')
                //->where('active', 1)
                ->where('role_id', 3)
                ->whereRaw('DATE(users.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(users.created_at) <= ?', [$end_at])
                ->count();

            $totalOrder = (new Order())
                ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                ->count();

            $totalAmount = (new Order())
                ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                ->sum('total_paid');

            $totalCommission = (new OrderLine())
                ->selectRaw('IF(seller_id IS NULL, ((total_price * deals.commission) / 100), IF(offered = 1,(order_lines.quantity * deals.offered_price),((total_price * deals.offline_commission) / 100))) AS total_commission')
                ->join('orders', 'orders.id', '=', 'order_lines.order_id')
                ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
                ->join('deals', 'deals.id', '=', 'offers.deal_id')
                ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
                ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
                ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
                ->get()->sum('total_commission');

            $row['TOTAL DES VENTES'] = $totalAmount + 0.0;
            $row['TOTAL COMMISSION GUICHET'] = $totalCommission;
            $row['TOTAL DES COMMANDES'] = $totalOrder;
            $row['TOTAL DES UTILISATEURS'] = $totalUsers;

            array_push($data, $row);
        }


        \Excel::create('reporting_guichet_maroc_' . date('Y-m-d_His'), function ($file) use ($data) {
            $file->sheet('Reporting', function ($sheet) use ($file, $data) {
                $sheet->fromArray($data);
                $sheet->getStyle('A1:E1')->applyFromArray(array(
                    'font' => array(
                        'bold' => true,
                        'size' => 16
                    ),
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => \PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'fill' => array(
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'rotation' => 0,
                        'startcolor' => array(
                            'rgb' => 'EAEAEA'
                        ),
                        'endcolor' => array(
                            'rgb' => 'EAEAEA'
                        )
                    )
                ));
                $sheet->getStyle('A1:A' . (count($data) + 4))->applyFromArray(array(
                    'font' => array(
                        'bold' => true,
                        'size' => 12
                    ),
                ));
                $sheet->getStyle('A2')->applyFromArray(array(
                    'font' => array(
                        'size' => 14
                    ),
                ));
                $sheet->getStyle('A13')->applyFromArray(array(
                    'font' => array(
                        'size' => 14,
                        'color' => array(
                            'rgb' => 'ff0000'
                        )
                    ),
                ));
                $sheet->getStyle('A' . (count($data) + 4))->applyFromArray(array(
                    'font' => array(
                        'size' => 16
                    ),
                ));
                $sheet->setCellValue('A' . (count($data) + 4), 'TOTAL');

                $sheet->getStyle('A2:E' . (count($data) + 4))->applyFromArray(array(
                    'alignment' => array(
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                ));


                $sheet->getStyle('B2:C' . (count($data) + 4))->getNumberFormat()->setFormatCode('#,##0.00_-[$MAD ]');

                $sheet->setCellValue('B' . (count($data) + 4), '=SUM(B3:B' . (count($data) + 1) . ')');
                $sheet->setCellValue('C' . (count($data) + 4), '=SUM(C3:C' . (count($data) + 1) . ')');
                $sheet->setCellValue('D' . (count($data) + 4), '=SUM(D3:D' . (count($data) + 1) . ')');
                $sheet->setCellValue('E' . (count($data) + 4), '=SUM(E3:E' . (count($data) + 1) . ')');

                $sheet->getPageSetup()->setHorizontalCentered(true);
                $sheet->getPageSetup()->setVerticalCentered(false);
                $sheet->getPageMargins()->setTop(0.75);
                $sheet->getPageMargins()->setRight(0.5);
                $sheet->getPageMargins()->setLeft(0.5);
                $sheet->getPageMargins()->setBottom(0.65);
            });
        })->export('xls');
    }

    public function exportTickets($provider_id)
    {
        $user = auth()->user();

        if (!$user->hasRole(['superadmin', 'admin', 'partner'])) {
            return redirect()->route('backend.index');
        }

        if ($user->hasRole('partner') && $user->provider->id != $provider_id) {
            return redirect()->route('backend.index');
        }

        $tickets = Ticket::select('tickets.*', 'orders.offered', 'order_lines.product_name', 'order_lines.offer_name', 'order_lines.product_price', 'users.first_name', 'users.last_name', 'users.email', 'users.phone')
            ->join('order_lines', 'order_lines.id', '=', 'tickets.order_line_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->leftJoin('users', 'users.id', '=', 'orders.user_id')
            ->where('deals.provider_id', $provider_id)->whereIn('orders.status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])->get()->toArray();

        $rows = [];
        foreach ($tickets as $key => $ticket) {
            $row = [];
            $row['ID'] = $ticket['id'];
            $row['Num Ticket'] = $ticket['number'];
            $row['Num Place'] = $ticket['seat'] ? $ticket['seat'] : '---';
            $row['Code Validation'] = $ticket['hash'];
            $row['Nom Prénom Client'] = $ticket['last_name'] ? $ticket['last_name'] . ' ' . $ticket['first_name'] : '---';
            $row['Email Client'] = $ticket['email'];
            $row['Téléphone Client'] = $ticket['phone'] ? $ticket['phone'] : '---';
            $row['Titre Evénement'] = $ticket['product_name'];
            $row['Titre Offre'] = $ticket['offer_name'];
            $row['Prix Offre'] = $ticket['offered'] ? 'Ticket Offert' : $ticket['product_price'];
            $row['Date Création'] = date('d/m/Y H:i:s', strtotime($ticket['created_at']));
            $rows[] = $row;
        }

        return \Excel::create('export_tickets_guichet_' . date('Y-m-d_His'), function ($excel) use ($rows) {
            $excel->sheet('Liste des tickets', function ($sheet) use ($rows) {
                $sheet->fromArray($rows);
            });
        })->download('xls');
    }

}
