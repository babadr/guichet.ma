<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\BackendController;
use App\Models\Deal\Deal;
use App\Models\Deal\Provider;
use App\Models\Acl\User;
use App\Models\Store\Order;
use App\Models\Store\OrderLine;
use App\Models\Store\Ticket;
use App\Models\Common\Newsletter;
use App\Models\Common\Contact;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Zizaco\Entrust\Entrust;

class DashboardController extends BackendController
{

    /**
     * Display Dashboard
     *
     * @param int $id
     * @param string $history
     * @return \Illuminate\Http\Response
     */
    public function index($id, $history = null)
    {
        $provider = (new Provider())->find($id);

        if (\Entrust::hasRole(['partner'])) {
            $this->breadcrumbs->addCrumb('Espace producteur');
        } else {
            $this->breadcrumbs->addCrumb('Gestion des producteurs', route('backend.provider.index'));
        }

        $this->breadcrumbs->addCrumb($provider->title . ' (événements et rapports)', '');


        $currentDeals = (new Deal())->where('active', '=', 1)
            ->orderBy('end_at', 'desc')
            ->where('deals.provider_id', $id);

        if (empty($history)) {
            $currentDeals->whereRaw('DATEDIFF(CURDATE(), deals.end_at) <= ?', [10]);
        }

        $currentDeals = $currentDeals->get();

        if ($currentDeals->count() == 0 && empty($history)) {
            return redirect()->route('backend.dashboard', ['id' => $id, 'history' => 'history']);
        }

        if ($currentDeals->count() >= 2) {
            $start_at = $currentDeals->last()->start_at->format('Y-m-d');
            $end_at = $currentDeals->first()->end_at->format('Y-m-d');
        } else {
            $start_at = $currentDeals->first()->start_at->format('Y-m-d');
            $end_at = $currentDeals->first()->end_at->format('Y-m-d');
        }

        $totalAmountOnLine = 0.00;
        $totalAmountOffLine = 0.00;
        $totalCommission = 0.00;
        $totalCommissionOnLine = 0.00;
        $totalCommissionOffLine = 0.00;

        foreach ($currentDeals as $deal) {
            //$totalCommission += (($deal->getTotalOrderedAmount(false) * $deal->commission) / 100) + (($deal->getTotalOrderedAmountOffline(false) * $deal->offline_commission) / 100) + $deal->getTotalOfferedCommission(false);

            $totalCommissionOnLine += ($deal->getTotalOrderedAmount(false) * $deal->commission) / 100;
            $totalCommissionOffLine += (($deal->getTotalOrderedAmountOffline(false) * $deal->offline_commission) / 100) + $deal->getTotalOfferedCommission(false);

            $totalCommission += $totalCommissionOnLine + $totalCommissionOffLine;

            $totalAmountOnLine += $deal->getTotalOrderedAmount(false);
            $totalAmountOffLine += $deal->getTotalOrderedAmountOffline(false);
        }

        $totalOfferedTickets = (new Order())
            ->join('order_lines', 'order_lines.order_id', '=', 'orders.id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->where('offered', 1)
            ->where('deals.provider_id', $id)
            ->whereIn('deals.id', $currentDeals->pluck('id'))
            ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->sum('order_lines.quantity');


        $totalAmount = $provider->getTotalAmount($currentDeals->pluck('id')->toArray());
        $totalQuantity = $provider->getOrderedQuantity($currentDeals->pluck('id')->toArray());

        $orders = OrderLine::select('orders.id')
            ->join('offers', 'offers.id', '=', 'order_lines.offer_id')
            ->join('deals', 'deals.id', '=', 'offers.deal_id')
            ->join('orders', 'orders.id', '=', 'order_lines.order_id')
            ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->whereIn('offers.deal_id', $currentDeals->pluck('id')->toArray())
            ->orderBy('deals.id')
            ->get()->pluck('id')->toArray();

        $totalOrders = (new Order())
            ->whereIn('id', $orders)
            ->count();

        $results = (new Order())
            ->selectRaw('UNIX_TIMESTAMP(DATE(created_at))*1000 as date')
            ->selectRaw('SUM(total_paid) as total')
            ->whereRaw('DATE(orders.created_at) >= ?', [$start_at])
            ->whereRaw('DATE(orders.created_at) <= ?', [$end_at])
            ->groupBy('date')
            ->whereIn('status', [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])
            ->whereIn('id', $orders)
            ->get()
            ->toArray();

        $sellers = [];
        foreach ($provider->sellers as $key => $seller) {
            $totalSellerAmount = 0.00;
            foreach ($currentDeals as $deal) {

                $totalSellerAmount += $deal->getTotalOrderedAmountOffline(false, $seller->id);
            }
            $sellers[$key]['title'] = $seller->title;
            $sellers[$key]['value'] = $totalSellerAmount;
        }

        return view('backend.contents.dashboard.dashboard')
            ->with('provider', $provider)
            ->with('currentDeals', $currentDeals)
            ->with('totalOrders', $totalOrders)
            ->with('totalAmount', $totalAmount)
            ->with('totalQuantity', $totalQuantity)
            ->with('totalOfferedTickets', $totalOfferedTickets)
            ->with('totalCommission', $totalCommission)
            ->with('totalCommissionOnLine', $totalCommissionOnLine)
            ->with('totalCommissionOffLine', $totalCommissionOffLine)
            ->with('totalAmountOnLine', $totalAmountOnLine)
            ->with('totalAmountOffLine', $totalAmountOffLine)
            ->with('start_at', $start_at)
            ->with('end_at', $end_at)
            ->with('results', $results)
            ->with('sellers', $sellers)
            ->with('history', $history);

    }

    public function history($id)
    {
        $provider = (new Provider())->find($id);

        if (\Entrust::hasRole(['partner'])) {
            $this->breadcrumbs->addCrumb('Espace producteur');
        } else {
            $this->breadcrumbs->addCrumb('Gestion des producteurs', route('backend.provider.index'));
        }

        $this->breadcrumbs->addCrumb($provider->title . ' (événements et rapports)', '');


        return view('backend.contents.dashboard.history')
            ->with('provider', $provider);

    }


}
