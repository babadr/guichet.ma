<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\BackendController;
use App\Models\Content\CategoryPost;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class CategoryPostController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['category_posts.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'name';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'category_post.manage_categories';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'category_posts.id',
            'prefix' => 'category_posts.'
        ],
        'name' => [
            'title' => 'category_post.name',
            'type' => 'text',
            'filterKey' => 'name',
            'orderKey' => 'category_posts.name',
            'prefix' => 'category_posts.'
        ],
        'created_at' => [
            'title' => 'category_post.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'category_posts.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'category_posts.'
        ],
        'active' => [
            'title' => 'category_post.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'category_posts.active',
            'callBack' => 'printStatus',
            'prefix' => 'category_posts.'
        ]
    ];


    /**
     * @param Request $request
     * @param CategoryPost $category
     */
    public function __construct(Request $request, CategoryPost $category)
    {
        $this->viewPrefix = 'content.category';
        $this->seo = true;
        parent::__construct($request, $category);
    }

    /**
     * Handle delete request
     *
     * @param int $id
     * @return void
     */
    public function delete($id)
    {
        $model = $this->currentModel->find($id);
        if ($model == null) {
            return response()->json(['success' => false, 'msg' => trans('app.not_found')]);
        }

        if ($model->posts()->count()) {
            return response()->json(['success' => false, 'msg' => trans('category_post.cannot_delete_category')]);
        }

        $result = $model->delete();

        return response()->json(['success' => $result, 'msg' => trans($result ? 'app.delete_success' : 'app.cannot_delete')]);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns
        ];
    }

    /**
     * Before Save category_post
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();
        $attributes['active'] = isset($attributes['active']) ? true : false;

        return $attributes;
    }


    /**
     * Generate URL alis
     *
     * @param array $attributes
     *
     * @return string
     */
    protected function generateAlias($attributes)
    {
        $alias = 'magazine/' . str_slug($attributes['name']);

        return trim($alias);
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.content.category.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }
}
