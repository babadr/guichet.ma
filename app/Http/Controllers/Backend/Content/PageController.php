<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\BackendController;
use App\Models\Content\Page;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class PageController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['pages.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'page.manage_pages';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'pages.id',
            'prefix' => 'pages.'
        ],
        'title' => [
            'title' => 'page.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'pages.title',
            'prefix' => 'pages.'
        ],
        'created_at' => [
            'title' => 'page.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'pages.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'pages.'
        ],
        'active' => [
            'title' => 'page.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'pages.active',
            'callBack' => 'printStatus',
            'prefix' => 'pages.'
        ]
    ];


    /**
     * @param Request $request
     * @param Page $page
     */
    public function __construct(Request $request, Page $page)
    {
        $this->viewPrefix = 'content.page';
        $this->seo = true;
        parent::__construct($request, $page);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.content.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

}
