<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\BackendController;
use App\Models\Content\Post;
use App\Models\Content\CategoryPost;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class PostController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['posts.*', 'category_posts.name'];

    /**
     * Current model path
     *
     * @var string
     */
    protected $files = ['image'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $leftJoins = [
        ['category_posts', 'category_posts.id', '=', 'posts.category_id'],
    ];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'post.manage_posts';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'posts.id',
            'prefix' => 'posts.'
        ],
        'title' => [
            'title' => 'post.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'posts.title',
            'prefix' => 'posts.'
        ],
        'name' => [
            'title' => 'post.category_id',
            'type' => 'select',
            'list' => 'categories',
            'filterKey' => 'category_id',
            'orderKey' => 'category_posts.name',
            'prefix' => 'posts.'
        ],
        'created_at' => [
            'title' => 'post.created_at',
            'type' => 'date',
            'filterKey' => 'created_at',
            'orderKey' => 'posts.created_at',
            'callBack' => 'printDateTime',
            'prefix' => 'posts.'
        ],
        'active' => [
            'title' => 'post.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'posts.active',
            'callBack' => 'printStatus',
            'prefix' => 'posts.'
        ]
    ];


    /**
     * @param Request $request
     * @param Post $post
     */
    public function __construct(Request $request, Post $post)
    {
        $this->viewPrefix = 'content.post';
        $this->seo = true;
        parent::__construct($request, $post);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns,
            'categories' => (new CategoryPost)->where('active', '=', 1)->get()->pluck('name', 'id'),
        ];
    }

    /**
     * Add the actions to use for each row in the list
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return string
     */
    protected function addRowActions($model)
    {
        return view('backend.contents.content.actions')
            ->with('model', $model)
            ->with('modelName', $this->modelName)
            ->render();
    }

    /**
     * Generate URL alis
     *
     * @param array $attributes
     *
     * @return string
     */
    protected function generateAlias($attributes)
    {
//        $alias = 'magazine/' . str_slug($attributes['title']);
//
//        return trim($alias);

        $categoryID = $attributes['category_id'];
        $prefix = 'magazine/';
        if ($categoryID) {
            $category = (new CategoryPost)->find($categoryID);
            $prefix = $category->seo->seo_alias . '/';
        }
        $alias = $prefix . str_slug($attributes['title']);

        return trim($alias);
    }

}
