<?php

namespace App\Http\Controllers\Backend\Content;

use App\Http\Controllers\BackendController;
use App\Models\Content\Faq;
use Illuminate\Http\Request;
use App\Traits\Datatableable;
use App\Traits\Crudable;

class faqController extends BackendController
{

    use Datatableable,
        Crudable;

    /**
     * Add fields into data query to display list
     *
     * @var array
     */
    protected $select = ['faqs.*'];

    /**
     * Join associated models
     *
     * @var array
     */
    protected $joins = [];

    /**
     * Current model path
     *
     * @var string
     */
    protected $title = 'title';

    /**
     * Current model path
     *
     * @var string
     */
    protected $listTitle = 'faq.manage_faqs';

    /**
     * Fields List to be generated
     *
     * @var array
     */
    protected $columns = [
        'id' => [
            'title' => '#',
            'type' => 'int',
            'filterKey' => 'id',
            'orderKey' => 'faqs.id',
            'prefix' => 'faqs.'
        ],
        'title' => [
            'title' => 'faq.title',
            'type' => 'text',
            'filterKey' => 'title',
            'orderKey' => 'faqs.title',
            'prefix' => 'faqs.'
        ],
        'active' => [
            'title' => 'faq.active',
            'type' => 'bool',
            'filterKey' => 'active',
            'orderKey' => 'faqs.active',
            'callBack' => 'printStatus',
            'prefix' => 'faqs.'
        ]
    ];


    /**
     * @param Request $request
     * @param faq $faq
     */
    public function __construct(Request $request, faq $faq)
    {
        $this->viewPrefix = 'content.faq';
        parent::__construct($request, $faq);
    }

    /**
     * Define view vars
     *
     * @return array
     */
    protected function getViewVars()
    {
        return [
            'columns' => $this->columns
        ];
    }

    /**
     * Before Save faq
     *
     * @param int $id
     * @return array
     */
    protected function beforeSave($id = null)
    {
        $attributes = $this->currentRequest->all();
        $attributes['active'] = isset($attributes['active']) ? true : false;

        return $attributes;
    }
}
