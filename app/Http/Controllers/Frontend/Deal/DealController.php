<?php

namespace App\Http\Controllers\Frontend\Deal;

use App\Http\Controllers\FrontendController;
use App\Models\Deal\Deal;
use App\Models\Store\Ticket;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Jenssegers\Agent\Agent;
use Tools;

class DealController extends FrontendController
{

    /**
     * Display deal page
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $deal = (new Deal)->findOrFail($id);
        if (!$deal->active) {
            throw (new ModelNotFoundException)->setModel(Deal::class, $id);
        }

        return $this->viewDeal($deal);
    }

    public function viewPrivate($token)
    {
        $deal = (new Deal)->where('private_token', $token)->first();
        if (!$deal || !$deal->active) {
            throw (new ModelNotFoundException);
        }

        return $this->viewDeal($deal);
    }

    /**
     * Display deal preview page
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function preview($id)
    {
        $deal = (new Deal)->findOrFail($id);
        $offers = $deal->offers()->where('active', true)->orderBy('id')->get();
        $preview = true;

        $agent = new Agent();
        $view = 'frontend.contents.deal.deal.index';
        if ($agent->isMobile()) {
            $view = 'frontend.contents.deal.deal.mobile';
        }

        $seat_offers = [];
        $items = [];
        $rows = [0 => 'a', 1 => 'b', 2 => 'c', 3 => 'd', 4 => 'e', 5 => 'f', 6 => 'g', 7 => 'h', 8 => 'i', 9 => 'j', 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p'];
        $results = $deal->offers()->where('active', true)->orderBy('price', 'desc')->get();;
        foreach ($results as $key => $offer) {
            $seat_offers[$rows[$key]]['price'] = number_format($offer->price, 0);
            $seat_offers[$rows[$key]]['classes'] = 'color_' . ($key + 1);
            $seat_offers[$rows[$key]]['category'] = $offer->title;
            $seat_offers[$rows[$key]]['offer_id'] = $offer->id;
            array_push($items, [$rows[$key], 'available', $offer->title . ' (' . Tools::displayPrice($offer->price, 0) . ')']);
        }

        array_push($items, [$rows[$offers->count()], 'unavailable', 'Vendue']);
        array_push($items, [$rows[$offers->count() + 1], 'selected', 'Sélectionnée']);
        $deals = $deal->getAssociatedEvents();

        return view($view)->with(compact('deal', 'offers', 'preview', 'seat_offers', 'items', 'deals'));
    }

    /*
     * Display seating plan
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function seat($id)
    {
        $deal = Deal::select('id', 'active', 'seating_plan_id', 'title')->where('id', $id)->first();
        if (empty($deal) || !$deal->active || empty($deal->seating_plan_id)) {
            return response()->json([
                'success' => false,
                'msg' => 'L\'événement n\'existe pas.',
            ]);
        }

        $offers = $deal->offers()->where('active', true)->orderBy('id')->get();

        return response()->json([
            'success' => true,
            'html' => view('frontend.contents.seat.index')
                ->with('deal', $deal)->with('offers', $offers)->render(),
        ]);
    }

    /*
     * Get reserved seats
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function reservedSeats($id)
    {
        $deal = Deal::select('id', 'active', 'seating_plan_id', 'reserved_seats')->where('id', $id)->first();
        if (empty($deal) || !$deal->active || empty($deal->seating_plan_id)) {
            return response()->json([
                'success' => false,
                'msg' => 'L\'événement n\'existe pas.',
            ]);
        }

        return response()->json([
            'success' => true,
            'seats' => $deal->getReservedSeats(),
        ]);
    }

    private function viewDeal($deal)
    {
        $offers = $deal->offers()->where('active', true)->orderBy('price')->get();
        $seo = $deal->seo;
        \SEO::setTitle(($seo->seo_title ? $seo->seo_title : $deal->title) . ' - Guichet.ma');
        \SEO::setDescription($seo->seo_description ? $seo->seo_description : $deal->short_description);
        \SEO::opengraph()->setUrl(config('app.url') . $seo->seo_alias);
        \SEO::setCanonical(config('app.url') . $seo->seo_alias);
        \SEO::opengraph()->addProperty('type', 'articles');

        $agent = new Agent();
        $view = 'frontend.contents.deal.deal.index';
        if ($agent->isMobile()) {
            $view = 'frontend.contents.deal.deal.mobile';
        }

        $seat_offers = [];
        $items = [];
        $rows = [0 => 'a', 1 => 'b', 2 => 'c', 3 => 'd', 4 => 'e', 5 => 'f', 6 => 'g', 7 => 'h', 8 => 'i', 9 => 'j', 10 => 'k', 11 => 'l', 12 => 'm', 13 => 'n', 14 => 'o', 15 => 'p'];
        $results = $deal->offers()->where('active', true)->orderBy('price', 'desc')->get();;
        foreach ($results as $key => $offer) {
            $seat_offers[$rows[$key]]['price'] = number_format($offer->price, 0);
            $seat_offers[$rows[$key]]['classes'] = 'color_' . ($key + 1);
            $seat_offers[$rows[$key]]['category'] = $offer->title;
            $seat_offers[$rows[$key]]['offer_id'] = $offer->id;
            array_push($items, [$rows[$key], 'available', $offer->title . ' (' . Tools::displayPrice($offer->price, 0) . ')']);
        }

        array_push($items, [$rows[$offers->count()], 'unavailable', 'Vendue']);
        array_push($items, [$rows[$offers->count() + 1], 'selected', 'Sélectionnée']);
        $deals = collect([]);//$deal->getAssociatedEvents();
        $medias = $deal->medias;

        return view($view)->with(compact('deal', 'offers', 'seat_offers', 'items', 'deals', 'medias'));
    }
}
