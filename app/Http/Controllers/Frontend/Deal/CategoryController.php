<?php

namespace App\Http\Controllers\Frontend\Deal;

use App\Http\Controllers\FrontendController;
use App\Models\Deal\Category;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryController extends FrontendController
{
    /**
     * Display category page
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $category = (new Category)->findOrFail($id);
        if (!$category->active) {
            throw (new ModelNotFoundException)->setModel(Category::class, $id);
        }

//        $pub = 'pub-voyage.jpg';
//        if ($category->id == 2) {
//            $pub = 'pub-food.jpg';
//        }
//        if ($category->id == 3) {
//            $pub = 'pub-loisir.jpg';
//        }
//        if (in_array($category->id, [4, 5, 6, 7, 8])) {
//            $pub = 'pub-billeterie.jpg';
//        }

        $seo = $category->seo;
        \SEO::setTitle(($seo->seo_title ? $seo->seo_title : $category->title) . ' - Guichet.ma');
        \SEO::setDescription($seo->seo_description);
        \SEO::opengraph()->setUrl(config('app.url') . $seo->seo_alias);
        \SEO::setCanonical(config('app.url') . $seo->seo_alias);
        \SEO::opengraph()->addProperty('type', 'articles');

        $deals = $category->currentEvents();
        $events = $category->pastEvents()->paginate(9);

        $slides = $category->findDeals(6, true)->get();

        return view('frontend.contents.deal.category.index')->with(compact('category', 'deals', 'slides', 'events'));
    }
}
