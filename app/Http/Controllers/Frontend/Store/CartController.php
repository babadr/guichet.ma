<?php

namespace App\Http\Controllers\Frontend\Store;

use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use App\Models\Deal\Offer;
use App\Models\Store\Order;
use App\Models\Deal\Deal;
use Carbon\Carbon;
use Cart;

class CartController extends FrontendController
{

    /**
     * @param Request $request
     * @param Offer $offer
     *
     * @return void
     */
    public function __construct(Request $request, Offer $offer)
    {
        parent::__construct($request, $offer);
    }

    /*
     * Collection of Cart Items
     */
    public function index()
    {
        \SEO::setTitle('Mon panier');

        $user = auth()->user();
        $order = null;
        if ($user) {
            if (session()->has($user->id . '_currentOrder')) {
                $orderID = session($user->id . '_currentOrder');
                $order = (new Order())->getByReference($orderID);
            }
            $attributes = $this->currentRequest->all();
            if (isset($attributes['RESPONSE_CODE'])) {
                if ($attributes['RESPONSE_CODE'] == '1028') {
                    flash()->error('Opération abondonnée.');
                }
                if ($attributes['RESPONSE_CODE'] == '1011') {
                    flash()->error('Suspicion de fraude.');
                }
                if ($attributes['RESPONSE_CODE'] == '0') {
                    $order = (new Order())->getByReference($attributes['ORDER_ID']);
                    if ($order && $order->user_id == $user->id && in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])) {
                        session()->forget($user->id . '_currentOrder');
                        Cart::destroy();
                        return redirect()->route('frontend.order.finalization', ['reference' => $order->reference]);
                    }
                }
            }
        }

        $default_payment = !empty($order->payment_method) ? $order->payment_method : 1;
        $hasSeat = false;
        $items = Cart::content();
        foreach ($items as $item) {
            if ($item->options->has('seats')) {
                $hasSeat = true;
                break;
            }
            $offer = Offer::find($item->id);
            if ($offer) {
                $start = Carbon::now();
                $end = Carbon::parse($offer->deal->end_at);
                $hours = $end->diffInHours($start);
                if ($hours <= 48 || $offer->deal->is_football) {
                    $hasSeat = true;
                    break;
                }
            }
        }

        return view('frontend.contents.store.cart.index')
            ->with('user', $user)
            ->with('order', $order)
            ->with('default_payment', $default_payment)
            ->with('hasSeat', $hasSeat);
    }

    public function getSelectedSeats($id)
    {
        $deal = Deal::select('id', 'active', 'seating_plan_id', 'title')->where('id', $id)->first();
        if (empty($deal) || !$deal->active || empty($deal->seating_plan_id)) {
            return response()->json([
                'success' => false,
                'msg' => 'L\'événement n\'existe pas.',
            ]);
        }

        $offers = $deal->offers()->where('active', true)->orderBy('id')->get()->pluck('id')->toArray();

        $items = Cart::content();
        $seats = [];
        foreach ($items as $item) {
            if (in_array($item->id, $offers)) {
                $seats = array_merge($seats, explode(',', $item->options->seats));
            }
        }

        return response()->json([
            'success' => true,
            'seats' => $seats,
        ]);
    }

    /*
     * Adding an item with seats to the cart
     */
    public function addSeats()
    {
        $attributes = $this->currentRequest->all();
        if (!isset($attributes['Seats']) || empty($attributes['Seats']) || !is_array($attributes['Seats'])) {
            return response()->json(['success' => false, 'msg' => 'Veuillez sélectionner une ou plusieurs places.']);
        }

        $seats = collect($attributes['Seats'])->groupBy('id');

        foreach ($seats as $id => $seat) {
            $offer = $this->currentModel->where('id', $id)->where('active', true)->first();
            if (!$offer) {
                return response()->json(['success' => false, 'msg' => 'L\'événement choisi n\'existe pas.']);
            }
            $ids = $offer->deal->offers()->pluck('id')->toArray();
            $items = Cart::content();
            foreach ($items as $key => $item) {
                if (in_array($item->id, $ids)) {
                    Cart::remove($key);
                }
            }
            break;
        }

        $orderedSeats = [];
        foreach ($seats as $id => $seat) {
            $offer = $this->currentModel->where('id', $id)->where('active', true)->first();
            if (!$offer) {
                return response()->json(['success' => false, 'msg' => 'L\'événement choisi n\'existe pas.']);
            }
            $qty = $seat->count();
            if (!$offer->checkAvailability($qty)) {
                return response()->json(['success' => false, 'msg' => 'La quantité demandée pour l\'offre "' . $offer->title . '" n\'est plus disponible en stock.']);
            }
            if (!$offer->checkMaxQuantity($qty)) {
                return response()->json(['success' => false, 'msg' => 'La quantité max autorisée pour l\'offre "' . $offer->title . '" est de ' . $offer->quantity_max . ' tickets.']);
            }
            $reservedSeats = $offer->getReservedSeats();
            foreach ($seat as $s) {
                if (in_array($s['seat'], $reservedSeats)) {
                    return response()->json(['success' => false, 'msg' => 'La place "' . $s['seat'] . '" n\'est plus disponible, Veuillez sélectionner une autre place.']);
                }
                if (in_array($s['seat'], $orderedSeats)) {
                    return response()->json(['success' => false, 'msg' => 'La place "' . $s['seat'] . '" est déjà sélectionnée, merci de vérifier votre commande.']);
                }
                $orderedSeats[] = $s['seat'];
            }
            $price = empty($offer->reservation_price) ? $offer->price : $offer->reservation_price;
            Cart::add($offer->id, $offer->deal->title, $qty, $price, [
                'offer' => $offer->title,
                'image' => $offer->deal->miniature,
                'url' =>$offer->deal->is_private ? route('frontend.event.private', $offer->deal->private_token) : '/' . $offer->deal->seo->seo_alias,
                'reservation' => !empty($offer->reservation_price) ? $offer->price : null,
                'seats' => implode(',', $seat->pluck('seat')->toArray()),
            ]);
        }

        return response()->json([
            'success' => true,
            'redirect' => route('frontend.cart.index')
        ]);
    }

    /*
     * Adding an item to the cart
     */
    public function add()
    {
        $attributes = $this->currentRequest->all();
        $offer = $this->currentModel->where('id', $attributes['offer_id'])->where('active', true)->first();
        if (!$offer) {
            return response()->json(['success' => false, 'msg' => 'L\'événement choisi n\'existe pas.']);
        }
        $qty = $this->getCartOfferQuantity($attributes['offer_id']) + $attributes['quantity'];
        if (!$offer->checkAvailability($qty)) {
            return response()->json(['success' => false, 'msg' => 'La quantité demandée n\'est plus disponible en stock.']);
        }
        if (!$offer->checkMaxQuantity($qty)) {
            return response()->json(['success' => false, 'msg' => 'La quantité max autorisée pour cette offre est de ' . $offer->quantity_max . ' tickets.']);
        }
        $price = empty($offer->reservation_price) ? $offer->price : $offer->reservation_price;
        $item = Cart::add($offer->id, $offer->deal->title, $attributes['quantity'], $price, [
            'offer' => $offer->title,
            'image' => $offer->deal->miniature,
            'url' => $offer->deal->is_private ? route('frontend.event.private', $offer->deal->private_token) : '/' . $offer->deal->seo->seo_alias,
            'reservation' => !empty($offer->reservation_price) ? $offer->price : null
        ]);

        return response()->json([
            'success' => true,
            'count' => Cart::count(),
            'html' => view('frontend.contents.store.cart.confirm')->with('item', $item)->with('user', auth()->user())->render()
        ]);
    }

    /*
     * To update an item in the cart
     */
    public function update($id)
    {
        try {
            $attributes = $this->currentRequest->all();
            $quantity = empty($attributes['quantity']) ? 1 : (int)$attributes['quantity'];
            $item = Cart::get($id);
            $qty = $item->qty;
            if ($attributes['type'] == 'up') {
                $qty = $quantity;
            } elseif ($attributes['type'] == 'down') {
                $qty -= $quantity;
            }

            $offer = $this->currentModel->where('id', $item->id)->where('active', true)->first();
            if (!$offer) {
                return response()->json(['success' => false, 'msg' => 'L\'événement choisi n\'existe pas.']);
            }
            if (!$offer->checkAvailability($qty)) {
                return response()->json(['success' => false, 'msg' => 'La quantité demandée n\'est plus disponible en stock.']);
            }
            if (!$offer->checkMaxQuantity($qty)) {
                return response()->json(['success' => false, 'msg' => 'La quantité max autorisée pour cette offre est de ' . $offer->quantity_max . ' tickets.']);
            }
            Cart::update($id, $quantity);
            return response()->json([
                'success' => true,
                'msg' => 'Panier mis à jour avec succès.',
                'cart' => view('frontend.contents.store.cart.items')->with('user', auth()->user())->with('order', [])->render(),
                'count' => Cart::count(),
                'total' => Cart::total() . ' DH'
            ]);
        } catch (\Exception $ex) {
            return response()->json(['success' => false, 'msg' => 'Votre panier ne contient pas ce produit. Veuillez recharger la page.']);
        }
    }

    /*
     * To remove an item for the cart
     */
    public function delete($id)
    {
        try {
            Cart::remove($id);
            return response()->json([
                'success' => true,
                'msg' => 'Evénement supprimé avec succès.',
                'cart' => view('frontend.contents.store.cart.items')->with('order', [])->render(),
                'count' => Cart::count(),
                'total' => Cart::total() . ' DH'
            ]);
        } catch (\Exception $ex) {
            return response()->json(['success' => false, 'msg' => 'Votre panier ne contient pas cet événement. Veuillez recharger la page.']);
        }
    }

    private function getCartOfferQuantity($offer_id)
    {
        $offer = Cart::search(function ($cartItem, $rowId) use ($offer_id) {
            return $cartItem->id == $offer_id;
        })->first();

        return $offer ? $offer->qty : 0;
    }

}
