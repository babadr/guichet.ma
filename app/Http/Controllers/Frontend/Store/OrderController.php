<?php

namespace App\Http\Controllers\Frontend\Store;

use App\Http\Controllers\FrontendController;
use App\Models\Store\OrderLine;
use App\Models\Store\Profiteer;
use App\Services\TicketService;
use Illuminate\Http\Request;
use App\Models\Deal\Offer;
use App\Models\Store\Order;
use App\Models\Store\Recipient;
use App\Services\FPayService;
use Cart;
use Carbon\Carbon;
use App\Services\MailService;
use App\Services\SmsService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderController extends FrontendController
{

    /**
     * The MailService instance.
     *
     * @var MailService
     */
    public $mailService;

    /**
     * The SmsService instance.
     *
     * @var SmsService
     */
    public $smsService;

    /**
     * The TicketService instance.
     *
     * @var TicketService
     */
    public $ticketService;

    /*
     * FPayService
     */
    protected $fPayService;

    /**
     * @param Request $request
     * @param Order $order
     * @param FPayService $fPayService
     * @param MailService $mailService
     * @param TicketService $ticketService
     */
    public function __construct(Request $request, Order $order, FPayService $fPayService, MailService $mailService, ticketService $ticketService)
    {
        $this->mailService = $mailService;
        $this->fPayService = $fPayService;
        $this->ticketService = $ticketService;
        $this->smsService = new SmsService();
        parent::__construct($request, $order);
    }

    public function index()
    {
        $user = auth()->user();
        $orders = $this->currentModel->where('user_id', $user->id)->orderBy('created_at', 'desc')->get();
        \SEO::setTitle('Mon compte - Mes commandes');
        return view('frontend.contents.store.order.index')
            ->with('user', $user)
            ->with('orders', $orders);
    }

    public function view($id)
    {
        $user = auth()->user();
        $order = $this->currentModel->where('id', $id)->where('user_id', $user->id)->first();
        if (!$order) {
            flash()->error('La commande demandée n\'existe pas.');
            return redirect()->route('frontend.order.list');
        }

        \SEO::setTitle('Mon compte - Détails commande Réf#' . $order->reference);

        return view('frontend.contents.store.order.view')->with('order', $order)->with('lines', $order->lines);
    }

    public function checkout()
    {
        if (Cart::count() == 0) {
            return redirect()->route('frontend.cart.index');
        }
        $user = auth()->user();
        $attributes = $this->currentRequest->all();
        if (isset($attributes['RESPONSE_CODE'])) {
            if ($attributes['RESPONSE_CODE'] == '1028') {
                flash()->error('Opération abondonnée.');
            }
            if ($attributes['RESPONSE_CODE'] == '1011') {
                flash()->error('Suspicion de fraude.');
            }
            if ($attributes['RESPONSE_CODE'] == '0') {
                $order = $this->currentModel->getByReference($attributes['ORDER_ID']);
                if ($order && $order->user_id == $user->id && in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_CAPTURED, Order::STATUS_VALIDATED])) {
                    session()->forget($user->id . '_currentOrder');
                    Cart::destroy();
                    return redirect()->route('frontend.order.finalization', ['reference' => $order->reference]);
                }
            }
        }
        if (session()->has($user->id . '_currentOrder')) {
            $this->updateOrder($user);
        } else {
            session([$user->id . '_currentOrder' => $this->createOrder($user)]);
        }

        \SEO::setTitle('Vérification de votre commande');

        return view('frontend.contents.store.order.checkout')
            ->with('user', $user)
            ->with('reference', session($user->id . '_currentOrder'));
    }

    public function finalization($reference)
    {
        $order = $this->currentModel->getByReference($reference);
        $user = auth()->user();
        if (!$order) {
            throw (new ModelNotFoundException)->setModel(get_class(Order), $order->id);
        }
        if ($order->user_id != $user->id || !in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED, Order::STATUS_RESERVED])) {
            return redirect()->route('frontend.cart.index');
        }

        \SEO::setTitle('Commande validée');

        $lines = [];
        foreach ($order->lines as $line) {
            if ($line->offer->deal->category_id == 17 && !$line->recipients->count()) {
                $lines[$line->id]['total'] = $line->quantity;
                $lines[$line->id]['offer'] = $line->offer_name;
            }
        }

        return view('frontend.contents.store.order.finalization')->with('order', $order)->with('lines', $lines);
    }

    public function payment()
    {
        if (Cart::count() == 0) {
            return redirect()->route('frontend.cart.index');
        }
        $payment = $this->currentRequest->get('payment_method');
        if (empty($payment)) {
            flash()->error("Veuillez choisir un mode de paiement.");
            return redirect()->route('frontend.cart.index');
        }

        $attributes = $this->currentRequest->all();

        $rows = Cart::content();
        $orderedSeats = [];
        foreach ($rows as $key => $row) {
            $offer = (new Offer)->where('id', $row->id)->where('active', true)->first();
            if (!$offer || $offer->deal->end_at->isPast()) {
                flash()->error("L'événement \"{$row->name}\" n'est plus disponible. Veuillez vérifier votre panier.");
                return redirect()->route('frontend.cart.index');
            }

            if (!isset($attributes['items'][$row->id]) || count($attributes['items'][$row->id]) != $row->qty) {
                flash()->error("Vous devez renseigner tous les bénéficiaires de l'événement \"{$row->name}\".");
                return redirect()->route('frontend.cart.index');
            }

            if ($offer->deal->is_football) {
                foreach ($attributes['items'][$row->id] as $prof) {
                    if (isset($prof['cin']) && !empty($prof['cin']) && $prof['is_minor'] === 'No' && Profiteer::checkExist($prof['cin'], $offer->deal_id)) {
                        flash()->error("Le bénéficiaire avec le N° CIN \"{$prof['cin']}\" a déjà commandé un ticket pour le match \"{$row->name}\".");
                        return redirect()->route('frontend.cart.index');
                    }
                }
            }

            if (!$offer->checkAvailability($row->qty)) {
                flash()->error("L'événement \"{$row->name}\" n'est plus disponible en stock. Veuillez vérifier votre panier.");
                return redirect()->route('frontend.cart.index');
            }

            if (!$offer->checkMaxQuantity($row->qty)) {
                flash()->error('La quantité max autorisée pour l\'événement "' . $row->name . '" est de ' . $offer->quantity_max . ' tickets.');
                return redirect()->route('frontend.cart.index');
            }

            if ($row->options->has('seats')) {
                $reservedSeats = $offer->getReservedSeats();
                $seats = explode(',', $row->options->seats);
                foreach ($seats as $seat) {
                    if (in_array($seat, $reservedSeats)) {
                        flash()->error('La place "' . $seat . '" n\'est plus disponible, Veuillez sélectionner une autre place.');
                        return redirect()->route('frontend.cart.index');
                    }
                    if (in_array($seat, $orderedSeats)) {
                        flash()->error('La place "' . $seat . '" est déjà sélectionnée, merci de vérifier votre commande.');
                        return redirect()->route('frontend.cart.index');
                    }
                    $orderedSeats[] = $seat;
                }
            }
        }

        $user = auth()->user();
        if (session()->has($user->id . '_currentOrder')) {
            $this->updateOrder($user);
        } else {
            session([$user->id . '_currentOrder' => $this->createOrder($user)]);
        }
        $reference = session($user->id . '_currentOrder');
        $order = $this->currentModel->getByReference($reference);
        if ($order && $order->user_id == $user->id && in_array($order->payment_method, [2, 3])) {
            session()->forget($user->id . '_currentOrder');
            Cart::destroy();
            //$this->ticketService->generate($order->id);
            $this->mailService->sendOrderConfirmation($order);
            //$this->smsService->sendOrderConfirmation($order);
            return redirect()->route('frontend.order.finalization', ['reference' => $order->reference]);
        }

        $data['MERCHANT_ID'] = \Setting::get('payment_merchant_id');
        $data['AMOUNT'] = $order->total_paid * 100;//Cart::total(2, '.', '') * 100;
        $data['CURRENCY_CODE'] = '504';
        $data['MERCHANT_URL'] = route('frontend.cart.index');
        $data['FPAY_URLREPAUTO'] = route('frontend.order.validatePayment');
        $data['ORDER_ID'] = $reference;
        $data['CUSTOMER_LASTNAME'] = $user->last_name;
        $data['CUSTOMER_FIRSTNAME'] = $user->first_name;
        $data['CUSTOMER_EMAIL'] = $user->email;
        $data['CUSTOMER_PHONE'] = $user->phone;
        $data['FPAY_URL'] = \Setting::get('payment_url');
        $data['MERCHANT_KEY'] = \Setting::get('payment_key_hmac');
        $data['TRANSACTION_CAPTURE'] = 'true';
        $data['MERCHANT_GO'] = 'true';
        $data['AMOUNT_CONVERSION'] = 'false';
        $data['CUSTOMER_MESSAGE'] = '';
        $data['LANGUAGE'] = 'fr';
        $data['ORDER_DETAILS'] = '';
        $data['CUSTOMER_ADDRESS'] = $user->address;
        $data['CUSTOMER_ZIPCODE'] = '';
        $data['CUSTOMER_CITY'] = $user->city;
        $data['CUSTOMER_STATE'] = '';
        $data['CUSTOMER_COUNTRY'] = $user->country ? $user->country : 'MAR';
        $validation = $this->fPayService->sendData(\Setting::get('payment_url'), $data);
        if (!is_array($validation)) {
            //$order = $this->currentModel->getByReference($reference);
            if ($order && $order->user_id == $user->id && !in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED])) {
                session()->forget($user->id . '_currentOrder');
                $order->forceDelete();
            }
            flash()->error('Une erreur s\'est produite lors du traitement de votre demande, veuillez réessayer à nouveau.');
            return redirect(route('frontend.cart.index'));
        }

        return redirect(route('frontend.order.forward', $validation));
    }

    public function validatePayment()
    {
        if (!empty($_GET["ORDER_ID"]) && !empty($_GET["REFERENCE_ID"]) && !empty($_GET["TRACK_ID"])) {
            echo 'transation effectuée avec succès';
        } else {
            $data = $this->fPayService->receiveData(\Setting::get('payment_key_hmac'));
            if ($data != null) {
                $MERCHANT_ID = $data['MERCHANT_ID'];
                $REFERENCE_ID = $data['REFERENCE_ID'];
                $TRACK_ID = $data['TRACK_ID'];
                $RESPONSE_CODE = $data['RESPONSE_CODE'];
                $REASON_CODE = $data['REASON_CODE'];
                $ORDER_ID = $data['ORDER_ID'];
                $TRANSACTION_ID = $data['TRANSACTION_ID'];
                $TRANSACTION_DATE = $data['TRANSACTION_DATE'];
                $AMOUNT = $data['AMOUNT'];
                $CURRENCY_CODE = $data['CURRENCY_CODE'];
                $TRANSACTION_STATE = $data['TRANSACTION_STATE'];
                $MERCHANT_GO = $data['MERCHANT_GO'];
                $FPAY_RETURN = $data['FPAY_RETURN'];
                $order = $this->currentModel->getByReference($ORDER_ID);
                if ($MERCHANT_GO == "true" && $order) {
                    if ($RESPONSE_CODE == 0) {
                        if ($TRANSACTION_STATE == "AUTHORIZED") {
                            $order->status = Order::STATUS_ACCEPTED;
                            $order->payment_reference = $REFERENCE_ID;
                            $order->payment_track = $TRACK_ID;
                            $order->payment_transaction = $TRANSACTION_ID;
                            $order->payment_transaction_date = Carbon::createFromFormat('YmdHis', $TRANSACTION_DATE)->toDateTimeString();
                            $order->payment_response_code = $RESPONSE_CODE;
                            $order->payment_reason_code = $REASON_CODE;
                            $order->update();
                            $this->ticketService->generate($order->id);
                            $this->mailService->sendOrderConfirmation($order);
                            $this->mailService->sendOrderTickets($order);
                            $this->smsService->sendOrderConfirmation($order);
                            // MAJ avec Paiement accépté. => STATUS_ACCEPTED
                        }
                        if ($TRANSACTION_STATE == "REVERSED") {
                            $order->payment_reference = $REFERENCE_ID;
                            $order->payment_track = $TRACK_ID;
                            $order->payment_response_code = $RESPONSE_CODE;
                            $order->payment_reason_code = $REASON_CODE;
                            $order->status = Order::STATUS_CANCELED;
                            $order->update();
                            // MAJ avec Paiement annulé. => STATUS_CANCELED
                        }
                        if ($TRANSACTION_STATE == "CAPTURED") {
                            $order->payment_reference = $REFERENCE_ID;
                            $order->payment_track = $TRACK_ID;
                            $order->payment_response_code = $RESPONSE_CODE;
                            $order->payment_reason_code = $REASON_CODE;
                            $order->status = Order::STATUS_CAPTURED;
                            $order->update();
                            // MAJ avec Paiement capturé. => STATUS_CAPTURED
                        }
                    } else {
                        if ($TRANSACTION_STATE == "EXPIRED") {
                            if ($order->payment_method == 1) {
                                $order->payment_reference = $REFERENCE_ID;
                                $order->payment_track = $TRACK_ID;
                                $order->payment_response_code = $RESPONSE_CODE;
                                $order->payment_reason_code = $REASON_CODE;
                                $order->status = Order::STATUS_EXPIRED;
                                $order->update();
                                // MAJ Paiement abondonné => STATUS_EXPIRED
                            }
                        } else {
                            if ($order->payment_method == 1) {
                                $order->payment_reference = $REFERENCE_ID;
                                $order->payment_track = $TRACK_ID;
                                $order->payment_response_code = $RESPONSE_CODE;
                                $order->payment_reason_code = $REASON_CODE;
                                $order->status = Order::STATUS_REFUSED;
                                $order->update();
                                // MAJ Paiement refusé. => STATUS_REFUSED
                            }
                        }
                    }
                }
                echo $FPAY_RETURN;
            } else {
                echo '{"FPAY_MESSAGE_VERSION":null,"MERCHANT_ID":"","ORDER_ID":null,"REFERENCE_ID":null,"TRACK_ID":null,"MERCHANT_GO":"false","MESSAGE_SIGNATURE":null}';
            }
        }
    }

    public function updateUser()
    {
        if (Cart::count() == 0) {
            return redirect()->route('frontend.cart.index');
        }
        $attributes = $this->currentRequest->all();
        $user = auth()->user();
        $attributes['email'] = $user->email;
        $validator = $user->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans('user.cannot_save'));

            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }
        if ($user->update($attributes)) {
            $this->updateOrder($user);
            flash()->success('Informations mises à jour avec succès.');

            return redirect(route('frontend.cart.index'));
        }
    }

    private function createOrder($user)
    {
        $payment = $this->currentRequest->get('payment_method');
        if (empty($payment)) {
            $payment = 1;
        }

        $attributes = $this->currentRequest->all();

        $data['user_id'] = $user->id;
        $data['reference'] = $this->currentModel->generateReference();
        $data['address'] = $user->address . ', ' . $user->city . ' - ' . $user->country;
        $data['status'] = ($payment == 1) ? Order::STATUS_WAITING : Order::STATUS_RESERVED;
        $data['comment'] = $this->currentRequest->get('comment');
        $data['payment_method'] = $payment;
        //$data['total_paid'] = Cart::total(2, '.', '');
        $this->currentModel->fill($data);
        if ($this->currentModel->save()) {
            $rows = Cart::content();
            $total = 0.00;
            foreach ($rows as $key => $row) {
                $offer = (new Offer)->where('id', $row->id)->where('active', true)->first();
                if (!$offer || $offer->deal->end_at->isPast()) {
                    continue;
                }
                if (!$offer->checkAvailability($row->qty)) {
                    continue;
                }
                $price = empty($offer->reservation_price) ? $offer->price : $offer->reservation_price;
                $total += $price * $row->qty;
                $line = new OrderLine([
                    'offer_id' => $row->id,
                    'product_name' => $row->name,
                    'offer_name' => $row->options->offer,
                    'product_price' => $price,
                    'quantity' => $row->qty,
                    'total_price' => $price * $row->qty,//$row->total
                    'seats' => $row->options->has('seats') ? $row->options->seats : null,
                ]);
                $this->currentModel->lines()->save($line);
                if (isset($attributes['items'][$row->id])) {
                    foreach ($attributes['items'][$row->id] as $prof) {
                        if (!isset($prof['full_name'])) {
                            continue;
                        }
                        $profiteer = new Profiteer;
                        $profiteer->order_line_id = $line->id;
                        $profiteer->full_name = $prof['full_name'];
                        if (isset($prof['cin'])) {
                            $profiteer->cin = $prof['cin'];
                        }
                        if (isset($prof['is_minor'])) {
                            $profiteer->is_minor = $prof['is_minor'] === 'Yes';
                        }
                        $profiteer->save();
                    }
                }
            }
            $this->currentModel->total_paid = $total;
            $this->currentModel->update();
        }

        return $data['reference'];
    }

    private function updateOrder($user)
    {
        $reference = session($user->id . '_currentOrder');
        $order = $this->currentModel->getByReference($reference);
        if ($order && !in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED, Order::STATUS_RESERVED])) {
            $attributes = $this->currentRequest->all();
            //$data['address'] = $user->address . ', ' . $user->city . ' - ' . $user->country;
            //$data['total_paid'] = Cart::total(2, '.', '');
            $order->lines()->delete();
            $rows = Cart::content();
            $total = 0.00;
            foreach ($rows as $key => $row) {
                $offer = (new Offer)->where('id', $row->id)->where('active', true)->first();
                if (!$offer || $offer->deal->end_at->isPast()) {
                    continue;
                }
                if (!$offer->checkAvailability($row->qty)) {
                    continue;
                }
                $price = empty($offer->reservation_price) ? $offer->price : $offer->reservation_price;
                $total += $price * $row->qty;
                $line = new OrderLine([
                    'offer_id' => $row->id,
                    'product_name' => $row->name,
                    'offer_name' => $row->options->offer,
                    'product_price' => $price,
                    'quantity' => $row->qty,
                    'total_price' => $price * $row->qty,
                    'seats' => $row->options->has('seats') ? $row->options->seats : null,
                ]);
                $order->lines()->save($line);
                if (isset($attributes['items'][$row->id])) {
                    foreach ($attributes['items'][$row->id] as $prof) {
                        if (!isset($prof['full_name'])) {
                            continue;
                        }
                        $profiteer = new Profiteer;
                        $profiteer->order_line_id = $line->id;
                        $profiteer->full_name = $prof['full_name'];
                        if (isset($prof['cin'])) {
                            $profiteer->cin = $prof['cin'];
                        }
                        if (isset($prof['is_minor'])) {
                            $profiteer->is_minor = $prof['is_minor'] === 'Yes';
                        }
                        $profiteer->save();
                    }
                }
            }
            $payment = $this->currentRequest->get('payment_method');
            if (empty($payment)) {
                $payment = 1;
            }
            $data['address'] = $user->address . ', ' . $user->city . ' - ' . $user->country;
            $data['total_paid'] = $total;
            $data['comment'] = $this->currentRequest->get('comment');
            $data['payment_method'] = $payment;
            $data['status'] = $payment == 1 ? Order::STATUS_WAITING : Order::STATUS_RESERVED;
            $order->update($data);
        } else {
            session([$user->id . '_currentOrder' => $this->createOrder($user)]);
        }
    }

    public function createRecipients($reference)
    {
        $order = $this->currentModel->getByReference($reference);
        if (!$order) {
            throw (new ModelNotFoundException)->setModel(get_class(Order), $order->id);
        }

        $user = auth()->user();
        if ($order->user_id != $user->id || !in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED, Order::STATUS_RESERVED])) {
            return redirect()->route('frontend.cart.index');
        }

        $attributes = $this->currentRequest->all();

        if (isset($attributes['rows']) && is_array($attributes['rows'])) {
            $ids = $order->lines->pluck('id')->toArray();
            $recipients = [];
            foreach ($attributes['rows'] as $id => $row) {
                if (in_array($id, $ids)) {
                    $line = OrderLine::find($id);
                    foreach ($row as $key => $values) {
                        if (!is_array($values)) {
                            continue;
                        }
                        $recipient = new Recipient();
                        $values['order_line_id'] = $id;
                        try {
                            $values['birth_date'] = Carbon::createFromFormat('d/m/Y', $values['birth_date'])->toDateTimeString();
                        } catch (\Exception $e) {
                            flash()->error('Le champ date de naissance est invalide.');
                            return redirect()->back()->withInput($attributes);
                        }

                        $validator = $recipient->validator($values);
                        if ($validator->fails()) {
                            flash()->error($validator->errors()->first());
                            $line->recipients()->delete();
                            return redirect()->back()->withInput($attributes);
                        }

                        $values['avatar'] = $values['avatar']->store($recipient->path, ['disk' => 'local']);
                        $recipient->fill($values);
                        if ($recipient->save() && in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED])) {
                            $recipients[] = $recipient;
                        }
                    }
                }
            }
            foreach ($recipients as $recipient) {
                $line = OrderLine::find($recipient->order_line_id);
                $this->mailService->sendNewSubscription($recipient, $line);
            }
            flash()->success('Votre abonnement a bien été enregisté. Merci !!');
        } else {
            flash()->error('Une erreur s\'est produite merci de réessayer à nouveau.');
        }

        return redirect()->route('frontend.order.finalization', ['reference' => $reference]);
    }
}
