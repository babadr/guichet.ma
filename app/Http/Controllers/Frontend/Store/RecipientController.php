<?php

namespace App\Http\Controllers\Frontend\Store;

use App\Http\Controllers\FrontendController;
use App\Models\Store\OrderLine;
use Illuminate\Http\Request;
use App\Models\Store\Order;
use App\Models\Store\Recipient;
use Carbon\Carbon;
use App\Services\MailService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RecipientController extends FrontendController
{

    /**
     * The MailService instance.
     *
     * @var MailService
     */
    public $mailService;


    /**
     * @param Request $request
     * @param Order $order
     * @param MailService $mailService
     */
    public function __construct(Request $request, Order $order, MailService $mailService)
    {
        $this->mailService = $mailService;
        parent::__construct($request, $order);
    }

    public function index($reference)
    {
        $order = $this->currentModel->getByReference($reference);
        $user = auth()->user();
        if (!$order) {
            throw (new ModelNotFoundException)->setModel(get_class(Order), $order->id);
        }
        if ($order->user_id != $user->id || !in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED, Order::STATUS_RESERVED])) {
            return redirect()->route('frontend.order.list');
        }

        \SEO::setTitle('Formulaire d\'abonnement Raja Club Athlétic');

        $lines = [];
        foreach ($order->lines as $line) {
            if ($line->offer->deal->category_id == 17 && !$line->recipients->count()) {
                $lines[$line->id]['total'] = $line->quantity;
                $lines[$line->id]['offer'] = $line->offer_name;
            }
        }

        if (!count($lines)) {
            return redirect()->route('frontend.order.list');
        }

        return view('frontend.contents.store.recipient.index')->with('order', $order)->with('lines', $lines);
    }


    public function create($reference)
    {
        $order = $this->currentModel->getByReference($reference);
        if (!$order) {
            throw (new ModelNotFoundException)->setModel(get_class(Order), $order->id);
        }

        $user = auth()->user();
        if ($order->user_id != $user->id || !in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED, Order::STATUS_RESERVED])) {
            return redirect()->route('frontend.order.list');
        }

        $attributes = $this->currentRequest->all();
        if (isset($attributes['rows']) && is_array($attributes['rows'])) {
            $ids = $order->lines->pluck('id')->toArray();
            $recipients = [];
            foreach ($attributes['rows'] as $id => $row) {
                if (in_array($id, $ids)) {
                    $line = OrderLine::find($id);
                    foreach ($row as $key => $values) {
                        if (!is_array($values)) {
                            continue;
                        }
                        $recipient = new Recipient();
                        $values['order_line_id'] = $id;
                        try {
                            $values['birth_date'] = Carbon::createFromFormat('d/m/Y', $values['birth_date'])->toDateTimeString();
                        } catch (\Exception $e) {
                            flash()->error('Le champ date de naissance est invalide.');
                            return redirect()->back()->withInput($attributes);
                        }

                        $validator = $recipient->validator($values);
                        if ($validator->fails()) {
                            flash()->error($validator->errors()->first());
                            $line->recipients()->delete();
                            return redirect()->back()->withInput($attributes);
                        }

                        $values['avatar'] = $values['avatar']->store($recipient->path, ['disk' => 'local']);
                        $recipient->fill($values);
                        if ($recipient->save() && in_array($order->status, [Order::STATUS_ACCEPTED, Order::STATUS_VALIDATED, Order::STATUS_CAPTURED])) {
                            $recipients[] = $recipient;
                        }
                    }
                }
            }
            foreach ($recipients as $recipient) {
                $line = OrderLine::find($recipient->order_line_id);
                $this->mailService->sendNewSubscription($recipient, $line);
            }
            flash()->success('Votre abonnement a bien été enregisté. Merci !!');
        } else {
            flash()->error('Une erreur s\'est produite merci de réessayer à nouveau.');
        }

        return redirect()->route('frontend.order.list');
    }
}
