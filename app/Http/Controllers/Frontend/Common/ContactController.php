<?php

namespace App\Http\Controllers\Frontend\Common;

use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use App\Models\Common\Contact;
use App\Services\MailService;

class ContactController extends FrontendController
{

    /**
     * The MailService instance.
     *
     * @var MailService
     */
    public $mailService;

    /**
     * @param Request $request
     * @param Contact $contact
     * @param MailService $mailService
     *
     * @return void
     */
    public function __construct(Request $request, Contact $contact, MailService $mailService)
    {
        $this->mailService = $mailService;
        parent::__construct($request, $contact);
    }

    /**
     * Handle create request.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \SEO::setTitle('Contactez-nous');
        return view('frontend.contents.common.contact.form');
    }

    /**
     * Store a newly created resource
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->currentRequest->all();
        $validator = $this->currentModel->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans('contact.cannot_save'));

            return redirect()->back()->withErrors($validator)->withInput($attributes);
        } else {
            $this->currentModel->fill($attributes);
            if ($this->currentModel->save()) {
                $this->mailService->sendContactRequest($this->currentModel);
                flash()->success(trans('contact.contact_success'));

                return redirect()->route('frontend.contact.index');
            }
        }
    }

}
