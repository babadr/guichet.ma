<?php

namespace App\Http\Controllers\Frontend\Common;

use App\Http\Controllers\FrontendController;

class ProfileController extends FrontendController
{
    /**
     * profile index controller.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        \SEO::setTitle('Mon compte - Profil');
        return view('frontend.contents.common.user.profile')
            ->with('user', auth()->user());
    }

    /**
     * update user profile
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $user = auth()->user();
        $attributes = $this->currentRequest->all();
        $attributes['email'] = $user->email;
        $validator = $user->validator($attributes);
        if ($validator->fails()) {
            flash()->error(trans('user.cannot_save'));
            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }
        if ($user->update($attributes)) {
            flash()->success('Informations mises à jour avec succès.');
        }

        return view('frontend.contents.common.user.profile')
            ->with('user', auth()->user());
    }

}
