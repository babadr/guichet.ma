<?php

namespace App\Http\Controllers\Frontend\Common;

use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use App\Models\Common\Newsletter;
use Carbon\Carbon;

class NewsletterController extends FrontendController
{

    /**
     * @param Request $request
     * @param Newsletter $newsletter
     *
     * @return void
     */
    public function __construct(Request $request, Newsletter $newsletter)
    {
        parent::__construct($request, $newsletter);
    }

    /**
     * Store a newly created resource
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $attributes = $this->currentRequest->all();
        $validator = $this->currentModel->validator($attributes);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => trans('newsletter.error_subscription')]);
        }

        $this->currentModel->fill($attributes);
        $this->currentModel->save();

        return response()->json(['success' => true, 'message' => trans('newsletter.success_subscription')]);
    }

    public function create()
    {
        $attributes = $this->currentRequest->all();
        if (empty($attributes['birth_date'])) {
            unset($attributes['birth_date']);
        } else {
            $attributes['birth_date'] = Carbon::createFromFormat('d/m/Y', $attributes['birth_date'])->toDateTimeString();
        }
        $validator = $this->currentModel->validator($attributes);
        if ($validator->fails()) {
            flash()->error($validator->errors()->first());

            return redirect()->back()->withErrors($validator)->withInput($attributes);
        }

        $this->currentModel->fill($attributes);
        if ($this->currentModel->save()) {
            try {
                $sources = [1 => 'Site web', 2 => 'LP Champions league', 3 => 'LP PS4', 4 => 'LP Women'];
                $civilities = [1 => 'Mlle.', 2 => 'Mme.', 3 => 'Mr.'];
                $post['firstname'] = $attributes['first_name'];
                $post['lastname'] = $attributes['last_name'];
                $post['email'] = $attributes['email'];
                $post['field[1]'] = 'Prospect';
                $post['field[3]'] = $attributes['birth_date'];
                $post['field[2]'] = $civilities[$attributes['civility']];
                $post['field[4]'] = $sources[$attributes['source']];
                $post['u'] = '1';
                $post['f'] = '1';
                $post['s'] = '';
                $post['c'] = '0';
                $post['m'] = '0';
                $post['act'] = 'sub';
                $post['v'] = '2';
                $ch = curl_init('https://guichetma.activehosted.com/proc.php');
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                curl_exec($ch);
                curl_close($ch);
            } catch (\Exception $e) {

            }
        }
        flash()->success('Votre inscription a bien été prise en compte.');

        return redirect()->back();
    }

}
