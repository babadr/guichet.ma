<?php

namespace App\Http\Controllers\Frontend\Content;

use App\Http\Controllers\FrontendController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\Content\Page;

class PageController extends FrontendController
{

    /**
     * @param Request $request
     * @param Page $page
     *
     * @return void
     */
    public function __construct(Request $request, Page $page)
    {
        parent::__construct($request, $page);
    }

    /**
     * View page details
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $page = $this->currentModel->findOrFail($id, ['id', 'title', 'content', 'active']);

        if (!$page->active) {
            throw (new ModelNotFoundException)->setModel(get_class($this->currentModel), $id);
        }

        \SEO::setTitle($page->seo->seo_title ? $page->seo->seo_title : $page->title);
        \SEO::setDescription($page->seo->seo_description);
        \SEO::opengraph()->setUrl(config('app.url') . $page->seo->seo_alias);
        \SEO::setCanonical(config('app.url') . $page->seo->seo_alias);
        \SEO::opengraph()->addProperty('type', 'articles');

        return view('frontend.contents.content.page.view')->with('page', $page);
    }

}
