<?php

namespace App\Http\Controllers\Frontend\Content;

use App\Http\Controllers\FrontendController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Models\Content\Post;
use App\Models\Content\CategoryPost;

class PostController extends FrontendController
{

    /**
     * @param Request $request
     * @param Post $post
     *
     * @return void
     */
    public function __construct(Request $request, Post $post)
    {
        parent::__construct($request, $post);
    }

    /**
     * List posts
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->currentModel->select(['id', 'title', 'category_id', 'content', 'image', 'created_at'])->active()->orderBy('created_at', 'desc')->paginate(12);

        \SEO::setTitle('Actualités des festivals et recommandations - Guichet Magazine');

        return view('frontend.contents.content.post.index')
            ->with('posts', $posts)
            ->with('categories', (new CategoryPost)->where('active', '=', 1)->get());
    }

    /**
     * View post details
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $post = $this->currentModel->findOrFail($id, ['id', 'title', 'content', 'active', 'image', 'created_at']);

        if (!$post->active) {
            throw (new ModelNotFoundException)->setModel(get_class($this->currentModel), $id);
        }

        $previous = $this->currentModel->where('id', '<', $id)->orderBy('id', 'desc')->first();
        $next = $this->currentModel->where('id', '>', $id)->orderBy('id', 'asc')->first();

        \SEO::setTitle(($post->seo->seo_title ? $post->seo->seo_title : $post->title) . ' - Guichet Magazine');
        \SEO::setDescription($post->seo->seo_description);
        \SEO::opengraph()->setUrl(config('app.url') . $post->seo->seo_alias);
        \SEO::setCanonical(config('app.url') . $post->seo->seo_alias);
        \SEO::opengraph()->addProperty('type', 'articles');

        return view('frontend.contents.content.post.view')
            ->with('post', $post)
            ->with('previous', $previous)
            ->with('categories', (new CategoryPost)->where('active', '=', 1)->get())
            ->with('next', $next);
    }

    public function category($id)
    {
        $category = (new CategoryPost)->where('id', '=', $id)->where('active', '=', 1)->first();
        if (!$category) {
            throw (new ModelNotFoundException)->setModel(get_class(CategoryPost), $id);
        }

        $posts = $this->currentModel->select(['id', 'title', 'category_id', 'content', 'image', 'created_at'])
            ->active()->where('category_id', $id)->orderBy('created_at', 'desc')->paginate(12);

        \SEO::setTitle(($category->seo->seo_title ? $category->seo->seo_title : $category->name) . ' - Guichet Magazine');
        \SEO::setDescription($category->seo->seo_description);
        \SEO::opengraph()->setUrl(config('app.url') . $category->seo->seo_alias);
        \SEO::setCanonical(config('app.url') . $category->seo->seo_alias);
        \SEO::opengraph()->addProperty('type', 'articles');

        return view('frontend.contents.content.post.index')
            ->with('posts', $posts)
            ->with('categories', (new CategoryPost)->where('active', '=', 1)->get());
    }


}
