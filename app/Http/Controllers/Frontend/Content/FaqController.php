<?php

namespace App\Http\Controllers\Frontend\Content;

use App\Http\Controllers\FrontendController;
use Illuminate\Http\Request;
use App\Models\Content\Faq;

class FaqController extends FrontendController
{

    /**
     * @param Request $request
     * @param Faq $faq
     *
     * @return void
     */
    public function __construct(Request $request, Faq $faq)
    {
        parent::__construct($request, $faq);
    }

    /**
     * List FAQ
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = $this->currentModel->select(['id', 'title', 'content'])->active()->get();

        \SEO::setTitle('Aide / Foire aux questions');

        return view('frontend.contents.content.faq.index')->with('faqs', $faqs);
    }

}
