<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\FrontendController;
use App\Models\Content\Seo;
use App\Models\Deal\Category;
use App\Models\Deal\Deal;
use App\Models\Content\Page;
use App\Models\Content\Post;

class IndexController extends FrontendController
{

    /**
     * Display Homepage
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = (new Deal())->getLastSliderDeals();
//        $categories = (new Category())
//            ->where('show_on_home', '=', 1)
//            ->where('active', '=', 1)
//            ->orderBy('order', 'asc')
//            ->get();

        $frontDeals = (new Deal())->select('*')
            ->with(['seo', 'category', 'city'])
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) AS date_diff')
            ->where('active', '=', 1)
            ->where('expired', '=', 0)
            ->where('is_private', '=', 0)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->orderBy('date_diff', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->get();

        \SEO::setTitle(\Setting::get('meta_title'));
        \SEO::setDescription(\Setting::get('meta_description'));
        \SEO::opengraph()->setUrl(route('frontend.index'));
        \SEO::setCanonical(route('frontend.index'));
        \SEO::opengraph()->addProperty('type', 'articles');

        return view('frontend.contents.index')->with(compact('slides', 'frontDeals'));
    }

    public function search()
    {
        $key = request()->get('q', null);
        $events = (new Deal())->select('*')
            ->selectRaw('TIMESTAMPDIFF(MINUTE, NOW(), end_at) AS date_diff')
            ->where('active', '=', 1)
            ->where('is_private', '=', 0)
            ->where('start_at', '<=', date('Y-m-d H:i:s'))
            ->where('end_at', '>=', date('Y-m-d H:i:s'))
            ->orderBy('date_diff', 'ASC')
            ->orderBy('created_at', 'DESC');

        if ($key) {
            $events = $events->where(function ($query) use ($key) {
                $query->where('title', 'like', '%' . $key . '%')
                    ->orWhere('short_description', 'like', '%' . $key . '%')
                    ->orWhere('description', 'like', '%' . $key . '%');
            });
        }

        $events = $events->paginate(9);

        return view('frontend.contents.search')->with(compact('events'));
    }

    /**
     * Display dynamic page
     *
     * @param string $slug
     *
     * @return \Illuminate\Http\Response
     */
    public function view($slug)
    {
        if (strpos($slug, 'deals/') !== false) {
            return \Redirect::to(str_replace('deals/', '', $slug), 301);
        }

        $minutes = \Config::get('cache.pages_caching_minutes');
        $page = \Cache::remember($slug, $minutes, function () use ($slug) {
            return Seo::where('seo_alias', $slug)->firstOrFail();
        });

        $uses = explode('@', $page->seo_route);

        return \App::make('App\Http\Controllers\\' . $uses[0])->callAction($uses[1], [$page->model_id]);
    }

    /**
     * generate dynamic sitempa xml
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function sitemap()
    {
        $url = 'https://www.guichet.ma/';
        $sitemap = \App::make('sitemap');
        $sitemap->setCache('laravel.sitemap', 60);
        if (!$sitemap->isCached()) {
            $sitemap->add(route('frontend.index'), date('Y-m-d H:i:s'), '1.0', 'daily');
            $sitemap->add(route('frontend.post.index'), date('Y-m-d H:i:s'), '1.0', 'daily');
            $sitemap->add(route('frontend.contact.index'), '2018-03-18T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(route('frontend.faq.index'), '2018-03-18T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(route('frontend.cart.index'), '2018-03-18T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(route('frontend.auth.showRegister'), '2018-03-18T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(route('frontend.auth.password.showForgot'), '2018-03-18T12:30:00+02:00', '0.5', 'monthly');
            $sitemap->add(route('frontend.auth.showLogin'), '2018-03-18T12:30:00+02:00', '0.5', 'monthly');
            $categories = Category::where('active', 1)->orderBy('order', 'asc')->get();
            foreach ($categories as $category) {
                $sitemap->add($url . $category->seo->seo_alias, $category->updated_at, '0.9', 'daily');
            }
            $pages = Page::where('active', 1)->get();
            foreach ($pages as $page) {
                $sitemap->add($url . $page->seo->seo_alias, $page->updated_at, '0.5', 'monthly');
            }
            $posts = Post::where('active', 1)->get();
            foreach ($posts as $post) {
                $images = [];
                if (!empty($post->image)) {
                    $images = [
                        ['url' => $url . 'img/cache/original/' . $post->image, 'title' => $post->title, 'caption' => $post->title],
                    ];
                }

                $sitemap->add($url . $post->seo->seo_alias, $post->updated_at, '0.5', 'monthly', $images);
            }
            $deals = Deal::where('active', 1)->where('is_private', 0)->get();
            foreach ($deals as $deal) {
                $images = array();
                foreach ($deal->medias as $image) {
                    $images[] = array(
                        'url' => $url . 'img/cache/original/' . $image->filename,
                        'title' => $deal->title,
                        'caption' => $deal->caption
                    );
                }

                $sitemap->add($url . $deal->seo->seo_alias, $deal->updated_at, '0.8', 'daily', $images);
            }
        }

        return $sitemap->render('xml');
    }

}
