<?php

namespace App\Http\Controllers\Frontend\Ticket;

use App\Http\Controllers\FrontendController;
use App\Models\Store\Order;
use App\Models\Store\Ticket;
use App\Services\TicketService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Nathanmac\Utilities\QRCode\Generator;
use PDF;

class TicketController extends FrontendController
{

    /**
     * The TicketService instance.
     *
     * @var TicketService $ticketService
     */
    public $ticketService;

    /**
     * @param Request $request
     * @param Ticket $ticket
     * @param TicketService $ticketService
     */
    public function __construct(Request $request, Ticket $ticket, TicketService $ticketService)
    {
        $this->ticketService = $ticketService;
        parent::__construct($request, $ticket);
    }

    /*
     * Download Ticket
     */
    public function download($id)
    {
        /** @var Order $order */
        $order = (new Order)->findOrFail($id);
        $user = auth()->user();
        if ($user->id !== $order->user_id) {
            abort(404);
        }

        if ($order->status == Order::STATUS_ACCEPTED || $order->status == Order::STATUS_CAPTURED) {
            config(['pdf.format' => 'A4']);
//            return view('frontend.partials.ticket_online', ['order' => $order, 'isOffile' => false]);
            $pdf = PDF::loadView('frontend.partials.ticket_online', ['order' => $order, 'isOffile' => false]);
            return $pdf->download('guichet.ma-tickets-' . $order->reference . '.pdf');
        }

        abort(404);
    }

    /*
     * validate Ticket
     */
    public function check($token)
    {
        $ticket = $this->ticketService->validate($token);
        return view('frontend.contents.common.ticket.validate')
            ->with('token', $token)
            ->with('ticket', $ticket);
    }

    /*
     * validate Ticket
     */
    public function validateTicket(Request $request)
    {
        $token = $request->get('token');
        $ticket = $this->ticketService->validate($token);
        if (!is_object($ticket)) {
            return view('frontend.contents.common.ticket.validate')
                ->with('token', $token)
                ->with('ticket', $ticket);
        }

        /** @var Ticket $ticket */
        if ($ticket && !$ticket->status) {
            $ticket['status'] = true;
            $ticket->save();
        }

        return view('frontend.contents.common.ticket.validated');
    }

    /**
     * Generates a QR Code
     *
     * @param int $id
     * @param int $size
     * @return \Endroid\QrCode\QrCode
     */
    public function generateQrCode($id, $size = 80)
    {
        $ticket = $this->currentModel->findOrFail($id);
        $token = $ticket->hash;//route('frontend.ticket.check', $ticket->hash);
        $file = Generator::generate($token, $size, '000000', false);
        return Response::stream(function () use ($file) {
            return $file;
        }, 200, ['Content-Type' => 'image/png']);
    }

    public function generateBarCode($id, $size = 80, $w = 1)
    {
        $ticket = $this->currentModel->findOrFail($id);
        $image = \DNS1D::getBarcodePNG(strtoupper($ticket->hash), 'C128A', $w, $size);
        $response = Response::make(base64_decode($image), 200);
        $response->header('Content-Type', 'image/png');

        return $response;
    }
}
