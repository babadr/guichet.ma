<?php

Route::group(['namespace' => 'Store', 'prefix' => 'ticket',  'as' => 'ticket.', 'middleware' => ['jwt.verify']], function () {

    Route::post('/validate', [
        'as' => 'validate',
        'uses' => 'TicketController@check'
    ]);

    Route::get('/download/{id}', [
        'as' => 'view',
        'uses' => 'TicketController@download',
    ])->where('id', '[0-9]+');

});
