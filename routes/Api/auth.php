<?php

Route::group(['namespace' => 'Common', 'prefix' => 'user',  'as' => 'user.'], function () {
    Route::post('/register', [
        'as' => 'register',
        'uses' => 'AuthController@register'
    ]);

    Route::post('/login', [
        'as' => 'login',
        'uses' => 'AuthController@login'
    ]);

    Route::post('/recover', [
        'as' => 'recover',
        'uses' => 'AuthController@recover'
    ]);

    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::post('/logout', [
            'as' => 'logout',
            'uses' => 'AuthController@logout'
        ]);

        Route::post('/refresh', [
            'as' => 'refresh',
            'uses' => 'AuthController@refresh'
        ]);

        Route::get('/me', [
            'as' => 'me',
            'uses' => 'AuthController@me'
        ]);
    });

});
