<?php

Route::group(['namespace' => 'Event', 'prefix' => 'category',  'as' => 'category.'], function () {

    Route::get('/list', [
        'as' => 'index',
        'uses' => 'CategoryController@index'
    ]);

    Route::get('/view/{id}', [
        'as' => 'view',
        'uses' => 'CategoryController@view',
    ])->where('id', '[0-9]+');

    Route::get('/view/{id}/current/events', [
        'as' => 'view.current.events',
        'uses' => 'CategoryController@currentEvents',
    ])->where('id', '[0-9]+');

    Route::get('/view/{id}/past/events', [
        'as' => 'view.past.events',
        'uses' => 'CategoryController@pastEvents',
    ])->where('id', '[0-9]+');

});
