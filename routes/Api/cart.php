<?php

Route::group(['namespace' => 'Store', 'prefix' => 'cart',  'as' => 'cart.'], function () {

    Route::get('/', [
        'as' => 'index',
        'uses' => 'CartController@index'
    ]);

    Route::post('/add', [
        'as' => 'add',
        'uses' => 'CartController@add',
    ]);

    Route::post('/update/{id}', [
        'as' => 'update',
        'uses' => 'CartController@update',
    ]);

    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'CartController@delete',
    ]);

});
