<?php

Route::group(['namespace' => 'Common', 'prefix' => '',  'as' => 'home.'], function () {

    Route::get('/home/slides', [
        'as' => 'slides',
        'uses' => 'HomeController@slides'
    ]);

    Route::post('/search', [
        'as' => 'search',
        'uses' => 'HomeController@search',
    ]);

});
