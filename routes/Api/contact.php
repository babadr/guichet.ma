<?php

Route::group(['namespace' => 'Common', 'prefix' => 'contact', 'as' => 'contact.'], function () {

    Route::post('/create', [
        'as' => 'store',
        'uses' => 'ContactController@create',
    ]);

});
