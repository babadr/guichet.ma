<?php

Route::group(['namespace' => 'Store', 'prefix' => 'order',  'as' => 'order.'], function () {



    Route::group(['middleware' => ['jwt.verify']], function() {

        Route::get('/list', [
            'as' => 'list',
            'uses' => 'OrderController@index'
        ]);

        Route::get('/view/{id}', [
            'as' => 'view',
            'uses' => 'OrderController@view',
        ])->where('id', '[0-9]+');
    });

});
