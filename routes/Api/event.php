<?php

Route::group(['namespace' => 'Event', 'prefix' => 'event',  'as' => 'event.'], function () {

    Route::get('/list', [
        'as' => 'index',
        'uses' => 'EventController@index'
    ]);

    Route::get('/view/{id}', [
        'as' => 'view',
        'uses' => 'EventController@view',
    ])->where('id', '[0-9]+');


    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::get('/validator', [
            'as' => 'validator',
            'uses' => 'EventController@validator'
        ]);
    });

});
