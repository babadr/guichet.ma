<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api', 'as' => 'api.'], function () {
    Route::get('/deals', [
        'as' => 'deal.index',
        'uses' => 'DealController@index'
    ]);

    Route::match(['get', 'post'], '/order/check', [
        'as' => 'order.check',
        'uses' => 'OrderController@check'
    ]);

    Tools::includeRoutes('Api');
});

