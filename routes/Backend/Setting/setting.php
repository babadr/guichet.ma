<?php

/*
  |--------------------------------------------------------------------------
  | Backend Setting Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Setting', 'prefix' => 'setting', 'as' => 'backend.setting.'], function () {

    /*
     * List of all Settings
     */
    Route::get('/', [
        'as' => 'index',
        'uses' => 'SettingController@index',
        'middleware' => ['permission:backend.setting.read']
    ]);

    /*
     * Submited form for edition of one Role
     */
    Route::post('/save', [
        'as' => 'update',
        'uses' => 'SettingController@update',
        'middleware' => ['permission:backend.setting.update']
    ]);
});
