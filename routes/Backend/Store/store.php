<?php

/*
  |--------------------------------------------------------------------------
  | Backend Order Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Store', 'prefix' => 'order', 'as' => 'backend.order.'], function () {

    /*
     * List of all Orders
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'OrderController@index',
        'middleware' => ['permission:backend.order.read']
    ]);

    Route::get('/show/{id}', [
        'as' => 'show',
        'uses' => 'OrderController@show',
        'middleware' => ['permission:backend.order.read']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Order
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'OrderController@update',
        'middleware' => ['permission:backend.order.update']
    ])->where('id', '[0-9]+');

    Route::get('/create', [
        'as' => 'create',
        'uses' => 'OrderController@create',
        'middleware' => ['permission:backend.order.create']
    ]);


    Route::post('/add/deal/{id}', [
        'as' => 'add_deal',
        'uses' => 'OrderController@addDeal',
        'middleware' => ['permission:backend.order.create']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for creation of one Seller
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'OrderController@store',
        'middleware' => ['permission:backend.order.create']
    ]);

    /*
     * Ajax deleteion of one Provider
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'OrderController@delete',
        'middleware' => ['ajax', 'permission:backend.order.delete'],
    ])->where('id', '[0-9]+');

    Route::post('/{id}/profiteers', [
        'as' => 'profiteers.update',
        'uses' => 'OrderController@updateProfiteers',
        'middleware' => ['permission:backend.order.update']
    ])->where('id', '[0-9]+');

    Route::get('/{id}/profiteers', [
        'as' => 'profiteers.edit',
        'uses' => 'OrderController@editProfiteers',
        'middleware' => ['permission:backend.order.update']
    ]);
});
