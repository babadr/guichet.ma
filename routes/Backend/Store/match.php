<?php


Route::group(['namespace' => 'Store', 'prefix' => 'match', 'as' => 'backend.match.'], function () {

    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'MatchController@index',
        'middleware' => ['role:superadmin|admin']
    ]);

    Route::match(['get', 'post'], '/{event_id}/profiteers', [
        'as' => 'profiteers.index',
        'uses' => 'ProfiteerController@index',
        'middleware' => ['role:superadmin|admin']
    ])->where('event_id', '[0-9]+');

    Route::get('/{event_id}/profiteers/edit/{id}', [
        'as' => 'profiteers.edit',
        'uses' => 'ProfiteerController@edit',
        'middleware' => ['role:superadmin|admin']
    ])->where('event_id', '[0-9]+')->where('id', '[0-9]+');


    Route::post('/{event_id}/profiteers/edit/{id}', [
        'as' => 'profiteers.update',
        'uses' => 'ProfiteerController@update',
        'middleware' => ['role:superadmin|admin']
    ])->where('event_id', '[0-9]+')->where('id', '[0-9]+');

});
