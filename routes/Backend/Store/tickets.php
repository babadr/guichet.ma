<?php

/*
  |--------------------------------------------------------------------------
  | Backend Order Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Store', 'as' => 'backend.tickets.'], function () {

    /*
     * List of all Tickets
     */
    Route::match(['get', 'post'], '/tickets', [
        'as' => 'index',
        'uses' => 'TicketsController@index',
        'middleware' => ['role:superadmin|admin']
    ]);

});
