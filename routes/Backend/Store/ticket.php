<?php

/*
  |--------------------------------------------------------------------------
  | Backend Order Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Store', 'as' => 'backend.ticket.'], function () {

    /*
     * List of all Tickets
     */
    Route::match(['get', 'post'], '/deal/{deal_id}/tickets/{partner_id?}', [
        'as' => 'index',
        'uses' => 'TicketController@index',
        'middleware' => ['role:partner|superadmin|admin']
    ])->where('deal_id', '[0-9]+')->where('partner_id', '[0-9]+');

    /*
     * Submited form for edition of one Deal
     */
    Route::post('/ticket/validate/{token}', [
        'as' => 'update',
        'uses' => 'TicketController@update',
        'middleware' => ['role:partner|superadmin|admin']
    ]);

    Route::get('/ticket/download/{id}/{ticket_id?}', [
        'as' => 'download',
        'uses' => 'TicketController@download',
        'middleware' => ['permission:backend.order.create']
    ])->where('id', '[0-9]+')->where('ticket_id', '[0-9]+');

    Route::get('/ticket/download/a4/{id}', [
        'as' => 'download.a4',
        'uses' => 'TicketController@downloadFront',
        'middleware' => ['permission:backend.order.create']
    ])->where('id', '[0-9]+')->where('ticket_id', '[0-9]+');

    Route::match(['get', 'post'], '/validator/event/{deal_id}/tickets/{partner_id}', [
        'as' => 'tickets',
        'uses' => 'TicketController@index',
        'middleware' => ['permission:backend.validator.update']
    ])->where('deal_id', '[0-9]+')->where('partner_id', '[0-9]+');

    /*
     * Ajax deletion of one ticket
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'TicketController@delete',
        'middleware' => ['role:superadmin|admin']
    ])->where('id', '[0-9]+');

});
