<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Store', 'prefix' => 'promotion', 'as' => 'backend.promotion.'], function () {
    /*
     * List of all Categories
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'PromotionController@index',
        'middleware' => ['role:superadmin|admin']
    ]);

    /*
     * Ajax deleteion of one Promotion
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'PromotionController@delete',
        'middleware' => ['role:superadmin|admin']
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Promotion
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'PromotionController@create',
        'middleware' => ['role:superadmin|admin']
    ]);

    /*
     * Submited form for creation of one Promotion
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'PromotionController@store',
        'middleware' => ['role:superadmin|admin']
    ]);

    /*
     * Edition of one Promotion
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'PromotionController@edit',
        'middleware' => ['role:superadmin|admin']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Promotion
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'PromotionController@update',
        'middleware' => ['role:superadmin|admin']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Promotion
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'PromotionController@executeSwitch',
        'middleware' => ['role:superadmin|admin']
    ])->where('id', '[0-9]+');
});
