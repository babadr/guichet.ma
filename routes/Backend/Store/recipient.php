<?php

/*
  |--------------------------------------------------------------------------
  | Backend Order Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Store', 'prefix' => 'recipient', 'as' => 'backend.recipient.'], function () {

    /*
     * List of all Tickets
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'RecipientController@index',
        'middleware' => ['role:superadmin|admin']
    ]);

    Route::get('/export', [
        'as' => 'export',
        'uses' => 'RecipientController@export',
        'middleware' => ['role:superadmin|admin']
    ]);

});
