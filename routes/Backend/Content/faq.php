<?php

/*
  |--------------------------------------------------------------------------
  | Backend Faq Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Content', 'prefix' => 'faq', 'as' => 'backend.faq.'], function () {
    /*
     * List of all Faqs
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'FaqController@index',
        'middleware' => ['permission:backend.faq.read']
    ]);

    /*
     * Ajax deleteion of one Faq
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'FaqController@delete',
        'middleware' => ['ajax', 'permission:backend.faq.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Faq
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'FaqController@create',
        'middleware' => ['permission:backend.faq.create']
    ]);

    /*
     * Submited form for creation of one Faq
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'FaqController@store',
        'middleware' => ['permission:backend.faq.create']
    ]);

    /*
     * Edition of one Faq
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'FaqController@edit',
        'middleware' => ['permission:backend.faq.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Faq
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'FaqController@update',
        'middleware' => ['permission:backend.faq.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Faq
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'FaqController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.faq.update'],
    ])->where('id', '[0-9]+');
});
