<?php

/*
  |--------------------------------------------------------------------------
  | Backend Post Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Content', 'prefix' => 'category-post', 'as' => 'backend.category_post.'], function () {
    /*
     * List of all Posts
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'CategoryPostController@index',
        'middleware' => ['permission:backend.post.read']
    ]);

    /*
     * Ajax deleteion of one Post
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'CategoryPostController@delete',
        'middleware' => ['ajax', 'permission:backend.post.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Post
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'CategoryPostController@create',
        'middleware' => ['permission:backend.post.create']
    ]);

    /*
     * Submited form for creation of one Post
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'CategoryPostController@store',
        'middleware' => ['permission:backend.post.create']
    ]);

    /*
     * Edition of one Post
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'CategoryPostController@edit',
        'middleware' => ['permission:backend.post.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Post
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'CategoryPostController@update',
        'middleware' => ['permission:backend.post.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Post
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'CategoryPostController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.post.update'],
    ])->where('id', '[0-9]+');
});
