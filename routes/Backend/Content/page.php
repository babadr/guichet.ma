<?php

/*
  |--------------------------------------------------------------------------
  | Backend Page Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Content', 'prefix' => 'page', 'as' => 'backend.page.'], function () {
    /*
     * List of all Pages
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'PageController@index',
        'middleware' => ['permission:backend.page.read']
    ]);

    /*
     * Ajax deleteion of one Page
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'PageController@delete',
        'middleware' => ['ajax', 'permission:backend.page.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Page
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'PageController@create',
        'middleware' => ['permission:backend.page.create']
    ]);

    /*
     * Submited form for creation of one Page
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'PageController@store',
        'middleware' => ['permission:backend.page.create']
    ]);

    /*
     * Edition of one Page
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'PageController@edit',
        'middleware' => ['permission:backend.page.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Page
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'PageController@update',
        'middleware' => ['permission:backend.page.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Page
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'PageController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.page.update'],
    ])->where('id', '[0-9]+');
});
