<?php

/*
  |--------------------------------------------------------------------------
  | Backend Auth Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Auth'], function () {
    /**
     * Login
     */
    Route::get('/login', [
        'as' => 'backend.auth.showLogin',
        'uses' => 'LoginController@showLoginForm'
    ]);
    Route::post('/login', [
        'as' => 'backend.auth.login',
        'uses' => 'LoginController@login'
    ]);
    Route::get('/logout', [
        'as' => 'backend.auth.logout',
        'uses' => 'LoginController@logout'
    ]);
    /**
     * Password reset
     */
    Route::get('/password/reset/{token}', [
        'as' => 'backend.auth.password.showReset',
        'uses' => 'ResetPasswordController@showResetForm'
    ]);
    Route::post('/password/reset', [
        'as' => 'backend.auth.password.reset',
        'uses' => 'ResetPasswordController@reset'
    ]);
    /**
     * Password forgot
     */
    Route::get('/password/email', [
        'as' => 'backend.auth.password.showForgot',
        'uses' => 'ForgotPasswordController@showLinkRequestForm'
    ]);
    Route::post('/password/email', [
        'as' => 'backend.auth.password.forgot',
        'uses' => 'ForgotPasswordController@sendResetLinkEmail'
    ]);
});
