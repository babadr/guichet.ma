<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Common', 'prefix' => 'contact', 'as' => 'backend.contact.'], function () {
    /*
     * List Contacts
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'ContactController@index',
        'middleware' => ['permission:backend.contact.read']
    ]);

    /*
     * Ajax deleteion of one Contact
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'ContactController@delete',
        'middleware' => ['ajax', 'permission:backend.contact.delete'],
    ])->where('id', '[0-9]+');


    /*
     * Edition of one Contact
     */
    Route::get('/view/{id}', [
        'as' => 'view',
        'uses' => 'ContactController@view',
        'middleware' => ['permission:backend.contact.read']
    ])->where('id', '[0-9]+');

});
