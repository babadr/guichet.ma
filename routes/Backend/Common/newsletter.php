<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Common', 'prefix' => 'newsletter', 'as' => 'backend.newsletter.'], function () {
    /*
     * List Newsletter Subscriptions
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'NewsletterController@index',
        'middleware' => ['permission:backend.newsletter.read']
    ]);

    /*
     * Ajax deleteion of one Newsletter
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'NewsletterController@delete',
        'middleware' => ['ajax', 'permission:backend.newsletter.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Newsletter
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'NewsletterController@create',
        'middleware' => ['permission:backend.newsletter.create']
    ]);

    /*
     * Submited form for creation of one Newsletter
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'NewsletterController@store',
        'middleware' => ['permission:backend.newsletter.create']
    ]);

    /*
     * Edition of one Newsletter
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'NewsletterController@edit',
        'middleware' => ['permission:backend.newsletter.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Newsletter
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'NewsletterController@update',
        'middleware' => ['permission:backend.newsletter.update']
    ])->where('id', '[0-9]+');

    /*
    * Export all subscriptions
    */
    Route::get('/export', [
        'as' => 'export',
        'uses' => 'NewsletterController@export',
        'middleware' => ['permission:backend.newsletter.read'],
    ]);
});
