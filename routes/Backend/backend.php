<?php

/*
  |--------------------------------------------------------------------------
  | Backend Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', [
        'as' => 'backend.index',
        'uses' => 'IndexController@index',
        'middleware' => ['permission:backend.dashboard.read']
    ]);

    Route::get('/dashboard/{id}/{history?}', [
        'as' => 'backend.dashboard',
        'uses' => 'DashboardController@index',
        'middleware' => ['role:superadmin|admin|partner']
    ])->where('id', '[0-9]+');


    Route::get('/generate-report', [
        'as' => 'backend.export.all',
        'uses' => 'IndexController@generateReport',
        'middleware' => ['role:superadmin|admin']
    ]);
    Route::get('/{partner_id}/event/{event_id?}', [
        'as' => 'backend.index.event',
        'uses' => 'IndexController@index',
        'middleware' => ['permission:backend.dashboard.read']
    ])->where('partner_id', '[0-9]+')->where('event_id', '[0-9]+');

    Route::get('/export/{provider_id}/events', [
        'as' => 'backend.index.export',
        'uses' => 'IndexController@export',
        'middleware' => ['permission:backend.dashboard.read']
    ])->where('provider_id', '[0-9]+');

    Route::get('/export/{provider_id}/tickets', [
        'as' => 'backend.index.export_tickets',
        'uses' => 'IndexController@exportTickets',
        'middleware' => ['permission:backend.dashboard.read']
    ])->where('provider_id', '[0-9]+');

    Route::group(['namespace' => 'Deal', 'as' => 'backend.event.'], function () {
        Route::get('/current-events', [
            'as' => 'current',
            'uses' => 'DealController@currentEvents',
            'middleware' => ['permission:backend.user.read']
        ]);
    });
    Route::get('/charts/{type}/{start_at}/{end_at}', [
        'as' => 'backend.charts',
        'uses' => 'IndexController@charts',
        'middleware' => ['permission:backend.dashboard.read']
    ]);
    Tools::includeRoutes('Backend/Acl');
    Tools::includeRoutes('Backend/Common');
    Tools::includeRoutes('Backend/Deal');
    Tools::includeRoutes('Backend/Store');
    Tools::includeRoutes('Backend/Content');
    Tools::includeRoutes('Backend/Setting');
});