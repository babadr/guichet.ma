<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal', 'prefix' => 'category', 'as' => 'backend.category.'], function () {
    /*
     * List of all Categories
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'CategoryController@index',
        'middleware' => ['permission:backend.category.read']
    ]);

    /*
     * Ajax deleteion of one Category
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'CategoryController@delete',
        'middleware' => ['ajax', 'permission:backend.category.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Category
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'CategoryController@create',
        'middleware' => ['permission:backend.category.create']
    ]);

    /*
     * Submited form for creation of one Category
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'CategoryController@store',
        'middleware' => ['permission:backend.category.create']
    ]);

    /*
     * Edition of one Category
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'CategoryController@edit',
        'middleware' => ['permission:backend.category.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Category
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'CategoryController@update',
        'middleware' => ['permission:backend.category.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Category
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'CategoryController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.category.update'],
    ])->where('id', '[0-9]+');
});
