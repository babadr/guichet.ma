<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal', 'prefix' => 'deal', 'as' => 'backend.deal.'], function () {
    /*
     * List of all Deals
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'DealController@index',
        'middleware' => ['permission:backend.deal.read']
    ]);

    /*
     * Ajax deleteion of one Deal
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'DealController@delete',
        'middleware' => ['ajax', 'permission:backend.deal.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Deal
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'DealController@create',
        'middleware' => ['permission:backend.deal.create']
    ]);

    /*
     * Submited form for creation of one Deal
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'DealController@store',
        'middleware' => ['permission:backend.deal.create']
    ]);

    /*
     * Edition of one Deal
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'DealController@edit',
        'middleware' => ['permission:backend.deal.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Deal
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'DealController@update',
        'middleware' => ['permission:backend.deal.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Deal
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'DealController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.deal.update'],
    ])->where('id', '[0-9]+');

    /*
     * AJax media upload
     */
    Route::post('/upload/{id}', [
        'as' => 'upload-media',
        'uses' => 'DealController@upload',
        'middleware' => ['ajax', 'permission:backend.deal.update'],
    ])->where('id', '[0-9]+');

    /*
     * AJax set deal cover image
     */
    Route::put('/switch-cover/{id}/{deal_id}', [
        'as' => 'switchCover',
        'uses' => 'DealController@switchCover',
        'middleware' => ['ajax', 'permission:backend.deal.update'],
    ])->where('id', '[0-9]+')->where('deal_id', '[0-9]+');

    /*
     * AJax set deal miniature
     */
    Route::put('/switch-miniature/{id}/{deal_id}', [
        'as' => 'switchMiniature',
        'uses' => 'DealController@switchMiniature',
        'middleware' => ['ajax', 'permission:backend.deal.update'],
    ])->where('id', '[0-9]+')->where('deal_id', '[0-9]+');

    /*
     * AJax set deal miniature
     */
    Route::delete('/delete-media/{id}', [
        'as' => 'delete.media',
        'uses' => 'DealController@deleteMedia',
        'middleware' => ['ajax', 'permission:backend.deal.update'],
    ])->where('id', '[0-9]+');

    Route::get('/export/{id}/tickets', [
        'as' => 'export_tickets',
        'uses' => 'DealController@exportTickets',
        'middleware' => ['permission:backend.deal.delete']
    ])->where('id', '[0-9]+');

    Route::get('/event/reserve/{id}', [
        'as' => 'reserve_seats',
        'uses' => 'DealController@reserveSeats',
        'middleware' => ['role:partner|superadmin|admin']
    ])->where('id', '[0-9]+');

    Route::post('/event/reserve/{id}', [
        'as' => 'post_reserve_seats',
        'uses' => 'DealController@saveReservedSeats',
        'middleware' => ['role:partner|superadmin|admin']
    ])->where('id', '[0-9]+');
});
