<?php


Route::group(['namespace' => 'Deal', 'prefix' => 'validator', 'as' => 'backend.validator.'], function () {

    Route::match(['get', 'post'], '/events', [
        'as' => 'index',
        'uses' => 'ValidatorController@index',
        'middleware' => ['permission:backend.validator.read']
    ]);

    Route::post('/ticket/validate/{token}', [
        'as' => 'update',
        'uses' => 'ValidatorController@update',
        'middleware' => ['permission:backend.validator.update']
    ]);
});
