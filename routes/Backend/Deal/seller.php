<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal', 'prefix' => 'seller', 'as' => 'backend.seller.'], function () {
    /*
     * List of all Sellers
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'SellerController@index',
        'middleware' => ['permission:backend.seller.read']
    ]);

    /*
     * Ajax deleteion of one Seller
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'SellerController@delete',
        'middleware' => ['ajax', 'permission:backend.seller.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Seller
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'SellerController@create',
        'middleware' => ['permission:backend.seller.create']
    ]);

    /*
     * Submited form for creation of one Seller
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'SellerController@store',
        'middleware' => ['permission:backend.seller.create']
    ]);

    /*
     * Edition of one Seller
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'SellerController@edit',
        'middleware' => ['permission:backend.seller.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Seller
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'SellerController@update',
        'middleware' => ['permission:backend.seller.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Seller
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'SellerController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.seller.update'],
    ])->where('id', '[0-9]+');

});
