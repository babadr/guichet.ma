<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal', 'prefix' => 'scene', 'as' => 'backend.scene.', 'middleware' => ['role:superadmin|admin']], function () {
    /*
     * List of all Scenes
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'SceneController@index',
    ]);

    /*
     * Ajax deleteion of one Scene
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'SceneController@delete',
        'middleware' => ['ajax'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Scene
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'SceneController@create',
    ]);

    /*
     * Submited form for creation of one Scene
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'SceneController@store',
    ]);

    /*
     * Edition of one Scene
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'SceneController@edit',
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Scene
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'SceneController@update',
    ])->where('id', '[0-9]+');


    Route::get('/export/{id}', [
        'as' => 'export',
        'uses' => 'SceneController@export',
    ])->where('id', '[0-9]+');

    Route::get('/export-all/{id}', [
        'as' => 'export.all',
        'uses' => 'SceneController@exportAll',
    ])->where('id', '[0-9]+');

});
