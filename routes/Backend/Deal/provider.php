<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal', 'prefix' => 'provider', 'as' => 'backend.provider.'], function () {
    /*
     * List of all Providers
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'ProviderController@index',
        'middleware' => ['permission:backend.provider.read']
    ]);

    /*
     * Ajax deleteion of one Provider
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'ProviderController@delete',
        'middleware' => ['ajax', 'permission:backend.provider.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Provider
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'ProviderController@create',
        'middleware' => ['permission:backend.provider.create']
    ]);

    /*
     * Submited form for creation of one Provider
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'ProviderController@store',
        'middleware' => ['permission:backend.provider.create']
    ]);

    /*
     * Edition of one Provider
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'ProviderController@edit',
        'middleware' => ['permission:backend.provider.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Provider
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'ProviderController@update',
        'middleware' => ['permission:backend.provider.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Provider
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'ProviderController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.provider.update'],
    ])->where('id', '[0-9]+');

    Route::get('/{partner_id}/statistics/{event_id?}', [
        'as' => 'statistics',
        'uses' => '\\App\\Http\\Controllers\\Backend\\IndexController@index',
        'middleware' => ['permission:backend.current_event.read']
    ])->where('partner_id', '[0-9]+')->where('event_id', '[0-9]+');


    Route::get('/export-subscriptions/{id}', [
        'as' => 'export_subscriptions',
        'uses' => 'ProviderController@exportSubscriptions',
        'middleware' => ['permission:backend.deal.delete']
    ])->where('id', '[0-9]+');

    Route::get('/export/oasis/{id}', [
        'as' => 'export.oasis',
        'uses' => 'ProviderController@exportOasis',
        'middleware' => ['role:superadmin|admin']
    ])->where('id', '[0-9]+');
});
