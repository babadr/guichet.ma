<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal', 'prefix' => 'offer', 'as' => 'backend.offer.'], function () {
    /*
     * List of all Providers
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'OfferController@index',
        'middleware' => ['permission:backend.offer.read']
    ]);

    /*
     * Ajax deleteion of one Provider
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'OfferController@delete',
        'middleware' => ['ajax', 'permission:backend.offer.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Provider
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'OfferController@create',
        'middleware' => ['permission:backend.offer.create']
    ]);

    /*
     * Submited form for creation of one Provider
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'OfferController@store',
        'middleware' => ['permission:backend.offer.create']
    ]);

    /*
     * Edition of one Provider
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'OfferController@edit',
        'middleware' => ['permission:backend.offer.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Provider
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'OfferController@update',
        'middleware' => ['permission:backend.offer.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Provider
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'OfferController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.offer.update'],
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one Provider
     */
    Route::put('/switchdefault/{id}', [
        'as' => 'switchDefault',
        'uses' => 'OfferController@switchDefault',
        'middleware' => ['ajax', 'permission:backend.offer.update'],
    ])->where('id', '[0-9]+');

    /*
     * AJax find offer by id
     */
    Route::get('/find/{id}', [
        'as' => 'find',
        'uses' => 'OfferController@find',
        'middleware' => ['ajax', 'permission:backend.offer.read'],
    ])->where('id', '[0-9]+');
});
