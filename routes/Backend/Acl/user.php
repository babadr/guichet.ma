<?php

/*
  |--------------------------------------------------------------------------
  | Backend User Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Acl', 'prefix' => 'user', 'as' => 'backend.user.'], function () {
    /*
     * List of all Users
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'UserController@index',
        'middleware' => ['permission:backend.user.read']
    ]);

    /*
     * Ajax deleteion of one User
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'UserController@delete',
        'middleware' => ['ajax', 'permission:backend.user.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one User
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'UserController@create',
        'middleware' => ['permission:backend.user.create']
    ]);

    /*
     * Submited form for creation of one User
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'UserController@store',
        'middleware' => ['permission:backend.user.create']
    ]);

    /*
     * Edition of one User
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'UserController@edit',
        'middleware' => ['permission:backend.user.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one User
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'UserController@update',
        'middleware' => ['permission:backend.user.update']
    ])->where('id', '[0-9]+');

    /*
     * AJax activation or unactivation of one User
     */
    Route::put('/switchactive/{id}', [
        'as' => 'switchActive',
        'uses' => 'UserController@executeSwitch',
        'middleware' => ['ajax', 'permission:backend.user.update'],
    ])->where('id', '[0-9]+');

    /*
    * Export all users
    */
    Route::get('/export', [
        'as' => 'export',
        'uses' => 'UserController@export',
        'middleware' => ['permission:backend.user.read'],
    ]);
});
