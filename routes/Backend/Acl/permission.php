<?php

/*
  |--------------------------------------------------------------------------
  | Backend Permission Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Acl', 'prefix' => 'permission', 'as' => 'backend.permission.'], function () {
    Route::get('/', [
        'middleware' => ['permission:backend.permission.read'],
        'as' => 'index',
        'uses' => 'PermissionController@index',
    ]);


    Route::post('/', [
        'middleware' => ['permission:backend.permission.update'],
        'uses' => 'PermissionController@postIndex',
        'as' => 'update',
    ]);
});
