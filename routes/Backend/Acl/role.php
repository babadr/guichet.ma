<?php

/*
  |--------------------------------------------------------------------------
  | Backend Role Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Acl', 'prefix' => 'role', 'as' => 'backend.role.'], function () {
    /*
     * List of all Roles
     */
    Route::match(['get', 'post'], '/', [
        'as' => 'index',
        'uses' => 'RoleController@index',
        'middleware' => ['permission:backend.role.read']
    ]);

    /*
     * Ajax deleteion of one Role
     */
    Route::delete('/delete/{id}', [
        'as' => 'delete',
        'uses' => 'RoleController@delete',
        'middleware' => ['ajax', 'permission:backend.role.delete'],
    ])->where('id', '[0-9]+');

    /*
     * Creation of one Role
     */
    Route::get('/create', [
        'as' => 'create',
        'uses' => 'RoleController@create',
        'middleware' => ['permission:backend.role.create']
    ]);

    /*
     * Submited form for creation of one Role
     */
    Route::post('/create', [
        'as' => 'store',
        'uses' => 'RoleController@store',
        'middleware' => ['permission:backend.role.create']
    ]);

    /*
     * Edition of one Role
     */
    Route::get('/edit/{id}', [
        'as' => 'edit',
        'uses' => 'RoleController@edit',
        'middleware' => ['permission:backend.role.update']
    ])->where('id', '[0-9]+');

    /*
     * Submited form for edition of one Role
     */
    Route::post('/edit/{id}', [
        'as' => 'update',
        'uses' => 'RoleController@update',
        'middleware' => ['permission:backend.role.update']
    ])->where('id', '[0-9]+');
});
