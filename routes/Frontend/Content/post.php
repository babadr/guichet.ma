<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Content', 'as' => 'frontend.post.'], function () {
    /*
     * List Posts
     */
    Route::get('/magazine', [
        'as' => 'index',
        'uses' => 'PostController@index'
    ]);

    Route::get('/post/{id}', [
        'as' => 'view',
        'uses' => 'PostController@view',
    ])->where('id', '[0-9]+');

    Route::get('/category/{id}', [
        'as' => 'view',
        'uses' => 'PostController@category',
    ])->where('id', '[0-9]+');

});
