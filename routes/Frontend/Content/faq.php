<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Content', 'prefix' => 'foire-aux-questions', 'as' => 'frontend.faq.'], function () {
    /*
     * List FAQ
     */
    Route::get('/', [
        'as' => 'index',
        'uses' => 'FaqController@index'
    ]);
});
