<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Content', 'as' => 'frontend.page.'], function () {

    Route::get('/page/{id}', [
        'as' => 'view',
        'uses' => 'PageController@view',
    ])->where('id', '[0-9]+');
});
