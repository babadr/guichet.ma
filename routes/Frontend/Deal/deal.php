<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal'], function () {
    Route::get('/deal/{id}', [
        'as' => 'frontend.deal.index',
        'uses' => 'DealController@view'
    ])->where('id', '[A-z]+');

    Route::get('/deal/preview/{id}', [
        'as' => 'frontend.deal.preview',
        'uses' => 'DealController@preview'
    ])->where('id', '[0-9]+');

    Route::post('/event/plan/{id}', [
        'as' => 'frontend.deal.seat',
        'uses' => 'DealController@seat'
    ])->where('id', '[0-9]+');

    Route::post('/event/reserved/{id}', [
        'as' => 'frontend.deal.reservedSeats',
        'uses' => 'DealController@reservedSeats'
    ])->where('id', '[0-9]+');

    Route::get('/private/event/{token}', [
        'as' => 'frontend.event.private',
        'uses' => 'DealController@viewPrivate'
    ]);
});
