<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Deal', 'prefix' => 'category', 'as' => 'frontend.category.'], function () {
    Route::get('/category/{id}', [
        'as' => 'view',
        'uses' => 'CategoryController@view'
    ])->where('id', '[0-9]+');
});
