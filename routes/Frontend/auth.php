<?php

/*
  |--------------------------------------------------------------------------
  | Frontend Auth Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Auth'], function () {
    /**
     * Login
     */
    Route::get('/connexion', [
        'as' => 'frontend.auth.showLogin',
        'uses' => 'LoginController@showLoginForm'
    ]);
    Route::post('/connexion', [
        'as' => 'frontend.auth.login',
        'uses' => 'LoginController@login'
    ]);
    Route::get('/deconnexion', [
        'as' => 'frontend.auth.logout',
        'uses' => 'LoginController@logout'
    ]);
    /**
     * Password reset
     */
    Route::get('/reinitialisation-mot-passe/{token}', [
        'as' => 'frontend.auth.password.showReset',
        'uses' => 'ResetPasswordController@showResetForm'
    ]);
    Route::post('/reinitialisation-mot-passe', [
        'as' => 'frontend.auth.password.reset',
        'uses' => 'ResetPasswordController@reset'
    ]);
    /**
     * Password forgot
     */
    Route::get('/mot-passe-oublie', [
        'as' => 'frontend.auth.password.showForgot',
        'uses' => 'ForgotPasswordController@showLinkRequestForm'
    ]);
    Route::post('/mot-passe-oublie', [
        'as' => 'frontend.auth.password.forgot',
        'uses' => 'ForgotPasswordController@sendResetLinkEmail'
    ]);
    /**
     * Registration
     */
    Route::get('/inscription', [
        'as' => 'frontend.auth.showRegister',
        'uses' => 'RegisterController@showRegistrationForm'
    ]);
    Route::post('/inscription', [
        'as' => 'frontend.auth.register',
        'uses' => 'RegisterController@register'
    ]);
    Route::get('/inscription/confirm/{token}', [
        'as' => 'frontend.auth.confirm',
        'uses' => 'RegisterController@userActivation'
    ]);
});
