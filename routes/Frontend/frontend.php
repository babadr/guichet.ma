<?php

/*
  |--------------------------------------------------------------------------
  | Frontend Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */
Route::group(['namespace' => 'Frontend'], function () {

    Route::get('/images/{type}/{path}/{filename}', function ($type, $path, $filename) {
        $w = (int)request()->query('w');
        $h = (int)request()->query('h');
        $image = storage_path('app') . '/' . $path . '/' . $filename;
        if (file_exists($image) && Tools::isCorrectFileExt($image)) {
            $response = Image::cache(function ($img) use($image, $type, $w, $h) {
                if ($type === 'resize' && ($w || $h)) {
                    return $img->make($image)->resize($w, $h, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                } elseif ($type === 'crop' && $w && $h) {
                    return $img->make($image)->fit($w, $h);
                }
            }, 180, true);
            return $response->response();
        }

        return 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=aucune+image';
    });

    Route::get('/files/{path}/{filename}', function ($path, $filename) {
        $filePath = storage_path('app') . '/' . $path . '/' . $filename;
        $fileContents = File::get($filePath);

        return response($fileContents, 200)->header('Content-Type', File::mimeType($filePath));
    });

    Route::get('/', [
        'as' => 'frontend.index',
        'uses' => 'IndexController@index'
    ]);

    Route::get('/recherche', [
        'as' => 'frontend.search',
        'uses' => 'IndexController@search'
    ]);

    Route::get('/sitemap.xml', [
        'as' => 'frontend.sitemap',
        'uses' => 'IndexController@sitemap'
    ]);

    Tools::includeRoutes('Frontend/Common');
    Tools::includeRoutes('Frontend/Content');
    Tools::includeRoutes('Frontend/Deal');
    Tools::includeRoutes('Frontend/Store');
});
