<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Common', 'prefix' => 'profile', 'as' => 'frontend.profile.'], function () {
    /*
     * List Contacts
     */
    Route::get('/', [
        'middleware' => ['auth'],
        'as' => 'index',
        'uses' => 'ProfileController@index'
    ]);

    /*
     * Submited form for creation of one Contact
     */
    Route::post('/', [
        'middleware' => ['auth'],
        'as' => 'update',
        'uses' => 'ProfileController@update',
    ]);

});
