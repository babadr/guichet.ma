<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Common', 'prefix' => 'contact', 'as' => 'frontend.contact.'], function () {
    /*
     * List Contacts
     */
    Route::get('/', [
        'as' => 'index',
        'uses' => 'ContactController@index'
    ]);

    /*
     * Submited form for creation of one Contact
     */
    Route::post('/', [
        'as' => 'store',
        'uses' => 'ContactController@store',
    ]);

});
