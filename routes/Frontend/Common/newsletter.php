<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Common', 'prefix' => 'subscription', 'as' => 'frontend.newsletter.'], function () {

    /*
     * Submited form for creation of one Newsletter
     */
    Route::post('/', [
        'as' => 'store',
        'uses' => 'NewsletterController@store',
        'middleware' => ['ajax'],
    ]);

    Route::post('/create', [
        'as' => 'create',
        'uses' => 'NewsletterController@create',
    ]);
});
