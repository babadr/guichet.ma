<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Store', 'as' => 'frontend.cart.'], function () {
    /*
     * List Items in cart
     */
    Route::get('/panier', [
        'as' => 'index',
        'uses' => 'CartController@index'
    ]);

    /*
     * Ajax add item to cart
     */
    Route::post('/cart/add', [
        'as' => 'add',
        'uses' => 'CartController@add',
        'middleware' => ['ajax'],
    ]);

    Route::post('/cart/seats/add', [
        'as' => 'addSeats',
        'uses' => 'CartController@addSeats',
        'middleware' => ['ajax'],
    ]);

    /*
     * Ajax update item in cart
     */
    Route::post('/cart/update/{id}', [
        'as' => 'update',
        'uses' => 'CartController@update',
        'middleware' => ['ajax'],
    ]);

    /*
     * Ajax delete item from cart
     */
    Route::delete('/cart/delete/{id}', [
        'as' => 'delete',
        'uses' => 'CartController@delete',
        'middleware' => ['ajax'],
    ]);

    Route::post('/cart/selected/seats/{id}', [
        'as' => 'selectedSeats',
        'uses' => 'CartController@getSelectedSeats'
    ])->where('id', '[0-9]+');
});
