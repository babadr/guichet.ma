<?php

/*
  |--------------------------------------------------------------------------
  | Backend Media Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Store', 'as' => 'frontend.order.'], function () {

    /*Route::get('/checkout', [
        'as' => 'checkout',
        'uses' => 'OrderController@checkout',
        'middleware' => ['auth']
    ]);*/

    Route::get('/mes-commandes', [
        'as' => 'list',
        'uses' => 'OrderController@index',
        'middleware' => ['auth']
    ]);

    Route::get('/mes-commandes/details-commande/{id}', [
        'as' => 'view',
        'uses' => 'OrderController@view',
        'middleware' => ['auth']
    ])->where('id', '[0-9]+');

    Route::post('/paiement', [
        'as' => 'payment',
        'uses' => 'OrderController@payment',
        'middleware' => ['auth']
    ]);

    /*Route::post('/paiement/{reference}', [
        'as' => 'payment',
        'uses' => 'OrderController@payment',
        'middleware' => ['auth']
    ]);*/

    Route::post('/checkout/user/update', [
        'as' => 'updateUser',
        'uses' => 'OrderController@updateUser',
        'middleware' => ['auth']
    ]);


    Route::post('/validation', [
        'as' => 'validatePayment',
        'uses' => 'OrderController@validatePayment',
    ]);

    Route::get('/confirmation/{reference}', [
        'as' => 'finalization',
        'uses' => 'OrderController@finalization',
        'middleware' => ['auth']
    ]);

    Route::post('/create/recipients/{reference}', [
        'as' => 'recipients',
        'uses' => 'OrderController@createRecipients',
        'middleware' => ['auth']
    ]);

    Route::get('/abonnemet-raja/{reference}', [
        'as' => 'abonnemet',
        'uses' => 'RecipientController@index',
        'middleware' => ['auth']
    ]);

    Route::post('/abonnemet-raja/save/{reference}', [
        'as' => 'abonnemet.store',
        'uses' => 'RecipientController@create',
        'middleware' => ['auth']
    ]);

    Route::get('/forward/{FPAY_URL?}/{ORDER_ID?}/{REFERENCE_ID?}/{TRACK_ID?}', ['as' => 'forward', 'middleware' => ['auth'], function ($FPAY_URL = null, $ORDER_ID = null, $REFERENCE_ID = null, $TRACK_ID = null) {
        return view('frontend.contents.store.order.forward')
            ->with('FPAY_URL', $FPAY_URL)
            ->with('ORDER_ID', $ORDER_ID)
            ->with('REFERENCE_ID', $REFERENCE_ID)
            ->with('TRACK_ID', $TRACK_ID);
    }]);
});
