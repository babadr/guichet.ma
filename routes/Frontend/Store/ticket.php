<?php

/*
  |--------------------------------------------------------------------------
  | FrontEnd Ticket Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your backendlication. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

Route::group(['namespace' => 'Ticket', 'as' => 'frontend.ticket.'], function () {

    /*
     * Download ticket
     */
    Route::get('/tickets/download/{id}', [
        'as' => 'download',
        'uses' => 'TicketController@download',
        'middleware' => ['auth']
    ])->where('id', '[0-9]+');

    /*
     * Download ticket
     */
    Route::get('/tickets/check/{token}', [
        'as' => 'check',
        'uses' => 'TicketController@check',
        'middleware' => ['auth']
    ])->where('token', '[A-Za-z0-9]+');

    /*
     * Download ticket
     */
    Route::post('/tickets/validate', [
        'as' => 'validate',
        'uses' => 'TicketController@validateTicket',
        'middleware' => ['auth']
    ]);

    /*
     * Download ticket
     */
    Route::get('/tickets/qrcode/{id}/{size}.png', [
        'as' => 'qrcode',
        'uses' => 'TicketController@generateQrCode'
    ])->where('id', '[0-9]+')->where('size', '[0-9]+');

    Route::get('/tickets/barcode/{id}/{size}.png', [
        'as' => 'barcode',
        'uses' => 'TicketController@generateBarCode'
    ])->where('id', '[0-9]+')->where('size', '[0-9]+');

    Route::get('/tickets/barcode/{id}/{size}/{w}.png', [
        'as' => 'barcode',
        'uses' => 'TicketController@generateBarCode'
    ])->where('id', '[0-9]+')->where('size', '[0-9]+')->where('w', '[0-9]+');
});
