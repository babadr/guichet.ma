<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('imagecache/{template}/{filename}', [
    'uses' => 'ImageCacheController@getResponse',
    'as'   => 'imagecache'
])->where('filename', '[ \w\\.\\/\\-\\@]+');

//Route::get('/lp/{filename}', function ($filename) {
//    return view('frontend.lp.' . $filename);
//});

Tools::includeRoutes('Frontend');

Route::group(['namespace' => 'Backend', 'prefix' => 'admin'], function () {
    Tools::includeRoutes('Backend');
});

Route::get('{slug}', [
    'uses' => 'Frontend\IndexController@view',
])->where('slug', '([A-Za-z0-9\-\/\_]+)');